clear

while true; do
    read -p "Do you want to run AUTOMATIC build process ?" yn
    case $yn in
        [Yy]* )
            bash hooks/shFiles/mscare-sandbox-phase2.sh; bash hooks/shFiles/mscare-dev-phase2.sh; bash hooks/shFiles/mscare-qa-phase2.sh; bash hooks/shFiles/mscare-master.sh; break;;
        [Nn]* ) 
            while true; do
                read -p "Want to create SANDBOX build ?" yn
                case $yn in
                    [Yy]* ) bash hooks/shFiles/mscare-sandbox-phase2.sh ; break;;
                    [Nn]* ) break;;
                    * ) echo "Please answer yes or no.";;
                esac
            done

            while true; do
                read -p "Want to create DEV build ?" yn
                case $yn in
                    [Yy]* ) bash hooks/shFiles/mscare-dev-phase2.sh; break;;
                    [Nn]* ) break;;
                    * ) echo "Please answer yes or no.";;
                esac
            done

            while true; do
                read -p "Want to create QA build ?" yn
                case $yn in
                    [Yy]* ) bash hooks/shFiles/mscare-qa-phase2.sh; break;;
                    [Nn]* ) break;;
                    * ) echo "Please answer yes or no.";;
                esac
            done
            while true; do
                read -p "Want to create MASTER build ?" yn
                case $yn in
                    [Yy]* ) bash hooks/shFiles/mscare-master.sh; break;;
                    [Nn]* ) break;;
                    * ) echo "Please answer yes or no.";;
                esac
            done
            break;;
        * ) echo "Please answer yes or no.";;
    esac
done

while true; do
    read -p "Want to create QAMASTER build ?" yn
    case $yn in
        [Yy]* ) bash hooks/shFiles/mscare-qamaster.sh; break;;
        [Nn]* ) break;;
        * ) echo "Please answer yes or no.";;
    esac
done