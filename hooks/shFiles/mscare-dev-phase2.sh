clear
macName=$(id -un)
folderPath=/Users/$macName/developer/workspace/mscare-dev/neu.proe.161001-app
desktopStore=/Users/$macName/Desktop/store
desktopCert=/Users/$macName/Desktop/certificates
appFolderName=mscare
buildName=mscare-dev-phase2
iosBuildName=MS-Care-Connect

cd $folderPath
echo ""
echo ""
echo "======================== GIT CHECKOUT dev-phase2 ==========================="
echo ""
echo ""
git checkout dev-phase2
echo ""
echo ""
echo "======================== GIT PULL dev-phase2 ==========================="
echo ""
echo ""
git pull origin dev-phase2
echo ""
echo ""
echo "======================== UPDATING VERSION ==========================="
echo ""
echo ""
gulp version
gulp sass
gulp scripts-dev
gulp templateJs
gulp inject_app

mv www/js/ backup/
mv www/templates/ backup/
mv www/css/ionic.app.css backup/
echo "======================== CLEANING THE IOS BUILD  ======================="
echo ""
echo ""
platforms/ios/cordova/./clean
echo "======================== CLEANING THE ANDROID BUILD  ======================="
echo ""
echo ""
platforms/android/cordova/./clean
echo ""
echo ""
echo "======================== CREATING IOS RELEASE BUILD ==========================="
echo ""
echo ""
ionic cordova build ios --release --device
echo ""
echo ""
echo "======================== CREATING ANDROID RELEASE BUILD ==========================="
echo ""
echo ""
ionic cordova build android --release --device
echo ""
echo ""
echo "======================== REMOVING OLD STORE INSTALLERS ==========================="
echo ""
echo ""
rm -f $desktopStore/$appFolderName/$buildName.apk
rm -f $desktopStore/$appFolderName/$buildName.ipa
echo ""
echo ""
echo "======================== CREATING THE .APK ==========================="
echo ""
echo ""
jarsigner -verbose -sigalg SHA1withRSA -storepass 'mscare' -digestalg SHA1 -keystore $desktopCert/$appFolderName/dev/mscare-release-key.keystore $folderPath/platforms/android/build/outputs/apk/android-release-unsigned.apk mscare

zipalign -v 4 $folderPath/platforms/android/build/outputs/apk/android-release-unsigned.apk $desktopStore/$appFolderName/$buildName.apk
echo ""
echo ""
echo ""
echo "======================== CREATING THE .IPA ==========================="
echo ""
echo ""
mv $folderPath/platforms/ios/build/device/$iosBuildName.ipa $folderPath/platforms/ios/build/device/$buildName.ipa
cp -rf $folderPath/platforms/ios/build/device/$buildName.ipa $desktopStore/$appFolderName
echo ""
echo ""
echo "======================== WATING FOR MAKING A STABLE BUILD ============================="
for i in {1..5}
do
	echo $i "in 5"
	sleep 1s
done
echo ""
echo ""
echo "======================== UPLOADING TO SERVER phase2==========================="
scp -i $desktopCert/iom-mhealth.pem -r $desktopStore/$appFolderName/$buildName.apk $desktopStore/$appFolderName/$buildName.ipa ec2-user@mhealth-dev.io-media.com:/janssen/dev/$appFolderName
echo ""
echo ""
mv backup/js/ www/
mv backup/templates/ www/
mv backup/ionic.app.css www/css/


git checkout www/css/
git checkout www/templates/authenticationModules/login.html
git checkout www/index.html
echo "======================== ALL DONE kthxbye ======================================"