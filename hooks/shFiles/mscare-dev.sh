clear
macName=$(id -un)
folderPath=/Users/$macName/developer/workspace/mscare-dev/neu.proe.161001-app
desktopStore=/Users/$macName/Desktop/store
desktopCert=/Users/$macName/Desktop/certificates
appFolderName=mscare
buildName=mscare-dev
iosBuildName=MS-Care-Connect

cd $folderPath
echo ""
echo ""
echo "======================== GIT CHECKOUT dev ==========================="
echo ""
echo ""
git checkout Dev
echo ""
echo ""
echo "======================== GIT PULL dev ==========================="
echo ""
echo ""
git pull origin Dev
echo ""
echo ""
echo "======================== UPDATING VERSION ==========================="
echo ""
echo ""
gulp version
gulp sass
gulp scripts-dev
gulp inject_app
echo "======================== CLEANING THE IOS BUILD  ======================="
echo ""
echo ""
platforms/ios/cordova/./clean
echo "======================== CLEANING THE ANDROID BUILD  ======================="
echo ""
echo ""
platforms/android/cordova/./clean
echo ""
echo ""
echo "======================== CREATING IOS RELEASE BUILD ==========================="
echo ""
echo ""
ionic build ios --release --device
echo ""
echo ""
echo "======================== CREATING ANDROID RELEASE BUILD ==========================="
echo ""
echo ""
ionic build android --release --device
echo ""
echo ""
echo "======================== REMOVING OLD STORE INSTALLERS ==========================="
echo ""
echo ""
rm -f $desktopStore/$appFolderName/$buildName.apk
rm -f $desktopStore/$appFolderName/$buildName.ipa
echo ""
echo ""
echo "======================== CREATING THE .APK ==========================="
echo ""
echo ""
jarsigner -verbose -sigalg SHA1withRSA -storepass 'mscare' -digestalg SHA1 -keystore $desktopCert/$appFolderName/dev/mscare-release-key.keystore $folderPath/platforms/android/build/outputs/apk/android-release-unsigned.apk mscare

zipalign -v 4 $folderPath/platforms/android/build/outputs/apk/android-release-unsigned.apk $desktopStore/$appFolderName/$buildName.apk
echo ""
echo ""
echo "======================== WATING FOR MAKING A STABLE BUILD ============================="
for i in {1..15}
do
	echo $i "in 15"
	sleep 1s
done
echo ""
echo ""
echo "======================== CREATING THE .IPA ==========================="
echo ""
echo ""
ionic build ios --release --device
mv $folderPath/platforms/ios/build/device/$iosBuildName.ipa $folderPath/platforms/ios/build/device/$buildName.ipa
cp -rf $folderPath/platforms/ios/build/device/$buildName.ipa $desktopStore/$appFolderName
echo ""
echo ""
echo "======================== WATING FOR MAKING A STABLE BUILD ============================="
for i in {1..10}
do
	echo $i "in 10"
	sleep 1s
done
echo ""
echo ""
echo "======================== UPLOADING TO SERVER ==========================="
scp -i $desktopCert/iom-mhealth.pem -r $desktopStore/$appFolderName/$buildName.apk $desktopStore/$appFolderName/$buildName.ipa ec2-user@mhealth-dev.io-media.com:/janssen/dev/$appFolderName
echo ""
echo ""
git checkout www/css/
git checkout www/templates/authenticationModules/login.html
git checkout www/index.html
echo "======================== ALL DONE kthxbye ======================================"