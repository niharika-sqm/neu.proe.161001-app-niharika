clear
macName=$(id -un)
folderPath=/Users/$macName/developer/workspace/mscare-client/neu.proe.161001-app
desktopStore=/Users/$macName/Desktop/store
desktopCert=/Users/$macName/Desktop/certificates
appFolderName=mscare
buildName=mscare-client
iosBuildName=MS-Care-Connect

cd $folderPath
echo ""
echo ""
echo "======================== GIT CHECKOUT master ==========================="
echo ""
echo ""
git checkout master
echo ""
echo ""
echo "======================== GIT PULL master ==========================="
echo ""
echo ""
git pull origin master
echo ""
echo ""
echo "======================== UPDATING VERSION ==========================="
echo ""
echo ""
gulp version-prod
gulp sass
gulp scripts-client
gulp templateJs
gulp obfuscation
gulp inject_app

mv www/js backup/
mv www/templates backup/
mv www/css/ionic.app.css backup/

echo "======================== CLEANING THE IOS & ANDROID BUILD  ======================="
echo ""
echo ""
cordova clean
echo ""
echo ""
echo "======================== CREATING IOS RELEASE BUILD ==========================="
echo ""
echo ""
ionic cordova build ios --release --prod --device -- --developmentTeam='V5JSZVG26J' --codeSignIdentity='iPhone Distribution' --provisioningProfile='a29dc592-bbfb-4152-88ae-257a68dd2805' --packageType='app-store' --bundleIdentifier='com.interpro.mscareconnect'
echo ""
echo ""
echo "======================== CREATING ANDROID RELEASE BUILD ==========================="
echo ""
echo ""
ionic cordova build android --release --device
echo ""
echo ""
echo "======================== REMOVING OLD STORE INSTALLERS ==========================="
echo ""
echo ""
rm -f $desktopStore/$appFolderName/$buildName.apk
rm -f $desktopStore/$appFolderName/$buildName.ipa
echo ""
echo ""
echo "======================== CREATING THE .APK ==========================="
echo ""
echo ""
jarsigner -verbose -sigalg SHA1withRSA -storepass 'MSCareM0bile!' -digestalg SHA1 -keystore $desktopCert/$appFolderName/client-prod/mscareconnect.keystore $folderPath/platforms/android/build/outputs/apk/android-release-unsigned.apk ms_care_connect

zipalign -v 4 $folderPath/platforms/android/build/outputs/apk/android-release-unsigned.apk $desktopStore/$appFolderName/$buildName.apk
echo ""
echo ""
echo "======================== CREATING THE .IPA ==========================="
echo ""
echo ""
mv $folderPath/platforms/ios/build/device/$iosBuildName.ipa $folderPath/platforms/ios/build/device/$buildName.ipa
cp -rf $folderPath/platforms/ios/build/device/$buildName.ipa $desktopStore/$appFolderName
echo ""
echo ""
echo "======================== WATING FOR MAKING A STABLE BUILD ============================="
echo ""
echo ""
mv backup/js www/
mv backup/templates www/
mv backup/ionic.app.css www/css/

rm -rf www/builds/templates.js

git checkout www/css/
git checkout www/templates/authenticationModules/login.html
git checkout www/index.html

date
echo "======================== ALL DONE kthxbye ======================================"