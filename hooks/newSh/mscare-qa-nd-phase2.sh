clear
macName=$(id -un)
folderPath=/Users/$macName/developer/workspace/mscare-dev/neu.proe.161001-app
desktopStore=/Users/$macName/Desktop/store
desktopCert=/Users/$macName/Desktop/certificates
appFolderName=mscare
buildName=mscare-qa-nd-phase2
iosBuildName=MS-Care-Connect
buildType=newConsole

cd $folderPath
echo ""
echo ""
echo "======================== UPDATE QA-PHASE2 BRANCH ==========================="

git checkout sandbox-phase2 && git pull origin sandbox-phase2 && git checkout dev-phase2 && git pull origin dev-phase2 && git merge sandbox-phase2 && git push origin dev-phase2 && git checkout qa-phase2 && git pull origin qa-phase2 && git merge dev-phase2 && git push origin qa-phase2

echo ""
echo ""
echo ""
echo "======================== UPDATING VERSION ==========================="
echo ""
echo ""
gulp version
gulp sass
gulp scripts-qa
gulp templateJs
gulp obfuscation
gulp inject_app

mv www/js/ backup/
mv www/templates/ backup/
mv www/css/ionic.app.css backup/

echo "======================== CLEANING THE IOS BUILD  ======================="
echo ""
echo ""
cordova clean
echo ""
echo ""
echo "======================== CREATING IOS RELEASE BUILD ==========================="
echo ""
echo ""
ionic cordova build ios --release --device -- --developmentTeam='5U42V2X96F' --codeSignIdentity='iPhone Distribution' --provisioningProfile='MSCC App - Dist' --packageType='enterprise' --bundleIdentifier='com.squintmetrics.mscc'
echo ""
echo ""
echo "======================== CREATING ANDROID RELEASE BUILD ==========================="
echo ""
echo ""
ionic cordova build android --release --device
echo ""
echo ""
echo "======================== REMOVING OLD STORE INSTALLERS ==========================="
echo ""
echo ""
rm -f $desktopStore/$appFolderName/$buildName.apk
rm -f $desktopStore/$appFolderName/$buildName.ipa
echo ""
echo ""
echo "======================== CREATING THE .APK ==========================="
echo ""
echo ""
jarsigner -verbose -sigalg SHA1withRSA -storepass 'mscare' -digestalg SHA1 -keystore $desktopCert/$appFolderName/dev/mscare-release-key.keystore $folderPath/platforms/android/build/outputs/apk/android-release-unsigned.apk mscare

zipalign -v 4 $folderPath/platforms/android/build/outputs/apk/android-release-unsigned.apk $desktopStore/$appFolderName/$buildName.apk
echo ""
echo ""
echo "======================== CREATING THE .IPA ==========================="
echo ""
echo ""
mv $folderPath/platforms/ios/build/device/$iosBuildName.ipa $folderPath/platforms/ios/build/device/$buildName.ipa
cp -rf $folderPath/platforms/ios/build/device/$buildName.ipa $desktopStore/$appFolderName
echo ""
echo ""
echo "======================== WATING FOR MAKING A STABLE BUILD ============================="
for i in {1..5}
do
    echo $i "in 5"
    sleep 1s
done
echo ""
echo ""
echo "======================== UPLOADING TO SERVER phase2==========================="
scp -i $desktopCert/iom-mhealth.pem -r $desktopStore/$appFolderName/$buildName.apk $desktopStore/$appFolderName/$buildName.ipa ec2-user@mhealth-dev.io-media.com:/janssen/dev/$appFolderName/$buildType
echo ""
echo ""
mv backup/js/ www/
mv backup/templates/ www/
mv backup/ionic.app.css www/css/

rm -rf www/builds/templates.js
rm -rf www/builds/app.min.js

git checkout www/css/
git checkout www/templates/authenticationModules/login.html
git checkout www/index.html
echo "======================== ALL DONE kthxbye ======================================"