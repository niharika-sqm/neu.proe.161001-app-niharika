clear
macName=$(id -un)
folderPath=/Users/$macName/developer/workspace/mscare-dev/neu.proe.161001-app
desktopStore=/Users/$macName/Desktop/store
desktopCert=/Users/$macName/Desktop/certificates
appFolderName=mscare
buildName=mscare-sandbox-phase2
iosBuildName=MS-Care-Connect
buildType=newConsole

cd $folderPath
echo ""
echo ""
echo "======================== GIT CHECKOUT sandbox-phase2 ==========================="
echo ""
echo ""
git checkout sandbox-phase2
echo ""
echo ""
echo "======================== GIT PULL sandbox-phase2 ==========================="
echo ""
echo ""
git pull origin sandbox-phase2
echo ""
echo ""
echo "======================== UPDATING VERSION ==========================="
echo ""
echo ""
gulp version
gulp sass
# gulp scripts-sandbox
# gulp inject_app
echo "======================== CLEANING THE IOS BUILD  ======================="
echo ""
echo ""
platforms/ios/cordova/./clean
echo "======================== CLEANING THE ANDROID BUILD  ======================="
echo ""
echo ""
platforms/android/cordova/./clean
echo ""
echo ""
echo "======================== CREATING IOS RELEASE BUILD ==========================="
echo ""
echo ""
ionic cordova build ios --device
echo ""
echo ""
echo "======================== CREATING ANDROID RELEASE BUILD ==========================="
echo ""
echo ""
ionic cordova build android
echo ""
echo ""
echo "======================== REMOVING OLD STORE INSTALLERS ==========================="
echo ""
echo ""
rm -f $desktopStore/$appFolderName/$buildName.apk
rm -f $desktopStore/$appFolderName/$buildName.ipa
echo ""
echo ""
echo "======================== CREATING THE .APK ==========================="
echo ""
echo ""
cp $folderPath/platforms/android/build/outputs/apk/android-debug.apk $desktopStore/$appFolderName/$buildName.apk
echo ""
echo ""
echo "======================== CREATING THE .IPA ==========================="
echo ""
echo ""
# ionic cordova build ios --device
mv $folderPath/platforms/ios/build/device/$iosBuildName.ipa $folderPath/platforms/ios/build/device/$buildName.ipa
cp -rf $folderPath/platforms/ios/build/device/$buildName.ipa $desktopStore/$appFolderName
echo ""
echo ""
echo "======================== WATING FOR MAKING A STABLE BUILD ============================="
for i in {1..5}
do
	echo $i "in 5"
	sleep 1s
done
echo ""
echo ""
echo "======================== UPLOADING TO SERVER phase2 ==========================="
scp -i $desktopCert/iom-mhealth.pem -r $desktopStore/$appFolderName/$buildName.apk $desktopStore/$appFolderName/$buildName.ipa ec2-user@mhealth-dev.io-media.com:/janssen/dev/$appFolderName/$buildType
echo ""
echo ""
git checkout www/css/
git checkout www/templates/authenticationModules/login.html
git checkout www/index.html
echo "======================== ALL DONE kthxbye ======================================"