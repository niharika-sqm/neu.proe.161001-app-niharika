macName=$(id -un)
echo ""
echo ""
echo "======================== UPLOADING TO SERVER ==========================="

scp -i /Users/$macName/Desktop/certificates/iom-mhealth.pem -3 ec2-user@mhealth-dev.io-media.com:/janssen/dev/mscare/newConsole/mscare-qa-nd-phase2.ipa ec2-user@mhealth-dev.io-media.com:/janssen/dev/mscare/newConsole/mscare-qa-ny-phase2.ipa

scp -i /Users/$macName/Desktop/certificates/iom-mhealth.pem -3 ec2-user@mhealth-dev.io-media.com:/janssen/dev/mscare/newConsole/mscare-qa-nd-phase2.apk ec2-user@mhealth-dev.io-media.com:/janssen/dev/mscare/newConsole/mscare-qa-ny-phase2.apk

echo "======================== ALL DONE kthxbye ======================================"