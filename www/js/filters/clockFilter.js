neuroScienceApp.filter('hhmmss', function () {
    return function (time) {
        if(time == undefined) return "0:00"
        var sec_num = parseInt(time,10);
        // var sec_num = Math.floor(time / 100); // don't forget the second param
        var hours   = Math.floor(sec_num / 3600);
        var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
        var seconds = sec_num - (hours * 3600) - (minutes * 60);
        if (hours   < 10) {hours   = "0"+hours;}
        if (minutes < 10) {minutes = "0"+minutes;}
        if (seconds < 10) {seconds = "0"+seconds;}
        var time    = minutes+':'+seconds;
        return time;
    }
});

neuroScienceApp.filter('hhmmssLanguageMemoryTest', function () {
    return function (time) {
        if(time == undefined) return "0:00"
        var sec_num = parseInt(time,10);
        // var sec_num = Math.floor(time / 100); // don't forget the second param
        var hours   = Math.floor(sec_num / 3600);
        var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
        var seconds = sec_num - (hours * 3600) - (minutes * 60);
        if (hours   < 10) {hours   = "0"+hours;}
        if (minutes < 10) {minutes = minutes;}
        if (seconds < 10) {seconds = "0"+seconds;}
        var time    = minutes+':'+seconds;
        return time;
    }
});

neuroScienceApp.filter('minutesfilterWalkTest', function () {
    return function (time) {
        if(time == undefined) return "0"
        var sec_num = Math.round(time / 100);
        var minutes = Math.floor(sec_num / 60);  
        var time  = minutes;
        return time;
    }
});



neuroScienceApp.filter('secondsfilterWalkTest', function () {
    return function (time) {
        if(time == undefined) return "00"
        var sec_num = Math.floor(time / 100);
        var seconds = sec_num % 60;
        if (seconds < 10) {seconds = "0"+seconds;}
        var time  = seconds;
        return time;
    }
});

neuroScienceApp.filter('secondsfilterTrailTest', function () {
    return function (time) {
        if(time == undefined) return "00"
        var sec_num = Math.round(time / 100);
        var seconds = sec_num % 60;
        if (seconds < 10) {seconds = "0"+seconds;}
        var time  = seconds;
        return time;
    }
});

neuroScienceApp.filter('microsecondsfilterWalkTest', function () {
    return function (time) {
        if(time == undefined) return "00"
        // var sec_num = Math.floor(time / 100); // don't forget the second param
        var microseconds = time % 100;
        if(microseconds == 0) return "00";
        else if (microseconds < 10) microseconds = "0"+microseconds;
        return microseconds;
    }
});

neuroScienceApp.filter('minutesfilterTowerTest', function () {
    return function (time) {
        if(time == undefined) return "0";
        var minutes = Math.floor(time / 60);  
        var time  = minutes;
        return time;
    }
});



neuroScienceApp.filter('secondsfilterTowerTest', function () {
    return function (time) {
        if(time == undefined) return "00";
        var seconds = time % 60;
        if (seconds < 10) {seconds = "0"+seconds;}
        var time  = seconds;
        return time;
    }
});


neuroScienceApp.filter('dateFilter', function () {
    return function (date) {
        if(date == undefined) return "00/00/0000";
        var splitDate = date.split('T');
        var splitedDate = splitDate[0].split('-');
        var newDateFormat = splitedDate[1]+'/'+splitedDate[2]+'/'+splitedDate[0];
        return newDateFormat;
    }
});

