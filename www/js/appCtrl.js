neuroScienceApp.controller('appCtrl',['$scope','$window','$ionicSideMenuDelegate','$kinvey','$ionicLoading', function($scope,$window,$ionicSideMenuDelegate,$kinvey,$ionicLoading){
	$scope.exitLoader = {"pointsLoader":false,"numberOfTestLoader":false,"forcastLoader":false,"highChart":false}
	$scope.chartSubcategory =1;
	$scope.physicalArray=[true,true,true,true];
	$scope.mindArray=[true,false,false,false];
	$scope.dailyArray=[true,false,false,false];
	$scope.generalArray=[true,false];
	$scope.chartSeries =[];
	$scope.highChartDate = {};
	$scope.highChartDate.currentDate = new Date();
	$scope.highChartDate.month = moment().format("MMMM");
	$scope.highChartDate.year = moment().format("YYYY");
	$scope.symptoms = "physicalSymptoms";
	$scope.chartItems="";

	var wd = $window.innerWidth;
	var ht = $window.innerHeight;
	var _width = (ht>wd) ? ht : wd;
	var _height = (wd<ht) ? wd : ht;
	_height = _height - 93;
	$scope._height = _height;
	var chart;
	var gram;
	var trackerDateArray;
	var meteogramForXaxis;
	var todaysDate = new Date();
	var currentMonth = todaysDate.getMonth();
	var currentMonthForKinvey = currentMonth;
	var currentYear = todaysDate.getFullYear();
	$scope.monthSelected = currentMonth;
	$scope.yearSelected = currentYear;
	var noOfDays = 32 - new Date($scope.yearSelected, $scope.monthSelected, 32).getDate();


	window.addEventListener("orientationchange", function(){
		if (typeof window.ga !== "undefined") {
      window.ga.trackEvent('High Chart', 'Rotate Device', 'High Chart', 1);
    }
		cordova.plugins.Keyboard.close();
		if(ionic.Platform.isAndroid()){
			if(screen.orientation.type == "landscape-primary" || screen.orientation.type == "landscape-secondary"){
	  		if($ionicSideMenuDelegate.isOpen()){
					$ionicSideMenuDelegate.toggleLeft();
				}
	  	}
  		// chart.reflow();
	  	if(screen.orientation.type.indexOf('landscape')){
	  		$scope.chartMonth = new Date();
	  		$scope.changeChartMonth();
	  	}
	  	console.log(screen.orientation.type);
		}
		else{
			if(screen.orientation == "landscape-primary" || screen.orientation == "landscape-secondary"){
	  		if($ionicSideMenuDelegate.isOpen()){
					$ionicSideMenuDelegate.toggleLeft();
				}
	  	}
  		// chart.reflow();
	  	if(screen.orientation.indexOf('landscape')){
	  		$scope.chartMonth = new Date();
	  		$scope.changeChartMonth();
	  	}
	  	console.log(screen.orientation);
		}
  	
	});

	$scope.changeCategory = function(index,name){
		if(index == 1 || index == 4){
			if($scope.chartItems.renderer) $scope.chartItems.renderer.image('img/chartSideImg.png', 23, 29, 13, $scope._height-38).add(); 	
		}
		else{
			if($scope.chartItems.renderer) $scope.chartItems.renderer.image('img/chartSideImg2.png', 23, 29, 13, $scope._height-38).add();
		}
		$scope.chartSubcategory = index;
		$scope.physicalArray=[false,false,false,false];
		$scope.mindArray=[false,false,false,false];
		$scope.dailyArray=[false,false,false,false];
		$scope.generalArray=[false,false];
		$scope.currentList="";
		$scope.physicalSeries=[10,11,12,13,14];
		$scope.mindSeries=[2,3,4,5];
		$scope.dailySeries=[6,7,8,9];
		$scope.generalSeries=[0,1];
		for(var i=0;i<$scope.chartSeries.length;i++){
			$scope.chartSeries[i].setVisible(false, false);
		}
		switch (index){
			case 1: 
							$scope.physicalArray=[true,true,true,true];
							$scope.chartSeries[10].setVisible(true, true); $scope.chartSeries[11].setVisible(true, true); $scope.chartSeries[12].setVisible(true, true); $scope.chartSeries[13].setVisible(true, true); $scope.chartSeries[14].setVisible(true, true);
							break;
			case 2: 
							$scope.mindArray=[true,true,true,true]; 
							$scope.chartSeries[2].setVisible(true, true);$scope.chartSeries[3].setVisible(true, true);$scope.chartSeries[4].setVisible(true, true);$scope.chartSeries[5].setVisible(true, true);
							break;
			case 3: 
							$scope.dailyArray=[true,true,true,true]; 
							$scope.chartSeries[6].setVisible(true, true);$scope.chartSeries[7].setVisible(true, true);$scope.chartSeries[8].setVisible(true, true);$scope.chartSeries[9].setVisible(true, true); 
							break;
			case 4: 
							$scope.generalArray=[true,true];  
							$scope.chartSeries[0].setVisible(true, true);$scope.chartSeries[1].setVisible(true, true);
							break;
		}
	}

	$scope.changeChartMonth = function(){
		if($scope.chartMonth == undefined || $scope.chartMonth == "" || $scope.chartMonth == null) $scope.chartMonth = moment()._d;
		console.log($scope.chartMonth);
		$scope.highChartDate.month = moment($scope.chartMonth).format("MMMM");
		$scope.highChartDate.year = moment($scope.chartMonth).format("YYYY");
		$scope.showLoader();
		$scope.changeCategory(1,'physicalSymptoms');
		$scope.symptoms = "physicalSymptoms";
		$scope.getHighChartDatafromKinvey(new Date($scope.chartMonth).getMonth()+1,new Date($scope.chartMonth).getFullYear()); 
	}

	$scope.selectSubcategory = function(name,value){
		if(name == "PAIN"){
			if(value){
				var i=0;
				for(i=0;i<$scope.chartSeries.length;i++){
					if("HEADPAIN" == $scope.chartSeries[i].name || "BODYPAIN" == $scope.chartSeries[i].name ) $scope.chartSeries[i].show();
				}		
			}
			else{
				var i=0;
				for(i=0;i<$scope.chartSeries.length;i++){
					if("HEADPAIN" == $scope.chartSeries[i].name || "BODYPAIN" == $scope.chartSeries[i].name ) $scope.chartSeries[i].hide();
				}
			}
		}
		else{
			if(value){
				var i=0;
				if(name == "PAINphysicalSymptoms"){
					for(i=0;i<$scope.chartSeries.length;i++){
						if($scope.chartSeries[i].name == "HEADPAINphysicalSymptoms" || $scope.chartSeries[i].name == "BODYPAINphysicalSymptoms") $scope.chartSeries[i].show();
					}
				}
				else{
					for(i=0;i<$scope.chartSeries.length;i++){
						if(name == $scope.chartSeries[i].name) break;
					}
					$scope.chartSeries[i].show();
				}
			}
			else{
				var i=0;
				if(name == "PAINphysicalSymptoms"){
					for(i=0;i<$scope.chartSeries.length;i++){
						if($scope.chartSeries[i].name == "HEADPAINphysicalSymptoms" || $scope.chartSeries[i].name == "BODYPAINphysicalSymptoms") $scope.chartSeries[i].hide();
					}
				}
				else{
					for(i=0;i<$scope.chartSeries.length;i++){
						if(name == $scope.chartSeries[i].name) break;
					}
					$scope.chartSeries[i].hide();
				}	
			}
		}
	}

	

	function Meteogram(xml, _treatmenChangesDates, container) {
		// Initialize
		this.list = {};
		this.xml = xml;
		this.container = container;

		// Run
		this.parseYrData();
	}
	Meteogram.prototype.tooltipFormatter = function (tooltip,a) {
		//chart.myTooltip.hide();
		var indexValue = tooltip.point.index, ret = '<div class="hiChrtPopCntnr clearfix">';
		var trackerTempDate = trackerDateArray[indexValue]._kmd.lmt.split('T')[0];
		var trackerOnlyDate = trackerTempDate.split('-')[2];
		var trackerOnlyMonth = trackerTempDate.split('-')[1];
		var trackerOnlyYear = trackerTempDate.split('-')[0];
		var trackerDate = trackerOnlyMonth + "/" + trackerOnlyDate + "/" + trackerOnlyYear;
		var ibdTreatments;

		switch(tooltip.key){
			case "generalWellBeing": 
				if(tooltip.series.name == "ENERGYgeneralWellBeing"){
					var activeClass = ["","","","",""];
					activeClass[tooltip.y-1] = "active";
					ret += '<h3 class="text-center upperCase lightPurple font20">Energy</h3>'+
					'<ul class="rangeBox clearfix">'+
					'<li class="'+activeClass[0]+'">1'+
					'<span class="upperCase lightPurple">TIRED</span></li>'+
					'<li class="'+activeClass[1]+'">2</li>'+
					'<li class="'+activeClass[2]+'">3</li>'+
					'<li class="'+activeClass[3]+'">4</li>'+
					'<li class="'+activeClass[4]+'">5'+
					'<span class="upperCase lightPurple">ENERGETIC</span></li>'+
					'</ul>'+
					'<div class="height1"></div>'+
					'</div>';
					break;
				}
				if (tooltip.series.name == "MOODgeneralWellBeing"){
					var activeClass = ["","","","",""];
					activeClass[tooltip.y-1] = "active";
					ret += '<h3 class="text-center upperCase lightPurple font20">MOOD</h3>'+
					'<ul class="rangeBox clearfix">'+
					'<li class="'+activeClass[0]+'">1'+
					'<span class="upperCase lightPurple">POOR</span></li>'+
					'<li class="'+activeClass[1]+'">2</li>'+
					'<li class="'+activeClass[2]+'">3</li>'+
					'<li class="'+activeClass[3]+'">4</li>'+
					'<li class="'+activeClass[4]+'">5'+
					'<span class="upperCase lightPurple">GREAT</span></li>'+
					'</ul>'+
					'<div class="height1"></div>'+
					'</div>';
					break;
				}
			case "mindMatters":
				var tooltipdata = this.xml[tooltip.point.tooltipIndex].response.mindMatters;
				var activeClass = ["","","","",""];
				ret += '<h3 class="text-center upperCase lightPurple font20">MIND MATTERS</h3>'+
					'<table><tbody><tr><td><div class="left">';
				for(var i=0;i<tooltipdata.length;i++){
					switch(tooltipdata[i].name){
						case "abnormalStress":
							activeClass[4] ="active" + tooltipdata[i].answer; break;
						case "anxiety":
							activeClass[3] = "active" + tooltipdata[i].answer; break;
						case "cognitive":
							activeClass[2] = "active" + tooltipdata[i].answer; break;
						case "depression":
							activeClass[1] = "active" + tooltipdata[i].answer; break;
						case "fatigue":
							activeClass[0] = "active" + tooltipdata[i].answer; break;
					}
				}	
				ret +='<div class="symptomsHldr upperCase"><p class="bold title">FATIGUE</p>'+
						'<ul class="symptomsRange clearfix '+activeClass[0]+'">'+
						'<li></li><li></li><li></li><li></li><li></li>'+
					'</ul></div>'+

					'<div class="symptomsHldr upperCase"><p class="bold title">DEPRESSION</p>'+
						'<ul class="symptomsRange clearfix '+activeClass[1]+'">'+
						'<li></li><li></li><li></li><li></li><li></li>'+
					'</ul></div>'+

					'<div class="symptomsHldr upperCase"><p class="bold title">COGNITION</p>'+
						'<ul class="symptomsRange clearfix '+activeClass[2]+'">'+
						'<li></li><li></li><li></li><li></li><li></li>'+
					'</ul></div>'+

					'<div class="symptomsHldr upperCase"><p class="bold title">ANXIETY</p>'+
						'<ul class="symptomsRange clearfix '+activeClass[3]+'">'+
						'<li></li><li></li><li></li><li></li><li></li>'+
					'</ul></div>'+
					'</div></td>'+

					'<td><div class="right">'+
					'<div class="symptomsHldr upperCase"><p class="bold title">ABDOMINAL STRESS</p>'+
						'<ul class="symptomsRange clearfix '+activeClass[4]+'">'+
						'<li></li><li></li><li></li><li></li><li></li>'+
					'</ul></div>'+

					'</div></td></tr></tbody></table>'+
					'</div>';
					break;
			case "dailyLifeSymptoms":
				var tooltipdata = this.xml[tooltip.point.tooltipIndex].response.dailyLifeSymptoms;
				var activeClass = ["","","","","","",""];
				ret += '<h3 class="text-center upperCase lightPurple font20">Daily Life Symptoms</h3>'+
					'<table><tbody><tr><td><div class="left">';
				for(var i=0;i<tooltipdata.length;i++){
					switch(tooltipdata[i].name){
						case "bladderProblems":
							activeClass[1] ="active" + tooltipdata[i].answer; break;
						case "bowelProblems":
							activeClass[2] = "active" + tooltipdata[i].answer; break;
						case "dizziness":
							activeClass[6] = "active" +tooltipdata[i].answer; break;
						case "heatSensitivity":
							activeClass[3] = "active" + tooltipdata[i].answer; break;
						case "sexualDysfunction":
							activeClass[4] = "active" + tooltipdata[i].answer; break;
						case "visionProblems":
							activeClass[5] = "active" + tooltipdata[i].answer; break;
						case "walkingDifficulties": 
							activeClass[0] = "active" + tooltipdata[i].answer; //tooltipdata[i].answer;
					}

				}	
				ret +='<div class="symptomsHldr upperCase"><p class="bold title">WALKING DIFFICULTY</p>'+
						'<ul class="symptomsRange clearfix '+activeClass[0]+'">'+
						'<li></li><li></li><li></li><li></li><li></li>'+
					'</ul></div>'+

					'<div class="symptomsHldr upperCase"><p class="bold title">BLADDER PROBLEMS</p>'+
						'<ul class="symptomsRange clearfix '+activeClass[1]+'">'+
						'<li></li><li></li><li></li><li></li><li></li>'+
					'</ul></div>'+

					'<div class="symptomsHldr upperCase"><p class="bold title">BOWEL PROBLEMS</p>'+
						'<ul class="symptomsRange clearfix '+activeClass[2]+'">'+
						'<li></li><li></li><li></li><li></li><li></li>'+
					'</ul></div>'+

					'<div class="symptomsHldr upperCase"><p class="bold title">Heat Sensitivity</p>'+
						'<ul class="symptomsRange clearfix '+activeClass[3]+'">'+
						'<li></li><li></li><li></li><li></li><li></li>'+
					'</ul></div>'+
					'</div></td>'+

					'<td><div class="right">'+
					'<div class="symptomsHldr upperCase"><p class="bold title">SEXUAL DYSFUNCTION</p>'+
						'<ul class="symptomsRange clearfix '+activeClass[4]+'">'+
						'<li></li><li></li><li></li><li></li><li></li>'+
					'</ul></div>'+

					'<div class="symptomsHldr upperCase"><p class="bold title">VISION PROBLEMS</p>'+
						'<ul class="symptomsRange clearfix '+activeClass[5]+'">'+
						'<li></li><li></li><li></li><li></li><li></li>'+
					'</ul></div>'+

					'<div class="symptomsHldr upperCase"><p class="bold title">Dizziness</p>'+
						'<ul class="symptomsRange clearfix '+activeClass[6]+'">'+
						'<li></li><li></li><li></li><li></li><li></li>'+
					'</ul></div>'+

					'</div></td></tr></tbody></table>'+
					'</div>';
					break;
			case "physicalSymptoms": 
				var tooltipdata = this.xml[tooltip.point.tooltipIndex].response.physicalSymptoms;
				var physicalTooltip = {}; var neckCheck=false;
					for(var i=0;i<tooltipdata.length;i++){
						if(tooltip.point.matchName == tooltipdata[i].name){
							physicalTooltip=tooltipdata[i];
							break;
						}
					}
				if(tooltip.point.subCategoryName == "HEAD PAIN"){
					var associatePart = ""; neckCheck=false;
					for(var j=0;j<physicalTooltip.answer.length;j++){
						if(physicalTooltip.answer[j].name == "head" && physicalTooltip.answer[j].associate){
							for(var i=0;i<physicalTooltip.answer[j].associate.length;i++){
								if(physicalTooltip.answer[j].associate[i] == "Neck"){
									neckCheck = true;
								}
								else{
									associatePart  +=  physicalTooltip.answer[j].associate[i]+", ";
								}		
							}
							break;
						}
					}
					associatePart = associatePart.trim(",");
					associatePart = associatePart.substring(0, associatePart.length - 1);
					var activeClass = ["","","","",""];
					activeClass[tooltip.y-1] = "active";	
					ret += '<h3 class="text-center upperCase lightPurple font20">'+tooltip.point.subCategoryName+'</h3>'+
						'<ul class="rangeBox clearfix">'+
						'<li class="'+activeClass[0]+'">1<span class="upperCase lightPurple">Mild</span></li>'+
						'<li class="'+activeClass[1]+'">2</li>'+
						'<li class="'+activeClass[2]+'">3</li>'+
						'<li class="'+activeClass[3]+'">4</li>'+
						'<li class="'+activeClass[4]+'">5<span class="upperCase lightPurple">Severe</span></li>'+
						'</ul>';
						if(neckCheck){
						ret +=	'<p class="font10">Pain occurs when neck is bent forward</p>';
						}
						ret +='<div class="height1"></div>';
						if(associatePart != ""){
							ret += '<p class="font10">Associated with:<br><span class="orange" style="white-space:normal">'+associatePart+'</span></p>';
						}			
						ret +='</div>';	
					break;			
				}
				else if(tooltip.point.subCategoryName == "BODY PAIN"){
					var associatePart = ""; neckCheck=false; var max = ""; var maxText=""
					highestValue = 0
					for(var i=0;i<physicalTooltip.answer.length;i++){	
						if(highestValue<physicalTooltip.answer[i].answer && physicalTooltip.answer[i].name != "head"){
							highestValue = physicalTooltip.answer[i].answer;
							max = physicalTooltip.answer[i].name;
							maxText = physicalTooltip.answer[i].highChartName;
						}
					}
					for(var i=0;i<physicalTooltip.answer.length;i++){	
						if(max != physicalTooltip.answer[i].name && physicalTooltip.answer[i].name != "head"){
							associatePart  +=  physicalTooltip.answer[i].highChartName+" Pain, ";
						}
						if(physicalTooltip.answer[i].name == max && physicalTooltip.answer[i].associate != undefined){
							neckCheck = true;
						}
					}
					associatePart = associatePart.trim(",");
					associatePart = associatePart.substring(0, associatePart.length - 1);
					var activeClass = ["","","","",""];
					activeClass[highestValue-1] = "active";
					ret += '<h3 class="text-center upperCase lightPurple font20">'+tooltip.point.subCategoryName+'</h3>'+
						'<p class="text-center upperCase font10 bold">'+maxText+'</p>'+
						'<ul class="rangeBox clearfix">'+
						'<li class="'+activeClass[0]+'">1<span class="upperCase lightPurple">Mild</span></li>'+
						'<li class="'+activeClass[1]+'">2</li>'+
						'<li class="'+activeClass[2]+'">3</li>'+
						'<li class="'+activeClass[3]+'">4</li>'+
						'<li class="'+activeClass[4]+'">5<span class="upperCase lightPurple">Severe</span></li>'+
						'</ul>';
						if(neckCheck){
						ret +=	'<p class="font10">Pain occurs when neck is bent forward</p>';
						}
						ret += '<div class="height1"></div>';
						if(associatePart != ""){
							ret += '<p class="font10">Lesser reported symptoms:<br><span class="orange" style="white-space:normal">'+associatePart+'</span></p>';
						}			
						ret +='</div>';				
					break;
				}
				else{
					var associatePart = "";
					for(var i=0;i<physicalTooltip.answer.length;i++){
						if(physicalTooltip.highest.name != physicalTooltip.answer[i].name){
							associatePart  +=  physicalTooltip.answer[i].highChartName+" "+ tooltip.point.subCategoryName +", ";
						}
					}
					if(physicalTooltip.highest.associate != undefined){
						neckCheck = true;
					}
					associatePart = associatePart.trim(",");
					associatePart = associatePart.substring(0, associatePart.length - 1);	
					highestValue = physicalTooltip.highest.text.trim(":");
					highestValue = highestValue.substring(0, highestValue.length - 1);
					var activeClass = ["","","","",""];
					activeClass[tooltip.y-1] = "active";
					ret += '<h3 class="text-center upperCase lightPurple font20">'+tooltip.point.subCategoryName+'</h3>'+
						'<p class="text-center upperCase font10 bold">'+highestValue+'</p>'+
						'<ul class="rangeBox clearfix">'+
						'<li class="'+activeClass[0]+'">1<span class="upperCase lightPurple">Mild</span></li>'+
						'<li class="'+activeClass[1]+'">2</li>'+
						'<li class="'+activeClass[2]+'">3</li>'+
						'<li class="'+activeClass[3]+'">4</li>'+
						'<li class="'+activeClass[4]+'">5<span class="upperCase lightPurple">Severe</span></li>'+
						'</ul>';
						if(neckCheck){
						ret +='<p class="font10">'+tooltip.point.subCategoryName +' occurs when neck<br/>is bent forward</p>';
						}
						ret += '<div class="height1"></div>';
						if(associatePart != ""){
							ret += '<p class="font10">Lesser reported symptoms:<br><span class="orange" style="white-space:normal">'+associatePart+'</span></p>';
						}			
						ret +='</div>';				
					break;
				}
		}
		//chart.myTooltip.refresh();
		return ret;
	};

	Meteogram.prototype.getChartOptions = function () {
		var meteogram = this;
		var _date = 0;
		return {
			chart: {
				renderTo: this.container,
				events: {
          load: function(){
            this.myTooltip = new Highcharts.Tooltip(this, this.options.tooltip);                    
          }
        },
				animation: false,
				marginBottom: 10,
				marginRight: 23,
				marginLeft: 37,
				marginTop: 30,
				plotBorderWidth: 1,
				width: _width,
				//ASI: this works but not implemented because the smileys are not set dynamically
				height: _height
			},
			title: {
				align: 'left'
			},
			tooltip: {
				shared: false,
				useHTML: true,
				backgroundColor: '#fff',
		    borderColor: 'black',
		    borderRadius: 0,
		    borderWidth: 0,
		    style: {
      			padding: 0
  			},
  			shape: "square",
				formatter: function () {
					return meteogram.tooltipFormatter(this,this.list);
				}
			},
			xAxis: [{ // Bottom X axis
				type: 'datetime',
				tickInterval: 24 * 3600 * 1000, //24 hour
				minorTickInterval: 24 * 3600 * 1000, // 1 day(24 hours) 
				minTickInterval: 24 * 3600 * 1000,
				tickLength: 0,
				gridLineWidth: 1,
				gridLineColor: (Highcharts.theme && Highcharts.theme.background2) || '#F0F0F0',
				startOnTick: true,
				endOnTick: true,
				minPadding: 0,
				maxPadding: 0,
				offset: 22,
				lineWidth: 0,
				showLastLabel: true,
				labels: {
					step:1,
					enabled: false
				}
			}, { // Top X axis
				linkedTo: 0,
				type: 'datetime',
				tickInterval: 24 * 3600 * 1000, //24 hour
				minorTickInterval: 24 * 3600 * 1000, // 1 day(24 hours) 
				minTickInterval: 24 * 3600 * 1000,
				tickColor: '#000',
				endOnTick: true,
				startOnTick: true,
				showFirstLabel: true,
				opposite: true,
				tickLength: 0,//20,
				gridLineWidth: 1,
				gridLineColor: (Highcharts.theme && Highcharts.theme.background2) || '#d3d3d3',
				labels: {
					enabled: true,
					step:1,
					formatter: function () {
						if (this.value) {
							var _month = $scope.monthSelected;
							_date = (_month > $scope.monthSelected) ? _date+1 : new Date(this.value).getUTCDate();
							if(_date > noOfDays){
								return '<span style="fill: #000;display:none;">' + _date + '</span>';
							}						
							return '<span style="fill: #000;">' + _date + '</span>';
						}
					},
					y: -10
				}
			}],
			yAxis: [{ // temperature axis
				title: {
					text: 'SYMPTOM SEVERITY',
					align: 'middle',
					color: "#ff0000",
					margin: 0,
					enabled:false
				},
				labels: {
					enabled: false,
					formatter: function () {
						if(this.isLast){
							return '<span style="background:#000">'+this.value+'</span>';
						}
						else if(this.isFirst){
							return '<span style="background:#000">'+this.value+'</span>';
						}
						else{
							return '<span style="background:#000">'+'</span>';
						}
						// else if(this.value == 3){
						// 	return '<span>'+this.value+'</span>';
						// }	 
					},
					x: -10
				},
				plotLines: [{ // zero plane
					value: 0,
					color: '#BBBBBB',
					width: 1,
					zIndex: 2
				}],
				tickInterval: 1,
				tickPosition: 'inside',
				gridLineColor: '#C0C0C0',
				startOnTick: true,
				min: 0,
				max: 6,
				endOnTick: true
			}],
			legend: {
				enabled: false
			},
			plotOptions: {
				series: {
					//pointPlacement: 'between',
					stickyTracking: false,
          events: {
            click: function(evt) {
                this.chart.myTooltip.refresh(evt.point, evt);
            },
            mouseOut: function() {
                this.chart.myTooltip.hide();
            }                       
          }
				}
			},
			series: [
				{
					name: 'ENERGYgeneralWellBeing',
					data: this.list.generalWellBeingEnergy != undefined ? this.list.generalWellBeingEnergy : [],
					type: 'line',
					marker: {
						enabled: true,
						symbol: 'circle',
						radius: 6,
						fillColor:'#b1d577',
	          lineColor:'#454545',
	          lineWidth:1,
						states: {
							hover: {
								enabled: true,
								fillColor:'#b1d577'
							}
						}
					},
					tooltip: {
						valueSuffix: ''
					},
					zIndex: 1,
					color: '#454545',
					negativeColor: '#b1d577'
				}, {
					name: 'MOODgeneralWellBeing',
					data: this.list.generalWellBeingMood != undefined ? this.list.generalWellBeingMood : [],
					type: 'line',
					marker: {
						enabled: true,
						symbol: 'circle',
						radius: 6,
						fillColor:'#3399ff',
	          lineColor:'#454545',
	          lineWidth:1,
						states: {
							hover: {
								enabled: true,
								fillColor:'#3399ff'
							}
						}
					},
					tooltip: {
						valueSuffix: ''
					},
					zIndex: 1,
					color: '#454545',
					negativeColor: '#3399ff'
				}, {
					name: 'ANXIETYmindMatters',
					data: this.list.mindMattersAnxiety != undefined ? this.list.mindMattersAnxiety : [],
					type: 'line',
					marker: {
						enabled: true,
						symbol: 'circle',
						radius: 6,
						fillColor:'#76489c',
	          lineColor:'#454545',
	          lineWidth:1,
						// enabled: (this.rectalBleeding.length == 1)?true:false,//true
						states: {
							hover: {
								enabled: true,
								fillColor:'#76489c'
							}
						}
					},
					tooltip: {
						valueSuffix: ''
					},
					zIndex: 1,
					color: '#454545',
					negativeColor: '#76489c'
				},{
					name: 'COGNITIVEmindMatters',
					data: this.list.mindMattersCognition != undefined ? this.list.mindMattersCognition : [],
					type: 'line',
					marker: {
						enabled: true,
						symbol: 'circle',
						radius: 6,
						fillColor:'#f18903',
	          lineColor:'#454545',
	          lineWidth:1,
						states: {
							hover: {
								enabled: true,
								fillColor:'#f18903'
							}
						}
					},
					tooltip: {
						valueSuffix: ''
					},
					zIndex: 1,
					color: '#454545',
					negativeColor: '#f18903'
				},{
					name: 'DEPRESSIONmindMatters',
					data: this.list.mindMattersDepression != undefined ? this.list.mindMattersDepression : [],
					type: 'line',
					marker: {
						enabled: true,
						symbol: 'circle',
						radius: 6,
						fillColor:'#3399ff',
	          lineColor:'#454545',
	          lineWidth:1,
						states: {
							hover: {
								enabled: true,
								fillColor:'#3399ff'
							}
						}
					},
					tooltip: {
						valueSuffix: ''
					},
					zIndex: 1,
					color: '#454545',
					negativeColor: '#3399ff'
				},{
					name: 'FATIGUEmindMatters',
					data: this.list.mindMattersFatigue != undefined ? this.list.mindMattersFatigue : [],
					type: 'line',
					marker: {
						enabled: true,
						symbol: 'circle',
						radius: 6,
						fillColor:'#b1d577',
	          lineColor:'#454545',
	          lineWidth:1,
						// enabled: (this.rectalBleeding.length == 1)?true:false,//true
						states: {
							hover: {
								enabled: true,
								fillColor:'#b1d577'
							}
						}
					},
					tooltip: {
						valueSuffix: ''
					},
					zIndex: 1,
					color: '#454545',
					negativeColor: '#b1d577'
				},{
					name: 'WALKINGDIFFICULTYdailyLifeSymptoms',
					data: this.list.dailyLifeSymptomsWalkingDifficulties != undefined ? this.list.dailyLifeSymptomsWalkingDifficulties : [],
					type: 'line',
					marker: {
						enabled: true,
						symbol: 'circle',
						radius: 6,
						fillColor:'#b1d577',
	          lineColor:'#454545',
	          lineWidth:1,
						states: {
							hover: {
								enabled: true,
								fillColor:'#b1d577'
							}
						}
					},
					tooltip: {
						valueSuffix: ''
					},
					zIndex: 1,
					color: '#454545',
					negativeColor: '#b1d577'
				},{
					name: 'BLADDERPROBLEMdailyLifeSymptoms',
					data: this.list.dailyLifeSymptomsBladderProblems != undefined ? this.list.dailyLifeSymptomsBladderProblems : [],
					type: 'line',
					marker: {
						enabled: true,
						symbol: 'circle',
						radius: 6,
						fillColor:'#3399ff',
	          lineColor:'#454545',
	          lineWidth:1,
						states: {
							hover: {
								enabled: true,
								fillColor:'#3399ff'
							}
						}
					},
					tooltip: {
						valueSuffix: ''
					},
					zIndex: 1,
					color: '#454545',
					negativeColor: '#3399ff'
				},{
					name: 'BOWELPROBLEMSdailyLifeSymptoms',
					data: this.list.dailyLifeSymptomsBowelProblems != undefined ? this.list.dailyLifeSymptomsBowelProblems : [],
					type: 'line',
					marker: {
						enabled: true,
						symbol: 'circle',
						radius: 6,
						fillColor:'#f18903',
	          lineColor:'#454545',
	          lineWidth:1,
						states: {
							hover: {
								enabled: true,
								fillColor:'#f18903'
							}
						}
					},
					tooltip: {
						valueSuffix: ''
					},
					zIndex: 1,
					color: '#454545',
					negativeColor: '#f18903'
				},{
					name: 'VISIONPROBLEMdailyLifeSymptoms',
					data: this.list.dailyLifeSymptomsVisionProblems != undefined ? this.list.dailyLifeSymptomsVisionProblems : [],
					type: 'line',
					marker: {
						enabled: true,
						symbol: 'circle',
						radius: 6,
						fillColor:'#76489c',
	          lineColor:'#454545',
	          lineWidth:1,
						states: {
							hover: {
								enabled: true,
								fillColor:'#76489c'
							}
						}
					},
					tooltip: {
						valueSuffix: ''
					},
					zIndex: 1,
					color: '#454545',
					negativeColor: '#76489c'
				},{
					name: 'BODYPAINphysicalSymptoms',
					data: this.list.physicalSymptomsBodyPain != undefined ? this.list.physicalSymptomsBodyPain : [],
					type: 'line',
					marker: {
						enabled: true,
						symbol: 'circle',
						radius: 6,
						fillColor:'#098400',
	          lineColor:'#454545',
	          lineWidth:1,
						states: {
							hover: {
								enabled: true,
								fillColor:'#098400'
							}
						}
					},
					tooltip: {
						valueSuffix: ''
					},
					zIndex: 1,
					color: '#454545',
					negativeColor: '#098400'
				},{
					name: 'HEADPAINphysicalSymptoms',
					data: this.list.physicalSymptomsHeadPain != undefined ? this.list.physicalSymptomsHeadPain : [],
					type: 'line',
					marker: {
						enabled: true,
						symbol: 'circle',
						radius: 6,
						fillColor:'#b1d577',
	          lineColor:'#454545',
	          lineWidth:1,
						states: {
							hover: {
								enabled: true,
								fillColor:'#b1d577'
							}
						}
					},
					tooltip: {
						valueSuffix: ''
					},
					zIndex: 1,
					color: '#454545',
					negativeColor: '#b1d577'
				},{
					name: 'NUMBNESSTINGLINGphysicalSymptoms',
					data: this.list.physicalSymptomsNumbnessTingling != undefined ? this.list.physicalSymptomsNumbnessTingling : [],
					type: 'line',
					marker: {
						enabled: true,
						symbol: 'circle',
						radius: 6,
						fillColor:'#3399ff',
	          lineColor:'#454545',
	          lineWidth:1,
						states: {
							hover: {
								enabled: true,
								fillColor:'#3399ff'
							}
						}
					},
					tooltip: {
						valueSuffix: ''
					},
					zIndex: 1,
					color: '#454545',
					negativeColor: '#3399ff'
				},{
					name: 'MUSCLEWEAKNESSphysicalSymptoms',
					data: this.list.physicalSymptomsWeakness != undefined ? this.list.physicalSymptomsWeakness : [],
					type: 'line',
					marker: {
						enabled: true,
						symbol: 'circle',
						radius: 6,
						fillColor:'#f18903',
	          lineColor:'#454545',
	          lineWidth:1,
						states: {
							hover: {
								enabled: true,
								fillColor:'#f18903'
							}
						}
					},
					tooltip: {
						valueSuffix: ''
					},
					zIndex: 1,
					color: '#454545',
					negativeColor: '#f18903'
				},{
					name: 'MUSCLESPASMSphysicalSymptoms',
					data: this.list.physicalSymptomsMuscleSpasms != undefined ? this.list.physicalSymptomsMuscleSpasms : [],
					type: 'line',
					marker: {
						enabled: true,
						symbol: 'circle',
						radius: 6,
						fillColor:'#76489c',
	          lineColor:'#454545',
	          lineWidth:1,
						states: {
							hover: {
								enabled: true,
								fillColor:'#76489c'
							}
						}
					},
					tooltip: {
						valueSuffix: ''
					},
					zIndex: 1,
					color: '#454545',
					negativeColor: '#76489c'
				}
			]
		}
	};
	Meteogram.prototype.onChartLoad = function (chart) {
		for(var i=0;i<chart.series.length;i++){
			chart.series[i].hide();
			if(chart.series[i].name.indexOf("physicalSymptoms") > -1) chart.series[i].show();
			else chart.series[i].hide();
		}
		$scope.chartSeries = chart.series;
	};

	Meteogram.prototype.createChart = function () {
		var meteogram = this;
		this.chart = new Highcharts.Chart(this.getChartOptions(), function (chart) {
			meteogram.onChartLoad(chart);
		});
		chart = this.chart;	

		if($scope.monthSelected<10){
			var setExtremesMonth = "0"+$scope.monthSelected;
		}
		else{
			var setExtremesMonth = $scope.monthSelected;
		}
		chart.xAxis[0].setExtremes(Date.UTC($scope.yearSelected, setExtremesMonth, 1), Date.UTC($scope.yearSelected, setExtremesMonth, (noOfDays)));
		gram = meteogram;
		$scope.chartItems = chart;
		chart.renderer.image('img/chartSideImg.png', 23, 29, 13, _height-38).add(); 
		//chart.renderer.image('img/chartSideImg2.png', 23, 29, 13, _height-38).add(); 
	};

	Meteogram.prototype.parseYrData = function () {
		var meteogram = this,
			xml = this.xml,
			pointStart;
			trackerDateArray = xml;
			var index=0;
		angular.forEach(xml, function (data,i) {
			// HA: Removing the time from date
			var modifiedDate = new Date(data.trackerHighChartDate);
			var timeZoneOffset = modifiedDate.getTimezoneOffset();
			modifiedDate.setHours(0);
			modifiedDate.setMinutes(0);
			modifiedDate.setSeconds(0);
			var localModifiedDate = new Date(Date.parse(modifiedDate) - Number(timeZoneOffset)*60*1000);
			// Get the times - only Safari can't parse ISO8601 so we need to do some replacements
			var from = Date.parse(localModifiedDate),
				to = Date.parse(localModifiedDate); //ASi: need to do + 1
			if(data.response.generalWellBeing!=undefined){
				for(var i=0;i<data.response.generalWellBeing.length;i++){
					if(data.response.generalWellBeing[i].name == "energy"){
						meteogram.list.generalWellBeingEnergy == undefined ? meteogram.list.generalWellBeingEnergy=[] :"";
						meteogram.list.generalWellBeingEnergy.push({
							x: from,
							y: data.response.generalWellBeing[i].answer,
							name : "generalWellBeing",
							// custom options used in the tooltip formatter
							tooltipIndex:index,
							to: to,
							index: i,
							
							dateNumber:String((modifiedDate.getDate() < 10)?"0"+modifiedDate.getDate():modifiedDate.getDate())
						});
					}
					else if(data.response.generalWellBeing[i].name == "feeling") {
						meteogram.list.generalWellBeingMood == undefined ? meteogram.list.generalWellBeingMood=[] :"";
						meteogram.list.generalWellBeingMood.push({
							x: from,
							y: data.response.generalWellBeing[i].answer,
							// custom options used in the tooltip formatter
							name : "generalWellBeing",
							tooltipIndex:index,
							to: to,
							index: i,
							dateNumber:String((modifiedDate.getDate() < 10)?"0"+modifiedDate.getDate():modifiedDate.getDate())
						});
					}
					
				}		
			}
			if(data.response.mindMatters!=undefined){
				for(var i=0;i<data.response.mindMatters.length;i++){
					if(data.response.mindMatters[i].name == "anxiety"){
						meteogram.list.mindMattersAnxiety == undefined ? meteogram.list.mindMattersAnxiety=[] :"";
						meteogram.list.mindMattersAnxiety.push({
							x: from,
							y: data.response.mindMatters[i].answer,
							// custom options used in the tooltip formatter
							name : "mindMatters",
							tooltipIndex:index,
							to: to,
							index: i,
							dateNumber:String((modifiedDate.getDate() < 10)?"0"+modifiedDate.getDate():modifiedDate.getDate())
						});
					}
					else if(data.response.mindMatters[i].name == "cognitive"){
						meteogram.list.mindMattersCognition == undefined ? meteogram.list.mindMattersCognition=[] :"";
						meteogram.list.mindMattersCognition.push({
							x: from,
							y: data.response.mindMatters[i].answer,
							// custom options used in the tooltip formatter
							name : "mindMatters",
							tooltipIndex:index,
							to: to,
							index: i,
							dateNumber:String((modifiedDate.getDate() < 10)?"0"+modifiedDate.getDate():modifiedDate.getDate())
						});
					}
					else if(data.response.mindMatters[i].name == "depression"){
						meteogram.list.mindMattersDepression == undefined ? meteogram.list.mindMattersDepression=[] :"";
						meteogram.list.mindMattersDepression.push({
							x: from,
							y: data.response.mindMatters[i].answer,
							// custom options used in the tooltip formatter
							name : "mindMatters",
							tooltipIndex:index,
							to: to,
							index: i,
							dateNumber:String((modifiedDate.getDate() < 10)?"0"+modifiedDate.getDate():modifiedDate.getDate())
						});
					}
					else if(data.response.mindMatters[i].name == "fatigue"){
						meteogram.list.mindMattersFatigue == undefined ? meteogram.list.mindMattersFatigue=[] :"";
						meteogram.list.mindMattersFatigue.push({
							x: from,
							y: data.response.mindMatters[i].answer,
							// custom options used in the tooltip formatter
							name : "mindMatters",
							tooltipIndex:index,
							to: to,
							index: i,
							dateNumber:String((modifiedDate.getDate() < 10)?"0"+modifiedDate.getDate():modifiedDate.getDate())
						});
					}			
				}		
			}	
			if(data.response.dailyLifeSymptoms!=undefined){
				for(var i=0;i<data.response.dailyLifeSymptoms.length;i++){
					if(data.response.dailyLifeSymptoms[i].name == "bladderProblems"){
						meteogram.list.dailyLifeSymptomsBladderProblems == undefined ? meteogram.list.dailyLifeSymptomsBladderProblems=[] :"";
						meteogram.list.dailyLifeSymptomsBladderProblems.push({
							x: from,
							y: data.response.dailyLifeSymptoms[i].answer,
							// custom options used in the tooltip formatter
							name : "dailyLifeSymptoms",
							tooltipIndex:index,
							to: to,
							index: i,
							dateNumber:String((modifiedDate.getDate() < 10)?"0"+modifiedDate.getDate():modifiedDate.getDate())
						});
					}
					else if(data.response.dailyLifeSymptoms[i].name == "bowelProblems"){
						meteogram.list.dailyLifeSymptomsBowelProblems == undefined ? meteogram.list.dailyLifeSymptomsBowelProblems=[] :"";
						meteogram.list.dailyLifeSymptomsBowelProblems.push({
							x: from,
							y: data.response.dailyLifeSymptoms[i].answer,
							// custom options used in the tooltip formatter
							name : "dailyLifeSymptoms",
							tooltipIndex:index,
							to: to,
							index: i,
							dateNumber:String((modifiedDate.getDate() < 10)?"0"+modifiedDate.getDate():modifiedDate.getDate())
						});
					}
					else if(data.response.dailyLifeSymptoms[i].name == "visionProblems"){
						meteogram.list.dailyLifeSymptomsVisionProblems == undefined ? meteogram.list.dailyLifeSymptomsVisionProblems=[] :"";
						meteogram.list.dailyLifeSymptomsVisionProblems.push({
							x: from,
							y: data.response.dailyLifeSymptoms[i].answer,
							// custom options used in the tooltip formatter
							name : "dailyLifeSymptoms",
							tooltipIndex:index,
							to: to,
							index: i,
							dateNumber:String((modifiedDate.getDate() < 10)?"0"+modifiedDate.getDate():modifiedDate.getDate())
						});
					}
					else if(data.response.dailyLifeSymptoms[i].name == "walkingDifficulties"){
						meteogram.list.dailyLifeSymptomsWalkingDifficulties == undefined ? meteogram.list.dailyLifeSymptomsWalkingDifficulties=[] :"";
						meteogram.list.dailyLifeSymptomsWalkingDifficulties.push({
							x: from,
							y: data.response.dailyLifeSymptoms[i].answer,
							// custom options used in the tooltip formatter
							name : "dailyLifeSymptoms",
							tooltipIndex:index,
							to: to,
							index: i,
							dateNumber:String((modifiedDate.getDate() < 10)?"0"+modifiedDate.getDate():modifiedDate.getDate())
						});
					}			
				}	
			}
			if(data.response.physicalSymptoms!=undefined){
				for(var i=0;i<data.response.physicalSymptoms.length;i++){
					if(data.response.physicalSymptoms[i].name == "numbnessTingling"){
						meteogram.list.physicalSymptomsNumbnessTingling == undefined ? meteogram.list.physicalSymptomsNumbnessTingling=[] :"";
						var max=data.response.physicalSymptoms[i].answer[0].answer;
						if(data.response.physicalSymptoms[i].answer.length>1){
							for(var j=1; j < data.response.physicalSymptoms[i].answer.length;j++){
								if(max < data.response.physicalSymptoms[i].answer[j].answer) max = data.response.physicalSymptoms[i].answer[j].answer
							}
						}
						meteogram.list.physicalSymptomsNumbnessTingling.push({
							x: from,
							y: max,
							// custom options used in the tooltip formatter
							name : "physicalSymptoms",
							subCategoryName:"Numbness/Tingling",
							matchName:"numbnessTingling",
							tooltipIndex:index,
							to: to,
							index: i,
							dateNumber:String((modifiedDate.getDate() < 10)?"0"+modifiedDate.getDate():modifiedDate.getDate())
						});
					}
					else if(data.response.physicalSymptoms[i].name == "pain"){
							var max=0; var associate=[];
						for(var z=0;z < data.response.physicalSymptoms[i].answer.length;z++){		
							if(data.response.physicalSymptoms[i].answer[z].name == "head"){
								meteogram.list.physicalSymptomsHeadPain == undefined ? meteogram.list.physicalSymptomsHeadPain=[] :"";
								meteogram.list.physicalSymptomsHeadPain.push({
									x: from,
									y: data.response.physicalSymptoms[i].answer[z].answer,
									// custom options used in the tooltip formatter
									name : "physicalSymptoms",
									subCategoryName:"HEAD PAIN",
									matchName:"pain",
									tooltipIndex:index,
									to: to,
									index: i,
									dateNumber:String((modifiedDate.getDate() < 10)?"0"+modifiedDate.getDate():modifiedDate.getDate())
								});
							}
							else{
								meteogram.list.physicalSymptomsBodyPain == undefined ? meteogram.list.physicalSymptomsBodyPain=[] :"";	
								associate.push(data.response.physicalSymptoms[i].answer[z].name);					
								if(max < data.response.physicalSymptoms[i].answer[z].answer) {
									max = data.response.physicalSymptoms[i].answer[z].answer
								}
							}
						}
						if(max>0){
							meteogram.list.physicalSymptomsBodyPain.push({
								x: from,
								y: max,
								// custom options used in the tooltip formatter
								name : "physicalSymptoms",
								subCategoryName:"BODY PAIN",
								matchName:"pain",
								tooltipIndex:index,
								to: to,
								index: i,
								dateNumber:String((modifiedDate.getDate() < 10)?"0"+modifiedDate.getDate():modifiedDate.getDate())
							});
						}					  
					}
					else if(data.response.physicalSymptoms[i].name == "weakness"){
						meteogram.list.physicalSymptomsWeakness == undefined ? meteogram.list.physicalSymptomsWeakness=[] :"";
						var max=data.response.physicalSymptoms[i].answer[0].answer;
						if(data.response.physicalSymptoms[i].answer.length>1){
							for(var j=1; j < data.response.physicalSymptoms[i].answer.length;j++){
								if(max < data.response.physicalSymptoms[i].answer[j].answer) max = data.response.physicalSymptoms[i].answer[j].answer
							}
						}
						meteogram.list.physicalSymptomsWeakness.push({
							x: from,
							y: max,
							// custom options used in the tooltip formatter
							name : "physicalSymptoms",
							subCategoryName:"Muscle Weakness",
							matchName:"weakness",
							tooltipIndex:index,
							to: to,
							index: i,
							dateNumber:String((modifiedDate.getDate() < 10)?"0"+modifiedDate.getDate():modifiedDate.getDate())
						});
					}
					else if(data.response.physicalSymptoms[i].name == "muscleSpasms"){
						meteogram.list.physicalSymptomsMuscleSpasms == undefined ? meteogram.list.physicalSymptomsMuscleSpasms=[] :"";
						var max=data.response.physicalSymptoms[i].answer[0].answer;
						if(data.response.physicalSymptoms[i].answer.length>1){
							for(var j=1; j < data.response.physicalSymptoms[i].answer.length;j++){
								if(max < data.response.physicalSymptoms[i].answer[j].answer) max = data.response.physicalSymptoms[i].answer[j].answer
							}
						}
						meteogram.list.physicalSymptomsMuscleSpasms.push({
							x: from,
							y: max,
							// custom options used in the tooltip formatter
							name : "physicalSymptoms",
							subCategoryName:"Muscle Spasms",
							matchName:"muscleSpasms",
							tooltipIndex:index,
							to: to,
							index: i,
							dateNumber:String((modifiedDate.getDate() < 10)?"0"+modifiedDate.getDate():modifiedDate.getDate())
						});
					}			
				}	
			}
			index++;
		});
		// Create the chart when the data is loaded
		meteogramForXaxis = meteogram;
		this.createChart();
	};

	var data = [];
	$scope.getHighChartDatafromKinvey = function(month,year,isStart) {
		console.log("in function");	
		$scope.showLoader();
		var data2 = [];
		var users = $kinvey.User.getActiveUser();
  	var highchartKinvey = $kinvey.DataStore.getInstance('healthTrackerAnswers', $kinvey.DataStoreType.Network);	
  	var col = "_acl";
  	var relatedJson = {"creator":users.data._id};
  	var kmd = "trackerHighChartDate";
  	var newMonth = month+1;
  	var kmdEndDate = ""
  	if(newMonth>12) { newMonth = 01; kmdEndDate = moment(new Date(year+1+ '-' +newMonth+'-01')).format();}
  	else{if(newMonth<10) {newMonth = "0"+newMonth;}kmdEndDate = moment(new Date(year+ '-' +newMonth+'-01')).format();}	
  	var nowMonth = month;
  	if(nowMonth<10) {nowMonth = "0"+nowMonth;}
		var kmdStartDate = moment(new Date(year+"-"+nowMonth+"-01")).format();
		var query = new $kinvey.Query();
    query.equalTo(col, relatedJson);
    var query1 = new $kinvey.Query();
    query1.greaterThanOrEqualTo(kmd,kmdStartDate);
    query.and(query1);
    var query2 = new $kinvey.Query();
    query2.lessThan(kmd,kmdEndDate);
    query.and(query2);
		query.ascending('trackerHighChartDate');
    highchartKinvey.find(query).subscribe(function(data) {      
      data2 = data;      
    }, function(error) {
    	$scope.internalGlitch();
    }, function(data) {
	  	$scope.monthSelected = parseInt(month)-1;
			$scope.yearSelected = parseInt(year);
			noOfDays = parseInt(moment(year+ '-' +nowMonth+'-01').endOf('month').format("DD"));
	    var meteogram = new Meteogram(data2, null, 'chartContainer');
	    if(isStart == "start"){
	    	$scope.exitAllLoader("highChart");
	    }
	    else{
	    	$scope.changeCategory(1,'physicalSymptoms');
	    	$scope.hideLoader();
	    }
    });
	};

	$scope.getHighChartDatafromKinvey(new Date().getMonth()+1,new Date().getFullYear(),"start");

	$scope.$on('refreshHighChartappCtrl',function(){
		$scope.symptoms = "physicalSymptoms";
    $scope.getHighChartDatafromKinvey(new Date().getMonth()+1,new Date().getFullYear()); 
  });

	$scope.$on('updateExitLoader',function(event,value,loader) {
		$scope.exitLoader[loader] = value;
	});

  $scope.exitAllLoader = function(param){
		$scope.exitLoader[param] = true;
		if($scope.exitLoader.pointsLoader && $scope.exitLoader.highChart){
			console.log("stop");
			//$scope.$apply();
			$scope.hideLoader();
			$scope.exitLoader = {"pointsLoader":false,"forcastLoader":false,"highChart":true}
		}
	}

}]);