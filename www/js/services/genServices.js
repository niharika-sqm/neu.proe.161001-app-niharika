// For ionic popup close on backbrop
neuroScienceApp.service('closePopupService', [function () {
  var currentPopup;
  var htmlEl = angular.element(document.querySelector('html'));
  htmlEl.on('click', function (event) {
    if (event.target.nodeName === 'HTML') {
      if (currentPopup) {
        currentPopup.close();
      }
    }
  });

  this.register = function (popup) {
    currentPopup = popup;
  }
  
  this.closePopup = function(){
    if(currentPopup){
      currentPopup.close();
    }
  }

}]);

neuroScienceApp.service('getSetDeviceUUID',['$q', function getSetDeviceUUID($q){
  var ss;
  var service = {
    initService:function(){
      var deferred = $q.defer();
      ss = new cordova.plugins.SecureStorage(
      function(){
        console.log('Success');
        deferred.resolve()
      },
      function(error){
        deferred.reject()
        console.log('Error ' + error);
      },'my_app');
      return deferred.promise;
    },
    getId:function(){
      var deferred = $q.defer();
      var this1 = this;
      this1.initService()
      .then(function(){
        ss.get(
        function(value){
          console.log('Success, got ' + value);
          deferred.resolve(value);
        },
        function(error){
          console.log('Error ' + error);
          this1.setId()
          .then(function(){
            deferred.resolve(window.device.uuid);
          },function(error){
            deferred.reject(error);
          });
        }, 'deviceUUID');
      },function(){
        deferred.reject();
      });
      return deferred.promise;
    },
    setId:function(){
      var deferred = $q.defer();
      ss.set(
      function(key){
        console.log(key);
        deferred.resolve();
      },
      function(error){
        deferred.reject(error);
        console.log('Error ' + error);
      }, 'deviceUUID', window.device.uuid);
      return deferred.promise;
    },
    removeId:function(){
      var deferred = $q.defer();
      ss.remove(
      function(){
        deferred.resolve();
      },
      function(error){
        deferred.reject(error);
        console.log('Error ' + error);
      }, 'deviceUUID');
      return deferred.promise;
    }
  }

  return service;
}]);