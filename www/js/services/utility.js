neuroScienceApp.service('utility',['$q','$http','CONFIG', function utility($q, $http, CONFIG) {
	var service = {
		request: function(args) {
			$http.defaults.headers.common.Authorization = 'Basic ' + btoa(CONFIG.kinveyCredentials.appKey+':'+CONFIG.kinveyCredentials.appSecret);

			params = args.params || {}
			args = args || {};
			var deferred = $q.defer(),
				url = args.url,
				method = args.method || "GET",
				params = params,
				data = args.data || {};

			$http({
				url: url,
				withCredentials:true,
				method: method.toUpperCase(),
				params: params,
				data: data,
				timeout:10000
			})
			.success(angular.bind(this,function(data, status, headers, config) {
				deferred.resolve(data, status);
			}))
			.error(angular.bind(this,function(data, status, headers, config) {
				deferred.reject({"data":data, "status":status});
			}));
			return deferred.promise;
		},
		forgetPassword: function(args){
			return this.request({
				'method': "POST",
				'url': CONFIG.kinveyCredentials.apiHostname+"/rpc/"+CONFIG.kinveyCredentials.appKey+"/"+args+"/user-password-reset-initiate"
			});
		},
		deleteUser : function(args){
			return this.request({
				'method': "DELETE",
				'url': CONFIG.kinveyCredentials.apiHostname+"/user/"+CONFIG.kinveyCredentials.appKey+"/"+args,
				'params':{"hard":true}
			});
		},
		login:function(args){
			return this.request({
				'method': "POST",
				'url': CONFIG.kinveyCredentials.apiHostname+"/user/"+CONFIG.kinveyCredentials.appKey+"/login",
				'data': args
			});
		},
		forecast:function(){
			return this.request({
				'method': "GET",
				'url': CONFIG.heatAlerts.requestUrl+"/forecast/"+CONFIG.heatAlerts.forecastKey+"/"+CONFIG.heatAlerts.latitude+","+CONFIG.heatAlerts.longitude
			});
		}
	}
	return service;
}]);