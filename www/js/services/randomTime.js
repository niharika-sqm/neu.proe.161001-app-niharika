neuroScienceApp.factory('randomTime', function(){
	var randomTime = {
		randomTimeOneSix:function(){
			var time = Math.floor(Math.random() * 6000);
		  if(time>1000){
				return time;
		  }else{
		  	return time+1000;
		  }
		},
		randomTimeOneTwo:function(){
			return (Math.floor(Math.random()*2)+1);
		}
	};
	
	return randomTime;
})