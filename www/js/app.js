var neuroScienceApp = angular.module('neuroScience', ['ionic','ui.knob', 'kinvey']);

neuroScienceApp.run(['$ionicPlatform','CONFIG', function($ionicPlatform,CONFIG){
	$ionicPlatform.ready(function() {
		if(!localStorage.welcomePopupShow){
			localStorage.setItem('welcomePopupShow',true);
		}

		var timeout = setInterval(function(){
			if (navigator && navigator.splashscreen) {
				navigator.splashscreen.hide();
			}

			if (window.navigator && window.navigator.splashscreen) {
				window.navigator.splashscreen.hide();
			}
			clearInterval(timeout);
		},2000);

		if (window.cordova && window.cordova.plugins.Keyboard) {
			cordova.plugins.Keyboard.hideKeyboardAccessoryBar(false);
			cordova.plugins.Keyboard.disableScroll(true);

		}
		if (window.StatusBar) {
			// org.apache.cordova.statusbar required
			StatusBar.styleDefault();
			ionic.Platform.fullScreen();
			ionic.Platform.showStatusBar(false);
		}
		if(CONFIG.isProduction) {
	    if(window.console && window.console.log){
	      window.console.log = function() {};
	    }
	    if(window.alert){
	      window.alert = function() {};
	    }
  	}
	});
}]);

neuroScienceApp.config(["$kinveyProvider","CONFIG", function($kinveyProvider,CONFIG){
	$kinveyProvider.init({
		apiHostname: CONFIG.kinveyCredentials.apiHostname,
		appKey: CONFIG.kinveyCredentials.appKey,
		appSecret: CONFIG.kinveyCredentials.appSecret
	});
}]);
neuroScienceApp.config(["$stateProvider", "$urlRouterProvider",'$ionicConfigProvider', function($stateProvider,$urlRouterProvider,$ionicConfigProvider){

	$ionicConfigProvider.views.swipeBackEnabled(false);

  $stateProvider
	
	.state('login', {
		url: '/login',
		cache:false,
		templateUrl: 'templates/authenticationModules/login.html',
		controller: 'loginCtrl'
	})
	.state('registration', {
		url: '/registration',
		cache:false,
		abstract:true,
		templateUrl: 'templates/authenticationModules/registration.html',
		controller: 'registrationCtrl'
	})
	// Personal Information 1
	.state('registration.page1', {
		url: '/page1',
		cache:false,
		templateUrl: 'templates/authenticationModules/registrationModule/page1.html',
		controller: 'registrationPage1Ctrl'
	})
	.state('registration.page2', {
		url: '/page2',
		cache:false,
		templateUrl: 'templates/authenticationModules/registrationModule/page2.html',
		controller: 'registrationPage2Ctrl'
	})
	.state('registration.page3', {
		url: '/page3',
		cache:false,
		templateUrl: 'templates/authenticationModules/registrationModule/page3.html',
		controller: 'registrationPage3Ctrl'
	})
	.state('registration.page4', {
		url: '/page4',
		cache:false,
		templateUrl: 'templates/authenticationModules/registrationModule/page4.html',
		controller: 'registrationPage4Ctrl'
	})
	.state('registration.page5', {
		url: '/page5',
		cache:false,
		templateUrl: 'templates/authenticationModules/registrationModule/page5.html',
		controller: 'registrationPage5Ctrl'
	})
	.state('registration.page6', {
		url: '/page6',
		cache:false,
		templateUrl: 'templates/authenticationModules/registrationModule/page6.html',
		controller: 'registrationPage6Ctrl'
	})

	.state('forgotPassword', {
		url: '/forgotPassword',
		cache:false,
		templateUrl: 'templates/authenticationModules/forgotPassword.html',
		controller: 'forgotPasswordCtrl'
	})
	.state('about', {
		url: '/about',
		cache:false,
		templateUrl: 'templates/authenticationModules/about.html',
		controller: 'aboutCtrl'
	})
	.state('tnc', {
		url: '/termsAndConditions',
		cache:false,
		templateUrl: 'templates/authenticationModules/tnc.html',
		controller: 'tncCtrl'
	})
	.state('privacyPolicy', {
		url: '/privacyPolicy',
		cache:false,
		templateUrl: 'templates/authenticationModules/privacyPolicy.html',
		controller: 'privacyPolicyCtrl'
	})
	.state('contactUsLogin', {
		url: '/contactUsLogin',
		cache:false,
		templateUrl: 'templates/authenticationModules/contactUsLogin.html',
		controller: 'contactUsLoginCtrl'
	})
	.state('faqLogin', {
		url: '/faqLogin',
		cache:false,
		templateUrl: 'templates/authenticationModules/faqLogin.html',
		controller: 'faqLoginCtrl'
	})
	.state('app', {
		url: '/app',
		abstract: true,
		cache:false,
		templateUrl: 'templates/app.html',
		controller: 'appCtrl'
	})
	.state('app.home', {
		url: '/home',
		cache:false,
		views: {
			'menuContent': {
				templateUrl: 'templates/homeScreenModules/homeScreen.html',
				controller: 'homeCtrl'
			}
		}
	})
	.state('app.preHome', {
		url: '/preHome',
		cache:false,
		views: {
			'menuContent': {
				templateUrl: 'templates/homeScreenModules/preHomeScreen.html',
				controller: 'preHomeCtrl'
			}
		}
	})
	.state('app.surveysLandingPage', {
		url: '/surveysLandingPage',
		cache:false,
		views: {
			'menuContent': {
				templateUrl: 'templates/surveyModules/surveysLandingPage.html',
				controller: 'surveysLandingPageCtrl'
			}
		} 
	})
	.state('app.surveySubmissionConfirmation', {
		url: '/surveySubmissionConfirmation',
		cache:false,
		views: {
			'menuContent': {
				templateUrl: 'templates/surveyModules/surveySubmissionConfirmation.html',
				controller: 'surveySubmissionConfirmationCtrl'
			}
		} 
	})
	.state('app.surveys', {
		url: '/surveys',
		params:{surveyInfo:null},
		cache:false,
		views: {
			'menuContent': {
				templateUrl: 'templates/surveyModules/surveys.html',
				controller: 'surveysCtrl'
			}
		} 
	})
	.state('app.healthTrackerLandingPage', {
		url: '/healthTrackerLandingPage',
		cache:false,
		views: {
			'menuContent': {
				templateUrl: 'templates/healthTrackerModules/landingPage.html',
				controller: 'healthTrackerLandingPageCtrl'
			}
		}
	})
	.state('app.viewPastHealthTracker', {
		url: '/viewPastHealthTracker',
		cache:false,
		views: {
			'menuContent': {
				templateUrl: 'templates/healthTrackerModules/viewPastHealthTracker.html',
				controller: 'viewPastHealthTrackerCtrl'
			}
		}
	})
	.state('app.healthTrackerLandingPage.generalWellbeing', {
		url: '/generalWellbeing',
		cache:false,
		views: {
			'healthtracker': {
				templateUrl: 'templates/healthTrackerModules/generalWellbeing.html',
				controller: 'healthTrackerGeneralWellbeingCtrl'
			}
		}
	})
	.state('app.healthTrackerLandingPage.physicalSymptoms', {
		url: '/physicalSymptoms',
		cache:false,
		views: {
			'healthtracker': {
				templateUrl: 'templates/healthTrackerModules/physicalSymptoms.html',
				controller: 'healthTrackerPhysicalSymptomsCtrl'
			}
		}
	})
	.state('app.healthTrackerLandingPage.mindMatters', {
		url: '/mindMatters',
		cache:false,
		views: {
			'healthtracker': {
				templateUrl: 'templates/healthTrackerModules/mindMatters.html',
				controller: 'healthTrackerMindMattersCtrl'
			}
		}
	})
	.state('app.healthTrackerLandingPage.dailyLifeSymptoms', {
		url: '/dailyLifeSymptoms',
		cache:false,
		views: {
			'healthtracker': {
				templateUrl: 'templates/healthTrackerModules/dailyLifeSymptoms.html',
				controller: 'healthTrackerDailyLifeSymptomsCtrl'
			}
		}
	})
	.state('app.performanceEvals', {
		url: '/performanceEvals',
		cache:false,
		views: {
			'menuContent': {
				templateUrl: 'templates/performanceTestModules/performanceEvals.html',
				controller: 'performanceEvaluationCtrl'
			}
		}
	})

	.state('app.performanceEvals.singlePerformanceResult', {
		url: '/singlePerformanceResult',
		cache:false,
		views: {
			'performanceResult': {
				templateUrl: 'templates/performanceTestModules/singlePerformanceResult.html',
				controller: 'singlePerformanceResultCtrl'
			}
		}
	})
	.state('app.heatAlerts', {
		url: '/heatAlerts',
		cache:false,
		views: {
			'menuContent': {
				templateUrl: 'templates/heatAlertsModules/heatAlerts.html',
				controller: 'heatAlertsCtrl'
			}
		}
	})
	.state('app.profileSettings', {
		url: '/profileSettings',
		cache:false,
		resolve: {
			profileData: ['$q','$kinvey', function($q,$kinvey) {
				var deferred = $q.defer();
				var promise = new $q(function(resolve) {
		      resolve($kinvey.User.getActiveUser().me());
		    });
		    promise.then(function(user) {
		    	return user;
		    }).then(function(user) {
		    	deferred.resolve(user.data);
		    }).catch(function(error) {
		      deferred.reject(error);
		    });
		    return deferred.promise;
			}]
		},
		views: {
			'menuContent': {
				templateUrl: 'templates/profileSettingsModules/profileSettings.html',
				controller: 'profileSettingsCtrl'
			}
		}
	})
	.state('app.profileSettings.profilePersonalInfo', {
		url: '/profilePersonalInfo',
		cache:false,
		views: {
			'profileSettings': {
				templateUrl: 'templates/profileSettingsModules/profilePersonalInfo.html',
				controller: 'profilePersonalInfoCtrl'
			}
		}
	})
	.state('app.profileSettings.profileMsHistory', {
		url: '/profileMsHistory',
		cache:false,
		views: {
			'profileSettings': {
				templateUrl: 'templates/profileSettingsModules/profileMsHistory.html',
				controller: 'profileMsHistoryCtrl'
			}
		}
	})
	.state('app.profileSettings.profileTreatmentHistory', {
		url: '/profileTreatmentHistory',
		cache:false,
		views: {
			'profileSettings': {
				templateUrl: 'templates/profileSettingsModules/profileTreatmentHistory.html',
				controller: 'profileTreatmentHistoryCtrl'
			}
		}
	})
	.state('app.profileSettings.profileSelectPhysician', {
		url: '/profileSelectPhysician',
		cache:false,
		views: {
			'profileSettings': {
				templateUrl: 'templates/profileSettingsModules/profileSelectPhysician.html',
				controller: 'profileSelectPhysicianCtrl'
			}
		}
	})
	.state('app.profileSettings.profileSettingsTab', {
		url: '/profileSettingsTab',
		cache:false,
		views: {
			'profileSettings': {
				templateUrl: 'templates/profileSettingsModules/profileSettingsTab.html',
				controller: 'profileSettingsTabCtrl'
			}
		}
	})
	.state('app.contactUs', {
		url: '/contactUs',
		cache:false,
		views: {
			'menuContent': {
				templateUrl: 'templates/contactUsModules/contactUs.html',
				controller: 'contactUsCtrl'
			}
		}
	})
	.state('app.tips', {
		url: '/tips',
		cache:false,
		views: {
			'menuContent': {
				templateUrl: 'templates/tipsModules/tips.html',
				controller: 'tipsCtrl'
			}
		}
	})
	.state('app.faq', {
		url: '/faq',
		cache:false,
		views: {
			'menuContent': {
				templateUrl: 'templates/faqModules/faq.html',
				controller: 'faqCtrl'
			}
		}
	})
	.state('app.aboutCareConnect', {
		url: '/aboutCareConnect',
		cache:false,
		views: {
			'menuContent': {
				templateUrl: 'templates/aboutCareConnectModules/aboutCareConnect.html',
				controller: 'aboutCareConnectCtrl'
			}
		}
	})
	.state('app.privacyPolicy', {
		url: '/privacyPolicy',
		cache:false,
		views: {
			'menuContent': {
				templateUrl: 'templates/miscellaneousModules/privacyPolicy.html',
				controller: 'privacyPolicyPageCtrl'
			}
		}
	})
	.state('app.tnc', {
		url: '/tnc',
		cache:false,
		views: {
			'menuContent': {
				templateUrl: 'templates/miscellaneousModules/tnc.html',
				controller: 'tncPageCtrl'
			}
		}
	})
	.state('app.reactionTest', {
		url: '/reactionTest',
		params: {test: null},
		cache:false,
		views: {
			'menuContent': {
				templateUrl: 'templates/performanceTestModules/reactionTest.html',
				controller: 'reactionTestCtrl'
			}
		}
	})

	.state('app.walkTest', {
		url: '/walkTest',
		params: {test: null},
		views: {
		  'menuContent': {
			templateUrl: 'templates/performanceTestModules/walkTest.html',
			controller: 'walkTestCtrl'
		  }
		}
	})

	.state('app.ishiharaTest', {
		url: "/ishiharaTest",
		params: {test: null},
		cache:false,
		views: {
		  'menuContent': {
			templateUrl: "templates/performanceTestModules/ishiharaTest.html",
			controller: 'ishiharaTestCtrl'
		  }
		}
	})

	.state('app.gonogo', {
		url: '/gonogo',
		cache:false,
		views: {
			'menuContent': {
				templateUrl: 'templates/performanceTestModules/go-nogo.html',
				controller: 'gonogoCtrl'
			}
		}
	})

	.state('app.symbolDigit', {
		url: '/symbolDigit',
		params: {test: null},
		cache:false,
		views: {
			'menuContent': {
				templateUrl: 'templates/performanceTestModules/symbolDigit.html',
				controller: 'symbolDigitCtrl'
			}
		}
	})

	.state('app.fingerTap', {
		url: '/fingerTap',
		params: {test: null},
		cache:false,
		views: {
			'menuContent': {
				templateUrl: 'templates/performanceTestModules/fingerTap.html',
				controller: 'fingerTapCtrl'
			}
		}
	})

	.state('app.trailTest', {
		url: '/trailTest',
		params: {test: null},
		cache:false,
		views: {
			'menuContent': {
				templateUrl: 'templates/performanceTestModules/trailTest.html',
				controller: 'trailTestCtrl'
			}
		}
	})
	.state('app.towerOfHanoi', {
		url: '/towerOfHanoi',
		params: {test: null},
		cache:false,
		views: {
			'menuContent': {
				templateUrl: 'templates/performanceTestModules/towerOfHanoi.html',
				controller: 'towerOfHanoiCtrl'
			}
		}
	})
	.state('app.languageMemoryTest', {
		url: '/languageMemoryTest',
		params: {test: null},
		cache:false,
		views: {
			'menuContent': {
				templateUrl: 'templates/performanceTestModules/languageMemoryTest.html',
				controller: 'languageMemoryTestCtrl'
			}
		}
	})
	/*  .state('app.bodycare', {
		url: '/bodycare',
		cache:false,
		views: {
			'menuContent': {
			templateUrl: 'templates/bodyCare.html',
			controller: 'bodyCareCtrl'
			}
		}
	})*/ 
	;
	// if none of the above states are matched, use this as the fallback
	//$urlRouterProvider.otherwise('/home');
}]);