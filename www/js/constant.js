neuroScienceApp.constant('CONSTANT', {
	"authentication":false,
	"loginErrorCode":{
		'required' : "This information is required.",
		'wrongEmail': "Sorry, email address or password is not recognized. Please try again.",
		'wrongPattern' : "Sorry, this email address is not recognized."
	},
	"gamificationPoints":{
		"completeRegistration":{"value":15,"name":"Complete Registration"},
		"activateWeatherAlerts":{"value":20,"name":"Activate Weather Alerts"},
		"fullPMOneWeek":{"value":30,"name":"Full PM Battery"},
		"healthTracker":{"value":10,"name":"Daily Health Tracker"},
		"proSurvey":{"value":25,"name":"Survey"},
		"pmEvaluation":{"value":25,"name":"PM Evaluation"},
		"viewTips":{"value":5,"name":"View Tip"}
	},
	"internalGlitch": "Sorry, an error has occurred. Please try again or come back later.",
	"userInfoInvalid":"An error occurred. Please contact us at info@mscareconnect.com.",
	"googlePlayURL":"https://play.google.com/store/apps/details?id=com.interpro.mscareconnect&hl=en",
	"appStoreURL":"https://itunes.apple.com/us/app/ms-care-connect/id1179230419?mt=8",
	"appVersion":208
});