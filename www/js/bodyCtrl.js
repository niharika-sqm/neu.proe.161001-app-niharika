neuroScienceApp.controller('bodyCtrl', ['$scope', '$state', '$kinvey', '$q', '$ionicLoading', 'CONSTANT', '$ionicHistory', '$timeout', '$ionicPopup', '$ionicPlatform', '$ionicScrollDelegate', 'CONFIG', 'utility', '$ionicModal', 'closePopupService', function($scope, $state, $kinvey, $q, $ionicLoading, CONSTANT, $ionicHistory, $timeout, $ionicPopup, $ionicPlatform, $ionicScrollDelegate, CONFIG, utility, $ionicModal, closePopupService) {

  $scope.setRelapsePopupVariable = function() {
    $scope.relapseTrack = { "feedback": { "effective": "", "treatment": "" }, "startRelapse": false, "error": false, "errorEffective": false, "errorTreatment": false, "relapseInfection": "", "kinveyData": {}, "errorClass": "error" };
    $scope.relapseStopPopup = { "popup1Title": "Did you return to your former state of health without any residual relapse symptoms?", "popup1ScreenType": "effective", "popup2Title": "Did you receive any treatment for your relapse?", "popup2ScreenType": "treatment" };
  }

  $scope.setRelapseEndPopup = function() {
    $scope.relapseImprovementOptions = [{ id: 1, text: 1, checked: false }, { id: 2, text: 2, checked: false }, { id: 3, text: 3, checked: false }, { id: 4, text: 4, checked: false }, { id: 5, text: 5, checked: false }];
    $scope.relapseTreatmentOptions = [{ id: 1, text: "IV steroids", checked: false }, { id: 2, text: "Oral steroid tablets only", checked: false }, { id: 3, text: "Oral steroid tablets after IV steroids", checked: false }, { id: 4, text: "Acthar injections", checked: false }, { id: 5, text: "Plasma exchange", checked: false }, { id: 6, text: "No treatment", checked: false }];
  }

  $scope.homeScreen = {"loaderWidth":50};

  $scope.islogout = false;
  $scope.disableBodyOnPopup = false;

  $scope.$on('disbaleBG',function(){
    $scope.disableBodyOnPopup = true;
  });

  $scope.$on('enableBG',function(){
    $scope.disableBodyOnPopup = false;
  });

  $scope.resetTreatmentUpdate = function(){
    $scope.newMSMedication = [{id:1, value:'Aubagio', checked:false, text:'Aubagio<sup>®</sup> (teriflunomide)' },{id:2, value:'Avonex', checked:false, text:'Avonex<sup>®</sup> (interferon beta-1a)'},{id:3, value:'Betaseron', checked:false, text:'Betaseron<sup>®</sup> (interferon beta-1b)'},{id:4, value:'Copaxone', checked:false, text:'Copaxone<sup>®</sup> (glatiramer acetate injection)'},{id:5, value:'Extavia', checked:false, text:'Extavia<sup>®</sup> (interferon beta-1b)'},{id:6, value:'Gilenya', checked:false, text:'Gilenya<sup>®</sup> (fingolimod)'},{id:7, value:'Lemtrada', checked:false, text:'Lemtrada<sup>®</sup> (alemtuzumab)'},{id:15, value:'Ocrevus', checked:false, text:'Ocrevus<sup>®</sup> (ocrelizumab)'},{id:8, value:"Plegridy", checked:false, text:'Plegridy<sup>®</sup> (peginterferon beta-1a)'},{id:9, value:'Rebif', checked:false, text:'Rebif<sup>®</sup> (interferon beta-1a)'},{id:10, value:'Rituxan', checked:false, text:'Rituxan<sup>®</sup> (rituximab)'},{id:11, value:'Tecfidera', checked:false, text:'Tecfidera<sup>®</sup> (dimethyl fumarate)'},{id:12, value:'Tysabri', checked:false, text:'Tysabri<sup>®</sup> (natalizumab)'},{id:13, value:'Zinbryta', checked:false, text:'Zinbryta™ (daclizumab)'}];

  $scope.reasonToStopMedication = [{ id: 1, text: "Failed to relieve my symptoms", checked: false }, { id: 2, text: "Safety concerns", checked: false }, { id: 3, text: "Too many side effects", checked: false }, { id: 4, text: "Insurance/Cost", checked: false }, { id: 5, text: "Inconvenience", checked: false }];

  $scope.treatmentUpdatePopupVariable = { "popup1Title": "New MS Medication", "popup1ScreenType": "newMSMedication", "popup2Title": "Why did you stop?", "popup2ScreenType": "reasonToStopMedication" };
  $scope.treatment = { "newMSMedication": "", "newMSMedicationError": false, "reasonToStopMedication": "", "reasonToStopMedicationError": false }
  }

  $scope.setRelapsePopupVariable();
  $scope.setRelapseEndPopup();
  $scope.resetTreatmentUpdate();
  $scope.showAndroidNotification = false;
  $scope.showIosNotification = false;
  $scope.showPasswordWorng = false;
  $scope.showPasswordRequired = false;
  $scope.healthTackerProperty = {"isHealthTackerTakenToday":undefined};
  $scope.homeScreenCount = { "PM": 0, "Survey": 0};
  var isScreenLock = false;
  var pointsCollection = $kinvey.DataStore.getInstance('gamificationPointsList', $kinvey.DataStoreType.Network);
  var relapseList = $kinvey.DataStore.getInstance('relapseList', $kinvey.DataStoreType.Network);
  var trackerAnswerCollection = $kinvey.DataStore.getInstance('healthTrackerAnswers', $kinvey.DataStoreType.Network);

  $scope.goToState = function(state,category,action,label) {
    if(category != undefined && action != undefined && label != undefined){
      if (typeof window.ga !== "undefined") {
          window.ga.trackEvent(category, action, label, 1);
        }
    }
    if ($state.current.name == "app.healthTrackerLandingPage.physicalSymptoms" || $state.current.name == "app.healthTrackerLandingPage.mindMatters" || $state.current.name == "app.healthTrackerLandingPage.generalWellbeing" || $state.current.name == "app.healthTrackerLandingPage.dailyLifeSymptoms") {
      $state.go('app.healthTrackerLandingPage');
      $timeout(function() {
        /*JVI pre home page implementation*/
        if(state =="app.home"){
          $scope.navigateToHome();
        }else{
          $state.go(state);
        }
      }, 500);
    } else if ($state.current.name == "app.profileSettings.profilePersonalInfo" || $state.current.name == "app.profileSettings.profileMsHistory" || $state.current.name == "app.profileSettings.profileTreatmentHistory" || $state.current.name == "app.profileSettings.profileSelectPhysician" || $state.current.name == "app.profileSettings.profileSettingsTab") {
      $state.go('app.profileSettings');
      $timeout(function() {
        /*JVI pre home page implementation*/
        if(state =="app.home"){
          $scope.navigateToHome();
        }else{
          $state.go(state);
        }
      }, 500);
    } else {
      /*JVI pre home page implementation*/
      if(state =="app.home"){
        $scope.navigateToHome();
      }else{
        $state.go(state);
      }
    }
  }

  $scope.hideAccountAccessPopupValidation = function() {
    $scope.showPasswordWorng = false;
    $scope.showPasswordRequired = false;
  }

  $scope.accountAccess = {'username':"", 'password':"", "type":"checkPassword"};
  $scope.openUserSettingPopup = function(state) {
    if ($state.current.name.indexOf("app.profileSettings") > -1) {
      $state.go('app.profileSettings');
      return
    }
    $scope.accountAccess.password = '';
    $scope.hideAccountAccessPopupValidation();
    $ionicModal.fromTemplateUrl('templates/genericTemplate/userSettingPopup.html', {
      scope: $scope,
      animation: 'modal-scale-animation'
    }).then(function(modal) {
      $scope.userSettingModal = modal;
      $scope.userSettingModal.show();
    });

    $scope.closeAccountAccessPopup = function() {
      $scope.userSettingModal.remove();
      if(!$kinvey.User.getActiveUser() || !$kinvey.User.getActiveUser().email){
        $scope.userSettingModal.remove();
        $state.go('login');
        $scope.hideLoader();
        $scope.linkDisabled = "";
      }
    }

    var userEmailAddress = "";
    $scope.callForgetPassword = function() {
      $scope.showLoader();
      if(!userEmailAddress){
        userEmailAddress = $kinvey.User.getActiveUser().email;
      }
      var promise = $kinvey.User.resetPassword(userEmailAddress);
      promise.then(function(response) {
        if (typeof window.ga !== "undefined") {
          window.ga.trackEvent('Forgot Password', 'Password Reset', 'Request Passowrd Reset', 1);
        }
        $scope.accountAccess.password = '';
        $scope.showPasswordWorng = false;
        $scope.hideLoader();
        $scope.showMessage('Reset password link has been sent successfully.',3000);
      }, function(err) {
        $scope.hideLoader();
        console.log(err)
      });
    }

    function userLogin(){
      var promise = $kinvey.User.login({
        username: userEmailAddress,
        password: $scope.accountAccess.password
      });
      promise.then(function(user) {
        continueCheckAccountAccessPopup();
      }).catch(function(error) {
        errorOnCheckAccountAccessPopup(error);
      });
    }

    $scope.checkAccountAccessPopup = function() {
      if ($scope.accountAccess.password == '' || $scope.accountAccess.password == undefined) {
        $scope.showPasswordRequired = true;
        return;
      }
      $scope.showLoader();
      if(userEmailAddress){
        if($kinvey.User && $kinvey.User.getActiveUser()){
          var promise = $kinvey.User.getActiveUser().logout();
          promise = promise.then(function onSuccess() {
            userLogin();
          }).catch(function onError(error) {
            userLogin();
          });
        }else{
          userLogin();
        }
      }else{
        $scope.accountAccess.username = $kinvey.User.getActiveUser().username;
        var checkPasswordBL = $kinvey.CustomEndpoint.execute('resetPassword', $scope.accountAccess);
        checkPasswordBL.then(function(data) {
          continueCheckAccountAccessPopup();
        }).catch(function(error) {
          errorOnCheckAccountAccessPopup(error);
        });
      }
  }

  function continueCheckAccountAccessPopup(){
    $scope.userSettingModal.remove();
    if ($state.current.name == "app.healthTrackerLandingPage.physicalSymptoms" || $state.current.name == "app.healthTrackerLandingPage.mindMatters" || $state.current.name == "app.healthTrackerLandingPage.generalWellbeing" || $state.current.name == "app.healthTrackerLandingPage.dailyLifeSymptoms") {
      $state.go('app.healthTrackerLandingPage');
      $timeout(function() {
        if (state) {
          $state.go(state); // to treatment page
        } else {
          $state.go('app.profileSettings');
        }
      }, 1000);
    } else {
      if (state) {
        $state.go(state);
      } else {
        $state.go('app.profileSettings');
      }
    }
  }

  function errorOnCheckAccountAccessPopup(error){
    if(error.code == 0){
      $scope.internalGlitch();
    }
    else{
      $scope.hideLoader();
      $scope.showPasswordWorng = true;
    }
  }
}

  $scope.lockOrientation = function(screenType) {
    if (ionic.Platform.isWebView() && !isScreenLock) {
      console.log(screen.orientation);
      $timeout(function() {
        isScreenLock = true;
        window.screen.lockOrientation('portrait-primary');
        console.log("lock");
      }, 2000);
    }
  }
  $scope.unlockOrientation = function() {
    if (ionic.Platform.isWebView() && isScreenLock) {
      console.log(screen.orientation);
      $timeout(function() {
        isScreenLock = false;
        window.screen.unlockOrientation();
        console.log("unlock");
      }, 2000);
    }
  }

  $scope.showMessage = function(message, time) {
    if (!time) {
      time = "long";
    }
    window.plugins.toast.showWithOptions({
        message: message,
        duration: time,
        position: "bottom",
        addPixelsY: -20
      },
      function() {
        console.log('sucess');
      },
      function() {
        console.log('error');
      });
  }


  $scope.internalGlitch = function(state) {
    /*$scope.internalGlitch = $ionicPopup.show({
      title: CONSTANT.internalGlitch,
      template: '<a class="popClose" ng-click="logoutPopup.close()"><span></span><span></span></a>',
      scope: $scope,
      buttons: [{
        text: 'OKAY',
        type: 'button-block button-outline button-darkPurple font40',
        onTap: function() {
          return
        }
      }]
    });
    $scope.internalGlitch.then(function(res) {
      console.log("Internal Glitch");
    });*/
    console.log("hide Glitch");
    $scope.hideLoader();
    if(!$scope.islogout){
      console.log("show Glitch");
      $scope.showMessage(CONSTANT.internalGlitch, 3000);
    }
  }

  $scope.infoEmptyToast = function() {
    console.log("hide Glitch");
    $scope.hideLoader();
    if(!$scope.islogout){
      console.log("show Glitch");
      $scope.showMessage(CONSTANT.userInfoInvalid, 5000);
    }
  }

  $scope.showLoader = function(duration) {
    $ionicLoading.show({
      animation: 'fade-in',
      showBackdrop: false,
      // template: '<ion-spinner icon="android" class="spinner-askmonk"></ion-spinner>'
      template: '<div class="spinner"><img src="img/loader.svg" alt="Loader" style="width:60px;height:60px;"></div>'
    });
  }

  $scope.hideLoader = function() {
    $ionicLoading.hide();

    function success(status) {};

    function error(status) {};
    if (CONFIG.isProduction) {
      window.cache.clear(success, error);
    }
  };

  $scope.goBack = function() {
    $ionicHistory.goBack();
  }

  var performanceTestResultsForUnSaveData = $kinvey.DataStore.getInstance('performanceTestResults', $kinvey.DataStoreType.Network);
  $scope.savePMunSavedData = function() {
    var unSavedPMData = localStorage.getItem("unSavedPMData");
    if (unSavedPMData != undefined && unSavedPMData != null) {
      performanceTestResultsForUnSaveData.save(JSON.parse(unSavedPMData)).then(function(entity) {
        console.log("entity " + entity);
        var unSavedPMPoints = localStorage.getItem("unSavedPMPoints");
        if (unSavedPMPoints) {
          $scope.$emit('saveGamificationPoints', CONSTANT.gamificationPoints.pmEvaluation.value, CONSTANT.gamificationPoints.pmEvaluation.name);
        }
        //$scope.hideLoader();
        localStorage.removeItem("unSavedPMData");
        localStorage.removeItem("unSavedPMPoints");
      }).catch(function(error) {
        localStorage.removeItem("unSavedPMData");
        localStorage.removeItem("unSavedPMPoints");
        $scope.internalGlitch();
      });
    }
    var unSavedPMData2 = localStorage.getItem("unSavedPMDataTrail");
    if (unSavedPMData != undefined && unSavedPMData != null) {
      performanceTestResultsForUnSaveData.save(JSON.parse(unSavedPMData)).then(function(entity) {
        console.log("entity " + entity);
        //$scope.hideLoader();
        localStorage.removeItem("unSavedPMDataTrail");
      }).catch(function(error) {
        localStorage.removeItem("unSavedPMDataTrail");
        $scope.internalGlitch();
      });
    }
  }

  $scope.checkAuthentication = function(isPreHomeScreen) {
    // HAI : Check the authentication of the application and decides the states
    var promise = new $q(function(resolve) {
      resolve($kinvey.User.getActiveUser());
    });
    promise.then(function(user) {
      if (user) {
        return user.me();
      }
    }).then(function(user) {
      if (user) {
        // console.log(user);
        user = user.data;

        // Case 1 for user info is in the app and it is updated fully redirect to home page
        if (user.info) {
          $scope.getRelapseEntryData(user._id);
          if(isPreHomeScreen){
            $state.go("app.preHome");
          }else if(user.changeMap && user.changeMap.preHomePage){
            $state.go("app.home");
          }else{
            /* notification number is mandartory to check first to decide weather to show prehome or home */
            $scope.showLoader();
            $scope.getPMSurveyNotificationNumber(true);
          }
          //$state.go("app.fingerTap");
        } else {
          // Case 2 When user is verified its email but info is incomplete redirect to page 2
          //$state.go("registration.page6");
          $state.go("registration.page2");
        }
      } else {
        $timeout(function() {
          // Case 3 When there is no user Default case redirect to login.
          $state.go("login");
        }, 100);
      }
    }).catch(function(error) {
      if (error && error.message && error.message.indexOf("app requires email address verification") > -1) {
        // Case 4 When user is not verified its email address redirect to page 1 with email verification.
        localStorage.setItem('verification', false);
        $state.go("registration.page1");
      }else if(error && error.message && error.message.indexOf("Invalid credentials") > -1){
        $scope.logoutUser();
      }else if(error.name == "KinveyError" && error.code == 503){ //Need to change the condidtion with actual error code.
        $scope.hideLoader();
        $scope.showAppUpdatePopup();
      }else{
        $scope.logoutUser();
      }
    });
  }
  $scope.checkAuthentication();

  $scope.logoutUser = function(user){
    var promise = new $q(function(resolve) {
      resolve($kinvey.User.getActiveUser());
    });
    promise.then(function(user) {
      if (user) {
        return user.logout();
      }
      return user;
    }).then(function() {
      $ionicHistory.clearCache();
      $ionicHistory.clearHistory();
      if(localStorage.verification){
        localStorage.removeItem('verification');
      }
      $state.go("login");
    }).catch(function(error) {
      $ionicHistory.clearCache();
      $ionicHistory.clearHistory();
      if(localStorage.verification){
        localStorage.removeItem('verification');
      }
      $state.go("login");
    });
  }
  /*
    $scope.updateUUID = function(){
      var usersDS = new $kinvey.UserStore();
      var user = $kinvey.User.getActiveUser();
      if(user){
        user.data.deviceUUID = device.uuid;
        usersDS.update(user.data)
        .then(function(user){
          console.log("success uuid")
          console.log(user);
        }).catch(function(error){
          console.log(error);
          console.log("error uuid")

        });
      }
    }*/

  //get number of test and survey
  $scope.getPMSurveyNotificationNumber = function(fromCheckAuth) {
    var user = $kinvey.User.getActiveUser();
    var promise = $kinvey.CustomEndpoint.execute('getNotificationCount', { "userId": user.data._id,"type":"getCount"});
    promise.then(function(response) {
      console.log(response);
      $scope.homeScreenCount = response;
      if(CONSTANT.appVersion < $scope.homeScreenCount.appVersion){
        if($scope.homeScreenCount.forceUpdate == true){
          $scope.forceUpdatePopup();
        }else{
          $scope.appVersionPopup("Please update the app in order to access new features");
        }
        $scope.hideLoader();
      }
      if(fromCheckAuth){
        $scope.healthTrackerCount();
      }else{
        $scope.$broadcast('updatePreHome',response);
        $scope.$apply();
      }
    }).catch(function(err) {
      console.log(err);
    });
  }

  //set number of PM
  $scope.setPMNotificationIsShown = function(notificationId) {
    var user = $kinvey.User.getActiveUser();
    var promise = $kinvey.CustomEndpoint.execute('getNotificationCount', { "userId": user.data._id,"notificationId":notificationId,"type":"changePMCount"});
    promise.then(function(response) {
      console.log(response);
    }).catch(function(err) {
      console.log(err);
    });
  }

  $scope.logout = function(state) {
    $scope.logoutPopup = $ionicPopup.show({
      title: "Are you sure you want to log out?",
      template: '<a class="popClose" ng-click="logoutPopup.close()"><span></span><span></span></a>',
      scope: $scope,
      buttons: [{
        text: 'Cancel',
        type: 'button-block button-outline button-darkPurple',
        onTap: function() {
          return false;
        }
      }, {
        text: 'log out',
        type: 'button-block button-block button-darkPurple bold',
        onTap: function() {
          return true;
        }
      }]
    });
    closePopupService.register($scope.logoutPopup);
    $scope.logoutPopup.then(function(res) {
      if (res) {
        $scope.deRegisterNotification();
        $scope.showLoader();
        var promise = new $q(function(resolve) {
          resolve($kinvey.User.getActiveUser().me());
        });
        promise.then(function(user) {
          $scope.islogout = true;
          $state.go('app.home');
          if (user) {
            return user.logout();
          }
          return user;
        }).then(function() {
          console.log("user logout success");
          $ionicHistory.clearCache();
          $ionicHistory.clearHistory();
          localStorage.removeItem('verification');
          $scope.healthTackerProperty = {"isHealthTackerTakenToday":undefined};
          $scope.homeScreenCount = { "PM": 0, "Survey": 0};
          $scope.hideLoader();
          $timeout(function() {
            $scope.islogout = false;
          }, 1000);
          $state.go(state);
        }).catch(function(error) {
          console.log(error);
          $scope.hideLoader();
          $scope.showMessage('Sorry, your device is offline. Please confirm a connection and try again.', 3000);
          //$state.go(state);
        });
      }
    });
  }

  $scope.multiSelection = [];
  $scope.singleSelection = '';
  $scope.toggleSelection = function(options, type, popupType) {
    if (popupType == "multi") {
      if ($scope.multiSelection.length == 0) {
        $scope.multiSelection.push(options);
      } else {
        var i = 0;
        for (i = 0; i < $scope.multiSelection.length; i++) {
          if (options.id == $scope.multiSelection[i].id) {
            break;
          }
        }
        if (i == $scope.multiSelection.length) {
          $scope.multiSelection.push(options);
        } else {
          $scope.multiSelection.splice(i, 1);
        }
      }
      /*var idx = $scope.multiSelection.indexOf(options);

      if (idx > -1) { // is currently selected
        $scope.multiSelection.splice(idx, 1);
      }else { // is newly selected
        $scope.multiSelection.push(options);
      }*/
    } else {
      if (!options.checked) {
        options.checked = true;
      }
      $scope.singleSelection = options;
    }
  };

  $scope.$on('emptyMultiSelectedArray', function() {
    $scope.multiSelection = [];
  });
  $scope.$on('updateMultiSelectedArray', function(event, data) {
    $scope.multiSelection = angular.copy(data);
  });
  $scope.$on('emptySingleSelectedArray', function() {
    $scope.singleSelection = '';
  });
  $scope.$on('updateSingleSelectedArray', function(event, data) {
    $scope.singleSelection = angular.copy(data);
  });
  //Relapse Start


  $scope.$on('refreshRelapseData', function(event, data) {
    user = $kinvey.User.getActiveUser()
    $scope.getRelapseEntryData(user._id);
  });

  $scope.getRelapseEntryData = function(userId) {
    var col = "_acl";
    var relatedJson = { "creator": userId };
    var query = new $kinvey.Query();
    query.equalTo(col, relatedJson);
    col = "active";
    var query1 = new $kinvey.Query();
    query1.equalTo(col, 1);
    query.and(query1);
    relapseList.find(query).subscribe(function(data) {
      if (data.length > 0) {
        $scope.relapseTrack.startRelapse = true;
        $scope.relapseTrack.kinveyData = data[0];
        $scope.replaceDays = moment().diff(moment($scope.relapseTrack.kinveyData.startDate), 'days') + 1;
        console.log($scope.replaceDays);
      } else {
        $scope.relapseTrack.startRelapse = false;
      }
    }, function(error) {}, function(data) {});
  }

  $scope.replaceTracking = function() {
    if (!$scope.relapseTrack.startRelapse) {
      $scope.startRelapse();
    } else {
      $scope.relapseTrack.feedback.treatment = "";
      $scope.relapseTrack.feedback.effective = "";
      $scope.stopRelapse();
    }
  }

  $scope.openSingleSelectRelapse = function(options, popupType, screenType, title, index, key) {
    $scope.typeSingleSelect = popupType;
    $scope.optionSingleSelect = options;
    $scope.screenTypeSingleSelect = screenType;
    $scope.index = index;
    $scope.key = key;
    var singleSelectPopup = "";
    if (popupType == "effective") {
      singleSelectPopup = $ionicPopup.show({
        title: title,
        subTitle: '(5=returned to how you felt before relapse;<br/> 1=no improvement)',
        templateUrl: 'templates/genericTemplate/singleSelectPopup.html',
        cssClass: "selectBoxPopup",
        scope: $scope,
        buttons: [{
          text: 'Cancel',
          type: 'button-block button-outline button-darkPurple font56',
          onTap: function() {
            return false;
          }
        }, {
          text: 'Okay',
          type: 'button-block button-outline button-darkPurple bold font56',
          onTap: function() {
            return true;
          }
        }]
      });
    } else {
      singleSelectPopup = $ionicPopup.show({
        title: title,
        templateUrl: 'templates/genericTemplate/singleSelectPopup.html',
        cssClass: "selectBoxPopup",
        scope: $scope,
        buttons: [{
          text: 'Cancel',
          type: 'button-block button-outline button-darkPurple font56',
          onTap: function() {
            return false;
          }
        }, {
          text: 'Okay',
          type: 'button-block button-outline button-darkPurple bold font56',
          onTap: function() {
            return true;
          }
        }]
      });
    }
    closePopupService.register(singleSelectPopup);
    singleSelectPopup.then(function(res) {
      console.log($scope.singleSelection, res);
      if (res) {
        if ($scope.typeSingleSelect == "effective") $scope.relapseTrack.errorEffective = false;
        else $scope.relapseTrack.errorTreatment = false;
        $scope.relapseTrack.feedback[$scope.screenTypeSingleSelect] = $scope.singleSelection;
        $scope.$emit('emptySingleSelectedArray');
      } else {
        $scope.$emit('emptySingleSelectedArray');
        for (var i = 0; i < $scope.optionSingleSelect.length; i++) {
          if ($scope.optionSingleSelect[i].checked == true) {
            $scope.optionSingleSelect[i].checked = false;
            break;
          }
        }
      }
    });
  }

  $scope.errorClass = "error";
  $scope.noerrorClass = "";
  $scope.stopRelapse = function() {
    $scope.relapseTrack.error = false;
    var select1 = '<div class="item item-input item-borderless no-padding">' +
      '<div class="selectBoxWrapper  item-floating-label">' +
      '<div class="selectBox" ng-class="relapseTrack.errorEffective ? errorClass:noerrorClass" ng-click="openSingleSelectRelapse(relapseImprovementOptions,relapseStopPopup.popup1ScreenType,relapseStopPopup.popup1ScreenType,relapseStopPopup.popup1Title)">' +
      '<div class="singleLine upperCase" ng-if="!relapseTrack.feedback.effective"> Choose one</div>' +
      '<div class="floatingSelect" ng-if="relapseTrack.feedback.effective"><span class="input-label upperCase font40">Choose one</span><div class="singleLine darkGray upperCase" >{{relapseTrack.feedback.effective.text}}</div></div>' +
      '</div></div></div><div ng-if="relapseTrack.errorEffective"><div class="height10"></div><p class="errorMsg text-center">Please select one.</p></div><div class="height20"></div>';
    var select2 = '<div class="item item-input item-borderless no-padding">' +
      '<div class="selectBoxWrapper  item-floating-label">' +
      '<div class="selectBox" ng-class="relapseTrack.errorTreatment ? errorClass:noerrorClass" ng-click="openSingleSelectRelapse(relapseTreatmentOptions,relapseStopPopup.popup2ScreenType,relapseStopPopup.popup2ScreenType,relapseStopPopup.popup2Title)">' +
      '<div class="singleLine upperCase" ng-if="!relapseTrack.feedback.treatment"> Choose one</div>' +
      '<div class="floatingSelect" ng-if="relapseTrack.feedback.treatment"><span class="input-label upperCase font40">Choose one</span><div class="singleLine darkGray upperCase" >{{relapseTrack.feedback.treatment.text}}</div></div>' +
      '</div></div></div><div ng-if="relapseTrack.errorTreatment"><div class="height10"></div><p class="errorMsg text-center">Please select one.</p></div><div class="height20"></div>';
    var template = '<div class="height10"></div><p class="text-center">Did you return to your former<br/>state of health without any<br/>residual relapse symptoms?</p><a class="popClose" ng-click="preRelapsePopup.close()"></a>' + select1 +
      '<p class="text-center">Did you receive any treatment <br/> for your relapse?</p><a class="popClose" ng-click="postRelapsePopup.close()"></a>' + select2;
    $scope.postRelapsePopup = $ionicPopup.show({
      title: "End Relapse Tracking",
      template: template,
      scope: $scope,
      cssClass : "endRelapsePopTop",
      buttons: [{
        text: 'Cancel',
        type: 'button-block button-outline button-darkPurple skip',
        onTap: function() {
          return false;
        }
      }, {
        text: 'End Tracking',
        type: 'button-block button-darkPurple',
        onTap: function(e) {
          if (($scope.relapseTrack.feedback.treatment == undefined || $scope.relapseTrack.feedback.treatment == "") || ($scope.relapseTrack.feedback.effective == undefined || $scope.relapseTrack.feedback.effective == "")) {
            if ($scope.relapseTrack.feedback.treatment == undefined || $scope.relapseTrack.feedback.treatment == "") $scope.relapseTrack.errorTreatment = true;
            else $scope.relapseTrack.errorTreatment = false;
            if ($scope.relapseTrack.feedback.effective == undefined || $scope.relapseTrack.feedback.effective == "") $scope.relapseTrack.errorEffective = true;
            else $scope.relapseTrack.errorEffective = false;
            e.preventDefault();
          } else {
            return true;
          }
        }
      }]
    });
    closePopupService.register($scope.postRelapsePopup);
    $scope.postRelapsePopup.then(function(res) {
      if (res) {
        if ($state.current.name != "app.surveys") {
          $scope.stopRelapse2();
        }
        $scope.putStopRelapseKinvey(1);
        $scope.relapseTrack.errorEffective = false;
        $scope.relapseTrack.errorTreatment = false;
      } else {
        $scope.setRelapseEndPopup();
        $scope.relapseTrack.errorEffective = false;
        $scope.relapseTrack.errorTreatment = false;
      }
      console.log($scope.relapseTrack);
    });
  }

  $scope.stopRelapse2 = function() {
    $scope.postRelapsePopup2 = $ionicPopup.show({
      title: "We're happy that your <br/>relapse has ended!",
      template: '<a class="popClose" ng-click="postRelapsePopup2.close()"></a><p class="text-center">Please take a moment to fill out a <br/>Health Tracker Entry to assess your <br/>current well-being.</p><div class="height10"></div>',
      scope: $scope,
      buttons: [{
        text: 'Continue',
        type: 'button-block button-outline button-darkPurple skip',

        onTap: function() {
          return false;
        }
      }, {
        text: 'Submit a Health <br/>Tracker Entry',
        type: 'button-block button-darkPurple font40',
        onTap: function() {
          return true;
        }
      }]
    });
    closePopupService.register($scope.postRelapsePopup2);
    $scope.postRelapsePopup2.then(function(res) {
      if (res) {
        $state.go('app.healthTrackerLandingPage');
      }
      console.log($scope.relapseTrack);
    });
  }

  $scope.startRelapse = function() {
    $scope.relapseTrack.error = false;
    //<span class="errorMsg" ng-if="relapseTrack.error">Please select one.</span>
    var button = '<div class="popBodyInput relapsePopStartYesBTN"><div class="item item-input item-borderless"><div class="radioButtonGroup"><label class="questionLabel ">Do you have any signs of infection (such as a fever)?</label>' +
      '<div class="radioButtonWrapper yes"><input class="radioButton" ng-class="relapseTrack.error ? errorClass : noerrorClass" type="radio" name="preRelapse" value=1 ng-model="relapseTrack.relapseInfection" ng-click="relapseInfectionHandler(1)"><label class="radioLabel">  Yes </label></div>' +
      '<div class="radioButtonWrapper no"><input class="radioButton" ng-class="relapseTrack.error ? errorClass : noerrorClass" type="radio" name="preRelapse" value=0 ng-model="relapseTrack.relapseInfection" ng-click="relapseInfectionHandler(0)"><label class="radioLabel">  No </label></div></div></div></div><div ng-if="relapseTrack.error"><div class="height10"></div><p class="errorMsg text-center">Please select one.</p></div><div class="height20"></div>';
    var template = '<p class="text-center">A relapse involves the appearance of<br/>new symptoms, or the worsening of<br/>symptoms, in the absence of a fever.<br/>Relapses last for 24 hours or more, and<br/>are separated from previous relapses<br/>by at least 30 days.</p><a class="popClose" ng-click="preRelapsePopup.close()"></a><div  class="height5"></div><p  class="text-center">If what you are feeling right now<br/>matches this definition, please click on<br/>Start Relapse Tracking.</p><div  class="height10"></div>' + button;
    $scope.preRelapsePopup = $ionicPopup.show({
      title: "Relapse Tracker",
      template: template,
      scope: $scope,
      buttons: [{
        text: 'Cancel',
        type: 'button-block button-outline button-darkPurple skip',
        onTap: function() {
          return false;
        }
      }, {
        text: 'Start Relapse<br/>Tracking',
        type: 'button-block button-darkPurple',
        onTap: function(e) {
          if ($scope.relapseTrack.relapseInfection == undefined || $scope.relapseTrack.relapseInfection == "") {
            $scope.relapseTrack.error = true;
            e.preventDefault();
          } else {
            return true;
          }
        }
      }]
    });

    $scope.relapseInfectionHandler = function(value) {
      $scope.relapseTrack.error = false;
    }
    closePopupService.register($scope.preRelapsePopup);
    $scope.preRelapsePopup.then(function(res) {
      if(res) {
        $scope.relapseTrack.startRelapse = true;
        $scope.checkTodayRelapse();
        if ($state.current.name != "app.surveys") {
          $scope.startRelapse2();
        }
        $scope.relapseTrack.error = false;
      } else {
        $scope.relapseTrack.relapseInfection = "";
        $scope.relapseTrack.error = false;
      }
      console.log($scope.relapseTrack);
    });
  }

  $scope.startRelapse2 = function() {
    $scope.preRelapsePopup2 = $ionicPopup.show({
      title: "Relapses can be hard...",
      template: '<a class="popClose" ng-click="preRelapsePopup2.close()"></a><p class="text-center">It can also be difficult to remember the<br>specifics of a relapse, such as your<br>symptoms, when it&#39;s time to discuss<br>them with your healthcare team at<br>your next appointment.</p><p class="text-center">Please take a moment to fill out a<br>Health Tracker entry to record your<br>current relapse-related symptoms and<br>state of well-being.</p><p class="text-center bold">You should also contact your<br>healthcare provider to report<br>the relapse. </p><div class="height10"></div>',
      scope: $scope,
      buttons: [{
        text: 'Cancel',
        type: 'button-block button-outline button-darkPurple skip',
        onTap: function() {
          return false;
        }
      }, {
        text: 'Submit a Health<br/>Tracker Entry',
        type: 'button-block button-darkPurple font40',
        onTap: function() {
          return true;
        }
      }]
    });
    closePopupService.register($scope.preRelapsePopup2);
    $scope.preRelapsePopup2.then(function(res) {
      if (res) {
        $scope.relapseTrack.relapseInfection = "";
        $state.go('app.healthTrackerLandingPage');
      } else {
        $scope.relapseTrack.relapseInfection = "";
      }
      console.log($scope.relapseTrack);
    });
  }

  $scope.$on('resetRelapse', function(event, data) {
    $scope.resetRelapse();
  });

  $scope.resetRelapse = function() {
    $scope.setRelapsePopupVariable();
    $scope.setRelapseEndPopup();
  }

  $scope.checkTodayRelapse = function() {
    var tempData = [];
    var users = $kinvey.User.getActiveUser();
    var col = "_acl";
    var relatedJson = { "creator": users.data._id };
    var query = new $kinvey.Query();
    query.equalTo(col, relatedJson);
    col = "startDate";
    relatedJson = moment().format("YYYY-MM-DD") + "T00:00:00.000Z";
    var query1 = new $kinvey.Query();
    query1.equalTo(col, relatedJson);
    query.and(query1);
    relapseList.find(query).subscribe(function(data) {
      tempData = data;
    }, function(error) {}, function(data) {
      if (tempData.length > 0) {
        tempData[0].active = 1;
        tempData[0].infection = parseInt($scope.relapseTrack.relapseInfection);
        tempData[0].cancel = 0;
        tempData[0].feedback = "";
        tempData[0].endDate = "";
        var promise = relapseList.update(tempData[0]).then(function(entity) {
          $scope.relapseTrack.kinveyData = entity;
          $scope.$broadcast('relapseHealthBroadcast');
          $scope.hideLoader();
          console.log(JSON.stringify(entity));
          $scope.relapseTrack.relapseInfection = "";
        }).catch(function(error) {
          $scope.internalGlitch();
          console.log(error);
        });
      } else {
        $scope.putStartRelapseKinvey();
      }
    });
  }

  $scope.putStartRelapseKinvey = function() {
    $scope.showLoader();
    promise = relapseList.save({
      "startDate": moment().format("YYYY-MM-DD") + "T00:00:00.000Z",
      "active": 1,
      "infection": parseInt($scope.relapseTrack.relapseInfection)
    }).then(function(entity) {
      $scope.relapseTrack.kinveyData = entity;
      $scope.relapseTrack.startRelapse = true;
      $scope.replaceDays = 1;
      $scope.hideLoader();
      console.log(JSON.stringify(entity));
      $scope.relapseTrack.relapseInfection = "";
      $scope.$broadcast('relapseHealthBroadcast');
      $scope.incrementRelapseCount();
    }).catch(function(error) {
      $scope.internalGlitch();
      console.log(error);
    });
  }

  /*$scope.incrementRelapseCount = function(){
   var usersDS = new $kinvey.UserStore();
     var user = $kinvey.User.getActiveUser();
     if(user) {
       user.data.relapse.totalRelapse++;
       usersDS.update(user.data)
       .then(function (user) {
         $scope.hideLoader();
         $scope.activeUserInfo = user;
         console.log("relapse ++");
       }).catch(function (error) {
           console.log(error)
       });
     }
  }*/

  $scope.incrementRelapseCount = function(type) {
    var relapseIncrement = {};
    var promise = new $q(function(resolve) {
      resolve($kinvey.User.getActiveUser().me());
    });
    promise.then(function(user) {
      if (user) {
        relapseIncrement = user.data.relapse;
        relapseIncrement.totalRelapse++;
        relapseIncrement.relapseDate = new Date();
        return user.update({
          relapse: relapseIncrement
        });
      }
    }).then(function(user) {
      $scope.hideLoader();
    }).catch(function(error) {
      $scope.internalGlitch();
      console.log(error);
    });
  }

  $scope.putStopRelapseKinvey = function(cancel) {
    $scope.showLoader();
    $scope.relapseTrack.kinveyData.cancel = cancel;
    $scope.relapseTrack.kinveyData.endDate = new Date();
    $scope.relapseTrack.kinveyData.active = 0;
    $scope.relapseTrack.kinveyData.feedback = $scope.relapseTrack.feedback;
    var promise = relapseList.update(JSON.parse(angular.toJson($scope.relapseTrack.kinveyData))).then(function(entity) {
      $scope.relapseTrack.kinveyData = entity
      $scope.hideLoader();
      $scope.relapseTrack.startRelapse = false;
      $scope.resetRelapse();
      console.log(JSON.stringify(entity));
    }).catch(function(error) {
      $scope.resetRelapse();
      $scope.internalGlitch();
      console.log(error);
    });
  }

  //Relapse End

  /*$ionicPlatform.onHardwareBackButton(function(e) {
    var backState = $ionicHistory.viewHistory().backView.stateName;
    var currentState = $ionicHistory.viewHistory().currentView.stateName;
    // if((currentState == 'registration.page2')&&(backState == 'registration.page1')){
      $scope.$broadcast('dontGoBackShowPopup');
      e.preventDefault();
      e.stopImmediatePropagation();
      e.stopPropagation();
    // }
  });*/

  $scope.$on('refreshHighChart', function() {
    $scope.$broadcast('refreshHighChartappCtrl');
  });

  $scope.$on('saveGamificationPoints', function(event, points, stage) {
    var promise = new $q(function(resolve) {
      resolve($kinvey.User.getActiveUser().me());
    });
    promise.then(function(user) {
      if (user) {
        if (user.data.points.latestPoints.length > 2) {
          user.data.points.latestPoints.shift();
        }
        user.data.points.latestPoints.push({ "points": points, "stage": stage });
        user.data.points.currentPoints += points;
        if (stage == "Full PM Battery") {
          user.data.points.fullBatteryPMs = true;
        }
        var usersDS = new $kinvey.UserStore();
        usersDS.update(user.data)
        .then(function(user) {
          console.log(JSON.stringify(user));
        }).catch(function(error) {
          $scope.internalGlitch();
          console.log(error)
        });
      }
      return user;
    }).then(function(user) {
      if (user) {
        var promise = pointsCollection.save({
          "points": points,
          "stage": stage
        }).then(function(entity) {
          $scope.hideLoader();
        }).catch(function(error) {
          $scope.internalGlitch();
          console.log(error);
        });
      } else {}
    }).catch(function(error) {
      $scope.internalGlitch();
      console.log(error);
    });
  });

  $ionicPlatform.registerBackButtonAction(function() {
    var backState = ($ionicHistory.viewHistory().backView && $ionicHistory.viewHistory().backView.stateName)?$ionicHistory.viewHistory().backView.stateName:'app.home';
    var currentState = $ionicHistory.viewHistory().currentView.stateName;
    if((currentState == 'registration.page2')&&(backState == 'registration.page1')){
      $scope.$broadcast('dontGoBackShowPopup');
    }else if(((currentState == 'login')&&(backState == 'app.home')) || ((currentState == 'app.preHome' || currentState == 'app.home') && (backState == 'registration.page6' || backState == 'login')) || (currentState=="app.home"&&backState == "app.preHome")){
      console.log('dont go');
    }else if((currentState == 'registration.page1') && localStorage.verification && !JSON.parse(localStorage.verification)){
      $scope.$broadcast('dontGoBackShowPopup');
    }else{
      if(backState == "app.reactionTest" || backState == "app.walkTest"|| backState == "app.ishiharaTest"|| backState == "app.symbolDigit"
      || backState == "app.fingerTap" || backState == "app.trailTest" || backState == "app.towerOfHanoi" || backState == "app.languageMemoryTest")
      {
        $scope.goToState("app.performanceEvals");
      }
      else if(currentState =="app.healthTrackerLandingPage" && ($ionicHistory.currentStateName() == "app.healthTrackerLandingPage.generalWellbeing" || $ionicHistory.currentStateName() == "app.healthTrackerLandingPage.physicalSymptoms"
      || $ionicHistory.currentStateName() == "app.healthTrackerLandingPage.mindMatters" || $ionicHistory.currentStateName() == "app.healthTrackerLandingPage.dailyLifeSymptoms")){
        $scope.goToState("app.healthTrackerLandingPage");
      }
      else if(currentState =="app.profileSettings" && ($ionicHistory.currentStateName()=="app.profileSettings.profilePersonalInfo" || $ionicHistory.currentStateName()=="app.profileSettings.profileMsHistory"
      ||$ionicHistory.currentStateName()=="app.profileSettings.profileSelectPhysician" || $ionicHistory.currentStateName()=="app.profileSettings.profileTreatmentHistory"
      || $ionicHistory.currentStateName()=="app.profileSettings.profileSettingsTab")){
        $scope.goToState("app.profileSettings");
      }
      else{
        $ionicHistory.goBack();
      }

    }
  },150);

  $scope.registerNotificaton = function(){
    var promise = $kinvey.Push.register({
      android: {
        senderID: CONFIG.pushSenderID,
        icon:"drawable/ic_notify"
      },
      ios: {
        alert: true,
        badge: true,
        sound: true
      }
    }).then(function(response){
      console.log(response);
    }).catch(function(error){
      $scope.internalGlitch();
      console.log(error);
    });
  }

  $scope.openMultiSelectTreatment = function(options, popupType, screenType, title) {
    $scope.typeMultiSelect = popupType;
    $scope.optionMultiSelect = options;
    $scope.screenTypeMultiSelect = screenType;

    if($scope.treatment[$scope.screenTypeMultiSelect]){
      var selectedData = $scope.treatment[$scope.screenTypeMultiSelect];
      for(var j=0;j<selectedData.length;j++){
        for (var i = 0; i<$scope.optionMultiSelect.length; i++) {
          if (selectedData[j].id == $scope.optionMultiSelect[i].id) {
            $scope.optionMultiSelect[i].checked = true;
          }
        }
      }
      $scope.$emit('updateMultiSelectedArray',$scope.treatment[$scope.screenTypeMultiSelect]);
    }

    var multiSelectPopup = "";
    multiSelectPopup = $ionicPopup.show({
      title: title,
      templateUrl: 'templates/genericTemplate/multiSelectPopup.html',
      cssClass: "selectBoxPopup",
      scope: $scope,
      buttons: [{
        text: 'Cancel',
        type: 'button-block button-outline button-darkPurple font56',
        onTap: function() {
          return false;
        }
      }, {
        text: 'Okay',
        type: 'button-block button-outline button-darkPurple bold font56',
        onTap: function() {
          return true;
        }
      }]
    });
    closePopupService.register(multiSelectPopup);
    multiSelectPopup.then(function(res) {
      console.log($scope.multiSelection, res);
      if (res) {
        if ($scope.typeMultiSelect == "newMSMedication") $scope.treatment.newMSMedicationError = false;
        else $scope.treatment.reasonToStopMedicationError = false;
        $scope.treatment[$scope.screenTypeMultiSelect] = $scope.multiSelection;
      } else {
        $scope.$emit('emptyMultiSelectedArray');
        for (var i = 0; i < $scope.optionMultiSelect.length; i++) {
          if ($scope.optionMultiSelect[i].checked == true) {
            $scope.optionMultiSelect[i].checked = false;
          }
        }
      }
    });
  }
  $scope.openSingleSelectTreatment = function(options, popupType, screenType, title) {
    $scope.typeSingleSelect = popupType;
    $scope.optionSingleSelect = options;
    $scope.screenTypeSingleSelect = screenType;
    var singleSelectPopup = "";
    singleSelectPopup = $ionicPopup.show({
      title: title,
      templateUrl: 'templates/genericTemplate/singleSelectPopup.html',
      cssClass: "selectBoxPopup",
      scope: $scope,
      buttons: [{
        text: 'Cancel',
        type: 'button-block button-outline button-darkPurple font56',
        onTap: function() {
          return false;
        }
      }, {
        text: 'Okay',
        type: 'button-block button-outline button-darkPurple bold font56',
        onTap: function() {
          return true;
        }
      }]
    });
    closePopupService.register(singleSelectPopup);
    singleSelectPopup.then(function(res) {
      console.log($scope.singleSelection, res);
      if (res) {
        if ($scope.typeSingleSelect == "newMSMedication") $scope.treatment.newMSMedicationError = false;
        else $scope.treatment.reasonToStopMedicationError = false;
        $scope.treatment[$scope.screenTypeSingleSelect] = $scope.singleSelection;

        $scope.$emit('emptySingleSelectedArray');
      } else {
        $scope.$emit('emptySingleSelectedArray');
        for (var i = 0; i < $scope.optionSingleSelect.length; i++) {
          if ($scope.optionSingleSelect[i].checked == true) {
            $scope.optionSingleSelect[i].checked = false;
            break;
          }
        }
      }
    });
  }

  $scope.treatmentUpdatePopup = function(user) {
    var select1 =
      '<div><div ng-if="treatment.newMSMedicationError"><div class="height10"></div><p class="errorMsg text-center">Please select one.</p></div>'+
      '<div class="item item-input item-borderless no-padding">' +
      '<div class="selectBoxWrapper  item-floating-label">' +
      '<div class="selectBox" ng-class="treatment.newMSMedicationError ? errorClass:noerrorClass" ng-click="openSingleSelectTreatment(newMSMedication,treatmentUpdatePopupVariable.popup1ScreenType,treatmentUpdatePopupVariable.popup1ScreenType,treatmentUpdatePopupVariable.popup1Title)">' +
      '<div class="singleLine upperCase" ng-if="!treatment.newMSMedication"> New MS Medication</div>' +
      '<div class="floatingSelect" ng-if="treatment.newMSMedication"><span class="input-label upperCase font40">Choose one</span><div class="singleLine darkGray upperCase" ng-bind-html="treatment.newMSMedication.text"></div></div>' +
      '</div></div></div><div class="height20"></div></div>';
    var select2 = "";
    if(user.data.info.treatmentHistory.currentTreatment.text != "None"){
      select2 =
      '<div><div ng-if="treatment.reasonToStopMedicationError"><div class="height10"></div><p class="errorMsg text-center">Please select one.</p></div>' +
      '<div class="item item-input item-borderless no-padding">' +
      '<div class="selectBoxWrapper  item-floating-label">' +
      '<div class="selectBox" ng-class="treatment.reasonToStopMedicationError ? errorClass:noerrorClass" ng-click="openMultiSelectTreatment(reasonToStopMedication,treatmentUpdatePopupVariable.popup2ScreenType,treatmentUpdatePopupVariable.popup2ScreenType,treatmentUpdatePopupVariable.popup2Title)">' +

      '<div class="multiLine" ng-if="!treatment.reasonToStopMedication || (treatment.reasonToStopMedication && treatment.reasonToStopMedication.length==0)" style="line-height:2">' +
      // '<div class="upperCase">Why did you stop?</div>' +
      '<div class="upperCase">Select all that apply</div>' +
      '</div>' +
      '<div class="floatingSelect" ng-if="treatment.reasonToStopMedication && treatment.reasonToStopMedication.length>0"><span class="input-label upperCase font40">Choose one</span><div class="singleLine darkGray"><span ng-repeat="value in treatment.reasonToStopMedication">{{value.text}}<span ng-hide="$last">, </span></span></div></div>' +
      '</div></div></div><div class="height20"></div></div>';
    }
    var template = '<div class="height10"></div><p class="text-center">If yes, click cancel. If not, please<br/> fill out the below information and<br/> click save and continue.</p><a class="popClose" ng-click="treatmentIonicPopUp.close()"></a>' + select1 + select2;
    var treatmentText = user.data.info.treatmentHistory.currentTreatment.text;
    var title = "Are you still not taking any medication for your MS?"
    if(user.data.info.treatmentHistory.currentTreatment.text != "None"){
      title = "Are you still taking "+user.data.info.treatmentHistory.currentTreatment.text;
    }
    $scope.treatmentIonicPopUp = $ionicPopup.show({
      title: title,
      template: template,
      scope: $scope,
      buttons: [{
        text: 'Cancel',
        type: 'button-block button-outline button-darkPurple skip font40',
        onTap: function() {
          return false;
        }
      }, {
        text: 'Save and Continue',
        type: 'button-block button-darkPurple font40',
        onTap: function(e) {

          if(treatmentText == "None"){
            if ($scope.treatment.newMSMedication == undefined || $scope.treatment.newMSMedication == "") {
              $scope.treatment.newMSMedicationError = true;
              e.preventDefault();
            }
              else{
                $scope.treatment.newMSMedicationError = false;
                $scope.treatment.reasonToStopMedicationError = false;
                return true;
              }
          }
          else{
            if (($scope.treatment.newMSMedication == undefined || $scope.treatment.newMSMedication == "") || ($scope.treatment.reasonToStopMedication == undefined || $scope.treatment.reasonToStopMedication == "")) {
              if ($scope.treatment.newMSMedication == undefined || $scope.treatment.newMSMedication == "") $scope.treatment.newMSMedicationError = true;
              else $scope.treatment.newMSMedicationError = false;
              if ($scope.treatment.reasonToStopMedication == undefined || $scope.treatment.reasonToStopMedication == "") $scope.treatment.reasonToStopMedicationError = true;
              else $scope.treatment.reasonToStopMedicationError = false;
              e.preventDefault();
            } else {
              return true;
            }
          }

        }
      }]
    });
    closePopupService.register($scope.treatmentIonicPopUp);
    $scope.treatmentIonicPopUp.then(function(res) {
      $scope.$emit('emptyMultiSelectedArray');
      if(res){
        $scope.updateTreatmentForPatient(user);
      }
      else{
        $scope.resetTreatmentUpdate();
      }
    });
  }

  $scope.updateTreatmentForPatient = function(user){
    $scope.showLoader();
    var usersDS = new $kinvey.UserStore();
    $scope.treatment.newMSMedication = JSON.parse(angular.toJson($scope.treatment.newMSMedication));
    if($scope.treatment.reasonToStopMedication){
      $scope.medicationStopReason = JSON.parse(angular.toJson($scope.treatment.reasonToStopMedication));
    }
    else{
      $scope.medicationStopReason = {};
    }
    var yearStarted = {"id":new Date().getFullYear(),"text":new Date().getFullYear(),"checked":true};
    var yearsStopped = {"id":new Date().getFullYear(),"text":new Date().getFullYear(),"checked":true};
    if($scope.treatment.newMSMedication.value != user.data.info.treatmentHistory.currentTreatment.value){
      var treatmentHistoryObj = {"to":{},"from":{},"treatmentChangeDate":new Date(),"reason":$scope.medicationStopReason}
      /*treatmentHistoryObj.to = {"treatment":$scope.treatment.newMSMedication,"yearStarted":yearStarted}
      treatmentHistoryObj.from = {"treatment":user.data.info.treatmentHistory.currentTreatment,"yearStarted":user.data.info.treatmentHistory.currentTreatmentPeriod == undefined ? {}:user.data.info.treatmentHistory.currentTreatmentPeriod,"yearStopped":yearsStopped}
      if(user.data.treatmentHistory){
        user.data.treatmentHistory.push(treatmentHistoryObj);
      }
      else{
        user.data.treatmentHistory = [];
        user.data.treatmentHistory.push(treatmentHistoryObj);
      }*/
      if(!user.data.info.treatmentHistory.priorTreatments){
        user.data.info.treatmentHistory.priorTreatments = [];
      }
      if(user.data.info.treatmentHistory.currentTreatmentPeriod){
        var priorTeatmentsUpdated = {"id":user.data.info.treatmentHistory.priorTreatments.length+1,"lastTreatment":user.data.info.treatmentHistory.currentTreatment,"yearsTaken":user.data.info.treatmentHistory.currentTreatmentPeriod,"yearsStopped":yearsStopped,"treatmentChangeDate":new Date(),"reason":$scope.medicationStopReason};
        if(user.data.info.treatmentHistory.priorTreatments.length >0){
          var pt= [];
          pt.push(priorTeatmentsUpdated);
          for(var i=0;i<user.data.info.treatmentHistory.priorTreatments.length;i++){
             pt.push(user.data.info.treatmentHistory.priorTreatments[i]);
          }
          user.data.info.treatmentHistory.priorTreatments= pt;
          for(var i=0;i<user.data.info.treatmentHistory.priorTreatments.length;i++){
            user.data.info.treatmentHistory.priorTreatments[i].id = i+1;
          }
        }
        else{
          user.data.info.treatmentHistory.priorTreatments.push(JSON.parse(angular.toJson(priorTeatmentsUpdated)));
        }

        user.data.info.treatmentHistory.previousTreatmentTaken = "yes";
      }
    }
    user.data.info.treatmentHistory.currentTreatmentPeriod = JSON.parse(angular.toJson(yearStarted));
    user.data.info.treatmentHistory.currentTreatment = $scope.treatment.newMSMedication;
    usersDS.update(user.data)
    .then(function(user) {
      $scope.hideLoader();
      $scope.resetTreatmentUpdate();
      console.log(JSON.stringify(user));
    }).catch(function(error) {
      console.log(error);
      $scope.internalGlitch();
    });
  }

  $scope.notificationDetails = "";
  $scope.timeOfNotification = "";
  $kinvey.Push.onNotification(function(data) {
    // alert(JSON.stringify(data.additionalData));
    console.log(JSON.stringify(data));
    $scope.notificationDetails = data;
    if ($scope.notificationDetails.additionalData.coldstart) {
      console.log("pushcallback")
      $timeout(function() {
        $scope.navigateFromNotification();
      }, 4000);
    } else {
      /*if (ionic.Platform.isAndroid()) {
        $scope.timeOfNotification = new Date();
        $scope.notificationDetails.message = JSON.parse($scope.notificationDetails.message).message;
        $scope.showAndroidNotification = true;
        $scope.showIosNotification = false;
        $timeout(function() {
          $scope.showAndroidNotification = false;
          $scope.getPMSurveyNotificationNumber();
        }, 5000);
      } else {
        $scope.showAndroidNotification = false;
        $scope.showIosNotification = true;
        $timeout(function() {
          $scope.showIosNotification = false;
          $scope.getPMSurveyNotificationNumber();
        }, 5000);
      }*/

      $scope.notificationResponseHandler();
    }
    $scope.$apply();
  });


  $scope.notificationbtn1Response = function() { //Notification 1 button response (Yes Button Response)
    if ($state.current.name == "app.surveys") {
      if($scope.notificationDetails.additionalData.payload.notificationType == "RELAPSE10DAYS") {
        $scope.showIosNotification = false;
      }
      else {
        $scope.$broadcast('showCancelSurveyPopup');
      }
    } else if ($state.current.name == "app.reactionTest" || $state.current.name == "app.walkTest" || $state.current.name == "app.ishiharaTest" || $state.current.name == "app.symbolDigit" || $state.current.name == "app.fingerTap" || $state.current.name == "app.trailTest" || $state.current.name == "app.towerOfHanoi" || $state.current.name == "app.languageMemoryTest") {
      console.log("Do nothing");
    } else {
      $scope.showAndroidNotification = false;
      $scope.showIosNotification = false;
      if ($scope.notificationDetails.additionalData.payload.notificationType) {
        switch ($scope.notificationDetails.additionalData.payload.notificationType) {
          case "RELAPSE10DAYS":
            $scope.showIosNotification = false;
            break;
          case "TREATMENTUPDATE":
            //$scope.openUserSettingPopup('app.profileSettings.profileTreatmentHistory'); //acceess profile pop up
            var promise = new $q(function(resolve) {
              resolve($kinvey.User.getActiveUser());
            });
            promise.then(function(user) {
              if (user) {
                return user.me();
              }
            }).then(function(user) {
              if (user) {
                $scope.treatmentUpdatePopup(user);
              }
            }).catch(function(error) {
              $scope.internalGlitch();
            });
          break;
          case "PM":
            $scope.redirectNotification('app.performanceEvals');
            //$state.go('app.performanceEvals');
          break;
          case "PMBULK":
            $scope.redirectNotification('app.performanceEvals');
            //$state.go('app.performanceEvals');
          break;
          case "SURVEY":
            $scope.redirectNotification('app.surveysLandingPage');
            //$state.go('app.surveysLandingPage');
          break;
          default :
          break;
        }
      }
    }
  }

  $scope.redirectNotification = function(state){ // if user is taking health tracker open side menu and if user is updating profile user first redirect to profile landing page than to other page
    var currentState = $ionicHistory.viewHistory().currentView.stateName;
    if(currentState =="app.healthTrackerLandingPage" && ($ionicHistory.currentStateName() == "app.healthTrackerLandingPage.generalWellbeing" || $ionicHistory.currentStateName() == "app.healthTrackerLandingPage.physicalSymptoms"
      || $ionicHistory.currentStateName() == "app.healthTrackerLandingPage.mindMatters" || $ionicHistory.currentStateName() == "app.healthTrackerLandingPage.dailyLifeSymptoms")){
        $scope.$broadcast('showPopOnExitTrackerForNotification');
    }
    else if(currentState =="app.profileSettings" && ($ionicHistory.currentStateName()=="app.profileSettings.profilePersonalInfo" || $ionicHistory.currentStateName()=="app.profileSettings.profileMsHistory"
    ||$ionicHistory.currentStateName()=="app.profileSettings.profileSelectPhysician" || $ionicHistory.currentStateName()=="app.profileSettings.profileTreatmentHistory"
    || $ionicHistory.currentStateName()=="app.profileSettings.profileSettingsTab")){
      $scope.goToState(state);
    }
    else{
      $state.go(state);
    }
  }

  $scope.notificationbtn2Response = function() { //Notification 2 button response (NO Button Response)
    if ($state.current.name == "app.surveys") {
      if($scope.notificationDetails.additionalData.payload.notificationType == "TREATMENTUPDATE") {
        $scope.showIosNotification = false;
      }
      else {
        $scope.$broadcast('showCancelSurveyPopup');
      }
    } else if ($state.current.name == "app.reactionTest" || $state.current.name == "app.walkTest" || $state.current.name == "app.ishiharaTest" || $state.current.name == "app.symbolDigit" || $state.current.name == "app.fingerTap" || $state.current.name == "app.trailTest" || $state.current.name == "app.towerOfHanoi" || $state.current.name == "app.languageMemoryTest") {
      console.log("Do nothing");
    } else {
      if ($scope.notificationDetails.additionalData.payload.notificationType) {
        $scope.showAndroidNotification = false;
        $scope.showIosNotification = false;
        switch ($scope.notificationDetails.additionalData.payload.notificationType) {
          case "RELAPSE10DAYS":
            $scope.showIosNotification = false;
            $scope.stopRelapse();
            break;
          case "TREATMENTUPDATE":
            $scope.showIosNotification = false;
            break;
          default:
            break;
        }
      }
    }
  }

  // TODO : Need to change the kinvey sdk to 3.1.0 for deregistration to work.
  $scope.deRegisterNotification = function(){
    var userData = $kinvey.User.getActiveUser();
    var promise = $kinvey.CustomEndpoint.execute('deregisterDuplicatedUUIDs',{"user":userData.data,"deviceType":"logout"});
    promise.then(function(){
      console.log("success deregister");
    }).catch(function(){
      console.log("error deregister");
    });
  }

  $scope.notificationResponseHandler = function() {
    $scope.getPMSurveyNotificationNumber();
    if ($state.current.name == "app.reactionTest" || $state.current.name == "app.walkTest" || $state.current.name == "app.ishiharaTest" || $state.current.name == "app.symbolDigit" || $state.current.name == "app.fingerTap" || $state.current.name == "app.trailTest" || $state.current.name == "app.towerOfHanoi" || $state.current.name == "app.languageMemoryTest") {
      return ;
    }
    if($state.current.name == "app.surveys") {
      if($scope.notificationDetails.additionalData.payload.notificationType == 'RELAPSE10DAYS' || $scope.notificationDetails.additionalData.payload.notificationType == 'TREATMENTUPDATE' ){
        $scope.relapseTreatmentPopup();
      }else if($scope.notificationDetails.additionalData.foreground == false){
        $scope.$broadcast('showCancelSurveyPopup');
      }
      return;
    }
    closePopupService.closePopup();
    if ($scope.notificationDetails.additionalData.payload.notificationType && $scope.notificationDetails.additionalData.foreground == false) {
      switch ($scope.notificationDetails.additionalData.payload.notificationType) {
        case "APPNOTVISITED":
          console.log("application open");
          break;
        case "PM":
          $scope.redirectNotification('app.performanceEvals');
          break;
        case "PMBULK":
          $scope.redirectNotification('app.performanceEvals');
          break;
        case "RELAPSE10DAYS":
          $scope.relapseTreatmentPopup();
          break;
        case "TREATMENTUPDATE":
          $scope.relapseTreatmentPopup();
          break;
        case "SURVEY":
          $scope.redirectNotification('app.surveysLandingPage');
          break;
        case "RELAPSE28DAYS":
          $scope.relapseTreatmentPopup();
          break;
        case "TIP":
          $scope.redirectNotification('app.tips');
          break;
        case "HEATALERT":
          $scope.redirectNotification('app.heatAlerts');
          break;
        case "APPUPDATE":
          $state.go('app.home');
          $scope.appVersionPopup("Please update the app in order to access new features");
          break;
        default:
          console.log('do nothing');
      }
    }else{
      $scope.relapseTreatmentPopup();
    }
  }

  $scope.relapseTreatmentPopup = function(){
    if($scope.notificationDetails.additionalData.payload.notificationType == 'RELAPSE10DAYS'){
      $scope.stopRelapse();
    }else if($scope.notificationDetails.additionalData.payload.notificationType == 'TREATMENTUPDATE'){
      var promise = new $q(function(resolve) {
        resolve($kinvey.User.getActiveUser());
      });
      promise.then(function(user) {
        if (user) {
          return user.me();
        }
      }).then(function(user) {
        if (user) {
          $scope.treatmentUpdatePopup(user);
        }
      }).catch(function(error) {
        $scope.internalGlitch();
      });
    }else if($scope.notificationDetails.additionalData.payload.notificationType == 'RELAPSE28DAYS'){
      $scope.resetRelapse();
    }
  }

  $scope.navigateFromInAppNotification = function() { // click on notification div from inside the app
    if ($scope.notificationDetails.additionalData.payload.notificationType == "TIP" || $scope.notificationDetails.additionalData.payload.notificationType == "HEATALERT" ||
      $scope.notificationDetails.additionalData.payload.notificationType == "RELAPSE28DAYS") {
      console.log($scope.notificationDetails);
      $scope.showAndroidNotification = false;
      $scope.showIosNotification = false;
      $scope.currentView = $state.current.name;
      if ($state.current.name == "app.surveys") {
        $scope.$broadcast('showCancelSurveyPopup');
      } else if ($state.current.name == "app.reactionTest" || $state.current.name == "app.walkTest" || $state.current.name == "app.ishiharaTest" || $state.current.name == "app.symbolDigit" || $state.current.name == "app.fingerTap" || $state.current.name == "app.trailTest" || $state.current.name == "app.towerOfHanoi" || $state.current.name == "app.languageMemoryTest") {
        console.log("Do nothing");
      } else {
        if ($scope.notificationDetails.additionalData.payload.notificationType) {
          switch ($scope.notificationDetails.additionalData.payload.notificationType) {
            case "TIP":
              //$state.go('app.tips');
              $scope.redirectNotification('app.tips');
              break;
            case "HEATALERT":
              $scope.redirectNotification('app.heatAlerts');
              //$state.go('app.heatAlerts');
              break;
            case "RELAPSE28DAYS":
              $scope.resetRelapse();
              break;
          }
        }
      }
    } else {
      return;
    }
  }

  $scope.navigateFromNotification = function() { // click on notification native response
    console.log($scope.notificationDetails);
    $scope.showAndroidNotification = false;
    $scope.showIosNotification = false;
    $scope.currentView = $state.current.name;
    if ($state.current.name == "app.surveys") {
      $scope.$broadcast('showCancelSurveyPopup');
    } else if ($state.current.name == "app.reactionTest" || $state.current.name == "app.walkTest" || $state.current.name == "app.ishiharaTest" || $state.current.name == "app.symbolDigit" || $state.current.name == "app.fingerTap" || $state.current.name == "app.trailTest" || $state.current.name == "app.towerOfHanoi" || $state.current.name == "app.languageMemoryTest") {
      console.log("Do nothing");
    } else {
      if ($scope.notificationDetails.additionalData.payload.notificationType) {
        switch ($scope.notificationDetails.additionalData.payload.notificationType) {
          case "APPNOTVISITED":
            console.log("application open");
            break;
          case "SURVEY":
            $state.go('app.surveysLandingPage');
            break;
          case "PM":
            $state.go('app.performanceEvals');
            break;
          case "PMBULK":
            $state.go('app.performanceEvals');
            break;
          case "TIP":
            $state.go('app.tips');
            break;
          case "HEATALERT":
            $state.go('app.heatAlerts');
            break;
          case "RELAPSE10DAYS":
            $state.go('app.home');
            $scope.stopRelapse();
            break;
          case "RELAPSE28DAYS":
            console.log("application open");
            $scope.resetRelapse();
            break;
          case "TREATMENTUPDATE":
            var promise = new $q(function(resolve) {
              resolve($kinvey.User.getActiveUser());
            });
            promise.then(function(user) {
              if (user) {
                return user.me();
              }
            }).then(function(user) {
              if (user) {
                $scope.treatmentUpdatePopup(user);
              }
            }).catch(function(error) {
              $scope.internalGlitch();
            });
            break;
          case "APPUPDATE":
            $state.go('app.home');
            $scope.appVersionPopup("Please update the app in order to access new features");
            break;
        }
      }
    }
  }


  $scope.$on('updateRelapseTracker', function(event, pmId) { //save PM Id to Relapse
     $scope.relapsePMtracker = $scope.relapseTrack.kinveyData;
      if ($scope.relapsePMtracker.pmIds == undefined || $scope.relapsePMtracker.pmIds == "") {
        $scope.relapsePMtracker.pmIds = [];
      }
      $scope.relapsePMtracker.pmIds.push({ "pmId": pmId });
      $scope.relapsePMtracker = JSON.parse(angular.toJson($scope.relapsePMtracker));
      relapseList.update($scope.relapsePMtracker).then(function(entity) {
        $scope.relapsePMtracker = {};
        $scope.$emit('refreshRelapseData');
        $scope.hideLoader();
        console.log(JSON.stringify(entity));
      }).catch(function(error) {
      $scope.internalGlitch();
      console.log(error);
    });
  });

  $scope.appVersionPopup = function(title){
    //$scope.updateAppDetails = detail;
    $scope.appConflictPopup = $ionicPopup.show({
      title: title,
      templateUrl: 'templates/miscellaneousModules/updateAppPopup.html',
      cssClass: "appStart ",
      scope: $scope,
      buttons: [{
        text: 'Update your app',
        type: 'button-block button-darkPurple',
        onTap: function() {
          return true;
        }
      }]
    });
    closePopupService.register($scope.appConflictPopup);
    $scope.appConflictPopup.then(function(res){
      //
      if(res){
        if (ionic.Platform.isAndroid()) {
          window.open(CONSTANT.googlePlayURL, "_system");
        }
        else{
          window.open(CONSTANT.appStoreURL, "_system");
        }
      }else{
        if($state.current.name != 'app.surveysLandingPage' && $state.current.name != 'app.performanceEvals' && $state.current.name != 'app.surveys'){
          $state.go('app.home');
        }
      }
    });
  }

  $scope.forceUpdatePopup = function(title){
    $scope.appUpdatePopup = $ionicPopup.show({
      title: "Please update the app in order to access new features",
      templateUrl: 'templates/miscellaneousModules/updateAppPopup.html',
      cssClass: "appStart ",
      scope: $scope,
      buttons: [{
        text: 'Update your app',
        type: 'button-block button-darkPurple',
        onTap: function(e) {
          if (ionic.Platform.isAndroid()) {
            window.open(CONSTANT.googlePlayURL, "_system");
          }
          else{
            window.open(CONSTANT.appStoreURL, "_system");
          }
          e.preventDefault();
        }
      }]
    });
  }

  $scope.showAppUpdatePopup = function(title){
    $scope.appUpdatePopup = $ionicPopup.show({
      title: "NOTICE",
      templateUrl: 'templates/miscellaneousModules/forceUpdateAppPopup.html',
      cssClass: "appStart ",
      scope: $scope,
      buttons: [{
        text: 'Update Now',
        type: 'button-block button-darkPurple',
        onTap: function(e) {
          if (ionic.Platform.isAndroid()) {
            window.open(CONSTANT.googlePlayURL, "_system");
          }
          else{
            window.open(CONSTANT.appStoreURL, "_system");
          }
          $scope.logoutUser();
          e.preventDefault();
        }
      }]
    });
  }

  document.addEventListener("deviceready", onDeviceReady, false);

  window.addEventListener('native.keyboardhide', keyboardHideHandler);

  function keyboardHideHandler(e){
    ionic.Platform.fullScreen();
  }

  function onDeviceReady() {
    document.addEventListener("online", onlineHandler, false);
    document.addEventListener("offline", offlineHandler, false);
    // Not calling on index because it take to much time on load so calling manually.
    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = 'https://maps.googleapis.com/maps/api/js?key=' + CONFIG.googleApiKey;
    document.body.appendChild(script);

    if (typeof window.ga !== undefined) {
      window.ga.startTrackerWithId(CONFIG.googleAnalytics);
    }
  }

  function onlineHandler() {
    $scope.showMessage('Device connected to internet.', 3000);
    $scope.hideLoader();
  }

  function offlineHandler() {
    $scope.showMessage('Sorry, your device is offline. Please confirm a connection and try again.', 3000);
    $scope.hideLoader();
  }

  $scope.preHomePage = function(_flag) {
    var changeMap = {};
    var promise = new $q(function(resolve) {
      resolve($kinvey.User.getActiveUser().me());
    });
    promise.then(function(user) {
      if (user) {
        if(user.data.changeMap){
          changeMap = user.data.changeMap;
        }
        changeMap.preHomePage = _flag;
        return user.update({
          changeMap: changeMap
        });
      }
    }).then(function(user) {
      //$scope.hideLoader();
    }).catch(function(error) {
      //$scope.internalGlitch();
      //console.log(error);
    });
  }

  $scope.navigateToHome = function(){
    var promise = new $q(function(resolve) {
      resolve($kinvey.User.getActiveUser().me());
    });
    promise.then(function(user) {
      if (user) {
        if(user.data.changeMap && user.data.changeMap.preHomePage){
          $state.go("app.home");
        }else{
          if($scope.homeScreenCount.PM > 0 || $scope.homeScreenCount.Survey > 0 || ($scope.healthTackerProperty.isHealthTackerTakenToday == false)){
            $state.go("app.preHome");
          }else if($scope.healthTackerProperty.isHealthTackerTakenToday == undefined){
            $scope.healthTrackerCount();
          }else{
            $state.go("app.home");
          }
        }
      }
    }).then(function(user) {
      //$scope.hideLoader();
    }).catch(function(error) {
      $state.go("app.preHome");
    });
  }

  $scope.healthTrackerCount = function(){
    var users = $kinvey.User.getActiveUser();
    var col = "_acl";
    var relatedJson = { "creator": users.data._id };
    var query = new $kinvey.Query();
    query.equalTo(col, relatedJson);
    col = "trackerEntryDate";
    relatedJson = moment().format("YYYY-MM-DD");
    var query1 = new $kinvey.Query();
    query1.equalTo(col, relatedJson);
    query.and(query1);
    var trackerData;
    trackerAnswerCollection.find(query).subscribe(function(data) {
      trackerData = data;
    }, function(error) {
      $scope.internalGlitch();
    }, function() {
      $scope.hideLoader();
      if (trackerData.length > 0) {
        $scope.healthTackerProperty.isHealthTackerTakenToday = true;
        if($scope.homeScreenCount.PM == 0 && $scope.homeScreenCount.Survey == 0){
          $state.go("app.home");
        }else{
          $state.go("app.preHome");
        }
      } else {
        $scope.healthTackerProperty.isHealthTackerTakenToday = false;
        $state.go("app.preHome");
      }
    });
  }

  $scope.takeToMSCareConnectDashboard = function(){
    window.open(CONFIG.dashboardNavigationLink, '_system', 'location=yes');
  }

}]);
