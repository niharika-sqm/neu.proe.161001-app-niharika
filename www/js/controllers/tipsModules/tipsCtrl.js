neuroScienceApp.controller('tipsCtrl', ['$scope', '$state', '$kinvey', '$timeout', '$q', function($scope, $state, $kinvey, $timeout, $q) {

	$timeout(function(){
    window.ga.trackView('Tips Landing Screen');
  }, 1000);
	
	var tipsCollections = $kinvey.DataStore.getInstance('tips', $kinvey.DataStoreType.Network);
	var userCopyCollections = $kinvey.DataStore.getInstance('userCopy', $kinvey.DataStoreType.Network);

	$scope.unreadTips = [];
	$scope.readTips = [];
	var activeUserInfo = null;
	var userCopyInfo = null;
	var readTipsArr = [];
	var readTipsArr = [];
	var localStorageTips = [];
	var usersDS = new $kinvey.UserStore();

	function onlyUnique(value, index, self) { 
    return self.indexOf(value) === index;
	}

	function getUpdatedTipsInfo(){
		$scope.showLoader();
		$scope.unreadTips = [];
		$scope.readTips = [];
		new $q(function(resolve) {
			resolve($kinvey.User.getActiveUser().me());
		}).then(function(user){
		  	return user;
		}).then(function(user){
			activeUserInfo = user.data;
			var query = new $kinvey.Query();
			query.equalTo('_acl.creator', $kinvey.User.getActiveUser()._id);
			userCopyCollections.find(query).subscribe(function(data) {
        userCopyInfo = data[0];
        if(userCopyInfo && userCopyInfo.nextNotification && (userCopyInfo.nextNotification.tips.order > 0)){
					readTipsArr = (activeUserInfo.tipsInfo && activeUserInfo.tipsInfo.length > 0)? activeUserInfo.tipsInfo : [];					
					var query = new $kinvey.Query();
					if(userCopyInfo.nextNotification.tips.allSent){
						query.lessThanOrEqualTo('order', userCopyInfo.nextNotification.tips.order);
					}else{
						query.lessThan('order', userCopyInfo.nextNotification.tips.order);
					}					
					var tempTips = null;
					tipsCollections.find(query).subscribe(function(data) {
						tempTips = data;
					}, function(error) {
						$scope.internalGlitch();
					}, function(data) {						
						if(localStorage.tipsInfo && localStorage.tipsInfo !=""){
							localStorageTips = JSON.parse(localStorage.tipsInfo);
							for(var i in localStorageTips){
								if(readTipsArr.indexOf(localStorageTips[i])<0){
									readTipsArr.push(localStorageTips[i]);
								}
							}
						}
						var timeoutPro = $timeout(function() {
							$timeout.cancel(timeoutPro);
							for(var i in readTipsArr){
								tempTips.find(function(element, index){
									if(element && readTipsArr[i] == element.order){
										$scope.readTips.push(element);
										tempTips.splice(index,1);
									}
								})
							}
							$scope.unreadTips = tempTips;
							$scope.hideLoader();
						}, 500);
					});	
				}else{
					$scope.unreadTips = [];
					$scope.readTips = [];
					$scope.hideLoader();
				}
      }, function(error) {
      	$scope.internalGlitch();
      }, function() {
      });
    }).catch(function(error) {
    	$scope.internalGlitch();
		});
	}

	/*function getUpdatedTipsInfo1(){
		new $q(function(resolve) {
			resolve($kinvey.User.getActiveUser().me());
		}).then(function(user){
			if (user && localStorage.tipsInfo) {
				var alreadyTips = user.data.tipsInfo.concat(JSON.parse(localStorage.tipsInfo));
				localStorage.removeItem('tipsInfo');
		    return user.update({
		      tipsInfo: alreadyTips.filter(onlyUnique)
		    });
		  }else{
		  	return user;
		  }
		}).then(function(user){
			activeUserInfo = user.data;
			// console.log(JSON.stringify(activeUserInfo));
			if(activeUserInfo.nextNotification){
				var query = new $kinvey.Query();
				query.lessThanOrEqualTo('order', userCopyInfo.nextNotification.tips.order);
				readTipsArr = (activeUserInfo.tipsInfo && activeUserInfo.tipsInfo.length > 0)? activeUserInfo.tipsInfo : []
				var tempTips = null;
				tipsCollections.find(query).subscribe(function(data) {
					tempTips = data;
				}, function(error) {}, function(data) {
					var timeoutPro = $timeout(function() {
						$timeout.cancel(timeoutPro);
						for(var i in readTipsArr){
							tempTips.find(function(element, index){
								// console.log("element "+element);
								if(element && readTipsArr[i] == element.order){
									$scope.readTips.push(element);
									tempTips.splice(index,1);
								}
							})
						}
						$scope.unreadTips = tempTips;
						$scope.hideLoader();
					}, 500);
				});	
			}else{
				$scope.unreadTips = [];
				$scope.readTips = [];
				$scope.hideLoader();
			}    		    	    	
		}).catch(function(error){
			$scope.internalGlitch();
			$scope.unreadTips = [];
			$scope.readTips = [];
			$scope.hideLoader(); 
		});
	}*/

	// var tipGamification = [];
	var tipsInfo = [];
	$scope.openUnreadTip = function(_unreadTip, _event){
		if(_event.currentTarget.className.indexOf("open")>=0){
			_event.currentTarget.className = "activated";
		}else{
			_event.currentTarget.className = "open activated";
		}
		
		if(localStorageTips && localStorageTips.indexOf(_unreadTip.order)<0){
			localStorageTips.push(_unreadTip.order);
			localStorage.tipsInfo = JSON.stringify(localStorageTips);
		}
		
		// updateTipInfo(_unreadTip.order);
		/*if(tipGamification.indexOf(_unreadTip.order)<0){
			tipGamification.push(_unreadTip.order);
			// todo : for gamification points add over here.
		}*/
	}

	var isActiveUserInfoUpdating = false;

	/*function updateTipInfo(_order){
		if(_order && activeUserInfo.tipsInfo){
			if(activeUserInfo.tipsInfo.indexOf(_order) < 0){
			  activeUserInfo.tipsInfo.push(_order);
			}
		}else if(_order && (!activeUserInfo.tipsInfo)) {
			activeUserInfo.tipsInfo = [];
			activeUserInfo.tipsInfo.push(_order);
			
		}

		if(!isActiveUserInfoUpdating){

			isActiveUserInfoUpdating = true;
			usersDS.update(activeUserInfo)
			.then(function (user) {
				isActiveUserInfoUpdating = false;

				if(user.tipsInfo.length != activeUserInfo.tipsInfo.length){
			   	updateTipInfo(); 
				}
				// console.log("updated");
			}).catch(function (error) {
				isActiveUserInfoUpdating = false;
				updateTipInfo();
				// console.log(error)
			});
		}
	}*/

	$scope.openReadTip = function( _event){
		if(_event.currentTarget.className.indexOf("open")>=0){
			_event.currentTarget.className = "activated";
		}else{
			_event.currentTarget.className = "open activated";
		}
	}

	getUpdatedTipsInfo();
}]);