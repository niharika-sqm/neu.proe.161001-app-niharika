neuroScienceApp.controller('healthTrackerDailyLifeSymptomsCtrl', ['$scope', '$state','$timeout','$ionicScrollDelegate', function ($scope, $state,$timeout,$ionicScrollDelegate) {
	$ionicScrollDelegate.scrollTop();
	$ionicScrollDelegate.resize();

	$timeout(function(){
    window.ga.trackView('Health Tracker - Daily Life Symptoms');
  }, 1000);
  
	$scope.healthTracker.dailyLifeSymptoms != undefined ? $scope.isDirty=true: $scope.isDirty=false;
	$scope.trackerDate.enable = false;
	$scope.questions= angular.copy($scope.dailyLifeSymptomsQuestions);
	$scope.checkFormisDirty = function(){
		$scope.isDirty = false;
		for(var i=0;i<$scope.questions.length;i++){
			if($scope.questions[i].value > 0){
				$scope.isDirty = true;
				break;
			}
		}
	}
	$scope.setDailySymptomValue = function(symptom,level){
		if(symptom.value == level){
			symptom.value = 0;
		}
		else{
			symptom.value = level;
		}
		//$scope.isDirty = true;	
		$scope.checkFormisDirty();
	}
	
	$scope.submit = function(){
		if (typeof window.ga !== "undefined") {
      window.ga.trackEvent('Health Tracker - Daily Life Symptoms', 'Save Entry and continue', 'Continue', 1);
    }
		$scope.healthTracker.dailyLifeSymptoms = [];
		for(var i=0;i<$scope.questions.length;i++){
			if($scope.questions[i].value!=0 && $scope.questions[i].value!=""){
				$scope.$parent.healthTracker.dailyLifeSymptoms.push({
					"name":$scope.questions[i].name,
					"answer":$scope.questions[i].value,
					"qIndex":i
				});
				$scope.dailyLifeSymptomsQuestions[i].value = $scope.questions[i].value;
			}
		}
		if($scope.healthTracker.dailyLifeSymptoms.length == 0){
			delete($scope.healthTracker.mindMatters);
		}
		$scope.$parent.landingPage.activeFinalSubmit = true;
		$scope.trackerDate.enable = true;
		$ionicScrollDelegate.scrollTop();
		$ionicScrollDelegate.resize();
		$scope.setRestSubCatagory('dailyLifeSymptomsQuestions',$scope.questions);
		$state.go('app.healthTrackerLandingPage');
	}
	$scope.goBackHeathTracker = function(){
		// $scope.restDailyLifeSymptoms();
		// delete($scope.$parent.healthTracker.dailyLifeSymptoms);
		if (typeof window.ga !== "undefined") {
      window.ga.trackEvent('Health Tracker - Daily Life Symptoms', 'Health Tracker Screen (Daily Life Symptoms)', 'Back', 1);
    }
		$scope.trackerDate.enable = true;
		$ionicScrollDelegate.scrollTop();
		$ionicScrollDelegate.resize();
		$state.go('app.healthTrackerLandingPage');

	}

	$scope.$on('$destroy',function(){
		$scope.goBackHeathTracker();
	})
	
}]);