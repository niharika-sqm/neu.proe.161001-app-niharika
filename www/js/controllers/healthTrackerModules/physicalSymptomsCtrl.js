neuroScienceApp.controller('healthTrackerPhysicalSymptomsCtrl', ['$scope', '$state','$timeout','$ionicScrollDelegate', function ($scope, $state,$timeout,$ionicScrollDelegate) {

	$timeout(function(){
    window.ga.trackView('Health Tracker - Physical Symptoms');
  }, 1000);

	$scope.healthTracker.physicalSymptoms != undefined ? $scope.isDirty=true: $scope.isDirty=false;
	$scope.selectIssue=false;
	$scope.isHead=true;
	$scope.trackerDate.enable = false;
	$scope.symptom={};
	$scope.parts = {};
	$scope.isServerity = false;
	$scope.index={};
	$scope.neck = {};
	$scope.headPain=[false,false,false]
	$ionicScrollDelegate.scrollTop();
	$ionicScrollDelegate.resize();
	$scope.filledAny = false;
	
	$scope.checkValueFilled = function(){ //considering order of question is fixed
		if($scope.healthTracker.physicalSymptoms.length>0){
			$scope.filledAny = true;
		}
		else{
			$scope.filledAny = false;
		}
		for(var i=0;i<$scope.healthTracker.physicalSymptoms.length;i++){
			switch($scope.healthTracker.physicalSymptoms[i].name){
				case "muscleSpasms": $scope.physicalSymptomsQuestions[0].filled = true; break;
				case "weakness":$scope.physicalSymptomsQuestions[1].filled = true; break;
				case "numbnessTingling":$scope.physicalSymptomsQuestions[2].filled = true; break;
				case "pain":$scope.physicalSymptomsQuestions[3].filled = true; break;
			}
		}
		$scope.questions = angular.copy($scope.physicalSymptomsQuestions);
	}

	if($scope.healthTracker.physicalSymptoms != undefined){
		$scope.checkValueFilled();
	}
	else{
		$scope.questions = angular.copy($scope.physicalSymptomsQuestions);
	}

	$scope.locateIssue = function(symptom,index){ // select pain, numbness     
		$scope.selectIssue = true;
		$scope.index.question = index;
		console.log("question = "+index);
		$scope.symptom = angular.copy(symptom);
		$scope.isHead = symptom.head;
		$timeout(function(){
			$ionicScrollDelegate.scrollTop();
			$ionicScrollDelegate.resize();
		},300);
	}

	$scope.headProblem = function(i){
		$scope.headPain[i] = !$scope.headPain[i];
		if($scope.headPain[i]) $scope.questions[$scope.index.question].parts[$scope.index.part].associate[i].value = 1;
		else $scope.questions[$scope.index.question].parts[$scope.index.part].associate[i].value = 0;
	}

	$scope.neckPain = function(){ // select checkbox of neck
		console.log($scope.neck);
		if($scope.neck.check) {
			if($scope.questions[$scope.index.question].parts[$scope.index.part].name == "head"){
				$scope.questions[$scope.index.question].parts[$scope.index.part].associate[3].value =1;
			}
			else{
				$scope.questions[$scope.index.question].parts[$scope.index.part].associate[0].value =1;
			}	
		}
		else {
			if($scope.questions[$scope.index.question].parts[$scope.index.part].name == "head"){
				$scope.questions[$scope.index.question].parts[$scope.index.part].associate[3].value =0;
			}
			else{
				$scope.questions[$scope.index.question].parts[$scope.index.part].associate[0].value =0;
			}	
		}
	}

	$scope.goBackHeathTracker = function(){
		if (typeof window.ga !== "undefined") {
	    window.ga.trackEvent('Physical Symptoms - '+ $scope.symptom.text,'Back to Physical Symptoms Screen', 'Back', 1);
	  } 
		$ionicScrollDelegate.scrollTop();
		$scope.selectIssue = false;	
		$scope.symptom={};
		$scope.parts = {};
		$scope.index={};
		$scope.questions = angular.copy($scope.physicalSymptomsQuestions);
		$scope.healthTracker.physicalSymptoms != undefined ? $scope.isDirty=true: $scope.isDirty=false;
		// $scope.restPhysicalSymptoms();
		// delete($scope.healthTracker.physicalSymptoms);
		//$scope.gotoHeathTracker();
	}

	$scope.symptomStrength = function(index){	//select body part
		$scope.index.part = index;
		console.log("part = "+index);
		$scope.parts = angular.copy($scope.questions[$scope.index.question].parts[$scope.index.part]);
		if($scope.parts.name != "head"){ 
			if($scope.parts.associate != undefined)  $scope.parts.associate[0].value == 1 ? $scope.neck.check = true : $scope.neck.check=false;
		}	
		else if($scope.parts.name == "head"){
			for(var i=0;i<$scope.parts.associate.length;i++){
				if($scope.parts.associate[i].text == "Neck") {
					$scope.parts.associate[i].value == 1 ? $scope.neck.check = true : $scope.neck.check=false;
				}
				else{
					$scope.parts.associate[i].value == 1 ? $scope.headPain[i] = true: $scope.headPain[i] = false;
				}			
			}
		}
		$scope.isServerity = true;
	}

	$scope.closeServerityPopup = function(){
		$scope.isServerity = !$scope.isServerity;
	}
	
	$scope.setPainValue = function(level){ // select intensity of pain
		$scope.questions[$scope.index.question].parts[$scope.index.part].value=level;
		$scope.parts = angular.copy($scope.questions[$scope.index.question].parts[$scope.index.part]);
		$scope.symptom = angular.copy($scope.questions[$scope.index.question]);
		$scope.isDirty=true;
		// if($scope.symptom.id!=3 && $scope.symptom.id!=4){
		// 	$scope.isServerity = !$scope.isServerity;
		// }
		$scope.isServerity = !$scope.isServerity;
		$ionicScrollDelegate.scrollBottom();
	}

	$scope.submit = function(){
		if (typeof window.ga !== "undefined") {
	    window.ga.trackEvent('Physical Symptoms - '+ $scope.symptom.text,'Save Entry and continue', 'Continue', 1);
	  } 
		$scope.healthTracker.physicalSymptoms = [];
		for(var i=0;i<$scope.questions.length;i++){
			var answer = [];var max={"name":"","answer":0};
			for(var j=0;j<$scope.questions[i].parts.length;j++){			
				if($scope.questions[i].parts[j].value!=0 && $scope.questions[i].parts[j].value!=""){
					var associate = [];
					//if($scope.questions[i].parts[j].partId==1 || $scope.questions[i].parts[j].partId==4){
						for(var z=0;z<$scope.questions[i].parts[j].associate.length;z++){
							if($scope.questions[i].parts[j].associate[z].value!=0) {
								associate.push($scope.questions[i].parts[j].associate[z].text);
								$scope.physicalSymptomsQuestions[i].parts[j].associate[z].value = $scope.questions[i].parts[j].associate[z].value;
							}
						}
					//}
					if(associate.length>0){
						answer.push({
							"name":$scope.questions[i].parts[j].name,
							"answer":$scope.questions[i].parts[j].value,
							"highChartName":$scope.questions[i].parts[j].highChartName,
							"associate":associate,
							"pIndex":j
						});
					}
					else{
						answer.push({
							"name":$scope.questions[i].parts[j].name,
							"answer":$scope.questions[i].parts[j].value,
							"highChartName":$scope.questions[i].parts[j].highChartName,
							"text":$scope.questions[i].parts[j].text,
							"pIndex":j
						});
					}
					$scope.physicalSymptomsQuestions[i].parts[j].value = $scope.questions[i].parts[j].value;
					if(max.answer<$scope.questions[i].parts[j].value){
						max.name = $scope.questions[i].parts[j].name;
						max.text = $scope.questions[i].parts[j].text;
						max.answer = $scope.questions[i].parts[j].value;
						if($scope.questions[i].parts[j].name == "head"){
							if($scope.questions[i].parts[j].associate[3].value == 1){
								max.associate = $scope.questions[i].parts[j].associate[0].text;
							}
						}
						else{
							if($scope.questions[i].parts[j].associate[0].value == 1){
								max.associate = $scope.questions[i].parts[j].associate[0].text;
							}
						}
					}
				}
			}
			if(answer.length>0){
				$scope.healthTracker.physicalSymptoms.push({
					"name":$scope.questions[i].name,
					"answer":answer,
					"highest":max,
					"qIndex":i			
				});
			}
		}
		$scope.landingPage.activeFinalSubmit = true;
		$scope.trackerDate.enable = true;
		$ionicScrollDelegate.scrollTop();
		$ionicScrollDelegate.resize();
		$scope.selectIssue = false;
		$scope.checkValueFilled();
		//$state.go('app.healthTrackerLandingPage');
	}

	$scope.gotoHeathTracker = function(){
		if (typeof window.ga !== "undefined") {
      window.ga.trackEvent('Health Tracker - Physical Symptoms', 'Health Tracker Screen (Physical Symptoms)', 'Health Tracker Screen', 1);
    }
		$scope.trackerDate.enable = true;
		$ionicScrollDelegate.scrollTop();
		$ionicScrollDelegate.resize();
		$state.go('app.healthTrackerLandingPage');
	}

	$scope.$on('$destroy',function(){
		$scope.gotoHeathTracker();
	})

}]);