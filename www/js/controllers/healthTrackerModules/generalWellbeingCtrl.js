neuroScienceApp.controller('healthTrackerGeneralWellbeingCtrl', ['$scope', '$state','$ionicScrollDelegate','$timeout', function ($scope, $state,$ionicScrollDelegate,$timeout) {

  $timeout(function(){
    window.ga.trackView('Health Tracker - General Well being');
  }, 1000);

  $scope.healthTracker.generalWellBeing != undefined ? $scope.isDirty=true: $scope.isDirty=false;
  $scope.addClassonMode = function(index,value){
    switch(value){
      case 1:  $scope.testResult[index] = 'one'; break;
      case 2:  $scope.testResult[index] = 'two'; break;
      case 3:  $scope.testResult[index] = 'three'; break;
      case 4:  $scope.testResult[index] = 'four'; break;
      case 5:  $scope.testResult[index] = 'five';break;
      default: $scope.testResult[index] = 'zero';
    }  
  }
  $scope.knobtype = "wellbeing";
  $scope.value = 1;
  $scope.trackerDate.enable = false;
  $timeout(function() {
    $ionicScrollDelegate.scrollBottom(); 
  }, 1000);
  $ionicScrollDelegate.resize();
  $scope.testResult=['',''];
  $scope.questions= angular.copy($scope.generalWellbeingQuestions);
  $scope.addClassonMode(0,$scope.questions[0].value);
  $scope.addClassonMode(1,$scope.questions[1].value);
  $scope.value = $scope.questions[0].value;
  $scope.options = {
    startAngle: -90,
    endAngle: 90,
    displayPrevious: true,
    prevBarColor: 'rgba(0,0,0,0)',
    trackColor: 'rgba(0,0,0,.2)',
    trackWidth: 20,
    barWidth: 5,
    textColor:'#000',
    scale: {
      enabled: true,
      type: 'triangle',
      color: 'gray',
      width: 1,
      quantity: 5,
      height: 8
    },
    min:1,
    max: 5,
    step:1,
    icons:['img/meterIcons/1.png','img/meterIcons/2.png','img/meterIcons/3.png','img/meterIcons/4.png','img/meterIcons/5.png'],
    size : parseInt((3 * screen.width)/4),
    svgwidth:parseInt((76 * screen.width)/100),
    svgheight: parseInt((45 * screen.width)/100),
    groupLineArc : parseInt((3 * screen.width)/8)
  };

  $scope.value1 = $scope.questions[1].value;
  $scope.options1 = {
    startAngle: -90,
    endAngle: 90,
    displayPrevious: true,
    prevBarColor: 'rgba(0,0,0,0)',
    trackColor: 'rgba(0,0,0,.2)',
    trackWidth: 20,
    barWidth: 5,
    textColor:'#000',
    scale: {
      enabled: true,
      type: 'triangle',
      color: 'gray',
      width: 1,
      quantity: 5,
      height: 8
    },
    min:1,
    max: 5,
    step:1,
    icons:['img/meterIcons/1.png','img/meterIcons/2.png','img/meterIcons/3.png','img/meterIcons/4.png','img/meterIcons/5.png'],
    svgwidth:parseInt((76 * screen.width)/100),
    size : parseInt((3 * screen.width)/4),
    svgheight: parseInt((45 * screen.width)/100),
    groupLineArc : parseInt((3 * screen.width)/8)
  };

  $scope.Changefx = function(index,value){
    $scope.isDirty=true;
    $scope.questions[index].value=value;
    $scope.addClassonMode(index,value);
  }	

  $scope.submit = function(){
    if (typeof window.ga !== "undefined") {
      window.ga.trackEvent('Health Tracker - General Well Being', 'Save Entry and continue', 'Continue', 1);
    }
    $scope.healthTracker.generalWellBeing = [];
    for(var i=0;i<$scope.questions.length;i++){
      if($scope.questions[i].value!=0 && $scope.questions[i].value!=""){
       $scope.healthTracker.generalWellBeing.push({
          "name":$scope.questions[i].name,
          "answer":$scope.questions[i].value,
          "qIndex":i
        });
       $scope.generalWellbeingQuestions[i].value =  $scope.questions[i].value;
      }
    }
    $scope.$parent.landingPage.activeFinalSubmit = true;
    $scope.trackerDate.enable = true;
    $ionicScrollDelegate.scrollTop();
    $ionicScrollDelegate.resize();
    $state.go('app.healthTrackerLandingPage');
	}
  
  $scope.goBackHeathTracker = function(){
    // $scope.restGeneralWellBeing();
    // delete($scope.healthTracker.generalWellBeing);
    if (typeof window.ga !== "undefined") {
      window.ga.trackEvent('Health Tracker - General Well Being', 'Back to Health Tracker Screen (General Well Being)', 'Back', 1);
    }
    $scope.trackerDate.enable = true;
    $ionicScrollDelegate.scrollTop();
    $ionicScrollDelegate.resize();
    $state.go('app.healthTrackerLandingPage');
  }

  $scope.holdKnob = function(){
    console.log("hold");
    $ionicScrollDelegate.freezeAllScrolls(true);
  }

  $scope.tabKnob = function(){
    $ionicScrollDelegate.freezeAllScrolls(true);
    console.log("tab");
  }

  $scope.releaseKnob = function(){
    console.log("release");
    $ionicScrollDelegate.freezeAllScrolls(false);
  }

  $scope.$on('$destroy',function(){
    $scope.goBackHeathTracker();
  })

}]);