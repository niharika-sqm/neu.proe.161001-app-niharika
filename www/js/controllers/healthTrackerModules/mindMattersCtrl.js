neuroScienceApp.controller('healthTrackerMindMattersCtrl', ['$scope', '$state','$timeout','$ionicScrollDelegate', function ($scope, $state,$timeout,$ionicScrollDelegate) {
	$scope.healthTracker.mindMatters != undefined ? $scope.isDirty=true: $scope.isDirty=false;
	$scope.trackerDate.enable = false;
	$ionicScrollDelegate.scrollTop();
	$ionicScrollDelegate.resize();

	$timeout(function(){
    window.ga.trackView('Health Tracker - Mind Matters');
  }, 1000);

	$scope.questions= angular.copy($scope.mindMattersQuestions);
	$scope.checkFormisDirty = function(){
		$scope.isDirty = false;
		for(var i=0;i<$scope.questions.length;i++){
			if($scope.questions[i].value > 0){
				$scope.isDirty = true;
				break;
			}
		}
	}
	$scope.setMindMattersValue = function(symptom,level){
		if(symptom.value == level){
			symptom.value = 0;
		}
		else{
			symptom.value = level;
		}	
		$scope.checkFormisDirty();
	}
	$scope.submit = function(){
		if (typeof window.ga !== "undefined") {
      window.ga.trackEvent('Health Tracker - Mind Matters', 'Save Entry and continue', 'Continue', 1);
    }
		$scope.healthTracker.mindMatters = [];
		for(var i=0;i<$scope.questions.length;i++){
			if($scope.questions[i].value!=0 && $scope.questions[i].value!=""){
				$scope.healthTracker.mindMatters.push({
					"name":$scope.questions[i].name,
					"answer":$scope.questions[i].value,
					"qIndex":i
				});
				$scope.mindMattersQuestions[i].value = $scope.questions[i].value;
			}
		}
		if($scope.healthTracker.mindMatters.length == 0){
			delete($scope.healthTracker.mindMatters);
		}
		$scope.landingPage.activeFinalSubmit = true;
		$scope.trackerDate.enable = true;
		$ionicScrollDelegate.scrollTop();
		$ionicScrollDelegate.resize();
		//$scope.restMindMatters();
		$scope.setRestSubCatagory('mindMattersQuestions',$scope.questions);
		$state.go('app.healthTrackerLandingPage');
	}

	$scope.goBackHeathTracker = function(){
		// $scope.restMindMatters();
		// delete($scope.healthTracker.mindMatters);
		if (typeof window.ga !== "undefined") {
      window.ga.trackEvent('Health Tracker - Mind Matters', 'Health Tracker Screen (Mind Matters)', 'Back', 1);
    }
		$scope.trackerDate.enable = true;
		$ionicScrollDelegate.scrollTop();
		$ionicScrollDelegate.resize();
		$state.go('app.healthTrackerLandingPage');
	}

	$scope.$on('$destroy',function(){
		$scope.goBackHeathTracker();
	})
	
}]);