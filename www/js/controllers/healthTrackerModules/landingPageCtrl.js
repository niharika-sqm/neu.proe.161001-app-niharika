neuroScienceApp.controller('healthTrackerLandingPageCtrl', ['$scope', '$state', '$kinvey', '$ionicScrollDelegate', '$q', 'CONSTANT','$ionicPopup','$ionicSideMenuDelegate','closePopupService','$timeout', function($scope, $state, $kinvey, $ionicScrollDelegate, $q, CONSTANT,$ionicPopup,$ionicSideMenuDelegate,closePopupService,$timeout) {

  $scope.$on('$ionicView.afterEnter', function() {
    $scope.initTrackerLanding();
  });

  $timeout(function(){
    window.ga.trackView('Health Tracker Landing Screen');
  }, 1000);

  $scope.replaceTrackingOnHealthTracker = function(){
    if (typeof window.ga !== "undefined") {
      window.ga.trackEvent('Health Tracker','Start / Stop Relapse (Health Tracker)', 'Relapse', 1);
    }
    $scope.replaceTracking();
  }

  $scope.initTrackerLanding = function() {
    // $scope.unlockOrientation();
    $scope.landingPage = {};
    $scope.landingPage.title = "Health Tracker Landing Page";
    $scope.landingPage.activeFinalSubmit = false;
    $scope.trackerDate = {};
    $scope.trackerDate.entryDate = new Date();
    $scope.trackerDate.enable = true;
    $scope.trackerDate.min = moment().subtract(30, 'days').format('YYYY-MM-DD');
    $scope.trackerDate.max = moment().add(1, 'days').format('YYYY-MM-DD');
    $scope.trackerDate.entryDateFormat = moment().format('dddd') + ", " + moment().format('LL');
    $scope.healthTracker = {};
    $ionicScrollDelegate.scrollTop();
    $ionicScrollDelegate.resize();
    $scope.getTrackerDatafromDate();
    $scope.healthTackerSubmitBtn = "Submit Report";
    $scope.relapseHealthTracker = "";
    $scope.previousTrackerId = "";
    $scope.healthTrackerReadOnly = false;
    $scope.getRelapseEntryData($kinvey.User.getActiveUser()._id);
  }

  $scope.restDailyLifeSymptoms = function() {
    $scope.dailyLifeSymptomsQuestions = [{ id: 1, text: "Bladder problems", value: 0, name: "bladderProblems" }, { id: 2, text: "Bowel problems", value: 0, name: "bowelProblems" }, { id: 3, text: "Dizziness", value: 0, name: "dizziness" }, { id: 4, text: "Heat sensitivity", value: 0, name: "heatSensitivity" }, { id: 5, text: "Sexual dysfunction", value: 0, name: "sexualDysfunction" }, { id: 6, text: "Vision problems", value: 0, name: "visionProblems" }, { id: 6, text: "Walking difficulties", value: 0, name: "walkingDifficulties" }];
  }

  $scope.restMindMatters = function() {
    $scope.mindMattersQuestions = [{ id: 1, text: "Abnormal Stress", value: 0, name: "abnormalStress" }, { id: 2, text: "Anxiety", value: 0, name: "anxiety" }, { id: 3, text: "Cognitive problems (thinking/memory/ attention)", value: 0, name: "cognitive" }, { id: 4, text: "Depression", value: 0, name: "depression" }, { id: 5, text: "Fatigue", value: 0, name: "fatigue" }];
  }

  $scope.restGeneralWellBeing = function() {
    $scope.generalWellbeingQuestions = [{ id: 1, text: "How is your energy today?", value: 0, name: "energy" }, { id: 2, text: "How are you feeling today?", value: 0, name: "feeling" }];
  }

  $scope.restPhysicalSymptoms = function() {  //order of question is fixed index value is using in Physical symptom page.
    $scope.physicalSymptomsQuestions = [{ id: 1, text: "Muscle Spasms", head: false, parts: [], name: "muscleSpasms" }, { id: 2, text: "Muscle Weakness", head: false, parts: [], name: "weakness" }, { id: 3, text: "Numbness/Tingling", head: false, parts: [], name: "numbnessTingling" }, { id: 4, text: "Pain", head: true, parts: [], name: "pain" }];

    var parts = [{ partId: 1, text: "HEAD:", highChartName: "Head", value: 0, name: "head", associate: [{ text: "Headache", value: 0 }, { text: "Eye Pain", value: 0 }, { text: "Facial Pain", value: 0 }, { text: "Neck", value: 0 }] }, { partId: 2, text: "RIGHT ARM:", highChartName: "Right Arm", value: 0, name: "rightArm", associate: [{ text: "Neck", value: 0 }] }, { partId: 3, text: "LEFT ARM:", highChartName: "Left Arm", value: 0, name: "leftArm", associate: [{ text: "Neck", value: 0 }] }, { partId: 4, text: "TORSO:", highChartName: "Torso", value: 0, name: "torso", associate: [{ text: "Neck", value: 0 }] }, { partId: 5, text: "RIGHT LEG:", highChartName: "Right Leg", value: 0, name: "rightLeg", associate: [{ text: "Neck", value: 0 }] }, { partId: 6, text: "LEFT LEG:", highChartName: "Left Leg", value: 0, name: "leftLeg", associate: [{ text: "Neck", value: 0 }] }]; // index is using in physical symptoms 

    for (var i = 0; i < $scope.physicalSymptomsQuestions.length; i++) {
      $scope.physicalSymptomsQuestions[i].parts = angular.copy(parts);
    }
  }
  $scope.restDailyLifeSymptoms();
  $scope.restMindMatters();
  $scope.restGeneralWellBeing();
  $scope.restPhysicalSymptoms();
  var trackerAnswerCollection = $kinvey.DataStore.getInstance('healthTrackerAnswers', $kinvey.DataStoreType.Network);
  var relapseList = $kinvey.DataStore.getInstance('relapseList', $kinvey.DataStoreType.Network);
  var pointsCollection = $kinvey.DataStore.getInstance('gamificationPointsList', $kinvey.DataStoreType.Network);

  $scope.setRestSubCatagory = function(object,data){
    $scope[object] = data;
  }

  $scope.healthTestDateChange = function() {
    if (moment(moment().format("YYYY-MM-DD") + "T00:00:00.000Z").diff(moment(moment($scope.trackerDate.entryDate).format("YYYY-MM-DD") + "T00:00:00.000Z"), 'days') >= 0 && moment().diff(moment($scope.trackerDate.entryDate), 'days') < 31) {
      $scope.trackerDate.entryDateFormat = moment($scope.trackerDate.entryDate).format('dddd') + ", " + moment($scope.trackerDate.entryDate).format('LL');
      $scope.getTrackerDatafromDate();
      $scope.healthTrackerReadOnly = false;
    } else if (moment().diff(moment($scope.trackerDate.entryDate), 'days') >= 31) {
      $scope.trackerDate.entryDateFormat = moment($scope.trackerDate.entryDate).format('dddd') + ", " + moment($scope.trackerDate.entryDate).format('LL');
      $scope.getTrackerDatafromDate();
      $scope.healthTrackerReadOnly = true;
    } else {
      $scope.trackerDate.entryDate = new Date();
      $scope.trackerDate.entryDateFormat = moment().format('dddd') + ", " + moment().format('LL');
      $scope.getTrackerDatafromDate();
      $scope.healthTrackerReadOnly = false;
    }
  }
  $scope.$on('relapseHealthBroadcast', function() {
    if ($scope.relapseTrack.startRelapse && (moment($scope.trackerDate.entryDate).diff(moment($scope.relapseHealthTracker.startDate), 'days') >= 0)) {
      $scope.relapseHealthTracker = $scope.relapseTrack.kinveyData;
    }
  });

  $scope.getTrackerDatafromDate = function() {
    $scope.showLoader();
    var users = $kinvey.User.getActiveUser();
    var col = "_acl";
    var relatedJson = { "creator": users.data._id };
    var query = new $kinvey.Query();
    query.equalTo(col, relatedJson);
    col = "trackerEntryDate";
    relatedJson = moment($scope.trackerDate.entryDate).format("YYYY-MM-DD");
    var query1 = new $kinvey.Query();
    query1.equalTo(col, relatedJson);
    query.and(query1);
    trackerAnswerCollection.find(query).subscribe(function(data) {
      $scope.trackerData = data;
      console.log("ddd");
    }, function(error) {
      $scope.internalGlitch();
    }, function(data) {
      if ($scope.trackerData.length > 0) {
        $scope.healthTackerSubmitBtn = "Resubmit Report";
        $scope.previousTrackerId = $scope.trackerData[0]._id;
        $scope.healthTracker = $scope.trackerData[0].response;
        $scope.setTrackerData($scope.trackerData[0].response);
      } else {
        $scope.healthTackerSubmitBtn = "Submit Report";
        $scope.previousTrackerId = "";
        $scope.healthTracker = {};
        $scope.restDailyLifeSymptoms();
        $scope.restMindMatters();
        $scope.restGeneralWellBeing();
        $scope.restPhysicalSymptoms();
        $scope.hideLoader();
      }
      if ($scope.relapseTrack.startRelapse && (moment(moment($scope.trackerDate.entryDate).format("YYYY-MM-DD") + "T00:00:00.000Z").diff(moment($scope.relapseHealthTracker.startDate), 'days') >= 0)) {
        $scope.relapseHealthTracker = $scope.relapseTrack.kinveyData;
      } else {
        $scope.getRelapseHistoryforTracker();
      }
    });
  }

  $scope.getRelapseHistoryforTracker = function() {
    var users = $kinvey.User.getActiveUser();
    var col = "_acl";
    var relatedJson = { "creator": users.data._id };
    var kmdStartDate = moment($scope.trackerDate.entryDate).format("YYYY-MM-DD") + "T00:00:00.000Z"
    var kmdEndDate = moment($scope.trackerDate.entryDate).add(1, 'days').format("YYYY-MM-DD") + "T00:00:00.000Z";
    var query = new $kinvey.Query();
    query.equalTo(col, relatedJson);
    var query1 = new $kinvey.Query();
    var kmd = "startDate";
    query1.greaterThanOrEqualTo(kmd, kmdStartDate);
    query.and(query1);
    var query2 = new $kinvey.Query();
    kmd = "endDate";
    query2.lessThan(kmd, kmdEndDate);
    query.and(query2);
    relapseList.find(query).subscribe(function(data) {
      $scope.trackerData = data;
      return data
    }, function(error) {
      $scope.internalGlitch();
    }, function(data) {
      if ($scope.trackerData.length > 0) {
        $scope.relapseHealthTracker = $scope.trackerData[0];
        $scope.$apply();
      } else {
        $scope.relapseHealthTracker = "";
        $scope.$apply();
      }
      
    });
  }

  $scope.setTrackerData = function(data) {
    if (data.physicalSymptoms != undefined) {
      for (var i = 0; i < data.physicalSymptoms.length; i++) {
        for (var j = 0; j < data.physicalSymptoms[i].answer.length; j++) {
          if (data.physicalSymptoms[i].answer[j].name == "head") {
            if(data.physicalSymptoms[i].answer[j].associate){
              for (var z = 0; z < data.physicalSymptoms[i].answer[j].associate.length; z++) {
                if (data.physicalSymptoms[i].answer[j].associate[z] == "Headache")
                  $scope.physicalSymptomsQuestions[data.physicalSymptoms[i].qIndex].parts[data.physicalSymptoms[i].answer[j].pIndex].associate[0].value = 1;
                else if (data.physicalSymptoms[i].answer[j].associate[z] == "Eye Pain")
                  $scope.physicalSymptomsQuestions[data.physicalSymptoms[i].qIndex].parts[data.physicalSymptoms[i].answer[j].pIndex].associate[1].value = 1;
                else if (data.physicalSymptoms[i].answer[j].associate[z] == "Facial Pain")
                  $scope.physicalSymptomsQuestions[data.physicalSymptoms[i].qIndex].parts[data.physicalSymptoms[i].answer[j].pIndex].associate[2].value = 1;
                else if (data.physicalSymptoms[i].answer[j].associate[z] == "Neck")
                  $scope.physicalSymptomsQuestions[data.physicalSymptoms[i].qIndex].parts[data.physicalSymptoms[i].answer[j].pIndex].associate[3].value = 1;
              }
            }      
          } else if (data.physicalSymptoms[i].answer[j].associate != undefined) {
            $scope.physicalSymptomsQuestions[data.physicalSymptoms[i].qIndex].parts[data.physicalSymptoms[i].answer[j].pIndex].associate[0].value = 1;
          }
          $scope.physicalSymptomsQuestions[data.physicalSymptoms[i].qIndex].parts[data.physicalSymptoms[i].answer[j].pIndex].value = data.physicalSymptoms[i].answer[j].answer;
        }
      }
    }
    else{
      $scope.restPhysicalSymptoms();
    }
    if (data.generalWellBeing != undefined) {
      for (var i = 0; i < data.generalWellBeing.length; i++) {
        $scope.generalWellbeingQuestions[data.generalWellBeing[i].qIndex].value = data.generalWellBeing[i].answer;
      }
    }
    else{
      $scope.restGeneralWellBeing();
    }
    if (data.mindMatters != undefined) {
      for (var i = 0; i < data.mindMatters.length; i++) {
        $scope.mindMattersQuestions[data.mindMatters[i].qIndex].value = data.mindMatters[i].answer;
      }
    }
    else{
      $scope.restMindMatters();
    }
    if (data.dailyLifeSymptoms != undefined) {
      for (var i = 0; i < data.dailyLifeSymptoms.length; i++) {
        $scope.dailyLifeSymptomsQuestions[data.dailyLifeSymptoms[i].qIndex].value = data.dailyLifeSymptoms[i].answer;
      }
    }
    else{
      $scope.restDailyLifeSymptoms();
    }
    $scope.hideLoader();
  }

  $scope.saveTrackerDataToRelapse = function(otherActions) {    
    if ($scope.relapseHealthTracker.healthTrackerIds == undefined || $scope.relapseHealthTracker.healthTrackerIds == "") {
      $scope.relapseHealthTracker.healthTrackerIds = [];
    }
    var relapseCheck = true;
    for (var i = 0; i < $scope.relapseHealthTracker.healthTrackerIds.length; i++) {
      if ($scope.relapseHealthTracker.healthTrackerIds[i].healthTrackerId == $scope.previousTrackerId) {
        relapseCheck = false;
        break;
      }
    }
    if (relapseCheck) {
      $scope.relapseHealthTracker.healthTrackerIds.push({ "healthTrackerId": $scope.previousTrackerId });
      $scope.relapseHealthTracker = JSON.parse(angular.toJson($scope.relapseHealthTracker));
      relapseList.update($scope.relapseHealthTracker).then(function(entity) {
        if ($scope.trackerDate.entryDate != "") {
          $scope.saveHealthTrackerPoints();
        } else {
          $scope.hideLoader();
          $scope.healthTracker = {};
          $scope.$emit('refreshRelapseData');
          $scope.$emit('refreshHighChart');
          $scope.hideLoader();
          if(otherActions){
            $ionicSideMenuDelegate.toggleLeft();
          }else{
            //$state.go("app.home");
            /*JVI pre home page implementation*/
            $scope.navigateToHome();
          }
          $scope.showMessage('Report submitted', 3000);
        }
        console.log(JSON.stringify(entity));
      }).catch(function(error) {
        $scope.internalGlitch();
        console.log(error);
      });
    } else {
      $scope.hideLoader();
      $scope.healthTracker = {};
      $scope.$emit('refreshHighChart');
      $scope.hideLoader();
      if(otherActions){
        $ionicSideMenuDelegate.toggleLeft();
      }else{
        //$state.go("app.home");
        /*JVI pre home page implementation*/
        $scope.navigateToHome();
      }
      $scope.showMessage('Report Submitted', 3000);
    }
  }

  $scope.submitHandler = function(otherActions) {
    $scope.showLoader();
    $scope.saveTrackerData($scope.previousTrackerId, moment($scope.trackerDate.entryDate).format("YYYY-MM-DD"),otherActions);
  }

  $scope.saveTrackerData = function(id, trackerDate,otherActions) {
    if ($scope.relapseHealthTracker != "" && $scope.relapseHealthTracker._id != "" && ($scope.healthTracker.relapseTrackerId == undefined || $scope.healthTracker.relapseTrackerId == "")) {
      $scope.healthTracker.relapseTrackerId = $scope.relapseHealthTracker._id;
    }
    if (id == "") {
      var promise = trackerAnswerCollection.save({
        "trackerHighChartDate": $scope.trackerDate.entryDate,
        "trackerEntryDate": trackerDate,
        "response": JSON.parse(angular.toJson($scope.healthTracker))
      }).then(function(entity) {
        if (typeof window.ga !== "undefined") {
          window.ga.trackEvent('Health Tracker', 'Submit Entry (Health Tracker)', 'Submit', 1);
        }
        if(moment().format("YYYY-MM-DD") == moment($scope.trackerDate.entryDate).format("YYYY-MM-DD")){
          $scope.healthTackerProperty.isHealthTackerTakenToday = true;
        }
        if ($scope.relapseHealthTracker._id != undefined && $scope.relapseHealthTracker._id != "") {
          $scope.previousTrackerId = entity._id;
          $scope.saveTrackerDataToRelapse(otherActions);
        } else {
          $scope.saveHealthTrackerPoints(otherActions);
        }
      }).catch(function(error) {
        $scope.internalGlitch();
        console.log(error);
      });
    } else {
      var promise = trackerAnswerCollection.save({
        "_id": id,
        "trackerHighChartDate": $scope.trackerDate.entryDate,
        "trackerEntryDate": trackerDate,
        "response": JSON.parse(angular.toJson($scope.healthTracker))
      }).then(function(entity) {
        if (typeof window.ga !== "undefined") {
          window.ga.trackEvent('Health Tracker', 'Re-Submit Entry (Health Tracker)', 'Re-Submit', 1);
        }
        if(moment().format("YYYY-MM-DD") == moment($scope.trackerDate.entryDate).format("YYYY-MM-DD")){
          $scope.healthTackerProperty.isHealthTackerTakenToday = true;
        }
        if ($scope.relapseHealthTracker._id != undefined && $scope.relapseHealthTracker._id != "" && (entity.response.relapseTrackerId == undefined || entity.response.relapseTrackerId == "")) {
          $scope.previousTrackerId = entity._id;
          $scope.saveTrackerDataToRelapse(otherActions);
        } else {
          $scope.healthTracker = {};
          $scope.$emit('refreshHighChart');
          $scope.hideLoader();
          if(otherActions){
            $ionicSideMenuDelegate.toggleLeft();
          }else{
            //$state.go("app.home");
            /*JVI pre home page implementation*/
            $scope.navigateToHome();
          }
          $scope.showMessage('Report submitted', 3000);
        }
      }).catch(function(error) {
        $scope.internalGlitch();
        console.log(error);
      });
    }
  }

  $scope.saveHealthTrackerPoints = function(otherActions) {
    var promise = new $q(function(resolve) {
      resolve($kinvey.User.getActiveUser());
    });
    promise.then(function(user) {
      if (user) {
        var points = {}
        if (user.data.points.latestPoints.length > 2) {
          user.data.points.latestPoints.shift();
        }
        user.data.points.latestPoints.push({ "points": CONSTANT.gamificationPoints.healthTracker.value, "stage": CONSTANT.gamificationPoints.healthTracker.name });
        user.data.points.currentPoints += CONSTANT.gamificationPoints.healthTracker.value;
        var usersDS = new $kinvey.UserStore();
        usersDS.update(user.data)
          .then(function(user) {
            return user
            console.log(JSON.stringify(user));
          }).catch(function(error) {
            $scope.internalGlitch();
            console.log(error)
          });
      }
      return user;
    }).then(function(user) {
      if (user) {
        var promise = pointsCollection.save({
          "points": CONSTANT.gamificationPoints.healthTracker.value,
          "stage": CONSTANT.gamificationPoints.healthTracker.name
        }).then(function(entity) {
          $scope.hideLoader();
          $scope.healthTracker = {};
          $scope.$emit('refreshRelapseData');
          $scope.$emit('refreshHighChart');
          $scope.hideLoader();
          if(otherActions){
            $ionicSideMenuDelegate.toggleLeft();
          }else{
            //$state.go("app.home");
            /*JVI pre home page implementation*/
            $scope.navigateToHome();
          }
          $scope.showMessage('Report submitted', 3000);
        }).catch(function(error) {
          console.log(error);
        });
      } else {}
    }).catch(function(error) {
      $scope.internalGlitch();
      console.log(error);
    });
  }

  $scope.showPopOnExitTracker = function(){
    $scope.trackerDate.enable = true;
    if(($scope.healthTracker.mindMatters!=undefined || $scope.healthTracker.dailyLifeSymptoms!=undefined|| $scope.healthTracker.physicalSymptoms!=undefined || $scope.healthTracker.generalWellBeing!=undefined)){
      $scope.cancelTestPopup = $ionicPopup.show({
        title:"Don't forget to submit your report before leaving the Health Tracker.",
        template:'<div class="height10"></div><a class="popClose" ng-click="cancelTestPopup.close()"></a>',
        scope: $scope,
        buttons: [{
          text: 'No thanks, delete report',
          type: 'button-block button-outline line1 bold button-darkPurple skip font40',
          onTap: function() {
            return true;
          }
        },{
          text: 'Submit report',
          type: 'button-block button-darkPurple bold',
          onTap: function() {
            $scope.submitTrackerPopup = true;
            return false;
          }
        }]
      });
      closePopupService.register($scope.cancelTestPopup);
      $scope.cancelTestPopup.then(function(res){
        if(res){
          $ionicSideMenuDelegate.toggleLeft();
        }else{
          if($scope.submitTrackerPopup){
            $scope.submitTrackerPopup = false;
            $scope.submitHandler('opneSideMenu');
          }
          else{
            $ionicSideMenuDelegate.toggleLeft();
          }
        }
      });
    }else{
      $ionicSideMenuDelegate.toggleLeft();
    }
  }

  $scope.$on('showPopOnExitTrackerForNotification', function() {
    $scope.showPopOnExitTracker();
  });

}]);
