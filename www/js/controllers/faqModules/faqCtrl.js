neuroScienceApp.controller('faqCtrl', ['$scope', '$state','$ionicScrollDelegate','$timeout', function ($scope, $state,$ionicScrollDelegate,$timeout) {

	console.log("After Login");
    $timeout(function(){
      window.ga.trackView('FAQs(After Login)');
    }, 1000);
 	$scope.dataset = [{
		"q": "Is my information secure?",
		"a": "MS Care Connect keeps all information private according to the Health Insurance Portability and Accountability Act guidelines provided by the government. All information is encrypted to ensure security, and comparisons to other patients are blinded and anonymous.",
		"show": false
	},
	{
		"q": "How do I connect with my healthcare provider?",
		"a": "If you elect to have your information sent to your healthcare provider, we will automatically email him/her to register.",
		"show": false
	},
	{
		"q":"Why don't I see my healthcare provider on the list?",
		"a":"Only MS specialists are in the database. Your general practicing physician will not be an option.",
		"show": false
	},
	{
		"q":"What if my healthcare provider doesn’t register?",
		"a":"You are still able to print out or email your healthcare provider all of your reports from your dashboard, so that you can have them at your next appointment.",
		"show": false
	},
	{
		"q":"How often will I receive a survey?",
		"a":"You will receive a survey every 10 days.",
		"show": false
	},
	{
		"q":"What is a relapse?",
		"a":"A relapse involves the appearance of new symptoms, or the worsening of symptoms, in the absence of a fever. Relapses last for 24 hours or more, and are separated from previous relapses by at least 30 days.",
		"show": false
	},
	{
		"q":"How often should I fill out a health tracker or survey, or take a performance measure?",
		"a":"You can fill out a health tracker daily in order to best maintain your information for your healthcare provider. We recommend filling out the health tracker at least weekly. The surveys become available every 10 days. Performance measures are only accessible every 6 months.",
		"show": false
	},
	{
		"q":"Why did my relapse tracking expire?",
		"a":"If you do not stop the relapse tracker then it automatically expires after 28 days.",
		"show": false
	},
	{
		"q":"What is the URL for my MS Care Connect patient dashboard?",
		"a":"MSCareConnect.com <br>Log in to your dashboard on your desktop computer to view your results over time, download and email your results, and even compare how you're doing with other MS Care Connect users.",
		"show": false
	},
	{
		"q":"What happens if I fail a performance metric?",
		"a":"You cannot fail a performance metric. You will receive a score no matter how you perform. Performance tests are simply administered to assess the impact of MS over time, so you and your healthcare provider can track your disease.",
		"show": false
	},
	{
		"q":"Can I share my reports with friends and family?",
		"a":"Yes, you can download and email all your reports via your patient dashboard.",
		"show": false
	}];

	$scope.toggleGroup = function(group) {
		group.show = !group.show;
		$timeout(function(){
			$ionicScrollDelegate.$getByHandle('scrollResize').resize();
		}, 450);
	};
	$scope.isGroupShown = function(group) {
		return group.show;
	};
	
}]);