neuroScienceApp.controller('homeCtrl', ['$scope', '$state', '$timeout', '$kinvey', '$q', 'CONFIG', 'utility','CONSTANT','getSetDeviceUUID', function($scope, $state, $timeout, $kinvey, $q, CONFIG, utility,CONSTANT,getSetDeviceUUID) {
  $scope.$on("$ionicView.afterEnter", function() {
    ionic.Platform.fullScreen();
  });

  $timeout(function(){
    window.ga.trackView('Home Screen');
  }, 1000);

  $scope.showLoader();
  $scope.healthTracker = function() {
    $state.go('app.healthTrackerLandingPage');
  }
  $scope.statistics = function() {
    $state.go('app.statistics');
  }
  $scope.tips = function() {
    $state.go('app.tips');
  }
  $scope.setupHeatAlerts = function() {
    $state.go('app.setupHeatAlerts');
  }

  $scope.replaceTrackingOnHome = function(){
    if (typeof window.ga !== "undefined") {
      window.ga.trackEvent('Home','Start / Stop Relapse (Home)', 'Relapse', 1);
    }
    $scope.replaceTracking();
  }

  var gamificationLevels = $kinvey.DataStore.getInstance('gamificationLevels', $kinvey.DataStoreType.Network);
  var notificationTestList = $kinvey.DataStore.getInstance('notifications', $kinvey.DataStoreType.Network);
  $scope.heatAlertsHome = {};
  $scope.heatAlertLoc = false;
  $scope.heatAlertTemp = false;
  $scope.userLevelData = { "currentLevel": 0, "currentPoints": "", "maxPoints": "", "latestPoints": [], "loaderWidth": "" };

  $scope.getActiveUser = function() { //get user and check lat & long
    var promise = new $q(function(resolve) {
      resolve($kinvey.User.getActiveUser());
    });
    promise.then(function(user) {
      if (user) {
        return user.me();
      }
    }).then(function(user) {
      if (user) {
        console.log(user);
        $scope.userData = user.data;
        //$scope.treatmentUpdatePopup(user.data);
        $scope.getRelapseEntryData(user._id);
        $scope.getLevelList();
        $scope.getPMSurveyNotificationNumber();
        if (user.data.heatAlerts != undefined) {
          $scope.heatAlertsHome = user.data.heatAlerts;
          $scope.heatAlertLoc = true;
          CONFIG.heatAlerts.latitude = user.data.heatAlerts.latitude;
          CONFIG.heatAlerts.longitude = user.data.heatAlerts.longitude;
          if (user.data.heatAlerts.maxTemperature && user.data.heatAlerts.maxTemperature.text != "NO ALERT SET") {
            $scope.heatAlertTemp = true;
          }
          // to get latest temperature on user
          if(moment(moment().format("YYYY-MM-DD") + "T00:00:00.000Z").diff(moment($scope.heatAlertsHome.entryDate + "T00:00:00.000Z"),'days')>=1){
            $scope.checkKinveyTemp();
          }
          //$scope.getTempFromForecastHome();
          console.log(user.data.heatAlerts);
        } else {
          $scope.exitAllLoader("forcastLoader");
        }
      } else {
        $timeout(function() {
          $state.go("login");
        }, 100);
      }
    }).catch(function(error) {
      $scope.internalGlitch();
      console.log(error);
      $scope.exitAllLoader("forcastLoader");
    }); 
  }

  $scope.updateTips = function() {
    if (localStorage.tipsInfo) {
      var updateUserTipsInfo = $kinvey.CustomEndpoint.execute('updateUserTipsInfo', { 'username': $kinvey.User.getActiveUser().username, "tipsArr": JSON.parse(localStorage.tipsInfo) });
      updateUserTipsInfo.then(function(response) {
        console.log("response on updateTips " + response);
        localStorage.removeItem('tipsInfo');
        $scope.getActiveUser();
      }).catch(function(err) {
        $scope.internalGlitch();
        console.log("response on error updateTips " + err);
        $scope.getActiveUser();
      });
    } else {
      $scope.getActiveUser();
    }
  }

  $scope.updateTips();


  $scope.getLevelList = function() { //get point level lists
    var query = new $kinvey.Query();
    query.ascending('levelNumber');
    gamificationLevels.find(query).subscribe(function(data) {
      $scope.levelList = data;
    }, function(error) {
      $scope.exitAllLoader("pointsLoader");
    }, function(data) {
      if ($scope.userData.points) {
        $scope.checkCurrentLevel();
      }
    }); 
  }

  /*$scope.getPMSurveyNotificationNumber = function() { //get number of test and survey
    var tempData = [];
    var query = new $kinvey.Query();
    query.equalTo("_acl", { "creator": $scope.userData._id });
    var query2 = new $kinvey.Query();
    query2.equalTo("isAck", false).equalTo('isSent', true).equalTo('isActive', true);
    var query3 = new $kinvey.Query();
    query3.equalTo("notificationType", "SURVEY");
    var query4 = new $kinvey.Query();
    query4.equalTo("notificationType", "PM");
    query.and(query2.and(query3.or(query4)));
    notificationTestList.find(query).subscribe(function(data) {
      tempData = data;
    }, function(error) {
      console.log(error);
      // $scope.exitLoader.numberOfTestLoader = true;
      // $scope.exitAllLoader();
    }, function(data) {
      console.log(tempData);
      for (var i = 0; i < tempData.length; i++) {
        if (tempData[i].notificationType == "PM") {
          $scope.homeScreenCount.PM++;
        } else {
          $scope.homeScreenCount.Survey++;
        }
      }
      $scope.exitAllLoader("numberOfTestLoader");
    });
  }*/

  $scope.checkCurrentLevel = function() { //check gamification level
    var i = 1;
    $scope.score = $scope.userData.points.currentPoints
    for (i = 1; i < 20; i++) {
      if ($scope.score > ((Math.pow((parseInt(i)), 7 / 5) * 50))) {
        $scope.score = $scope.score - (Math.pow((parseInt(i)), 7 / 5) * 50);
      } else {
        $scope.userLevelData.maxPoints = Math.round(Math.pow((parseInt(i)), 7 / 5) * 50);
        break;
      }
    }
    
    if($scope.userData.version == undefined || $scope.userData.version == "" || $scope.userData.version != CONSTANT.appVersion){
      $scope.userData.points.currentLevel = i;
      $scope.updateUserLevelAndVersion();
    }
    else if($scope.userData.points.currentLevel != i) {
      $scope.userData.points.currentLevel = i;
      $scope.updateUserLevel();
    } else {
      $scope.updatePointsScreen();
    } 
    //$scope.updatePointsScreen();
  }

  $scope.updatePointsScreen = function() { //update home screen
    $scope.userLevelData = angular.copy($scope.userData.points);
    $scope.userLevelData.maxPoints = Math.round(Math.pow((parseInt($scope.userData.points.currentLevel)), 7 / 5) * 50);
    $scope.userLevelData.currentPoints = Math.round($scope.score);
    $scope.userLevelData.latestPoints = $scope.userData.points.latestPoints;
    $scope.userLevelData.levelName = $scope.levelList[$scope.userData.points.currentLevel].levelName;
    $scope.homeScreen.loaderWidth = ($scope.userLevelData.currentPoints / $scope.userLevelData.maxPoints) * 100;
    $scope.userLevelData.loaderWidth = ($scope.userLevelData.currentPoints / $scope.userLevelData.maxPoints) * 100;
    //$scope.exitLoader.pointsLoader = true;
    //console.log($scope.exitLoader);
    $scope.getUUID();
  }

  $scope.updateUserLevelAndVersion = function(){
    var promise = new $q(function(resolve) {
      resolve($kinvey.User.getActiveUser().me());
    });
    promise.then(function(user) {
      if (user) {
        $scope.userData = JSON.parse(angular.toJson($scope.userData));
        return user.update({
          points: $scope.userData.points,
          version: CONSTANT.appVersion
        });
      }
    }).then(function(user) {
      console.log("updateUserLevel");
      console.log(user);
      $scope.updatePointsScreen();
    }).catch(function(error) {
      $scope.internalGlitch();
      console.log(error);
    }); 
  }

  $scope.updateUserLevel = function() { //update user level on kinvey
    var promise = new $q(function(resolve) {
      resolve($kinvey.User.getActiveUser().me());
    });
    promise.then(function(user) {
      if (user) {
        $scope.userData = JSON.parse(angular.toJson($scope.userData));
        return user.update({
          points: $scope.userData.points
        });
      }
    }).then(function(user) {
      console.log("updateUserLevel");
      console.log(user);
      $scope.updatePointsScreen();
    }).catch(function(error) {
    	$scope.internalGlitch();
      console.log(error);
    }); 
  }

  $scope.getUUID = function(){
    getSetDeviceUUID.getId()
    .then(function(data){
      $scope.updateDeviceDetails(data);
    },function(error){
      $scope.updateDeviceDetails(window.device.uuid);
    });
  }

  $scope.updateDeviceDetails = function(uuid) { //update user level on kinvey
    var promise = new $q(function(resolve) {
      resolve($kinvey.User.getActiveUser().me());
    });
    promise.then(function(user) {
      if (user) {
        var device = window.device;
        device.uuid = uuid;
        return user.update({
          deviceDetails: device
        });
      }
    }).then(function(user) {
      $scope.exitAllLoader("pointsLoader");  
      console.log("updateUserDevice");
      console.log(user);
    }).catch(function(error) {
      $scope.internalGlitch();
      console.log(error);
    }); 
  }


 /* $scope.getTempFromForecastHome = function() {
    utility.forecast()
      .then(function(data) { 
        if (data != undefined) {
          $scope.heatAlertsHome.temperatureMax = data.daily.data[0].temperatureMax;
          $scope.heatAlertsHome.apparentTemperatureMax = data.daily.data[0].apparentTemperatureMax;
          $scope.heatAlertsHome.humidity = data.daily.data[0].humidity;
          $scope.heatAlertLoc = true;
          $scope.exitAllLoader("forcastLoader");
          console.log(data);
        }
        $scope.exitAllLoader("forcastLoader");
      }, function(error) {
        $scope.internalGlitch();
        $scope.exitAllLoader("forcastLoader");
      });
  }*/

  $scope.checkKinveyTemp = function() {
    var promise = $kinvey.CustomEndpoint.execute('latLongApi', {
      latitude: $scope.heatAlertsHome.cityLat,
      longitude:$scope.heatAlertsHome.cityLng,
      checkLatLong: true
    });
    promise.then(function(response) {  
      if(response.maxTemp && response.maxTemp!=""){
        $scope.heatAlertsHome.temperatureMax = response.maxTemp;
        $scope.heatAlertsHome.apparentTemperatureMax = response.maxTempApparent;
        $scope.heatAlertsHome.humidity = response.humidity;
        $scope.heatAlertLoc = true;
        $scope.updateUserKinveyHeatAlert();
      }
    }).catch(function(err) {
      $scope.exitAllLoader("forcastLoader");
      $scope.internalGlitch();
    });
  }

  $scope.updateUserKinveyHeatAlert = function(type){
    $scope.showLoader();
    var promise = new $q(function(resolve) {
      resolve($kinvey.User.getActiveUser().me());
    });
    promise.then(function(user) {
      return user;
    }).then(function(user) {
      var usersDS = new $kinvey.UserStore();
      $scope.heatAlertsHome.entryDate = moment().format("YYYY-MM-DD");
      user.data.heatAlerts = JSON.parse(angular.toJson($scope.heatAlertsHome)); 
      usersDS.update(user.data)
      .then(function (user) {
         return user;
      }).then(function(user) {
        if(user){
          $scope.exitAllLoader("forcastLoader");
          console.log(JSON.stringify(user));
        }
      }).catch(function (error) {
        $scope.hideLoader();
        $scope.internalGlitch();
        console.log(error)
      });
    }).catch(function(error){
      console.log(error);
      $scope.hideLoader();
      $scope.internalGlitch();
    });
  }

  $scope.takeMeToHeatAlert = function(){
    if(!$scope.heatAlertTemp){
      return;
    }
    $scope.goToState('app.heatAlerts','Heat Alert Screen','Heat Alert Screen (Home)','Heat Alert Screen');
  }

}]);
