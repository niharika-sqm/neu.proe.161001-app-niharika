neuroScienceApp.controller('preHomeCtrl', ['$scope', '$state','$kinvey','$timeout','$interval','$ionicHistory','CONSTANT','$q', function($scope, $state,$kinvey,$timeout,$interval,$ionicHistory,CONSTANT,$q) {
  $scope.$on("$ionicView.afterEnter", function() {
    ionic.Platform.fullScreen();
  });
  $scope.init = function(){
    $scope.divClass = "";
    $scope.preHomeCount = { "PM": 0, "Survey": 0 };
    $scope.firstName =  $kinvey.User.getActiveUser().data.first_name;
    $scope.cascading = $interval(function () { 
      if($scope.divClass == ""){
        $scope.divClass = "health";
      }else if($scope.divClass == "health"){
        $scope.divClass = "survey";
      }else if($scope.divClass == "survey"){
        $scope.divClass = "pm";
      }else if($scope.divClass == "pm"){
        if (angular.isDefined($scope.cascading)) {
          $interval.cancel($scope.cascading);
        }
        $scope.divClass = "";
      }   
    },1000);
  }

  $scope.$on('$destroy',function(){
    if (angular.isDefined($scope.cascading)) {
      $interval.cancel($scope.cascading);
    }
  });
  
  $timeout(function(){
    window.ga.trackView('Pre Home Screen');
  }, 1000);

  $scope.goToHealthTracker = function() {
    window.ga.trackEvent('Health Tracker','Health Tracker Options (Pre Home)','Health Tracker Screen', 1);
    $state.go('app.healthTrackerLandingPage');
  }

  $scope.goToSurvey = function() {
    window.ga.trackEvent('Survey','List of pending Surveys (Pre Home)','Pending Surveys', 1);
    $state.go('app.surveysLandingPage');
  }

  $scope.goToPerformanceEvals = function() {
    window.ga.trackEvent('Performance Metric','Performance Metric Screen (Pre Home)','Performance Metric Screen', 1);
    $state.go('app.performanceEvals');
  }

  $scope.goToHome = function(){
    $scope.preHomePage(true);
    window.ga.trackEvent('Home','Home Screen (Pre Home)', 'Home Screen', 1);
    $state.go('app.home');
  } 

  $scope.selectOption = function(value){
    if (angular.isDefined($scope.cascading)) {
      $interval.cancel($scope.cascading);
    }
    $scope.divClass = value;
  }

  $scope.$on('updatePreHome', function(event, data) {
    /*$scope.preHomeCount = angular.copy(data);
    $scope.$apply();*/
  });

  $scope.updateUserVersion = function(){
    var promise = new $q(function(resolve) {
      resolve($kinvey.User.getActiveUser().me());
    });
    promise.then(function(user) {
      if (user) {
        if(user.data.version != CONSTANT.appVersion){
          return user.update({
            version: CONSTANT.appVersion
          });
        }else{
          return user;
        }
      }
    }).then(function(success) {
      $scope.hideLoader();
      console.log(success);
    }).catch(function(error) {
      $scope.internalGlitch();
      console.log(error);
    }); 
  }
  $scope.showLoader();
  $scope.updateUserVersion();
  $scope.init();
  $scope.getPMSurveyNotificationNumber();

  
}]);