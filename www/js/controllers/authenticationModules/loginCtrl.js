neuroScienceApp.controller('loginCtrl',["$scope","$ionicModal","$timeout","$kinvey","$state","$ionicPopup",'CONSTANT','getSetDeviceUUID','$q','closePopupService','$ionicHistory', function($scope, $ionicModal, $timeout, $kinvey, $state,$ionicPopup,CONSTANT,getSetDeviceUUID,$q,closePopupService,$ionicHistory) {

  $timeout(function(){
    window.ga.trackView('Login Screen');
  }, 1000);

  $scope.$on("$ionicView.afterEnter", function(event, data){
    $scope.lockOrientation("lock");
    $timeout(function(){
      if(localStorage.welcomePopupShow && JSON.parse(localStorage.welcomePopupShow)){
        $scope.welcomePopup = $ionicPopup.show({
          title: 'Welcome to<br/>MS Care Connect!',
          templateUrl: 'templates/authenticationModules/welcomePopup.html',
          cssClass:"appStart",
          scope: $scope,
          buttons: [{
            text: 'Continue',
            type: 'button-block button-darkPurple font45',
            onTap: function(e) {
              return e;
            }
          }]
        });
        closePopupService.register($scope.welcomePopup);
        $scope.welcomePopup.then(function(res) {
          // console.log('Tapped!', res);
          localStorage.setItem('welcomePopupShow',false);
        });
      }
    }, 1000);
  });

  $scope.loginData = {};
  $scope.localMessage = null;
  $scope.serverMessage = null;

  $scope.goToAbout = function(){
    $state.go('about');
    $scope.welcomePopup.close();
  }

  $scope.doLogin = function(formData) {
    console.log(formData);
    $scope.formDataScope = formData;
    if(formData.email.$error.pattern){
      $scope.serverMessage = CONSTANT.loginErrorCode.wrongPattern;
      $scope.localMessage = null;
      return;
    }
    if(formData.email.$error.required || formData.password.$error.required){
      $scope.localMessage = CONSTANT.loginErrorCode.required;
      $scope.serverMessage = null;
      return;
    }
    if(formData.$invalid){
      return;
    }
    $scope.showLoader();
    var promise = $kinvey.User.login({
      username: $scope.loginData.email,
      password: $scope.loginData.password
    });
    // var user = new $kinvey.User();
    // var promise = user.login($scope.loginData.email, $scope.loginData.password);
    promise.then(function(user) {
      console.log(user)
      if(user.data.loginUserType=="USER_TYPE_PATIENT"){
        if (typeof window.ga !== "undefined") {
          window.ga.trackEvent('Existing User', 'Log In', 'Log In', 1);
        }
        $ionicHistory.clearHistory();
        $ionicHistory.clearCache();
        $scope.serverMessage = null;
        $scope.localMessage = null;
        $scope.userProfileData = user.data;
        if(typeof window.device == "undefined"){
          var device = {};
          device.uuid = "chrome";
        }
        if(ionic.Platform.isAndroid()){
          var promise = $kinvey.CustomEndpoint.execute('deregisterDuplicatedUUIDs',{"user":user.data,"uuid":window.device.uuid,"deviceType":"android"});
          promise.then(function(response) {
            console.log(response);
            $scope.unlockOrientation();
            $scope.checkAuthentication();
            $scope.registerNotificaton();
            CONSTANT.authentication = true;
          }).catch(function(err) {
            $scope.internalGlitch();
            console.log(err);
            $scope.unlockOrientation();
            $scope.checkAuthentication();
            $scope.registerNotificaton();
            CONSTANT.authentication = true;
          });
        }else{
          afterPushTokenRegister();
        }
      }else{
        user.logout();
        $scope.internalGlitch();
        $scope.localMessage = null;
        $scope.serverMessage = CONSTANT.loginErrorCode.wrongEmail;
        $scope.$apply();
      }
    }).catch(function(error) {
      console.log(error)
      if(error.name == "InvalidCredentialsError"){
        $scope.hideLoader();
        $scope.localMessage = null;
        $scope.serverMessage = CONSTANT.loginErrorCode.wrongEmail;
        $scope.$apply();
      }else if(error && error.message && error.message.indexOf("app requires email address verification")>-1){
        // Case 4 When user is not verified its email address.
        $scope.hideLoader();
        $scope.serverMessage = null;
        $scope.localMessage = "Please verify your email address.";
        $scope.$apply();
      }else if(error && error.message && error.message.indexOf("An active user already exists.")>-1){
        $scope.logoutAndLoginAgain();
      }else if(error.name == "KinveyError" && error.code == 503){
        // (error.name == "KinveyError" && error.code == 0) || (error.name == "KinveyError" && error.code == 503) || (error.name == "NotFoundError" && error.code == -1)
        $scope.hideLoader();
        $scope.showAppUpdatePopup();
      }
    });

    function afterPushTokenRegister(){
      // console.log($scope.userProfileData);
      var deviceUUID;
      getSetDeviceUUID.getId()
      .then(function(data){
        deviceUUID = data;
        var promise = $kinvey.CustomEndpoint.execute('deregisterDuplicatedUUIDs',{"user":$scope.userProfileData,"uuid":deviceUUID,"deviceType":"ios"});
        promise.then(function(response) {
          console.log(response);
          $scope.unlockOrientation();
          $scope.registerNotificaton();
          $scope.checkAuthentication();
          CONSTANT.authentication = true;
        }).catch(function(err) {
          console.log(err);
          $scope.unlockOrientation();
          $scope.registerNotificaton();
          $scope.checkAuthentication();
          CONSTANT.authentication = true;
        });
      },function(error){
        deviceUUID = window.device.uuid;
        var promise = $kinvey.CustomEndpoint.execute('deregisterDuplicatedUUIDs',{"user":$scope.userProfileData,"uuid":deviceUUID,"deviceType":"ios"});
        promise.then(function(response) {
          console.log(response);
          $scope.unlockOrientation();
          $scope.registerNotificaton();
          $scope.checkAuthentication();
          CONSTANT.authentication = true;
        }).catch(function(err) {
          $scope.hideLoader();
          console.log(err);
          $scope.unlockOrientation();
          $scope.registerNotificaton();
          $scope.checkAuthentication();
          CONSTANT.authentication = true;
        });
      });
    }
  }

  $scope.showAppUpdatePopup = function(){
    $scope.appUpdatePopup = $ionicPopup.show({
      title: "NOTICE",
      templateUrl: 'templates/miscellaneousModules/forceUpdateAppPopup.html',
      cssClass: "appStart ",
      scope: $scope,
      buttons: [{
        text: 'Update Now',
        type: 'button-block button-darkPurple',
        onTap: function(e) {
          if (ionic.Platform.isAndroid()) {
            window.open(CONSTANT.googlePlayURL, "_system");
          }
          else{
            window.open(CONSTANT.appStoreURL, "_system");
          }
          e.preventDefault();
        }
      }]
    });
  }

  $scope.logoutAndLoginAgain = function(user){
    var promise = new $q(function(resolve) {
      resolve($kinvey.User.getActiveUser());
    });
    promise.then(function(user) {
      if (user) {
        return user.logout();
      }
      return user;
    }).then(function() {
      $timeout(function(){
        $scope.doLogin($scope.formDataScope);
      }, 1000);
    }).catch(function(error) {
      $timeout(function(){
        $scope.doLogin($scope.formDataScope);
      }, 1000);
    });
  }
}]);
