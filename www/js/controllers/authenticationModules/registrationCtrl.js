neuroScienceApp.controller('registrationCtrl',["$scope",'$ionicPopup','$kinvey','$q','utility','$state','$ionicScrollDelegate','$timeout','closePopupService','getSetDeviceUUID', function($scope,$ionicPopup,$kinvey,$q,utility,$state,$ionicScrollDelegate,$timeout,closePopupService,getSetDeviceUUID){
  $scope.lockOrientation("lock");
  ionic.Platform.fullScreen();
  $scope.backOnRegistration = function(action){
    if (typeof window.ga !== "undefined") {
      window.ga.trackEvent('Registration',action, 'Back', 1);
    }
    $scope.goBack();
  }
  $scope.registrationData = {};
  $scope.infoRegistration = {};
  $scope.infoRegistration.personalInformation = {"dob":"","gender":"","race":"","zipCode":"","insurance":"","employmentStatus":"","education":"","preferHand":""};
  $scope.datedob = {'month':"","date":"","year":""};
  $scope.dateOfLastRelapse = {'month':"","year":""};
  $scope.infoRegistration.msHistoryA = {};
  $scope.infoRegistration.msHistoryB = {};
  $scope.infoRegistration.treatmentHistory = {};
  $scope.infoRegistration.selectHCP = {};
  $scope.selectedDoctorArray = [];

  // Page 2 
  $scope.racesOptions = [{id:1, text:"White", checked:false},{id: 2, text:"Black or African American", checked:false},{id: 3, text:"American Indian or Alaska Native", checked:false},{id: 4, text:"Asian", checked:false},{id: 5, text:"Native Hawaiian or Other Pacific Islander", checked:false}];
  $scope.insuranceOptions = [{id:1, text:"Private, employer-paid insurance", checked:false},{id:2, text:"Private, self-paid insurance (including insurance through the Affordable Care Act)", checked:false},{id:3, text:"Medicare or Medicaid", checked:false},{id:4, text:"Other", checked:false},{id:5, text:"None", checked:false}];
  $scope.employmentStatusOptions = [{id:1, text:'Working full-time without any MS-related limitations', checked:false},{id:2, text:'Working full-time with limitations due to MS', checked:false},{id:3, text:'Working part-time due to MS-related limitations', checked:false},{id:4, text:'Working part-time', checked:false},{id:5, text:'Unemployed- Looking for work', checked:false},{id:6, text:'Retired', checked:false},{id:7, text:'Disabled- permanently or temporarily', checked:false},{id:8, text:'Stay at home', checked:false},{id:9, text:'Student', checked:false},{id:10, text:'Sick leave', checked:false},{id:11, text:'Maternity leave', checked:false}];

  $scope.educationOptions = [{id:1, text:"Elementary school", checked:false},{id:2, text:"Middle school", checked:false},{id:3, text:"High school diploma/GED", checked:false},{id:4, text:"Associate's degree", checked:false},{id:5, text:"Bachelor's degree", checked:false},{id:6, text:"Master's degree", checked:false},{id:7, text:"Doctoral degree", checked:false},{id:8, text:"No formal education", checked:false}];

  // Page 3
  $scope.ageOfFirstSymptomsOptions = [];
  $scope.ageAtDiagnosisOptions = [];
  for(var i=0; i<=105; i++){
    if(i+5 == 30){
      $scope.ageOfFirstSymptomsOptions.push({id:i+1,text:i+5,checked:false,scroll:true});
      $scope.ageAtDiagnosisOptions.push({id:i+1,text:i+5,checked:false,scroll:true});
    }else{
      $scope.ageOfFirstSymptomsOptions.push({id:i+1,text:i+5,checked:false,scroll:false});
      $scope.ageAtDiagnosisOptions.push({id:i+1,text:i+5,checked:false,scroll:false});
    }
  }
  $scope.initialDiagnosisOptions = [{id:1, text:"Relapsing-remitting MS (RRMS)", checked:false},{id:2, text:"Secondary progressive MS (SPMS)", checked:false},{id:3, text:"Primary progressive MS (PPMS)", checked:false},{id:4, text:"Progressive relapsing MS (PRMS)", checked:false},{id:5, text:"Clinically isolated syndrome", checked:false},{id:6, text:"Not sure", checked:false},{id:7, text:"I don’t have MS", checked:false}];
  
  $scope.currentDiagnosisOptions = [{id:1, text:"Relapsing-remitting MS (RRMS)", checked:false},{id:2, text:"Secondary progressive MS (SPMS)", checked:false},{id:3, text:"Primary progressive MS (PPMS)", checked:false},{id:4, text:"Progressive relapsing MS (PRMS)", checked:false},{id:5, text:"Clinically isolated syndrome", checked:false},{id:6, text:"Not sure", checked:false},{id:7, text:"I don’t have MS", checked:false}];
  
  $scope.familyMemberRelationshipOptions = [{id:1, text:'I prefer not to answer', checked:false},{id:2, text:'Mother (biological)', checked:false},{id:3, text:'Father (biological)', checked:false},{id:4, text:'Sibling', checked:false},{id:5, text:'Daughter', checked:false},{id:6, text:'Son', checked:false},{id:7, text:'Maternal grandmother ', checked:false},{id:8, text:"Maternal grandfather", checked:false},{id:9, text:'Paternal grandmother', checked:false},{id:10, text:'Paternal grandfather', checked:false},{id:11, text:'Aunt, uncle, or cousin', checked:false}];

  // Page 4
  //$scope.numMSRelapsesLifeTimeOptions = [{id:1, text:"1-5", checked:false, range:5},{id:2, text:"6-10", checked:false ,range:10},{id:3, text:"10+", checked:false, range:12}];
  
  $scope.numMSRelapsesLifeTimeOptions = [];
  for(var i=0;i<=50;i++){
    var item = {
      id:i+1,
      text: i+"",
      checked:false,
    }
    $scope.numMSRelapsesLifeTimeOptions.push(item);
  }
  
  $scope.overAllAbilityOptions = [{value:"Normal",cssClass:"normal",detail:"I may have some mild symptoms, mostly sensory due to MS, but they do not limit my activity. If I do have an attack, I return to normal when the attack has passed."},{value:"Mild Disability",cssClass:"mildDisability",detail:"I have some noticeable symptoms from my MS but they are minor and have only a small effect on my lifestyle."},{value:"Moderate Disability",cssClass:"moderateDisability",detail:"I don't have any limitations in my walking ability. However, I do have significant problems due to MS that limit daily activities in other ways."},{value:"Gait Disability",cssClass:"gaitDisability",detail:"MS does interfere with my activities, especially my walking. I can work a full day, but athletic or physically demanding activities are more difficult than they used to be. I usually don't need a cane or other assistance to walk, but I might need some assistance during an attack."},{value:"Early Cane",cssClass:"earlyCane",detail:"I use a cane or a single crutch or some other form of support (such as touching the wall, or leaning on someone's arm) for walking all the time or part of the time, especially when walking outside. I think I can walk 25 feet in 20 seconds without a cane or crutch. I may use a scooter or wheelchair if I want to go greater distances."},{value:"Late Cane",cssClass:"lateCane",detail:"To be able to walk 25 feet, I have to have a cane, crutch or someone to hold onto. I can get around the house or other buildings by holding onto furniture or touching the walls for support. I may use a scooter or wheelchair if I want to go greater distances."},{value:"Bilateral Support",cssClass:"bilateralSupport",detail:"To be able to walk as far as 25 feet I must have 2 canes or crutches or a walker. I may use a scooter or wheelchair for longer distances."},{value:"Wheelchair / Scooter",cssClass:"wheelchairScooter",detail:"My main form of mobility is a wheelchair. I may be able to stand and/or take one or two steps, but I can't walk 25 feet, even with crutches or a walker."},{value:"Bedridden",cssClass:"bedridden",detail:"Unable to sit in a wheelchair for more than one hour."}]
  $scope.steroidsUsedInLastRelapseOptions = [{id:1, text:"IV steroids", checked:false},{id:2, text:"Oral steroid tablets only", checked:false},{id:3, text:"Oral steroid tablet after IV steroids", checked:false},{id:4, text:"Acthar injections", checked:false},{id:5, text:"Plasma exchange", checked:false},{id:6, text:"No treatment", checked:false}];

  // Page 5
  $scope.currentTreatmentOptions = [{id:1, value:'Aubagio', checked:false, text:'Aubagio<sup>®</sup> (teriflunomide)' },{id:2, value:'Avonex', checked:false, text:'Avonex<sup>®</sup> (interferon beta-1a)'},{id:3, value:'Betaseron', checked:false, text:'Betaseron<sup>®</sup> (interferon beta-1b)'},{id:4, value:'Copaxone', checked:false, text:'Copaxone<sup>®</sup> (glatiramer acetate injection)'},{id:5, value:'Extavia', checked:false, text:'Extavia<sup>®</sup> (interferon beta-1b)'},{id:6, value:'Gilenya', checked:false, text:'Gilenya<sup>®</sup> (fingolimod)'},{id:7, value:'Lemtrada', checked:false, text:'Lemtrada<sup>®</sup> (alemtuzumab)'},{id:15, value:'Ocrevus', checked:false, text:'Ocrevus<sup>®</sup> (ocrelizumab)'},{id:8, value:"Plegridy", checked:false, text:'Plegridy<sup>®</sup> (peginterferon beta-1a)'},{id:9, value:'Rebif', checked:false, text:'Rebif<sup>®</sup> (interferon beta-1a)'},{id:10, value:'Rituxan', checked:false, text:'Rituxan<sup>®</sup> (rituximab)'},{id:11, value:'Tecfidera', checked:false, text:'Tecfidera<sup>®</sup> (dimethyl fumarate)'},{id:12, value:'Tysabri', checked:false, text:'Tysabri<sup>®</sup> (natalizumab)'},{id:13, value:'Zinbryta', checked:false, text:'Zinbryta™ (daclizumab)'},{id:14, value:'None', checked:false, text:'None'}];
  
  /*$scope.currentTreatmentPeriodOptions = [{id:1, text:'1 Month', checked:false},{id:2, text:'3 Months', checked:false},{id:3, text:'6 Months', checked:false},{id:4, text:'1 Year', checked:false},{id:5, text:'1.5 Years', checked:false},{id:6, text:'2 Years', checked:false},{id:7, text:'3 Years', checked:false},{id:8, text:"4 Years", checked:false},{id:9, text:'5 Years', checked:false},{id:10, text:'6 Years', checked:false},{id:11, text:'7 Years', checked:false},{id:12, text:'8 Years', checked:false},{id:13, text:'9 Years', checked:false},{id:14, text:'10 Years', checked:false},{id:15, text:'>10 Years', checked:false}];*/
  $scope.priorTreatments = [];
  $scope.currentTreatmentPeriodOptions = [];
  for(var i=1980;i<=new Date().getFullYear();i++){
    $scope.currentTreatmentPeriodOptions.push({id:i,text:i,checked:false})
  }

  //$scope.currentTreatmentEffectivenessOptions = [{id:1, text:"Very effective", checked:false},{id:2, text:"Partially effective", checked:false},{id:3, text:"Not sure", checked:false},{id:4, text:"Not effective", checked:false}];
  // Text Changed
  
  $scope.currentTreatmentEffectivenessOptions = [{id:1, text:"Completely effective", checked:false},{id:2, text:"Mostly effective", checked:false},{id:3, text:"Slightly effective", checked:false},{id:4, text:"Not effective", checked:false}];

  $scope.lastTreatmentOptions = [{id:1, value:'Aubagio', checked:false, text:'Aubagio<sup>®</sup> (teriflunomide)' },{id:2, value:'Avonex', checked:false, text:'Avonex<sup>®</sup> (interferon beta-1a)'},{id:3, value:'Betaseron', checked:false, text:'Betaseron<sup>®</sup> (interferon beta-1b)'},{id:4, value:'Copaxone', checked:false, text:'Copaxone<sup>®</sup> (glatiramer acetate injection)'},{id:5, value:'Extavia', checked:false, text:'Extavia<sup>®</sup> (interferon beta-1b)'},{id:6, value:'Gilenya', checked:false, text:'Gilenya<sup>®</sup> (fingolimod)'},{id:7, value:'Lemtrada', checked:false, text:'Lemtrada<sup>®</sup> (alemtuzumab)'},{id:15, value:'Ocrevus', checked:false, text:'Ocrevus<sup>®</sup> (ocrelizumab)'},{id:8, value:"Plegridy", checked:false, text:'Plegridy<sup>®</sup> (peginterferon beta-1a)'},{id:9, value:'Rebif', checked:false, text:'Rebif<sup>®</sup> (interferon beta-1a)'},{id:10, value:'Rituxan', checked:false, text:'Rituxan<sup>®</sup> (rituximab)'},{id:11, value:'Tecfidera', checked:false, text:'Tecfidera<sup>®</sup> (dimethyl fumarate)'},{id:12, value:'Tysabri', checked:false, text:'Tysabri<sup>®</sup> (natalizumab)'},{id:13, value:'Zinbryta', checked:false, text:'Zinbryta™ (daclizumab)'}];
  
  /*$scope.yearsTakenOptions = [{id:1, text:"Less than 1", checked:false}];
  for(var i=1; i<=50; i++){
    $scope.yearsTakenOptions.push({id:i+1,text:i,checked:false})
  }*/
  $scope.priorTreatmentStartDateLimit = new Date().getFullYear();
  $scope.yearsTakenOptions = [];
  for(var i=1980;i<=new Date().getFullYear();i++){
    $scope.yearsTakenOptions.push({id:i,text:i,checked:false})
  } 

  $scope.yearsStoppedOptions = [];
  for(var i=1980;i<=new Date().getFullYear();i++){
    $scope.yearsStoppedOptions.push({id:i,text:i,checked:false})
  }

  $scope.updateYearsTakenOption = function(endDate){
    $scope.yearsTakenOptions = [];
    for(var i=1980;i<=endDate;i++){
      $scope.yearsTakenOptions.push({id:i,text:i,checked:false})
    }
  }

  $scope.takingAdditionalMedicationsOptions =[{text:'-Fatigue-', title:true}, {id:1, text:'Symmetrel<sup>®</sup> (amantadine)', checked:false,category:"Fatigue"}, {id:2, text:'Provigil<sup>®</sup> (modafinil)', checked:false,category:"Fatigue"}, {id:3, text:'Nuvigil<sup>®</sup> (armodafinil)', checked:false,category:"Fatigue"}, {id:4, text:'Adderall<sup>®</sup> (amphetamine)', checked:false,category:"Fatigue"}, {id:5, text:'Ritalin<sup>®</sup> (methylphenidate)', checked:false,category:"Fatigue"}, {id:6, text:'Concerta<sup>®</sup> (methylphenidate)', checked:false,category:"Fatigue"},{text:'-Cognition-', title:true}, {id:7, text:'Aricept<sup>®</sup> (donepezil)', checked:false,category:"Cognition"}, {id:8, text:'Exelon<sup>®</sup> (rivastigmine)', checked:false,category:"Cognition"}, {id:9, text:"Namenda<sup>®</sup> (memantine)", checked:false,category:"Cognition"},{text:'-Muscle Spasm-', title:true}, {id:10, text:'Zanaflex<sup>®</sup> (tizanidine)', checked:false,category:"Muscle Spasm"}, {id:11, text:'Lioresal<sup>®</sup> (baclofen)', checked:false,category:"Muscle Spasm"}, {id:12, text:'Valium<sup>®</sup> (diazepam)', checked:false,category:"Muscle Spasm"}, {id:13, text:'Klonopin<sup>®</sup> (clonazepam)', checked:false,category:"Muscle Spasm"},{text:'-Bladder Dysfunction-', title:true}, {id:14, text:'Detrol<sup>®</sup> (tolterodine)', checked:false,category:"Bladder"}, {id:15, text:'Ditropan<sup>®</sup> (oxybutynin)', checked:false,category:"Bladder"}, {id:16, text:'Oxytrol<sup>®</sup> (oxybutynin)', checked:false,category:"Bladder"}, {id:17, text:'Tofranil<sup>®</sup> (imipramine)', checked:false,category:"Bladder"},{text:'-Mobility-', title:true}, {id:18, text:'Ampyra<sup>®</sup> (dalfampridine)', checked:false,category:"Mobility"}, {text:'-Depression-', title:true}, {id:19, text:"Zoloft<sup>®</sup> (sertraline)", checked:false,category:"Depression"}, {id:20, text:'Wellbutrin<sup>®</sup> (bupropion)', checked:false,category:"Depression"}, {id:21, text:'Prozac<sup>®</sup> (fluoxetine)', checked:false,category:"Depression"}, {id:22, text:'Paxil<sup>®</sup> (paroxetine)', checked:false,category:"Depression"}, {id:23, text:'Effexor<sup>®</sup>, Effexor XR<sup>®</sup> (venlafaxine)', checked:false,category:"Depression"}, {id:24, text:'Cymbalta<sup>®</sup> (duloxetine)', checked:false,category:"Depression"}, {text:'-Sleep-', title:true}, {id:25, text:'Ambien<sup>®</sup> (zolpidem)', checked:false,category:"Sleep"}, {id:26, text:'Lunesta<sup>®</sup> (eszopiclone)', checked:false,category:"Sleep"}, {id:27, text:'Melatonin', checked:false,category:"Sleep"}, {id:28, text:'Rozerem<sup>®</sup> (ramelteon)', checked:false,category:"Sleep"}, {text:'-Pain-', title:true}, {id:29, text:"Neurontin<sup>®</sup> (gabapentin)", checked:false,category:"Pain"}, {id:30, text:'Tofranil<sup>®</sup> (imipramine)', checked:false,category:"Pain"}, {id:31, text:'Lyrica<sup>®</sup> (pregabalin)', checked:false,category:"Pain"}, {id:32, text:'Pamelor<sup>®</sup> (nortriptyline) ', checked:false,category:"Pain"}, {id:33, text:'Elavil<sup>®</sup> (amitriptyline)', checked:false,category:"Pain"}, {id:34, text:'Dilantin<sup>®</sup> (phenytoin)', checked:false,category:"Pain"}, {id:35, text:'Cymbalta<sup>®</sup> (duloxetine)', checked:false,category:"Pain"}];
  $scope.optionSingleSelect = null;
  $scope.typeSingleSelect = null;
  $scope.typeMultiSelect = null;
  $scope.optionMultiSelect = null;


  $scope.openSingleSelect = function(options,popupType,screenType,title,index,key){
    $scope.$emit('disbaleBG');
    if(popupType == "priorTreatments.yearsTaken"){
      if(index == 0){
        $scope.yearsTakenOptions = [];
        $scope.infoRegistration.treatmentHistory.currentTreatmentPeriod != undefined ? $scope.priorTreatmentStartDateLimit = $scope.infoRegistration.treatmentHistory.currentTreatmentPeriod.text : $scope.priorTreatmentStartDateLimit = new Date().getFullYear();
        for(var i=1980;i<=$scope.priorTreatmentStartDateLimit;i++){
          $scope.yearsTakenOptions.push({id:i,text:i,checked:false})
        }
        options = $scope.yearsTakenOptions;
      }
      else{
        $scope.yearsTakenOptions = [];
        $scope.priorTreatmentStartDateLimit = $scope.infoRegistration.treatmentHistory.priorTreatments[index-1].yearsTaken.text;
        $scope.priorTreatmentStartDateLimit == undefined ? $scope.priorTreatmentStartDateLimit = new Date().getFullYear():"";
        for(var i=1980;i<=$scope.priorTreatmentStartDateLimit;i++){
          $scope.yearsTakenOptions.push({id:i,text:i,checked:false})
        }
        options = $scope.yearsTakenOptions;
      }
    }
    if(popupType == "priorTreatments.yearsStopped"){
      if(index == 0){
        $scope.yearsStoppedOptions = [];
        if($scope.infoRegistration.treatmentHistory.priorTreatments && $scope.infoRegistration.treatmentHistory.priorTreatments[index].yearsTaken){
          $scope.priorTreatmentEndYearStartLimit = $scope.infoRegistration.treatmentHistory.priorTreatments[index].yearsTaken.text;
          $scope.priorTreatmentEndYearStartLimit == undefined ? $scope.priorTreatmentEndYearStartLimit = 1980:"";
        }else{
          $scope.priorTreatmentEndYearStartLimit = 1980;
        }      
        $scope.infoRegistration.treatmentHistory.currentTreatmentPeriod != undefined ? $scope.priorTreatmentEndYearEndLimit = $scope.infoRegistration.treatmentHistory.currentTreatmentPeriod.text : $scope.priorTreatmentEndYearEndLimit = new Date().getFullYear();
        for(var i=$scope.priorTreatmentEndYearStartLimit;i<=$scope.priorTreatmentEndYearEndLimit;i++){
          $scope.yearsStoppedOptions.push({id:i,text:i,checked:false})
        }
        options = $scope.yearsStoppedOptions;
      }
      else{
        $scope.yearsStoppedOptions = [];       
        if($scope.infoRegistration.treatmentHistory.priorTreatments[index].yearsTaken.text == undefined){
          $scope.priorTreatmentEndYearStartLimit = $scope.infoRegistration.treatmentHistory.priorTreatments[index-1].yearsTaken.text;
        }
        else{
          $scope.priorTreatmentEndYearStartLimit = $scope.infoRegistration.treatmentHistory.priorTreatments[index].yearsTaken.text;
        }
        $scope.priorTreatmentEndYearStartLimit == undefined ? $scope.priorTreatmentEndYearStartLimit = 1980:"";
        $scope.priorTreatmentEndYearEndLimit = $scope.infoRegistration.treatmentHistory.priorTreatments[index-1].yearsTaken.text;
        $scope.priorTreatmentEndYearEndLimit == undefined ? $scope.priorTreatmentEndYearEndLimit = new Date().getFullYear():"";
        for(var i=$scope.priorTreatmentEndYearStartLimit;i<=$scope.priorTreatmentStartDateLimit;i++){
          $scope.yearsStoppedOptions.push({id:i,text:i,checked:false})
        }
        options = $scope.yearsStoppedOptions;
      }
    }
    $scope.typeSingleSelect = popupType;
    $scope.optionSingleSelect = options;
    $scope.screenTypeSingleSelect = screenType;
    $scope.index = index;
    $scope.key = key;
    if($scope.typeSingleSelect.split('.').length>1){
      var child1 = $scope.typeSingleSelect.split('.')[0];
      var child2 = $scope.typeSingleSelect.split('.')[1];
      if($scope.infoRegistration[$scope.screenTypeSingleSelect][child1] && $scope.infoRegistration[$scope.screenTypeSingleSelect][child1].length>0){
        if($scope.infoRegistration[$scope.screenTypeSingleSelect][child1][$scope.index]){
          if ((popupType=='priorTreatments.lastTreatment' || popupType=='priorTreatments.yearsTaken')&&($scope.infoRegistration[$scope.screenTypeSingleSelect][child1][$scope.index][child2]=='')) {
            for (var i = 0; i<$scope.optionSingleSelect.length; i++) {
              if ($scope.optionSingleSelect[i].checked == true) {
                $scope.optionSingleSelect[i].checked = false;
              }
            }
          }else{
            $scope.$emit('updateSingleSelectedArray',$scope.infoRegistration[$scope.screenTypeSingleSelect][child1][$scope.index][child2]);
          }
        }else{
          $scope.infoRegistration[$scope.screenTypeSingleSelect][child1][$scope.index] = {};
          $scope.$emit('updateSingleSelectedArray',$scope.infoRegistration[$scope.screenTypeSingleSelect][child1][$scope.index][child2]);
        }
      }else{
        $scope.infoRegistration[$scope.screenTypeSingleSelect][child1] = [];
        $scope.$emit('updateSingleSelectedArray',$scope.infoRegistration[$scope.screenTypeSingleSelect][child1][$scope.index]);
      }
    }else{
      if($scope.infoRegistration[$scope.screenTypeSingleSelect][$scope.typeSingleSelect]){
        $scope.$emit('updateSingleSelectedArray',$scope.infoRegistration[$scope.screenTypeSingleSelect][$scope.typeSingleSelect]);
      }
    }
    var singleSelectPopup = $ionicPopup.show({
      title: title,
      templateUrl: 'templates/genericTemplate/singleSelectPopup.html',
      cssClass:"selectBoxPopup",
      scope: $scope,
      buttons: [{
        text: 'Cancel',
        type: 'button-block button-outline button-darkPurple',
        onTap: function() {
          return false;
        }
      },{
        text: 'Okay',
        type: 'button-block button-outline button-darkPurple',
        onTap: function() {
          return true;
        }
      }]
    });

    // Close the popup on clicking outside the popup.
    closePopupService.register(singleSelectPopup);
    
    if((popupType == "ageAtDiagnosis"||popupType == "ageOfFirstSymptoms") && options[25].scroll){
      if(window.innerWidth<360){ //iphone 5
        $timeout(function() {
          $ionicScrollDelegate.$getByHandle('scrollPopup').scrollTo(0,580,true);
        },500);
      }else if(window.innerWidth>=360 && window.innerWidth<400){ //iphone 6
        $timeout(function() {
          $ionicScrollDelegate.$getByHandle('scrollPopup').scrollTo(0,700,true);
        },500);
      }else if(window.innerWidth>=400 && window.innerWidth<767){ //iphone 6 plus
        $timeout(function() {
          $ionicScrollDelegate.$getByHandle('scrollPopup').scrollTo(0,820,true);
        },500);
      }else if(window.innerWidth>=768){ // ipad
        $timeout(function() {
          $ionicScrollDelegate.$getByHandle('scrollPopup').scrollTo(0,1000,true);
        },500);
      }else{ // default
        $timeout(function() { 
          $ionicScrollDelegate.$getByHandle('scrollPopup').scrollTo(0,740,true);
        },500);
      }
    }
    singleSelectPopup.then(function(res) {
      $timeout(function(){
        $scope.$emit('enableBG');
      }, 1000);

      console.log($scope.singleSelection, res);
      if(res){
        if (popupType =='numMSRelapsesLifeTime') {
          $scope.infoRegistration.msHistoryB.numMSRelapsesLastYear = false;
          $scope.numMSRelapsesLifeTimeData = $scope.singleSelection.id;
          $scope.numMSRelapsesLastYearOptions = [];
          for(var i=0; i<$scope.numMSRelapsesLifeTimeData; i++){
            $scope.numMSRelapsesLastYearOptions.push({id:i+1,text:i+"",checked:false});
          }
        }
        if($scope.typeSingleSelect.split('.').length>1){
          var child1 = $scope.typeSingleSelect.split('.')[0];
          var child2 = $scope.typeSingleSelect.split('.')[1];
          if($scope.infoRegistration[$scope.screenTypeSingleSelect][child1].length== 0){
            $scope.infoRegistration[$scope.screenTypeSingleSelect][child1] = [];
          }
          if($scope.infoRegistration[$scope.screenTypeSingleSelect][child1][$scope.index] && Object.keys($scope.infoRegistration[$scope.screenTypeSingleSelect][child1][$scope.index]).length>0){
            $scope.infoRegistration[$scope.screenTypeSingleSelect][child1][$scope.index][child2] = $scope.singleSelection;
            $scope.infoRegistration[$scope.screenTypeSingleSelect][child1][$scope.index].id = $scope.key;
          }else{
            $scope.infoRegistration[$scope.screenTypeSingleSelect][child1][$scope.index] = {};
            $scope.infoRegistration[$scope.screenTypeSingleSelect][child1][$scope.index][child2] = $scope.singleSelection;
            $scope.infoRegistration[$scope.screenTypeSingleSelect][child1][$scope.index].id = $scope.key;
          }
          if(child1 == "priorTreatments" && child2 == "yearsTaken"){
            delete ($scope.infoRegistration[$scope.screenTypeSingleSelect][child1][$scope.index]['yearsStopped']);
          }
          $scope.$emit('emptySingleSelectedArray');
        }else{
          if($scope.singleSelection == ''){
            for(var i = 0; i<$scope.optionSingleSelect.length; i++){
              if($scope.optionSingleSelect[i].checked == true){
                $scope.infoRegistration[$scope.screenTypeSingleSelect][$scope.typeSingleSelect] = $scope.optionSingleSelect[i];
                break;
              }
            }
          }else{
            if($scope.infoRegistration[$scope.screenTypeSingleSelect][$scope.typeSingleSelect]){
              if($scope.infoRegistration[$scope.screenTypeSingleSelect][$scope.typeSingleSelect].text > $scope.singleSelection.text && $scope.typeSingleSelect != 'numMSRelapsesLastYear'){
                $scope.$broadcast('resetPreviousTreatmentRegistration');
              }
            }
            $scope.infoRegistration[$scope.screenTypeSingleSelect][$scope.typeSingleSelect] = angular.copy($scope.singleSelection);
            if($scope.screenTypeSingleSelect == "treatmentHistory" && $scope.typeSingleSelect == "currentTreatmentPeriod"){
              $scope.priorTreatmentStartDateLimit = $scope.singleSelection.text;
            }
          }
          $scope.$emit('emptySingleSelectedArray');
        }
        console.log($scope.infoRegistration);

      } else {
        $scope.$emit('emptySingleSelectedArray');  
        for (var i = 0; i<$scope.optionSingleSelect.length; i++) {
          if ($scope.optionSingleSelect[i].checked == true) {
            $scope.optionSingleSelect[i].checked = false;
            break;
          }
        }
      }
    });
  }

  $scope.openMultiSelect = function(options,popupType,screenType,title){
    $scope.$emit('disbaleBG');
    $scope.typeMultiSelect = popupType;
    $scope.optionMultiSelect = options;
    $scope.screenTypeMultiSelect = screenType;
    if($scope.infoRegistration[$scope.screenTypeMultiSelect][$scope.typeMultiSelect]){
      var selectedData = $scope.infoRegistration[$scope.screenTypeMultiSelect][$scope.typeMultiSelect];
      for(var j=0;j<selectedData.length;j++){
        for (var i = 0; i<$scope.optionMultiSelect.length; i++) {
          if (selectedData[j].id == $scope.optionMultiSelect[i].id) {
            $scope.optionMultiSelect[i].checked = true;
            break;
          }
        } 
      }
      $scope.$emit('updateMultiSelectedArray',$scope.infoRegistration[$scope.screenTypeMultiSelect][$scope.typeMultiSelect]);
    }
    var multiSelectPopup = $ionicPopup.show({
      title: title,
      subTitle:"Select all that apply",
      templateUrl: 'templates/genericTemplate/multiSelectPopup.html',
      cssClass:"selectBoxPopup",
      scope: $scope,
      buttons: [{
        text: 'Cancel',
        type: 'button-block button-outline button-darkPurple',
        onTap: function() {
          return false;
        }
      },{
        text: 'Okay',
        type: 'button-block button-outline button-darkPurple',
        onTap: function() {
          return true;
        }
      }]
    });
    closePopupService.register(multiSelectPopup);
    multiSelectPopup.then(function(res) {
      $timeout(function(){
        $scope.$emit('enableBG');
      }, 1000);
      console.log($scope.multiSelection, res);
      if(res){
        $scope.infoRegistration[$scope.screenTypeMultiSelect][$scope.typeMultiSelect] = $scope.multiSelection;
        $scope.$emit('emptyMultiSelectedArray');
        console.log($scope.infoRegistration);
      } else {
        $scope.$emit('emptyMultiSelectedArray');
        for (var i = 0; i<$scope.optionMultiSelect.length; i++) {
          if ($scope.optionMultiSelect[i].checked == true) {
            $scope.optionMultiSelect[i].checked = false;
          }
        }
      }
    });
  }

  $scope.cancelRegistration = function(termsModal,category,action,label){
    /*var promise = new $q(function(resolve) {
      resolve($kinvey.User.getActiveUser());
    });
    promise.then(function(user) {
      if (user) {
        return user;
      }
    }).then(function(user) {
      console.log(user.toJSON());
      var userData = user.toJSON();*/

      /*var deleteUserEndpoint = $kinvey.CustomEndpoint.execute('deleteUser', {'username':userData.username});
      deleteUserEndpoint.then(function(response) {
        console.log(response);
      }).catch(function(err) {
        console.log(err);
      });*/
      // var user = $kinvey.User.getActiveUser();
      // debugger;
      // var userStore = new $kinvey.UserStore();
      // userStore.removeById(user._id).then(function(res) {
      //   console.log(res);
      // }).catch(function(error) {
      //   console.log(error)
      // });
      /*utility.deleteUser(userData._id)
      .then(function(response) {
        console.log(response);
        if(response){
          $scope.logout('registration.page1');
        }
      },function(err) {
        console.log(err);
      });*/
    /*}).catch(function(error) {
      if(error && error.message.indexOf("app requires email address verification")>0){
        console.log("Please verify your email address.");
        $scope.showRequireEmailVerification = true;
        return;
      }
    });*/
    if(category != undefined && action != undefined && label != undefined){
      if (typeof window.ga !== "undefined") {
        window.ga.trackEvent(category, "Initiated Registration Cancellation ("+action+")", label, 1);
      }
    }
    $scope.cancelRegistrationPopup = $ionicPopup.show({
      title:"Are you sure you want to cancel your registration?",
      template: '<a class="popClose" ng-click="cancelRegistrationPopup.close()"><span></span><span></span></a>',
      scope: $scope,
      buttons: [{
        text: 'Cancel<br />registration',
        type: 'button-block button-outline button-multiLine button-darkPurple skip font40',
        onTap: function() {
          return true;
        }
      },{
        text: 'Continue',
        type: 'button-block  button-darkPurple',
        onTap: function() {
          return false;
        }
      }]
    });
    closePopupService.register($scope.cancelRegistrationPopup);
    $scope.cancelRegistrationPopup.then(function(res){
      if(res){
        if(category != undefined && action != undefined && label != undefined){
          if (typeof window.ga !== "undefined") {
            window.ga.trackEvent(category, "Cancelled Registration ("+action+")", label, 1);
          }
        }
        if(termsModal && termsModal.isShown()){
          termsModal.remove();
        }
        $scope.logoutFromRegistration();
      }
      else{
        if(category != undefined && action != undefined && label != undefined){
          if (typeof window.ga !== "undefined") {
            window.ga.trackEvent(category, "Cancelled Registration ("+action+")", "Continue", 1);
          }
        }
      }
    });
  }

  $scope.logoutFromRegistration = function(){
    $scope.showLoader();
    var promise = new $q(function(resolve) {
      resolve($kinvey.User.getActiveUser());
    });
    promise.then(function(user) {
      if (user) {
        return user.logout();
      }
      return user;
    }).then(function() {
      console.log("user logout success");
      localStorage.removeItem('verification');
      $scope.hideLoader();
      $state.go('login');
    }).catch(function(error) {
      console.log(error);
      $scope.hideLoader();
      localStorage.removeItem('verification');
      $state.go('login');
    });
  }

  $scope.$on('dontGoBackShowPopup',function(){
    $scope.cancelRegistration();
  });
}]);

neuroScienceApp.controller('registrationPage1Ctrl', ['$scope','$kinvey','$state','$q','$ionicScrollDelegate','$timeout','getSetDeviceUUID', function ($scope,$kinvey,$state,$q,$ionicScrollDelegate,$timeout,getSetDeviceUUID){
  ionic.Platform.fullScreen();
  $scope.showEmailVerificationScreen = false;
  $scope.showRequireEmailVerification = false;
  $scope.alreadyExist = false;
  $scope.passwordMatch = false;

  $timeout(function(){
    window.ga.trackView('Registration Screen1 - Personal Information');
  }, 1000);

  $scope.$on("$ionicView.afterEnter", function(){
    if(localStorage.verification && !JSON.parse(localStorage.verification)){
      console.log("please verify");
      $scope.showEmailVerificationScreen = true;
      $scope.$apply();
    }
  });
  $scope.changeEmail = function(){
    $scope.alreadyExist = false;
  }
  $scope.passwordFieldChange = function(){
    if($scope.registrationData.cPpassword && $scope.registrationData.password && ($scope.registrationData.password != $scope.registrationData.cPpassword)){
      $scope.passwordMatch = true;
    }else{
      $scope.passwordMatch = false;
    }
  }

  $scope.doRegistration = function(formData) {
    $scope.formDataScope  = formData;
    $scope.alreadyExist = false;
    if($scope.registrationData.password !== $scope.registrationData.cPpassword){
      return;
    }

    if(formData.$invalid){
      return;
    }
    $scope.showLoader();
    getSetDeviceUUID.getId()
    .then(function(data){
      $scope.pushUserDataOnKinevy(data);
    },function(error){
      $scope.pushUserDataOnKinevy(window.device.uuid);
    });
  };

  $scope.pushUserDataOnKinevy = function(uuid){
    var kinveyUser = new $kinvey.User();
    var device = window.device;
    device.uuid = uuid;
    var promise = kinveyUser.signup({
      username:$scope.registrationData.emailAddress,
      email:$scope.registrationData.emailAddress,
      password:$scope.registrationData.password,
      first_name:$scope.registrationData.firstName,
      lastname:$scope.registrationData.lastName,
      loginUserType:"USER_TYPE_PATIENT",
      deviceDetails:device
    }).then(function(user){
      // localStorage.setItem('newUser', JSON.stringify(user.toJSON()));
      if (typeof window.ga !== "undefined") {
        window.ga.trackEvent('Registration', 'Continued Registration (Screen 1)', 'Continue', 1);
      }
      $scope.hideLoader();
      $ionicScrollDelegate.$getByHandle('mainScroll').scrollTop();
      $scope.showEmailVerificationScreen = true;
      localStorage.setItem('verification',false);
      $scope.$apply()
    }).catch(function(error){
      if(error && error.message && error.message.indexOf("active user already exists")>-1){
        $scope.logoutAndLoginAgain();
        return;
      }else if(error && error.message && error.message.indexOf('This username is already taken')>-1){
        $scope.hideLoader();
        $scope.alreadyExist = true;
        $scope.$apply();
      }else{
        $scope.hideLoader();
        $scope.internalGlitch();
      }
    });
  }

  $scope.logoutAndLoginAgain = function(){
    var promise = new $q(function(resolve) {
      resolve($kinvey.User.getActiveUser());
    });
    promise.then(function(user) {
      if (user) {
        return user.logout();
      }
      return user;
    }).then(function() {
      $timeout(function(){
        $scope.doRegistration($scope.formDataScope);
      }, 1000);
    }).catch(function(error) {
      $timeout(function(){
        $scope.doRegistration($scope.formDataScope);
      }, 1000);
    });
  }

  $scope.checkVerificationStatus = function(){
    $scope.showLoader();
    var promise = new $q(function(resolve) {
      resolve($kinvey.User.getActiveUser());
    });
    promise.then(function(user) {
      if (user) {
        return user.me();
      }
      return user;
    }).then(function(user) {
      if (typeof window.ga !== "undefined") {
        window.ga.trackEvent('Registration', 'Email Verification (Screen 1)', 'Continue', 1);
      }
      $scope.showRequireEmailVerification = false;
      localStorage.removeItem('verification');
      $scope.hideLoader();
      console.log(user);
      console.log(user.isEmailVerified());
      if(user && user.isEmailVerified()){
        $state.go('registration.page2');
      }
    }).catch(function(error) {
      $scope.hideLoader();
      if(error && error.message.indexOf("app requires email address verification")>0){
        console.log("Please verify your email address.");
        $scope.showRequireEmailVerification = true;
        return;
      }
      else{
        $scope.internalGlitch();
      }
    });
  }

  $scope.resendVerificationEmail = function(){
    $scope.showLoader();
    var promise = $q(function(resolve) {
      resolve($kinvey.User.getActiveUser());
    });
    promise.then(function(user) {
      console.log(user);
      if (user) {
        return user.verifyEmail();
      }
      return user;
    }).then(function(response) {
      if (typeof window.ga !== "undefined") {
        window.ga.trackEvent('Registration', 'Resend Email Verification (Screen 1)', 'Resend', 1);
      }
      $scope.hideLoader();
      $scope.showMessage('Email resent, please check your inbox again.');  //Successfully resent the email. Kindly check your inbox.
      console.log(response);
    }).catch(function(error) {
      $scope.hideLoader();
      $scope.internalGlitch();
      console.log(error);
    });
  }
}]);

neuroScienceApp.controller('registrationPage2Ctrl', ['$scope','$state','$timeout', function ($scope,$state,$timeout){
  $scope.$on("$ionicView.afterEnter", function(){
    $scope.hideLoader();
    ionic.Platform.fullScreen();
  });

  $timeout(function(){
    window.ga.trackView('Registration Screen2 - Personal Info, demographics');
  }, 1000);

  $scope.error = {};
  $scope.popupValidate = false;
  $scope.changeDob = function(){
    $scope.error.date = false;
  }
  $scope.secondStepSubmit = function (formData) {
    console.log(formData);
    console.log($scope.infoRegistration);
    var year = Number($scope.datedob.year);
    var month = Number($scope.datedob.month);
    var date = Number($scope.datedob.date);
    // HAI: Doing minus 1 in the second condition in if because it is inside the array. It takes month strats from 0 and ends at 11.
    if(moment(year+' '+(month<=9?'0'+month:month)+' '+(date<=9?'0'+date:date),"YYYY MM DD",true).isValid() && 
      moment().diff(moment([year, month-1, date]),'years') >= 18 && moment().diff(moment([year, month-1, date]),'years') < 110){
      $scope.infoRegistration.personalInformation.dob = moment(year+' '+month+' '+date,"YYYY MM DD");
    }else{
      $scope.error.date = true;
      return;
    }
    var info = $scope.infoRegistration.personalInformation;
    if(!info.employmentStatus || !info.insurance || !info.education || (!info.race || info.race.length == 0)){
      console.log("novalidated");
      return;
    }else{
      $scope.popupValidate = true;
    }

    if(formData.$valid && $scope.popupValidate){
      if (typeof window.ga !== "undefined") {
        window.ga.trackEvent('Registration', 'Continued Registration (Screen 2)', 'Continue', 1);
      }
      $state.go('registration.page3');
    }   
  }
}]);

neuroScienceApp.controller('registrationPage3Ctrl', ['$scope','$state','$timeout', function ($scope,$state,$timeout) {
  ionic.Platform.fullScreen();

  $timeout(function(){
    window.ga.trackView('Registration Screen3 - MS History, symptoms/diagnosis');
  }, 1000);

  $scope.popupValidate = false;
  $scope.thirdStepSubmit = function(formData) {
    
    console.log($scope.infoRegistration);
    var info = $scope.infoRegistration.msHistoryA;

    if(!info.ageOfFirstSymptoms || !info.ageAtDiagnosis || !info.initialDiagnosis || !info.currentDiagnosis){
      console.log("novalidated");
      return;
    }else{
      $scope.popupValidate = true;
    }
    if(formData.$valid && $scope.popupValidate){
      if (typeof window.ga !== "undefined") {
        window.ga.trackEvent('Registration', 'Continued Registration (Screen 3)', 'Continue', 1);
      }
      $state.go('registration.page4');
    }
  }

}]);

neuroScienceApp.controller('registrationPage4Ctrl', ['$scope','$state','$ionicPopup','closePopupService','$timeout', function ($scope,$state,$ionicPopup,closePopupService,$timeout){
  ionic.Platform.fullScreen();

  $timeout(function(){
    window.ga.trackView('Registration Screen4 - MS History, relapses/ability');
  }, 1000);

  $scope.errorOfLastRelapse = {}; 
  $scope.popupValidate = false;
  $scope.dateError = false;

  $scope.openRelapseInfoPopup = function(){
    $scope.relapsePopup = $ionicPopup.show({               
      title:"<div class='relapseHistoryicon' ><i class='icon'></i> </div> Relapse?",
      templateUrl: 'templates/authenticationModules/registrationModule/relapsePopup.html',
      cssClass:"appStart",
      scope: $scope
    });
    closePopupService.register($scope.relapsePopup);
  }
  $scope.openOverallAbility = function(detail,title,cssClass,option){
    $scope.overallAbilityDetail = detail;
    $scope.currentOverallAbility = title;
    $scope.infoRegistration.msHistoryB.overallAbility = "";
    $scope.overallAbilityPopup = $ionicPopup.show({
      title:title.toUpperCase(),
      templateUrl: 'templates/authenticationModules/registrationModule/overallAbilityPopup.html',
      cssClass:"appStart "+cssClass,
      scope: $scope
    });
    closePopupService.register($scope.overallAbilityPopup);
  }

  if($scope.infoRegistration.msHistoryB && $scope.infoRegistration.msHistoryB.overallAbility){
    $scope.previousOverallValue = $scope.infoRegistration.msHistoryB.overallAbility;
  }

  $scope.confirmOverallAbility = function(){
    $scope.previousOverallValue = $scope.currentOverallAbility;
    $scope.infoRegistration.msHistoryB.overallAbility = $scope.currentOverallAbility;
    $scope.overallAbilityPopup.close();
  }

  $scope.cancelOverallAbility = function(){
    $scope.infoRegistration.msHistoryB.overallAbility = $scope.previousOverallValue;
    $scope.overallAbilityPopup.close();
  }

  $scope.changeDateOfLastRelapse = function(){
    $scope.errorOfLastRelapse.date = false;
    $scope.dateError = false;
  }
  $scope.$watch(
    function(scope) { return $scope.infoRegistration.msHistoryB.numMSRelapsesLastYear },
    function(newValue, oldValue) {
      if($scope.infoRegistration.msHistoryB.numMSRelapsesLastYear && $scope.infoRegistration.msHistoryB.numMSRelapsesLastYear.text == '0'){
        $scope.dateError = false;
      }
    }
  );
  $scope.fourthStepSubmit = function(formData) {
    
    console.log($scope.infoRegistration);
    var info = $scope.infoRegistration.msHistoryB;
    if(formData.month && (formData.month.$error.required || formData.year.$error.required)){
      $scope.dateError = true;
    }
    
    if(info.numMSRelapsesLastYear && info.numMSRelapsesLifeTime && info.numMSRelapsesLastYear.text == '0' && info.numMSRelapsesLifeTime.text != '0'){
      if($scope.dateOfLastRelapse.month.length == 1){
        $scope.dateOfLastRelapse.month = "0"+$scope.dateOfLastRelapse.month;
      }
      if(moment($scope.dateOfLastRelapse.year+' '+($scope.dateOfLastRelapse.month),"YYYY MM",true).isValid() && moment($scope.dateOfLastRelapse.year+' '+($scope.dateOfLastRelapse.month),"YYYY MM",true).diff(moment())<0){
        $scope.infoRegistration.msHistoryB.dateOfLastRelapse = moment($scope.dateOfLastRelapse.year+' '+$scope.dateOfLastRelapse.month,"YYYY MM");
        $scope.dateError = false;
        $scope.errorOfLastRelapse.date = false;
      }else{
        $scope.errorOfLastRelapse.date = true;
        return;
      }
    }else{
      $scope.infoRegistration.msHistoryB.dateOfLastRelapse = null;
    }

    if(!info.numMSRelapsesLifeTime || !info.steroidsUsedInLastRelapse || !info.numMSRelapsesLastYear || ((info.numMSRelapsesLastYear && info.numMSRelapsesLastYear.text == '0' && info.numMSRelapsesLifeTime.text != '0')&&!info.dateOfLastRelapse)){
      console.log("novalidated");
      return;
    }else{
      $scope.popupValidate = true;
    }
    if(formData.$valid && $scope.popupValidate){
      if (typeof window.ga !== "undefined") {
        window.ga.trackEvent('Registration', 'Continued Registration (Screen 4)', 'Continue', 1);
      }
      $state.go('registration.page5');
    }
  }
}]);

neuroScienceApp.controller('registrationPage5Ctrl', ['$scope','$state','$ionicScrollDelegate','$timeout', function ($scope,$state,$ionicScrollDelegate,$timeout) {
  ionic.Platform.fullScreen();

  $timeout(function(){
    window.ga.trackView('Registration Screen5 - Treatment History');
  }, 1000);

  $scope.popupValidate = false;
  $scope.treatmentHistoryerror = {};
  $scope.treatmentHistoryerror.index = -1;
  $scope.treatmentHistoryerror.iserror = true;
  console.log($scope.priorTreatments.length);
  if($scope.priorTreatments.length == 0){
    $scope.priorTreatments.push($scope.priorTreatments.length+1);
  }
  if($scope.infoRegistration.treatmentHistory.priorTreatments){
    $scope.priorTreatments = $scope.infoRegistration.treatmentHistory.priorTreatments;
  }
  $scope.addAnotherTreatment = function() {
    var key = $scope.priorTreatments[$scope.priorTreatments.length-1]+1;
    $scope.priorTreatments.push(key);
    if(!$scope.infoRegistration.treatmentHistory.priorTreatments){
      $scope.infoRegistration.treatmentHistory.priorTreatments = [];
      $scope.infoRegistration.treatmentHistory.priorTreatments.push({id:key,lastTreatment:'',yearsTaken:'',yearsStopped:''});
    }else{
      $scope.infoRegistration.treatmentHistory.priorTreatments.push({id:key,lastTreatment:'',yearsTaken:'',yearsStopped:''});
    }
    $scope.treatmentHistoryerror.index = $scope.priorTreatments.length;
    console.log($scope.priorTreatments);
  }

  $scope.removeAnotherTreatment = function(index,id){
    if($scope.priorTreatments.length == 1){
      return;
    }
    var priorTreatmentsArray = $scope.infoRegistration.treatmentHistory.priorTreatments;
    if(priorTreatmentsArray){
      for(var i=0; i<priorTreatmentsArray.length;i++){
        if(priorTreatmentsArray[i].id == id){
          priorTreatmentsArray.splice(i, 1);
        }
        console.log("remove");
      }
    }
    $scope.priorTreatments.splice(index,1);
    console.log($scope.priorTreatments,$scope.infoRegistration);
  }
  $scope.previousTreatmentTakenChange = function(){
    if($scope.infoRegistration.treatmentHistory.previousTreatmentTaken == 'no'){
      $scope.priorTreatments = [];
      $scope.priorTreatments.push(1);
      if($scope.infoRegistration.treatmentHistory.priorTreatments && $scope.infoRegistration.treatmentHistory.priorTreatments.length>=0){
        delete $scope.infoRegistration.treatmentHistory['priorTreatments'];       
      }
      $scope.treatmentHistoryerror.iserror = true;
      $scope.resetLastTreatment();
    }else{
      //$scope.updateYearsTakenOption($scope.priorTreatmentStartDateLimit);
      $scope.treatmentHistoryerror.iserror = true;
    }
  }

  $scope.resetLastTreatment = function(){
    for (var i=0;i<$scope.lastTreatmentOptions.length;i++){
      if($scope.lastTreatmentOptions[i].checked == true){
        $scope.lastTreatmentOptions[i].checked = false;
      }
    }
  }

  $scope.$on('resetPreviousTreatmentRegistration', function(event, data) {
    $scope.priorTreatments = [];
    $scope.priorTreatments.push(1);
    if($scope.infoRegistration.treatmentHistory.priorTreatments && $scope.infoRegistration.treatmentHistory.priorTreatments.length>=0){
      $scope.infoRegistration.treatmentHistory['priorTreatments'] = $scope.priorTreatments;    
    }
    $scope.treatmentHistoryerror.iserror = true;
    delete $scope.infoRegistration.treatmentHistory['priorTreatments']; 
    $scope.resetLastTreatment();
    //$scope.$emit('emptySingleSelectedArray');
  });

  var formDataPage5="";
  $scope.fiveStepSubmit = function(formData) {
    
    if($scope.infoRegistration.treatmentHistory.currentTreatment && $scope.infoRegistration.treatmentHistory.currentTreatment.value == "None"){
      delete($scope.infoRegistration.treatmentHistory.currentTreatmentEffectiveness);
      delete($scope.infoRegistration.treatmentHistory.currentTreatmentPeriod);
      delete($scope.infoRegistration.treatmentHistory.currentWeight);
    }
    $scope.treatmentHistoryerror.iserror = true;
    console.log($scope.infoRegistration,$scope.infoRegistration.treatmentHistory.priorTreatments);
    var info = $scope.infoRegistration.treatmentHistory;
    formDataPage5 = formData;
    $scope.$watch(
      function(scope) { return $scope.infoRegistration.treatmentHistory.currentTreatment},
      function(newValue, oldValue) {
        if(newValue != oldValue){
          $ionicScrollDelegate.$getByHandle('mainScroll').resize();
          if(newValue && oldValue == undefined && $scope.infoRegistration.treatmentHistory.currentTreatment && typeof $scope.infoRegistration.treatmentHistory.currentTreatment == 'object'){
            formDataPage5.$submitted = false;
            // $scope.treatmentHistoryerror.iserror = false;
            console.log(formDataPage5);
          }
        }
      }
    );

    if(info.previousTreatmentTaken == 'yes'){
      if(info.priorTreatments && info.priorTreatments.length>0){
        for(var i=0;i<info.priorTreatments.length; i++){
          if(!info.priorTreatments[i].lastTreatment || !info.priorTreatments[i].yearsTaken || !info.priorTreatments[i].yearsStopped){
            if(!info.priorTreatments[i].lastTreatment || !info.priorTreatments[i].yearsTaken){
              $scope.treatmentHistoryerror.index = i;
              $scope.treatmentHistoryerror.iserror = false;
            }
            if(!info.priorTreatments[i].lastTreatment || !info.priorTreatments[i].yearsStopped){
              $scope.treatmentHistoryerror.index = i;
              $scope.treatmentHistoryerror.iserror = false;
            }
            return;
          }
        }
        for(var i=0;i<info.priorTreatments.length;i++){
          for(var j=i;j<info.priorTreatments.length;j++){
            if(info.priorTreatments[i].yearsTaken.text<info.priorTreatments[j].yearsTaken.text){
              var swap = info.priorTreatments[i];
              info.priorTreatments[i] = info.priorTreatments[j];
              info.priorTreatments[j] = swap;
            }
          }
        }
        for(var i=0;i<info.priorTreatments.length;i++){
          info.priorTreatments[i].id = i+1;
        }
      }else{
        $scope.treatmentHistoryerror.index = 0;
        $scope.treatmentHistoryerror.iserror = false;
        return;
      }
    }

    if(!info.currentTreatment || !info.currentTreatmentEffectiveness || !info.currentTreatmentPeriod){
      if(info.currentTreatment && info.currentTreatment.value == "None") {
        $scope.popupValidate = true;
      }else{
       return;
      }
    }else{
      $scope.popupValidate = true;
    }

    if(formData.$valid && $scope.popupValidate){
      if (typeof window.ga !== "undefined") {
        window.ga.trackEvent('Registration', 'Continued Registration (Screen 5)', 'Continue', 1);
      }
      $state.go('registration.page6');
    }
  }
}]);

neuroScienceApp.controller('registrationPage6Ctrl', ['$scope','$state','$ionicModal','$kinvey','$ionicPopup','$q','CONFIG','CONSTANT','closePopupService','getSetDeviceUUID','$timeout','$ionicHistory', function ($scope,$state,$ionicModal,$kinvey,$ionicPopup,$q,CONFIG,CONSTANT,closePopupService,getSetDeviceUUID,$timeout,$ionicHistory){
  ionic.Platform.fullScreen();
  var surveyQuestionCollection = $kinvey.DataStore.getInstance('surveys', $kinvey.DataStoreType.Network);
  var surveyAnswerCollection = $kinvey.DataStore.getInstance('surveyAnswers', $kinvey.DataStoreType.Network);

  $timeout(function(){
    window.ga.trackView('Registration Screen6 - HCP Selector');
  }, 1000);

  if ($scope.selectedDoctorArray.length>0) {
    $scope.showDoctorsList = true;
  }else{
    $scope.showDoctorsList = false;
  }
  $scope.doctorSearch = {"key":'','limit':100};
  $scope.showSpinner = false;
  $scope.showContent = false;
  $scope.showDocNotFound = false;
  $scope.doctorNotFoundMessage = false;
  $scope.isDoctorNotFoundClicked = false;
  $scope.doctors = [];
  $scope.infoRegistration.selectHCP.agreedToSharedInformation = true;
  $scope.searchDoctor = function(){
    if($scope.doctorSearch.key.length>2){
      $scope.showSpinner = true;
      var promise = $kinvey.CustomEndpoint.execute('getDoctorList', $scope.doctorSearch);
      promise.then(function(response) {
        console.log(response);
        $scope.showSpinner = false;
        $scope.showContent = true;
        $scope.doctors = [];
        $scope.doctors = response;
        // $ionicScrollDelegate.$getByHandle('doctorListScroll').scrollTop();
        if($scope.doctors.length == 0){
          $scope.showContent = false;
          $scope.showDocNotFound = true;
        }
        $scope.$apply();

      }).catch(function(err) {
        $scope.internalGlitch();
        console.log(err);
      });
    }else{
      $scope.showSpinner = false;
      $scope.showContent = false;
      $scope.showDocNotFound = false;
      $scope.doctors = [];
      // $ionicScrollDelegate.$getByHandle('doctorListScroll').scrollTop();
    }
  }

  $scope.onDoctorSelect = function ($item) {
    $scope.showContent = false;
    $scope.showDocNotFound = false;
    $scope.doctorSearch.key = '';
    if($scope.selectedDoctorArray.length != 0 ){
      for(var c=0; c < $scope.selectedDoctorArray.length; c++){
        if($item._id == $scope.selectedDoctorArray[c]._id){
          return;
        }
      }
      $scope.selectedDoctorArray.push($item);
    }else{
      $scope.selectedDoctorArray.push($item);
    }

    if($scope.showDoctorsList == false){
      $scope.showDoctorsList = true;
      // $scope.doctorValidation = true;
      // $scope.errorVisibilityCheckOnDoctorInfo = false;
    }
    console.log($scope.selectedDoctorArray);
  };

  $scope.removeSelectedDoctor = function($item){
    for(var i=0; i < $scope.selectedDoctorArray.length; i++){
      if($item._id == $scope.selectedDoctorArray[i]._id){
        $scope.selectedDoctorArray.splice(i,1);
      }
    }
  };
  $scope.doctorNotFoundClicked = function(){
    if (typeof window.ga !== "undefined") {
      window.ga.trackEvent('Registration', 'Neurologist not found', "I Can't find my neurologist", 1);
    }
    $scope.doctorNotFoundMessage = true;
    $scope.isDoctorNotFoundClicked = true;
    var promise = $q(function(resolve) {
      resolve($kinvey.User.getActiveUser());
    });
    promise.then(function(user) {
      $scope.user = user.data;
      var sendDocNotFoundEmail = $kinvey.CustomEndpoint.execute('sendDocNotFoundEmail', {username:$scope.user.username});
      sendDocNotFoundEmail.then(function(response){
        console.log(response);
      }).catch(function(err) {
        console.log(err);
      });
    });
  }

  $scope.sixthStepSubmit = function(formData,label) {
    if (typeof window.ga !== "undefined") {
      window.ga.trackEvent('Registration', 'Continued Registration (Screen 6)', label, 1);
    }
    console.log($scope.infoRegistration);
    cordova.plugins.Keyboard.close();      
    $scope.continueRegistration();
  }

  $scope.continueRegistration = function(){
    $scope.infoRegistration.selectHCP.neurologist = [];
    $scope.infoRegistration.selectHCP.HCPNotFound = angular.copy($scope.isDoctorNotFoundClicked);
    $scope.selectedDoctorNew = [];
    for(var i=0;i<$scope.selectedDoctorArray.length;i++){
      $scope.infoRegistration.selectHCP.neurologist.push({id:$scope.selectedDoctorArray[i]._id,physician_full_name:$scope.selectedDoctorArray[i].physician_full_name,practice_street:$scope.selectedDoctorArray[i].practice_street,practice_state:$scope.selectedDoctorArray[i].practice_state,practice_city:$scope.selectedDoctorArray[i].practice_city,zip:$scope.selectedDoctorArray[i].zip})
      $scope.selectedDoctorNew.push($scope.selectedDoctorArray[i]._id);
    }
    console.log($scope.selectedDoctorArray,$scope.infoRegistration);
    if($scope.selectedDoctorArray.length == 0 && $scope.isDoctorNotFoundClicked == false){
      $scope.noDoctorSelectPopup = $ionicPopup.show({
        title:"Are you sure you don't want to share your information? We value your privacy and only your healthcare team will have access to your personal information.<br><br>",
        template:'<a class="popClose" ng-click="noDoctorSelectPopup.close()"><span></span><span></span></a>',
        scope: $scope,
        buttons: [{
          text: 'Skip',
          type: 'button-block button-outline button-darkPurple skip',
          onTap: function() {
            return true;
          }
        },{
          text: 'Select Your<br>HCP',
          type: 'button-block button-block button-darkPurple bold',
          onTap: function() {
            return false;
          }
        }]
      });
      closePopupService.register($scope.noDoctorSelectPopup);
      $scope.noDoctorSelectPopup.then(function(res){
        if(res){
          $scope.openTermsPopup();
        }
      });
    }else if($scope.selectedDoctorArray.length > 0){
      if($scope.infoRegistration.selectHCP.agreedToSharedInformation){
        $scope.openTermsPopup();
        $scope.sendMailtoDoctor();
      }else{
        $scope.notShareWithDoctorPopup = $ionicPopup.show({
          title:"You’ve selected your HCP. In order to get the full benefit of MS Care Connect and its features, you need to share your information with your healthcare team. Your health information and identity are kept secure.",
          template: '<a class="popClose" ng-click="notShareWithDoctorPopup.close()"><span></span><span></span></a>',
          scope: $scope,
          buttons: [{
            text: 'Skip',
            type: 'button-block button-outline button-darkPurple skip',
            onTap: function() {
              return false;
            }
          },{
            text: 'Share Info',
            type: 'button-block  button-darkPurple',
            onTap: function() {
              return true;
            }
          }]
        });
        closePopupService.register($scope.notShareWithDoctorPopup);
        $scope.notShareWithDoctorPopup.then(function(res){
          if(res){
            console.log("Send Email to user. For sharing");
            $scope.infoRegistration.selectHCP.agreedToSharedInformation = true;
            $scope.sendMailtoDoctor();
            $scope.openTermsPopup();
          }else if(res == false){
            $scope.openTermsPopup();
          }
        });
      }
    }else{
      $scope.openTermsPopup();
    }
  }

  $scope.openTermsPopup = function(){
    ionic.Platform.fullScreen();
    $ionicModal.fromTemplateUrl('templates/authenticationModules/registrationModule/termPage.html', function (termsModal) {
      $scope.termsModal = termsModal;
      $scope.termsModal.show();
    }, {
      scope: $scope,
      animation: 'slide-in-up',
      hardwareBackButtonClose: true
    });
  }

  $scope.sendMailtoDoctor = function(){
    var promise = $kinvey.CustomEndpoint.execute('emailTemp-shareInfoWithHCP', {
      doctorId: $scope.selectedDoctorNew,
      user:$kinvey.User.getActiveUser().data
    });
    promise.then(function(response) {           
    }).catch(function(err) {
      
    });
  }

  $scope.openPrivacyPolicyPopup = function(){
    ionic.Platform.fullScreen();
    $ionicModal.fromTemplateUrl('templates/authenticationModules/registrationModule/privacyPolicy.html', function (privacyModal) {
      $scope.privacyModal = privacyModal;
      $scope.privacyModal.show();
    }, {
      scope: $scope,
      animation: 'slide-in-up',
      hardwareBackButtonClose: true
    });
  }

  var options = {
      message: 'MS Care Connect', // not supported on some apps (Facebook, Instagram)
      subject: 'MS Care Connect', // fi. for email
      files: ['', ''], // an array of filenames either locally or remotely
      url: '',
      chooserTitle: '' // Android only, you can override the default share sheet title
    }

  var onSuccess = function(result) {
    console.log("Share completed? " + result.completed); // On Android apps mostly return false even while it's true
    console.log("Shared to app: " + result.app); // On Android result.app is currently empty. On iOS it's empty when sharing is cancelled (result.completed=false)
  }

  var onError = function(msg) {
    console.log("Sharing failed with message: " + msg);
  }

  $scope.openEmailInNative = function(){
    window.plugins.socialsharing.shareViaEmail(
      'MS Care Connect', // Message
      'MS Care Connect', // Subject
      [CONFIG.contactEmailAddress], // TO: must be null or an array
      null, // CC: must be null or an array
      null, // BCC: must be null or an array
      null, // FILES: can be null, a string, or an array
      function(){ // success
        $scope.showMessage('Email sent successfully.',3000);
      }, 
      function(error){  
        window.plugins.socialsharing.shareWithOptions(options, onSuccess, onError);
      }
    );
    /*
    cordova.plugins.email.open({
        to:      CONFIG.contactEmailAddress,
        //cc:      '',
        //bcc:     ['', 'j'],
        subject: 'MS Care Connect',
        body:    'MS Care Connect'
    }, function(){      
      $scope.showMessage('Please configure your email account',3000);
    });*/
  }

  $scope.finishRegistration = function() {
    $scope.showLoader();
    console.log($scope.infoRegistration);
    var usersDS = new $kinvey.UserStore();
    var user = $kinvey.User.getActiveUser();
    if(user) {
      user.data.info = JSON.parse(angular.toJson($scope.infoRegistration));
      user.data.personalInfoCompTime = new Date();
      user.data.points = {"currentPoints": CONSTANT.gamificationPoints.completeRegistration.value,"fullBatteryPMs":false,"currentLevel": 1,"latestPoints": [{"points": CONSTANT.gamificationPoints.completeRegistration.value,"stage": CONSTANT.gamificationPoints.completeRegistration.name}]};
      user.data.relapse = {"totalRelapse":0};
      user.data.version = CONSTANT.appVersion;
      user.data.subscribe = true;
      user.data.isErrorOccurred = false;
      user.data.isRegistration = true;
      usersDS.update(user.data)
      .then(function (user) {
        if (typeof window.ga !== "undefined") {
          window.ga.trackEvent('Terms and Conditions', 'Terms and Conditions accepted', "Accept", 1);
        }
        //$scope.updateDeviceRegistering(user);
        $scope.overallAbilitySurveyFx(user)
        console.log(JSON.stringify(user));
      }).catch(function (error) {
        if(error && error.message == "errorOccurred"){
          delete user.data.info;
          delete user.data.personalInfoCompTime;
          delete user.data.points;
          delete user.data.relapse;
          delete user.data.version;
          delete user.data.subscribe;
          user.data.isErrorOccurred = true;
          usersDS.update(user.data)
          .then(function (user1) {
            $scope.infoEmptyToast();
            console.log(user1);
          }).catch(function (error1) {
            $scope.internalGlitch();
            $scope.hideLoader();
            console.log(error1);
          });
        }else{
          $scope.internalGlitch();
          $scope.hideLoader();
          console.log(error);
        }      
      });
    }
  }

  $scope.addPointsforRegistration = function(){
    var pointsCollection = $kinvey.DataStore.getInstance('gamificationPointsList', $kinvey.DataStoreType.Network);
    var promise = pointsCollection.save({
      "points": CONSTANT.gamificationPoints.completeRegistration.value,
      "stage": CONSTANT.gamificationPoints.completeRegistration.name
    }).then(function(entity) {
      console.log(entity);
    }).catch(function(error) {
      $scope.internalGlitch();
      console.log(error);
    });
  }

  $scope.setPMandSurvey = function(id){
    console.log(id);
    var promise = $kinvey.CustomEndpoint.execute('userNotifApi',{"userId":id,"userRegistering":true,"currentTreatment":JSON.parse(angular.toJson($scope.infoRegistration.treatmentHistory.currentTreatment))});
    promise.then(function(response) {  
      $scope.getPMSurveyNotificationNumber();
      console.log(response);         
    }).catch(function(err) {
      console.log(err);
    });
  }

  $scope.closeModal = function(){
    if($scope.privacyModal && $scope.privacyModal.isShown()){
      $scope.privacyModal.remove();
      return;
    }
    if($scope.termsModal && $scope.termsModal.isShown()){
      $scope.termsModal.remove();
      return;
    }
  }

  $scope.afterPushTokenRegister = function(){
    // console.log($scope.userProfileData);
    var deviceUUID;
    getSetDeviceUUID.getId()
    .then(function(data){
      deviceUUID = data;
      var promise = $kinvey.CustomEndpoint.execute('deregisterDuplicatedUUIDs',{"user":$scope.userProfileData,"uuid":deviceUUID,"deviceType":"ios"});
      promise.then(function(response) {  
        $scope.gotoHomescreenAfterReg($scope.userProfileData);
      }).catch(function(err) {
        $scope.gotoHomescreenAfterReg($scope.userProfileData);
      });
    },function(error){
      deviceUUID = window.device.uuid;
      var promise = $kinvey.CustomEndpoint.execute('deregisterDuplicatedUUIDs',{"user":$scope.userProfileData,"uuid":deviceUUID,"deviceType":"ios"});
      promise.then(function(response) {  
        $scope.gotoHomescreenAfterReg($scope.userProfileData);
      }).catch(function(err) {
        $scope.gotoHomescreenAfterReg($scope.userProfileData);
      });
    });

  }

  $scope.updateDeviceRegistering = function(user){
    $scope.userProfileData = user;
    if(ionic.Platform.isAndroid()){
      var promise = $kinvey.CustomEndpoint.execute('deregisterDuplicatedUUIDs',{"user":user,"uuid":window.device.uuid,"deviceType":"android"});
      promise.then(function(response) {  
        $scope.gotoHomescreenAfterReg(user);
      }).catch(function(err) {
        $scope.gotoHomescreenAfterReg(user);
      });
    }else{
      $scope.afterPushTokenRegister();
    }
  }

  $scope.gotoHomescreenAfterReg = function(user){
    $scope.hideLoader();
    $scope.termsModal.remove();
    $scope.unlockOrientation();
    $scope.addPointsforRegistration();
    $scope.setPMandSurvey(user._id);
    $scope.registerNotificaton();
    ionic.Platform.fullScreen();
    $scope.$emit('resetRelapse');
    $ionicHistory.clearHistory();
    $ionicHistory.clearCache();
    $state.go('app.preHome');
    //$state.go('app.home');
  }

  $scope.cancelRegistrationFromTermsPage = function(category,action,label){
    $scope.cancelRegistration($scope.termsModal,category,action,label);
  } 

  $scope.overallAbilitySurveyFx = function(user){
    $scope.activeUser = user;
    $scope.overAllAbilityQues = "";
    var query = new $kinvey.Query();
    query.equalTo("type", "SURVEY_1")
    surveyQuestionCollection.find(query).subscribe(function(data) {
       $scope.overAllAbilitySurvey = data;
      console.log("ddd");
    }, function(error) {
      $scope.internalGlitch();
    }, function(data) {
      if($scope.overAllAbilitySurvey && $scope.overAllAbilitySurvey.length > 0){
        $scope.overAllAbilityQues = $scope.overAllAbilitySurvey[0];
        console.log($scope.overAllAbilityQues);
        $scope.surveyAnswers = {
          "category": $scope.overAllAbilityQues.category,
          "type": $scope.overAllAbilityQues.title,
          "title": $scope.overAllAbilityQues.type,
          "answers": []
        };
        $scope.answer = {
          "preQuestion": $scope.overAllAbilityQues.screens[0].question.preQuestion,
          "order": $scope.overAllAbilityQues.screens[0].order,
          "type": $scope.overAllAbilityQues.screens[0].question.type,
          "question": "",
          "answer":{"options":[]}
        };
        for(var i=0;i<$scope.overAllAbilityQues.screens[0].question.options.length;i++){
          if(user.info.msHistoryB.overallAbility == $scope.overAllAbilityQues.screens[0].question.options[i].text){
            $scope.answer.answer.options.push({
              "order": $scope.overAllAbilityQues.screens[0].question.options[i].order,
              "score": $scope.overAllAbilityQues.screens[0].question.options[i].score, 
              "text":  $scope.overAllAbilityQues.screens[0].question.options[i].text
            });
            break;
          }
        }
        $scope.surveyAnswers.answers.push($scope.answer);
        $scope.saveSurveyAnswerForOverallAbility();
      }else{
        $scope.updateDeviceRegistering($scope.activeUser);
      }
    });
  }

  $scope.saveSurveyAnswerForOverallAbility = function(){
  var userAdditionalDetails = {};
  userAdditionalDetails.currentTreatment = angular.copy($scope.activeUser.info.treatmentHistory.currentTreatment);
  userAdditionalDetails.overallAbility = angular.copy($scope.activeUser.info.msHistoryB.overallAbility);
  userAdditionalDetails.employmentStatus = angular.copy($scope.activeUser.info.personalInformation.employmentStatus);
  userAdditionalDetails.currentDiagnosis = angular.copy($scope.activeUser.info.msHistoryA.currentDiagnosis);
  userAdditionalDetails.insurance = angular.copy($scope.activeUser.info.personalInformation.insurance);
  userAdditionalDetails.age = moment().diff($scope.activeUser.info.personalInformation.dob, 'years', true);
  var promise = surveyAnswerCollection.save({"userId":$scope.activeUser._id,"notificationId":null,"userResponse_1":$scope.surveyAnswers, "userResponse_2":null,"cycle":1,"cycleDiff":null,"relapseTrackerId":null,"currentTreatment":userAdditionalDetails.currentTreatment,"overallAbility":userAdditionalDetails.overallAbility,"employmentStatus":userAdditionalDetails.employmentStatus,"currentDiagnosis":userAdditionalDetails.currentDiagnosis,"insurance":userAdditionalDetails.insurance,"age":userAdditionalDetails.age,"appVersion":CONSTANT.appVersion})
    .then(function(entity) {
      $scope.updateDeviceRegistering($scope.activeUser);
      console.log(entity);
    }).catch(function(error) {
      $scope.internalGlitch();
      console.log("error " + error);
    });
  }
}]);

