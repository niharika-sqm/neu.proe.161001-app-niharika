neuroScienceApp.controller('forgotPasswordCtrl', ['$scope','$kinvey','$state','$timeout', function ($scope,$kinvey,$state,$timeout){
	
	$scope.args = {email:""};
	$scope.userNotExist = false;
	$scope.requiredEmail = false;
	$scope.patternEmail = false;

	$timeout(function(){
    window.ga.trackView('Forgot Password');
  }, 1000);

	$scope.callForgetPassword = function() {
		var promise = $kinvey.User.resetPassword($scope.args.email);
		promise.then(function(response) {
			if (typeof window.ga !== "undefined") {
				window.ga.trackEvent('Forgot Password', 'Password Reset', 'Request Passowrd Reset', 1);
			}
			$scope.hideLoader();
			$state.go('login');
			console.log(response)
		}, function(err) {
			$scope.hideLoader();
		  console.log(err)
		});
	}
	$scope.doForgetPassword = function(formData) {
		$scope.changeEmailField();
		if(formData.email.$error.required){
			$scope.requiredEmail = true;
			return;
		}
		if(formData.email.$error.pattern){
			$scope.patternEmail = true;
			return;
		}
		if(formData.$valid){
			$scope.showLoader();
			var usersStore = new $kinvey.UserStore();
			usersStore.exists($scope.args.email)
			.then(function(exists) {
			  if(exists){
			  	$scope.userNotExist = false;
			  	$scope.$apply();
			  	$scope.callForgetPassword();
			  }else{
					$scope.hideLoader();
			  	$scope.userNotExist = true;
			  	$scope.$apply();
			  }
			}, function(error) {
				$scope.hideLoader();
			  console.log(error)
			});
		}
	}
	$scope.changeEmailField = function(){
		$scope.requiredEmail = false;
		$scope.userNotExist = false;
		$scope.patternEmail = false;
	}
}]);