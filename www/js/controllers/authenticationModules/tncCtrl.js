neuroScienceApp.controller('tncCtrl', ['$scope', '$state','CONFIG','$timeout', function ($scope, $state,CONFIG,$timeout) {

	console.log("Before Login");
	$timeout(function(){
    window.ga.trackView('Terms and Conditions (Before Login)');
  }, 1000);

	var options = {
      message: 'MS Care Connect', // not supported on some apps (Facebook, Instagram)
      subject: 'MS Care Connect', // fi. for email
      files: ['', ''], // an array of filenames either locally or remotely
      url: '',
      chooserTitle: '' // Android only, you can override the default share sheet title
    }

  var onSuccess = function(result) {
    console.log("Share completed? " + result.completed); // On Android apps mostly return false even while it's true
    console.log("Shared to app: " + result.app); // On Android result.app is currently empty. On iOS it's empty when sharing is cancelled (result.completed=false)
  }

  var onError = function(msg) {
    console.log("Sharing failed with message: " + msg);
  }

	$scope.openContactViaEmailTnc = function(){
		window.plugins.socialsharing.shareViaEmail(
	      'MS Care Connect', // Message
	      'MS Care Connect', // Subject
	      [CONFIG.contactEmailAddress], // TO: must be null or an array
	      null, // CC: must be null or an array
	      null, // BCC: must be null or an array
	      null, // FILES: can be null, a string, or an array
	      function(){ // success
	        $scope.showMessage('Email sent successfully.',3000);
	      }, 
	      function(error){  
	        window.plugins.socialsharing.shareWithOptions(options, onSuccess, onError);
	      }
	    );
		/*
		cordova.plugins.email.open({
		    to:      CONFIG.contactEmailAddress,
		    //cc:      '',
		    //bcc:     ['', 'j'],
		    subject: 'MS Care Connect',
		    body:    'MS Care Connect'
		}, function(){			
			$scope.showMessage('Please configure your email account',3000);
		});*/
	}
}]);