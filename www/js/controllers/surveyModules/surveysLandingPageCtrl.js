neuroScienceApp.controller('surveysLandingPageCtrl', ['$scope','$state','$kinvey','$stateParams','CONFIG','$timeout', function($scope, $state, $kinvey, $stateParams,CONFIG,$timeout) {
	var surveyNotificationCollection = $kinvey.DataStore.getInstance('notifications', $kinvey.DataStoreType.Network);

	$scope.surveyNotifications = null;
	$scope.showLoader();

	$timeout(function(){
    window.ga.trackView('Survey Landing Screen');
  }, 1000);

  $scope.replaceTrackingOnSurvey = function(){
    if (typeof window.ga !== "undefined") {
      window.ga.trackEvent('Survey','Start / Stop Relapse (Survey)', 'Relapse', 1);
    }
    $scope.replaceTracking();
  }

	//pending surveys
	function fetchSurveyNotifications() {
		var query = new $kinvey.Query();
		query.equalTo('_acl.creator', $kinvey.User.getActiveUser()._id).and().equalTo('isSent', true).and().equalTo('isAck', false).equalTo('notificationType', "SURVEY");

		surveyNotificationCollection.find(query)
		.subscribe(function(data) {
			$scope.surveyNotifications = data;
			console.log(data);
		}, function(error) {
			$scope.hideLoader();
			$scope.internalGlitch();
		}, function() {
			$scope.hideLoader();
		});
	}

	fetchSurveyNotifications();
	$scope.getRelapseEntryData($kinvey.User.getActiveUser()._id);
	
	$scope.takeSurvey = function(_surveys, _notificationId){
		if (typeof window.ga !== "undefined") {
      window.ga.trackEvent('Survey', 'Survey question appers', 'Take Survey', 1);
    }
		$state.go("app.surveys", {"surveyInfo": {"surveys":_surveys.surveys, "notificationId":_notificationId,"cycle":_surveys.cycle,"differentCycle":_surveys.differentCycle || null, "order":_surveys.order}});
	}

	$scope.contactMSCare = function(){
		window.open(CONFIG.dashboardNavigationLink, '_system', 'location=yes'); 
	}
}]);