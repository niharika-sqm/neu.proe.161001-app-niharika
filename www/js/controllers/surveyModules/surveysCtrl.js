neuroScienceApp.controller('surveysCtrl', ['$scope', '$state', '$kinvey', '$stateParams', '$ionicSlideBoxDelegate', '$timeout', '$ionicModal','$ionicPopup','closePopupService','$q','$ionicSideMenuDelegate','CONSTANT','$ionicScrollDelegate','$ionicHistory', function($scope, $state, $kinvey, $stateParams, $ionicSlideBoxDelegate, $timeout, $ionicModal,$ionicPopup,closePopupService,$q,$ionicSideMenuDelegate,CONSTANT,$ionicScrollDelegate,$ionicHistory){
	var surveyInfo = $stateParams.surveyInfo;
	console.log(surveyInfo);
	if (!surveyInfo) {
		$state.go("app.surveysLandingPage");
		return;
	}
	/* update user copy if survey is (mri and side effect) and order is even */
	if(surveyInfo.surveys){
		for(var i=0;i<surveyInfo.surveys.length;i++){
			if(surveyInfo.surveys[i] == "SURVEY_3" && (surveyInfo.order < 10)){
				$scope.isUpdateUserCopy = true;
				if(surveyInfo.differentCycle){
					$scope.paramsForUpdateCopy = {"surveyName":"SURVEY_3","surveyCycle":surveyInfo.differentCycle["SURVEY_3"]};
				}else{
					$scope.paramsForUpdateCopy = {"surveyName":"SURVEY_3", "surveyCycle":surveyInfo.cycle};
				}
				break;
			}if(surveyInfo.surveys[i] == "SURVEY_26" && (surveyInfo.order < 10)){
				$scope.isUpdateUserCopy = true;
				if(surveyInfo.differentCycle){
					$scope.paramsForUpdateCopy = {"surveyName":"SURVEY_26","surveyCycle":surveyInfo.differentCycle["SURVEY_26"]};
				}else{
					$scope.paramsForUpdateCopy = {"surveyName":"SURVEY_26", "surveyCycle":surveyInfo.cycle};
				}
				break;
			}
		}
	}

	$timeout(function(){
    window.ga.trackView('Single Survey Screen');
  }, 1000);

	console.log(surveyInfo);
	$scope.showLoader();
	var surveyQuestionCollection = $kinvey.DataStore.getInstance('surveys', $kinvey.DataStoreType.Network);
	var surveyNotificationCollection = $kinvey.DataStore.getInstance('surveyNotifications', $kinvey.DataStoreType.Network);
	var pastSurveyAnswerCollection = $kinvey.DataStore.getInstance('surveyAnswers', $kinvey.DataStoreType.Network);
	var relapseList = $kinvey.DataStore.getInstance('relapseList', $kinvey.DataStoreType.Network);
	var userCopyCollecton = $kinvey.DataStore.getInstance('userCopy', $kinvey.DataStoreType.Network);

	var surveyAnswerCollection = []; 
	$scope.surveyPastAnswers = null;
	$scope.submitButtonDisabled = true;
	var currentSurveyIndex = 0;
	var tempSurveyQst = null;

	var answerObj = [];
	$scope.progressBarFilled = false;
	$scope.numberTypeUpperText = "Not At All";
	$scope.numberTypeBottomText = "Severely";

	$scope.getActiveUser = function(){
		var promise = new $q(function(resolve) {
			resolve($kinvey.User.getActiveUser());
		});
		promise.then(function(user) {
			if (user) {
				return user.me();
			}
			return user;
		}).then(function(user) {
			$scope.activeUser = user.data;
			console.log($scope.activeUser);
			fetchSurveyQuestions(surveyInfo.surveys[currentSurveyIndex]);
		}).catch(function(error) {
			console.log(error);
		});
	}
	$scope.getActiveUser();


	function fetchSurveyQuestions(_surveyType) {
		if($scope.surveyQuestions){
			$ionicSlideBoxDelegate.slide(0,10);
			$scope.surveyQuestions.screens.splice(0);
			$scope.fetchedQuestions.splice(0);
			$ionicSlideBoxDelegate.update()
		}
		$scope.surveyQuestions = tempSurveyQst = null;
		$scope.fetchedQuestions = null;
		$scope.answer = {};
		$scope.subAnswer = {};
		$scope.modelAnswer = {};
		$scope.modelSubQuestAnswer = {};
		$scope.sub_sub_answer = {};
		$scope.multiAnswer = [];
		$scope.subAnswerDropDown = [];

		var query = new $kinvey.Query();
		query.equalTo('type', _surveyType);
		surveyQuestionCollection.find(query).subscribe(function(data) {
			tempSurveyQst = data[0];
		}, function(error) {
			$scope.internalGlitch();
		}, function(data) {
			if(CONSTANT.appVersion < tempSurveyQst.version){
				$scope.appVersionPopup("Please update the app in order to access new features");
				$state.go('app.surveysLandingPage');
				return;
			}
			console.log(tempSurveyQst);
			if(tempSurveyQst.type == "SURVEY_4"){
				tempSurveyQst.screens = tempSurveyQst.screens[$scope.activeUser.info.treatmentHistory.currentTreatment.value];
			}
			if(tempSurveyQst.type == "SURVEY_9" || tempSurveyQst.type == "SURVEY_14"){
				for(var i=0;i<tempSurveyQst.screens.length; i++){
					if(tempSurveyQst.screens[i].question.preQuestion.indexOf('[currentMSmedication]')>0){
						tempSurveyQst.screens[i].question.preQuestion = tempSurveyQst.screens[i].question.preQuestion.replace('[currentMSmedication]',$scope.activeUser.info.treatmentHistory.currentTreatment.text);
					}
				}
			}
			if(tempSurveyQst.type == "SURVEY_2" || tempSurveyQst.type == "SURVEY_4" || tempSurveyQst.type == "SURVEY_7" || tempSurveyQst.type == "SURVEY_8" || tempSurveyQst.type == "SURVEY_12" || tempSurveyQst.type == "SURVEY_14" || tempSurveyQst.type == "SURVEY_19" || tempSurveyQst.type == "SURVEY_20" || tempSurveyQst.type == "SURVEY_21" || tempSurveyQst.type == "SURVEY_25" || tempSurveyQst.type == "SURVEY_26" || tempSurveyQst.type == "SURVEY_27" || tempSurveyQst.type == "SURVEY_28" || tempSurveyQst.type == "SURVEY_29"){
				$scope.showIndexNumber = false;
			}else if(tempSurveyQst.screens.length < 2){
				$scope.showIndexNumber = false;
			}else{
				$scope.showIndexNumber = true;
			}

			if(tempSurveyQst.type == "SURVEY_15" || tempSurveyQst.type == "SURVEY_16"){
				$scope.numberTypeUpperText = "Not At All";
				$scope.numberTypeBottomText = "Severely";
			}else if(tempSurveyQst.type == "SURVEY_20"){
				$scope.numberTypeUpperText = "Very Little Pain";
				$scope.numberTypeBottomText = "As Bad As It Can Be";
			}

			var timeoutPro = $timeout(function() {
				$timeout.cancel(timeoutPro);
				$scope.surveyQuestions = tempSurveyQst;
				$scope.fetchedQuestions = angular.copy($scope.surveyQuestions.screens);
				$ionicSlideBoxDelegate.update();
				$scope.fillWidth = 100 / $scope.fetchedQuestions.length;
				$scope.hideLoader();
				$scope.slideHasChanged(0);
				updateSubmitButtonStatus();
				$ionicSlideBoxDelegate.enableSlide(false)
			}, 500);
		});
	}

	
	
	// for custom dropdown 
	$scope.optionSingleSelect = null;
	$scope.questOrderSingleSelect = null;
	$scope.lastSelectedValue = [];
	$scope.openSingleSelect = function(options, _qstOrder, title, optionType, _mainQstOrder,_subQstOrder) {
		$scope.optionSingleSelect = angular.copy(options);
		$scope.questOrderSingleSelect = _qstOrder;
		if($scope.surveyQuestions.type=="SURVEY_17"){
			var ageOfsmoking = [];
			for(var i=0; i<options; i++){
				ageOfsmoking.push({id:i+1,text:i+1,order:i+1,checked:false})
			}
			$scope.optionSingleSelect = ageOfsmoking;
			if($scope.lastSelectedValue[_mainQstOrder-1]){
				$scope.$emit('updateSingleSelectedArray',angular.copy($scope.lastSelectedValue[_mainQstOrder-1][_subQstOrder]));
			}
		}else{
			$scope.$emit('updateSingleSelectedArray',angular.copy($scope.lastSelectedValue[$scope.questOrderSingleSelect]));
		}
		
		var singleSelectPopup = $ionicPopup.show({
			title: title,
			templateUrl: 'templates/genericTemplate/singleSelectPopup.html',
			cssClass: "selectBoxPopup",
			scope: $scope,
			buttons: [{
				text: 'Cancel',
				type: 'button-block button-outline button-darkPurple',
				onTap: function() {
					return false;
				}
			}, {
				text: 'Okay',
				type: 'button-block button-outline button-darkPurple bold',
				onTap: function() {
					return true;
				}
			}]
		});
		closePopupService.register(singleSelectPopup);
		if(($scope.surveyQuestions.type=="SURVEY_17") && ((_mainQstOrder ==1 && $scope.subAnswerDropDown.length == 0) ||
		 (_mainQstOrder ==2 &&  (($scope.subAnswerDropDown[1] != undefined && $scope.subAnswerDropDown[1][1] == undefined && _subQstOrder == 1) ||($scope.subAnswerDropDown[1] != undefined && $scope.subAnswerDropDown[1][2] == undefined && _subQstOrder == 2))) || _mainQstOrder ==2 && $scope.subAnswerDropDown[1] == undefined)){
			console.log($scope.subAnswerDropDown)
      if(window.innerWidth<360){ //iphone 5
        $timeout(function() {
				 $ionicScrollDelegate.$getByHandle('scrollPopup').scrollTo(0,380,true);
        },500);
      }else if(window.innerWidth>=360 && window.innerWidth<400){ //iphone 6
        $timeout(function() {
          $ionicScrollDelegate.$getByHandle('scrollPopup').scrollTo(0,450,true);
        },500);
      }else if(window.innerWidth>=400 && window.innerWidth<767){ //iphone 6 plus
        $timeout(function() {
          $ionicScrollDelegate.$getByHandle('scrollPopup').scrollTo(0,500,true);
        },500);
      }else if(window.innerWidth>=768){ // ipad
        $timeout(function() {
          $ionicScrollDelegate.$getByHandle('scrollPopup').scrollTo(0,700,true);
        },500);
      }else{ // default
        $timeout(function() { 
          $ionicScrollDelegate.$getByHandle('scrollPopup').scrollTo(0,450,true);
        },500);
      }
    }
		singleSelectPopup.then(function(res) {
			// console.log($scope.singleSelection, res);
			if(res){
				if(optionType == "main"){
					$scope.lastSelectedValue[$scope.questOrderSingleSelect] = angular.copy($scope.singleSelection);
					$scope.optionValueHandler($scope.questOrderSingleSelect,$scope.singleSelection);
				}else if(optionType == "sub"){
					$scope.lastSelectedValue[$scope.questOrderSingleSelect] = angular.copy($scope.singleSelection);
					$scope.subOptionValueHandler($scope.questOrderSingleSelect,$scope.singleSelection,_mainQstOrder);
				}else if(optionType == "subDropDown"){
					$scope.subOptionDropDown($scope.questOrderSingleSelect,_subQstOrder,$scope.singleSelection,_mainQstOrder);
					if($scope.lastSelectedValue[_mainQstOrder-1]==undefined){
						$scope.lastSelectedValue[_mainQstOrder-1] = {};
					}
					$scope.lastSelectedValue[_mainQstOrder-1][_subQstOrder] = angular.copy($scope.singleSelection);
				}
				
				$scope.$emit('emptySingleSelectedArray');
			}else{
				$scope.$emit('emptySingleSelectedArray');
				for (var i = 0; i < $scope.optionSingleSelect.length; i++) {
					if ($scope.optionSingleSelect[i].checked == true) {
						$scope.optionSingleSelect[i].checked = false;
						break;
					}
				}
			}
		});
	}
	// for custom dropdown 

	$scope.optionValueHandler = function(_qstOrder, _ans) {
		/*if(!_ans.hasSubOptions){
			$scope.subAnswer = {};
			$scope.modelSubQuestAnswer = {};
		}*/
		
		if(($scope.surveyQuestions.type=="SURVEY_20" || $scope.surveyQuestions.type=="SURVEY_22")&& _ans.order==1 && _qstOrder==1){
			$scope.fetchedQuestions.splice(_ans.order,$scope.fetchedQuestions.length-1);
			$scope.answer = {};
			$scope.modelAnswer = {};
			$scope.answer[_qstOrder] = {"order":_ans.order, "text":_ans.text, "score":_ans.score};
			$scope.modelAnswer[_qstOrder] = {"order":_ans.order};
			$scope.slideHasChanged(0);
			$ionicSlideBoxDelegate.update();
		}else if(($scope.surveyQuestions.type=="SURVEY_20" || $scope.surveyQuestions.type=="SURVEY_22") && _ans.order!=1 && _qstOrder==1){
			$scope.fetchedQuestions = [];
			$scope.fetchedQuestions = angular.copy($scope.surveyQuestions.screens);
			$scope.slideHasChanged(0);
			$ionicSlideBoxDelegate.update();
		}

		if($scope.surveyQuestions.type=="SURVEY_9" && _qstOrder == 1 && (_ans.order < 5)){
			$scope.fetchedQuestions = angular.copy($scope.surveyQuestions.screens);
			$scope.answer = {};
			$scope.modelAnswer = {};
			$scope.modelAnswer[_qstOrder] = {"order":_ans.order};
			$scope.answer[_qstOrder] = {"order":_ans.order, "text":_ans.text, "score":_ans.score};
			$ionicSlideBoxDelegate.update();
		}else if($scope.surveyQuestions.type=="SURVEY_9" && _qstOrder == 1 && (_ans.order ==5)){
			$scope.fetchedQuestions = angular.copy($scope.surveyQuestions.screens);
			$scope.fetchedQuestions.splice(_qstOrder,1);
			$scope.answer = {};
			$scope.subAnswer = {};
			$scope.modelAnswer = {};
			$scope.modelAnswer[_qstOrder] = {"order":_ans.order};
			$scope.answer[_qstOrder] = {"order":_ans.order, "text":_ans.text, "score":_ans.score};
			$scope.modelSubQuestAnswer = {}
			$scope.slideHasChanged(0);
			$ionicSlideBoxDelegate.update();
		}
		if($scope.surveyQuestions.type=="SURVEY_3" && _ans.order==1 && _qstOrder == 1){
			$scope.fetchedQuestions = angular.copy($scope.surveyQuestions.screens);
			$scope.answer = {};
			$scope.modelAnswer = {};
			$scope.modelAnswer[_qstOrder] = {"order":_ans.order};
			$scope.answer[_qstOrder] = {"order":_ans.order, "text":_ans.text, "score":_ans.score};
			$scope.slideHasChanged(0);
			$ionicSlideBoxDelegate.update();
		}else if($scope.surveyQuestions.type=="SURVEY_3" && _ans.order==2 && _qstOrder == 1){
			$scope.fetchedQuestions = angular.copy($scope.surveyQuestions.screens);
			$scope.answer = {};
			$scope.modelAnswer = {};
			$scope.subAnswer = {};
			$scope.modelSubQuestAnswer = {};
			$scope.modelAnswer[_qstOrder] = {"order":_ans.order};
			$scope.answer[_qstOrder] = {"order":_ans.order, "text":_ans.text, "score":_ans.score};
			$scope.slideHasChanged(0);
			$ionicSlideBoxDelegate.update();
		}
		
		$scope.answer[_qstOrder] = {"order":_ans.order, "text":_ans.text, "score":_ans.score};
		updateSubmitButtonStatus();
		
		if($scope.fetchedQuestions.length > 1){ //Object.keys($scope.answer).length == 1 && 
			if($scope.surveyQuestions.type=="SURVEY_3"  && $scope.surveyQuestions.screens[_qstOrder-1].question.options[0].hasSubOptions && _ans.order==1){
				console.log("do nothing")
			}else if($scope.surveyQuestions.type=="SURVEY_17" && $scope.answer[1].order == 1 && _qstOrder == 1){
				console.log("do nothing")
			}else if($scope.surveyQuestions.type=="SURVEY_17" && $scope.answer[1].order == 2 && _qstOrder == 1){
				$ionicSlideBoxDelegate.next(2000);
			}else if($scope.surveyQuestions.type=="SURVEY_17" && $scope.answer[2]!=undefined && $scope.answer[2].order == 1){
				console.log("do nothing")
			}
			else if($scope.surveyQuestions.type=="SURVEY_9" && _qstOrder == 2){
				console.log("do nothing")
			}
			else{
				$ionicSlideBoxDelegate.next(2000);
			}
		}
		
		if(Object.keys($scope.answer).length == $scope.fetchedQuestions.length){
			$ionicScrollDelegate.scrollBottom(true);
		}
	}
	$scope.subOptionValueHandler = function(_qstOrder, _ans, _mainQstOrder) {
		if(_mainQstOrder){ // only from "subOptionThreeBoxHolder" function
			$scope.subQustOrder = _mainQstOrder;
		}else{
			$scope.subQustOrder = _qstOrder;
		}
		$scope.subAnswer[_qstOrder] = {"order":_ans.order, "text":_ans.text, "score":_ans.score};
		// console.log($scope.subAnswer);
		if($scope.surveyQuestions.type=="SURVEY_3"){
			$ionicSlideBoxDelegate.next(2000);
		}
		if($scope.surveyQuestions.type=="SURVEY_9" && (Object.keys($scope.subAnswer).length) == 8){
			$ionicSlideBoxDelegate.next(2000);
		}
		if($scope.surveyQuestions.type=="SURVEY_10" && $scope.multiAnswer.length == (Object.keys($scope.subAnswer).length)){
			$scope.submitButtonDisabled = false;
		}
		else if($scope.surveyQuestions.type=="SURVEY_10" && $scope.multiAnswer.length != (Object.keys($scope.subAnswer).length)){
			$scope.submitButtonDisabled = true;
		}
		else{
			updateSubmitButtonStatus();
		}	
	}
	$scope.optionMultiHandler = function(_mainQstOrder,_ans){
		$scope.optionValueHandler(_mainQstOrder, _ans);
		var idx = $scope.multiAnswer.indexOf(_ans);

		if(idx > -1){
			if($scope.surveyQuestions.type=="SURVEY_10"){
				// for(var i=0;i<_ans.subOptions.length;i++){
				// 	if(_ans.subOptions[i].checked){
				// 		_ans.subOptions[i].checked = false;
				// 	}
				// }
				delete $scope.lastSelectedValue[_ans.order];	
				delete $scope.subAnswer[_ans.order];
				delete $scope.modelAnswer[_ans.order];
			}
			$scope.multiAnswer.splice(idx, 1);
		}else{
			if(_ans.order == 9 && $scope.surveyQuestions.type=="SURVEY_10"){
				$scope.multiAnswer = [];
				$scope.lastSelectedValue = {};
				$scope.modelAnswer = {};
				$scope.subAnswer = {};
				$scope.modelAnswer[_ans.order] = {"checked":true};
				$scope.multiAnswer.push(_ans);
			}else if($scope.multiAnswer.length>0 && $scope.multiAnswer[0].order == 9 && $scope.surveyQuestions.type=="SURVEY_10" && _ans.order != 9){
				$scope.multiAnswer.shift();
				delete $scope.modelAnswer[9];
				$scope.multiAnswer.push(_ans);
			}else{
				$scope.multiAnswer.push(_ans);
			}
		}

		if($scope.multiAnswer.length<1){
			$scope.answer = {};
			updateSubmitButtonStatus();
		}
		if($scope.surveyQuestions.type=="SURVEY_10" && _ans.order == 9 && $scope.multiAnswer.length>0){
			$scope.submitButtonDisabled = false;
		}
		else if($scope.surveyQuestions.type=="SURVEY_10" && $scope.multiAnswer.length == (Object.keys($scope.subAnswer).length) && $scope.multiAnswer.length>0){
			$scope.submitButtonDisabled = false;
		}
		else{
			$scope.submitButtonDisabled = true;
		}
	}
	$scope.subOptionThreeBoxHolder = function(_qstOrder, _ans, _subQstOrder, _subAns, _mainQstOrder){
		// console.log(_qstOrder, _ans, _subQstOrder, _subAns, _mainQstOrder);
		if($scope.sub_sub_answer[_mainQstOrder]){
			$scope.sub_sub_answer[_mainQstOrder].push({"order":_qstOrder, "text":_ans.text, "score":_ans.score});
		}else{
			$scope.sub_sub_answer[_mainQstOrder] = [];
			$scope.sub_sub_answer[_mainQstOrder].push({"order":_qstOrder, "text":_ans.text, "score":_ans.score});
		}
		
		$scope.optionValueHandler(_mainQstOrder, _ans);
		$scope.subOptionValueHandler(_qstOrder, _subAns, _mainQstOrder);
	}
	$scope.subOptionDropDown = function(_qstOrder, _subQstOrder, _subAns, _mainQstOrder){
		if($scope.subAnswerDropDown[_mainQstOrder-1]==undefined){
			$scope.subAnswerDropDown[_mainQstOrder-1] = {};
		}
		$scope.subAnswerDropDown[_mainQstOrder-1][_subQstOrder] = {"order":_subAns.order, "text":_subAns.text, "score":_subAns.score};
		// console.log(_qstOrder, _subQstOrder, _subAns, _mainQstOrder);
		// console.log($scope.subAnswerDropDown);
		if($scope.surveyQuestions.type=="SURVEY_17" && _mainQstOrder == 1){
			$ionicSlideBoxDelegate.next(2000);
		}
		else if($scope.surveyQuestions.type=="SURVEY_17" && _mainQstOrder == 2 && (Object.keys($scope.subAnswerDropDown[1]).length) == 2){
			$ionicSlideBoxDelegate.next(2000);
		}
	}
	$scope.openOverallAbility = function(detail,title,cssClass,option,questionNo){
		$scope.overallAbilityDetail = detail;
		$scope.previousOverallOption = option;
		$scope.questionNo = questionNo;
		$scope.currentOverallAbility = $scope.modelAnswer[$scope.questionNo].order;
		$scope.modelAnswer[$scope.questionNo].order = "";
		$scope.overallAbilityPopup = $ionicPopup.show({
			title:title.toUpperCase(),
			templateUrl: 'templates/authenticationModules/registrationModule/overallAbilityPopup.html',
			cssClass:"appStart "+cssClass,
			scope: $scope
		});
		closePopupService.register($scope.overallAbilityPopup);
	}

	$scope.previousOverallValue = "";
	$scope.confirmOverallAbility = function(){
    $scope.previousOverallValue = $scope.currentOverallAbility;
    $scope.modelAnswer[$scope.questionNo].order = $scope.currentOverallAbility;
    $scope.overallAbilityPopup.close();
    $scope.optionValueHandler($scope.questionNo, $scope.previousOverallOption)
  }

  $scope.cancelOverallAbility = function(){
    $scope.modelAnswer[$scope.questionNo].order = $scope.previousOverallValue;
    $scope.overallAbilityPopup.close();
  }

	$scope.submitHandler = function() {
		$scope.submitButtonDisabled = true;
		$scope.showLoader();
		var response = [];

		var keysInAnswerArray = Object.keys($scope.answer);

		// for(var i in $scope.answer){
		if($scope.surveyQuestions.type=="SURVEY_18"){
			for(var i=0;i<keysInAnswerArray.length;i++){
				response.push({
					"order": $scope.fetchedQuestions[i].order,
					"question": $scope.fetchedQuestions[i].question.text,
					"type": $scope.fetchedQuestions[i].question.type,
					"preQuestion": $scope.fetchedQuestions[i].question.preQuestion,
					"category": $scope.fetchedQuestions[i].question.category,
					"answer": {
						"options": [{
							"text": $scope.answer[keysInAnswerArray[i]].text,
							"order": $scope.answer[keysInAnswerArray[i]].order,
							"score": $scope.answer[keysInAnswerArray[i]].score
						}]
					}
				})
			}
		}else{
			for(var i=0;i<keysInAnswerArray.length;i++){
				response.push({
					"order": $scope.fetchedQuestions[i].order,
					"question": $scope.fetchedQuestions[i].question.text,
					"type": $scope.fetchedQuestions[i].question.type,
					"preQuestion": $scope.fetchedQuestions[i].question.preQuestion,
					"answer": {
						"options": [{
							"text": $scope.answer[keysInAnswerArray[i]].text,
							"order": $scope.answer[keysInAnswerArray[i]].order,
							"score": $scope.answer[keysInAnswerArray[i]].score
						}]
					}
				})
			}
		}
		if($scope.surveyQuestions.type=="SURVEY_9" && $scope.answer[1].order<5){
			if($scope.sub_sub_answer){
				for(var i in $scope.sub_sub_answer){
					console.log(i);
					var multipleAnswer = {
						"order": $scope.fetchedQuestions[Number(i)-1].order,
						"question": $scope.fetchedQuestions[Number(i)-1].question.text,
						"type": $scope.fetchedQuestions[Number(i)-1].question.type,
						"preQuestion": $scope.fetchedQuestions[Number(i)-1].question.preQuestion,
						"answer": {
							"options": $scope.sub_sub_answer[i]
						}
					}
					response.splice(i-1, 1, multipleAnswer);
				}
			}
		}

		if($scope.surveyQuestions.type=="SURVEY_10" && $scope.multiAnswer){
			response[0].answer.options = $scope.multiAnswer;
		}

		// For sub options
		if(Object.keys($scope.subAnswer).length > 0){
			response[$scope.subQustOrder-1].subAnswer = $scope.subAnswer;
		}

		if($scope.surveyQuestions.type=="SURVEY_17" && $scope.subAnswerDropDown){
			for(var i=0;i<response.length;i++){
				if($scope.subAnswerDropDown[i] && response[i].answer.options[0].order == 1){
					response[i].subAnswer = $scope.subAnswerDropDown[i];
				}
			}
		}

		console.log(response);

		answerObj.push({"type":$scope.surveyQuestions.title,"category":$scope.surveyQuestions.category,"title":$scope.surveyQuestions.type, "answers":angular.copy(response)});
		
		// For this survey only.
		if($scope.surveyQuestions.type=="SURVEY_4"){
			answerObj[0].currentTreatment = $scope.activeUser.info.treatmentHistory.currentTreatment.id;
		}

		if (typeof window.ga !== "undefined") {
      window.ga.trackEvent('Survey', 'Submit - '+$scope.surveyQuestions.title, 'Submit', 1);
	  }


		console.log(answerObj);
		if(surveyInfo.surveys[++currentSurveyIndex]){ 
			var timeoutPro = $timeout(function(){
				fetchSurveyQuestions(surveyInfo.surveys[currentSurveyIndex]);
			},100); 
		}else{
			if(answerObj.length<1){
				answerObj.push({});
			}
			console.log(surveyInfo);
			$scope.relapseTrackerId = null;
			if($scope.relapseTrack.startRelapse){
				$scope.relapseTrackerId = $scope.relapseTrack.kinveyData._id;
			}
			var userAdditionalDetails = {};
			userAdditionalDetails.currentTreatment = angular.copy($scope.activeUser.info.treatmentHistory.currentTreatment);
			userAdditionalDetails.overallAbility = angular.copy($scope.activeUser.info.msHistoryB.overallAbility);
			userAdditionalDetails.employmentStatus = angular.copy($scope.activeUser.info.personalInformation.employmentStatus);
			userAdditionalDetails.currentDiagnosis = angular.copy($scope.activeUser.info.msHistoryA.currentDiagnosis);
			userAdditionalDetails.insurance = angular.copy($scope.activeUser.info.personalInformation.insurance);
			userAdditionalDetails.age = moment().diff($scope.activeUser.info.personalInformation.dob, 'years', true);

			/*speacial case for overall ability survey and insurance survey*/
			if(answerObj[0] && answerObj[0].title == "SURVEY_1" && answerObj[0].answers[0]){
				userAdditionalDetails.overallAbility = angular.copy(answerObj[0].answers[0].answer.options[0].text);
			}
			if(answerObj[0] && answerObj[0].title == "SURVEY_23" && answerObj[0].answers[1]){
				userAdditionalDetails.employmentStatus = angular.copy(answerObj[0].answers[0].answer.options[0]);
				userAdditionalDetails.insurance = angular.copy(answerObj[0].answers[1].answer.options[0]);
			}
			/*speacial case for overall ability survey and insurance survey*/
			$scope.prepareResponseArray = [];
			$scope.saveArrayLength = 1
			if(surveyInfo.differentCycle){
				for(var i=0;i<surveyInfo.surveys.length;i++){
					$scope.prepareResponseArray.push({"userId":$kinvey.User.getActiveUser()._id,"notificationId":surveyInfo.notificationId,"userResponse_1":answerObj[i], "userResponse_2":null,"cycle":surveyInfo.differentCycle[surveyInfo.surveys[i]],"relapseTrackerId":$scope.relapseTrackerId,"currentTreatment":userAdditionalDetails.currentTreatment,"overallAbility":userAdditionalDetails.overallAbility,"employmentStatus":userAdditionalDetails.employmentStatus,"currentDiagnosis":userAdditionalDetails.currentDiagnosis,"insurance":userAdditionalDetails.insurance,"age":userAdditionalDetails.age,"appVersion":CONSTANT.appVersion});
				}
				console.log($scope.prepareResponseArray);
				$scope.saveArrayLength = $scope.prepareResponseArray.length;
				$scope.saveSurveyResponse($scope.prepareResponseArray[0],0);
			}else{
				$scope.prepareResponseArray.push({"userId":$kinvey.User.getActiveUser()._id,"notificationId":surveyInfo.notificationId,"userResponse_1":answerObj[0], "userResponse_2":answerObj[1],"cycle":surveyInfo.cycle,"relapseTrackerId":$scope.relapseTrackerId,"currentTreatment":userAdditionalDetails.currentTreatment,"overallAbility":userAdditionalDetails.overallAbility,"employmentStatus":userAdditionalDetails.employmentStatus,"currentDiagnosis":userAdditionalDetails.currentDiagnosis,"insurance":userAdditionalDetails.insurance,"age":userAdditionalDetails.age,"appVersion":CONSTANT.appVersion});
				$scope.saveArrayLength = $scope.prepareResponseArray.length;
				console.log($scope.prepareResponseArray);
				$scope.saveSurveyResponse($scope.prepareResponseArray[0],0);
			}
		}
	}
	var surveyEntryArray = [];
	$scope.saveSurveyResponse = function(request,index){
		var promise = pastSurveyAnswerCollection.save(request)
		.then(function(entity) {
			surveyEntryArray.push(entity._id);
			if(index == 0){
				$scope.$emit('saveGamificationPoints',CONSTANT.gamificationPoints.proSurvey.value,CONSTANT.gamificationPoints.proSurvey.name);
			}
			if($scope.saveArrayLength > (index+1)){
				$scope.saveSurveyResponse($scope.prepareResponseArray[index+1],index+1);
			}else {
				$scope.lastSelectedValue = [];
				console.log()
				if($scope.relapseTrack.startRelapse){
					$scope.updateSurveyTracker(surveyEntryArray);
				}else if($scope.isUpdateUserCopy){
					$scope.updateUserCopy();
				}else{
					$scope.openSurveyComplete();
					$scope.hideLoader();
				}	
			}
		}).catch(function(error) {
			$scope.submitButtonDisabled = false;
			$scope.internalGlitch();
			console.log("error " + error);
		});
	}

	//console.log($scope.fetchedQuestions.length);
	$scope.slideHasChanged = function(index) {
		if($scope.surveyQuestions){
			$scope.fillWidth = ((index+1) * 100) / $scope.fetchedQuestions.length;
			if($scope.fillWidth == 100){
				$scope.progressBarFilled = true;
			} else {
				$scope.progressBarFilled = false;
			}
			updateSubmitButtonStatus();
		}
		
	}

	$scope.rightSwipeProSurvey = function(){
		if(($scope.surveyQuestions.type=="SURVEY_10" || $scope.surveyQuestions.type=="SURVEY_1") && $ionicSlideBoxDelegate.currentIndex() != 0 ){
			console.log("do nothing");
		}
		else if($ionicSlideBoxDelegate.currentIndex() != 0){
			$ionicSlideBoxDelegate.previous();
			$scope.sub_sub_answer = {};
		}
	}
	$scope.leftSwipeProSurvey = function(){
		if(($scope.surveyQuestions.type=="SURVEY_10" || $scope.surveyQuestions.type=="SURVEY_1")){
			console.log("do nothing");
		}
		else if($scope.surveyQuestions.type=="SURVEY_3" && (Object.keys($scope.answer).length) > $ionicSlideBoxDelegate.currentIndex()){
			if(($scope.answer[1].order == 1 && (Object.keys($scope.subAnswer).length) > 0) || $scope.answer[1].order == 2){
				$ionicSlideBoxDelegate.next();
				$scope.sub_sub_answer = {};
			}
		}
		else if($scope.surveyQuestions.type=="SURVEY_17" && (Object.keys($scope.answer).length) > $ionicSlideBoxDelegate.currentIndex()){

			if(Object.keys($scope.answer).length >= 1 && (($scope.answer[1].order == 1 && (Object.keys($scope.subAnswerDropDown).length) > 0) || $scope.answer[1].order == 2)){
				$ionicSlideBoxDelegate.next();
				$scope.sub_sub_answer = {};
			}
			else if($scope.answer[2] && ($scope.answer[2].order == 1 && $scope.subAnswerDropDown[1] !=undefined && (Object.keys($scope.subAnswerDropDown[1]).length) == 2) || $scope.answer[1].order == 2){
				$ionicSlideBoxDelegate.next();
				$scope.sub_sub_answer = {};
			}
		}
		else if((Object.keys($scope.answer).length) > $ionicSlideBoxDelegate.currentIndex()){
			$ionicSlideBoxDelegate.next();
			$scope.sub_sub_answer = {};
		}
	}

	function updateSubmitButtonStatus(){
		if(Object.keys($scope.answer).length == $scope.fetchedQuestions.length){
			$scope.submitButtonDisabled = false;
		}else{
			/*if($scope.surveyQuestions.type=="SURVEY_9" && $scope.answer[1] && $scope.answer[1].order>5 && Object.keys($scope.answer).length == $scope.fetchedQuestions.length-1){
				$scope.submitButtonDisabled = false;
			}else{*/
				$scope.submitButtonDisabled = true;
			// }
		}
	}

	$scope.updateUserCopy = function(){
		$scope.userCopy = {}
		var query = new $kinvey.Query();
		query.equalTo('_acl.creator', $kinvey.User.getActiveUser()._id);
		userCopyCollecton.find(query).subscribe(function(data) {
			$scope.userCopy = data[0];
		}, function(error) {
			$scope.internalGlitch();
		}, function(data) {
			if($scope.userCopy.surveyCycles){
				$scope.userCopy.surveyCycles[$scope.paramsForUpdateCopy.surveyName] = $scope.paramsForUpdateCopy.surveyCycle;
			}else{
				$scope.userCopy.surveyCycles = {};
				$scope.userCopy.surveyCycles[$scope.paramsForUpdateCopy.surveyName] = $scope.paramsForUpdateCopy.surveyCycle;
			}
			var promise = userCopyCollecton.update($scope.userCopy).then(function(entity) {
				$scope.openSurveyComplete();
				$scope.hideLoader();
        console.log("updateUserCopy");
      }).catch(function(error) {
        $scope.internalGlitch();
        console.log(error);
      });
		});
	}

	$scope.updateSurveyTracker = function(surveyIdArray) {
		$scope.relapseSurveytracker = $scope.relapseTrack.kinveyData;
    if ($scope.relapseSurveytracker.surveyIds == undefined || $scope.relapseSurveytracker.surveyIds == "") {
      $scope.relapseSurveytracker.surveyIds = [];
    }
    for(var i=0;i<surveyIdArray.length;i++){
    	$scope.relapseSurveytracker.surveyIds.push({ "surveyId": surveyIdArray[i] });
    }
    $scope.relapseSurveytracker = JSON.parse(angular.toJson($scope.relapseSurveytracker));
    relapseList.update($scope.relapseSurveytracker).then(function(entity) {
      $scope.relapseSurveytracker = {};
      $scope.$emit('refreshRelapseData');
      console.log(JSON.stringify(entity));
      if($scope.isUpdateUserCopy){
				$scope.updateUserCopy();
			}else{
				$scope.openSurveyComplete();
				$scope.hideLoader();
			}
    }).catch(function(error) {
    	if($scope.isUpdateUserCopy){
				$scope.updateUserCopy();
			}else{
				$scope.openSurveyComplete();
			}
      $scope.internalGlitch();
      console.log(error);
  	});
	}
	$scope.openSurveyComplete = function() {
		$scope.surveyCompletePopup = $ionicPopup.show({
			title:"Thank you! Your survey <br>has been submitted.",
			template:'<div class="SurveyBadge kelsonSansBold">+25<span>PTS</span></div><p class="text-center orange bold">You’ve earned 25 points!</p><div class="height10"></div><p class="text-center">You can review your results on your<br>patient dashboard, and compare your<br>results with other MS patients.</p><div class="height20"></div>',
			scope: $scope,
			cssClass:'finishedSurvey',
			buttons: [{
				text: 'HOME',
				type: 'button-block button-darkPurple',
				onTap: function() {
					return true;
				}
			}]
		});
		closePopupService.register($scope.surveyCompletePopup);
		$scope.surveyCompletePopup.then(function(res){
			if (typeof window.ga !== "undefined") {
	      window.ga.trackEvent('Home ', 'Home Screen (Survey)', 'Home Screen', 1);
	    }
			//$state.go('app.home')
			$scope.navigateToHome();
		});

	}

	/*$ionicModal.fromTemplateUrl('templates/modal.html', {
		scope: $scope
	}).then(function(modal) {
		$scope.modal = modal;
		$scope.modal.show();
	});*/



	$scope.showPopOnExitSurvey = function(){
		$scope.cancelTestPopup = $ionicPopup.show({
			title:"Are you sure you want to cancel the survey?",
			template:'<p style="text-align: center;">All questions within a survey must be answered at the same time. Leaving the survey now will cause your previous answer to be lost.</p><div class="height10"></div><a class="popClose" ng-click="cancelTestPopup.close()"></a>',
			scope: $scope,
			buttons: [{
				text: 'CANCEL',
				type: 'button-block button-outline button-darkPurple skip',
				onTap: function() {
					return true;
				}
			},{
				text: 'CONTINUE',
				type: 'button-block button-darkPurple',
				onTap: function() {
					return false;
				}
			}]
		});
		closePopupService.register($scope.cancelTestPopup);
		$scope.cancelTestPopup.then(function(res){
			if(res){
				$ionicSideMenuDelegate.toggleLeft();
			}
		});
	}

	$scope.$on('showCancelSurveyPopup',function(){
		$scope.showPopOnExitSurvey();
	});


	$scope.getSortBy = function(option){
		console.log(option);
		// if(_option.sort){return "sort"}
		// else { return "order"}
	}


	$scope.$on('$ionicView.unloaded', function(){
		$ionicHistory.clearHistory();
		$ionicHistory.clearCache();
	});
}]);