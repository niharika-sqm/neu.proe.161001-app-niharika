neuroScienceApp.controller('reactionTestCtrl', ['$scope', '$state', '$interval', 'randomTime', '$ionicPopup', '$kinvey', '$ionicScrollDelegate', 'CONSTANT', '$q', '$stateParams','$timeout','closePopupService', function($scope, $state, $interval, randomTime, $ionicPopup, $kinvey, $ionicScrollDelegate, CONSTANT, $q, $stateParams,$timeout,closePopupService) {

  $timeout(function(){
    window.ga.trackView('Reaction Test');
  }, 1000);

  $scope.$on('$ionicView.enter', function() {
    $scope.testDisplay = false;
    $scope.visual = true;
    $scope.timerForShow = 0;
    $scope.startVisualTrial = false;
    $scope.showAudio = false;
    $scope.cong = false;
    $scope.startTestDisplay = true;
    $scope.tapColor = "";
    $scope.visualScore = 0;
    $scope.audioScore = 0;
    $scope.totalScore = 0;
    $scope.retakeCancelBtnDisabled = false;
    $scope.isTrailTaken = $stateParams.test.isTrailTaken;
    if ($stateParams.test != null) {
      $scope.notificationId = $stateParams.test.notificationId;
      $scope.attempts = $stateParams.test.attempts;
      $scope.testResultId = $stateParams.test.testResultId;
      $scope.cycle = $stateParams.test.cycle;
      $scope.type = $stateParams.test.type;
      $scope.isPointGiven = $stateParams.test.isPointGiven;
      $stateParams.test.relapseTrackerId == undefined ? $scope.relapseTrackerId = "" : $scope.relapseTrackerId = $stateParams.test.relapseTrackerId;
      delete $stateParams.test;
    }
  });

  $scope.$on('$ionicView.afterEnter', function() {
    $scope.lockOrientation();
  });

  var simpleReaction;
  var pathVariable = getDeviceDimension();

  function getDeviceDimension() {
    var innerWidth = window.innerWidth;
    if (innerWidth < 360) {
      return "img/phaser/iPhone5/";
    } else if (innerWidth >= 360 && innerWidth < 400) {
      return "img/phaser/iPhone6/";
    } else if (innerWidth >= 400 && innerWidth < 767) {
      return "img/phaser/iPhone6Plus/";
    } else if (innerWidth >= 768) {
      return "img/phaser/iPad/";
    }
  }

  $scope.relapse = {
    relapseInfection1: null,
    relapseInfection2: null,
    relapseInfection3: null,
    relapseInfection4: null,
  };

  $scope.prePMSurveyAnswers = [
  	{"question":"Did you take your medications as usual?", "answer":null},
  	{"question":"Did you get a typical amount of sleep last night?", "answer":null},
  	{"question":"Do you have 6-8 minutes in a low distraction environment to focus on today's tests?", "answer":null},
  	{"question":"Do you feel any of these factors could impact your performance test today?", "answer":null}
  ];

  $scope.recommendationPopup = function() {
    $scope.recommendForPm = '';
    var button = '<div class="popBodyInput orange"><div class="item item-input item-borderless"><div class="radioButtonGroup"><label class="questionLabel ">Did you take your medications as usual?</label>' +

      '<div class="radioButtonWrapper yes"><input class="radioButton" type="radio" name="preRelapse1" ng-value=true ng-model="relapse.relapseInfection1"><label class="radioLabel">  Yes </label></div>' +

      '<div class="radioButtonWrapper no"><input class="radioButton" type="radio" name="preRelapse1" ng-value=false ng-model="relapse.relapseInfection1"><label class="radioLabel">  No </label></div></div></div>' +

      '<div class="item item-input item-borderless"><div class="radioButtonGroup"><label class="questionLabel ">Did you get a typical amount of sleep last night?</label>' +

      '<div class="radioButtonWrapper yes"><input class="radioButton" type="radio" name="preRelapse2" ng-value=true ng-model="relapse.relapseInfection2"><label class="radioLabel">  Yes </label></div>' +

      '<div class="radioButtonWrapper no"><input class="radioButton" type="radio" name="preRelapse2" ng-value=false ng-model="relapse.relapseInfection2"><label class="radioLabel">  No </label></div></div></div>' +

      '<div class="item item-input item-borderless"><div class="radioButtonGroup"><label class="questionLabel ">Do you have 6-8 minutes in a low distraction environment to focus on today' + "'s " + 'tests?</label>' +

      '<div class="radioButtonWrapper yes"><input class="radioButton" type="radio" name="preRelapse3" ng-value=true ng-model="relapse.relapseInfection3"><label class="radioLabel">  Yes </label></div>' +

      '<div class="radioButtonWrapper no"><input class="radioButton" type="radio" name="preRelapse3" ng-value=false ng-model="relapse.relapseInfection3"><label class="radioLabel">  No </label></div></div></div>' +

      '<div class="item item-input item-borderless"><div class="radioButtonGroup"><label class="questionLabel ">Do you feel any of these factors could impact your performance test today?</label>' +

      '<div class="radioButtonWrapper yes"><input class="radioButton" type="radio" name="preRelapse4" ng-value=true ng-model="relapse.relapseInfection4"><label class="radioLabel">  Yes </label></div>' +

      '<div class="radioButtonWrapper no"><input class="radioButton" type="radio" name="preRelapse4" ng-value=false ng-model="relapse.relapseInfection4"><label class="radioLabel">  No </label></div></div></div></div>' +

      '<div ng-show="relapse.relapseInfection1!=null && relapse.relapseInfection2!=null && relapse.relapseInfection3!=null && relapse.relapseInfection4!=null"><div ng-show="relapse.relapseInfection1==true && relapse.relapseInfection2==true && relapse.relapseInfection3==true && relapse.relapseInfection4==false"><div class="height20"></div><div class="popup-buttons" ><button class="button button-block button-darkPurple font40" ng-click="takeTestNow(true)">GREAT! LET' + "'" + 'S START TODAY' + "'" + 'S TASKS.</button></div><div class="height20"></div>' +

      '</div>' +

      '<div ng-show="!(relapse.relapseInfection1==true && relapse.relapseInfection2==true && relapse.relapseInfection3==true && relapse.relapseInfection4==false)"><p class="text-center italicFont smallText">You'+"'ve"+' indicated that some factors may affect your performance on today' + "'" + 's tasks.</p><div class="popup-buttons" ><button class="button button-block button-outline button-darkPurple font40 skip" ng-click="recommendPopup.close()">TAKE TEST <br/>LATER</button>' +

      '<div class="height10"></div><button class="button button-block button-multiline button-darkPurple font40" ng-click="takeTestNow(false)">I' + "'" + 'D RATHER TAKE <br/>THE TEST NOW</button></div></div></div></div>';

    var template = '<p class="text-center">Performance can be influenced by many factors, including health, medical treatments, and life events, even having a ' + '"bad day".' +'</p><a class="popClose" ng-click="recommendPopup.close()"></a><p class="text-center">We want you to do the best you can on the performance measures.  So tell us if today is a typical day for you by answering the questions below.</p>' + button;
    $timeout(function() {
      $scope.recommendPopup = $ionicPopup.show({
        title: "Performance Metric Testing",
        template: template,
        scope: $scope,
        cssClass: "pmStartPop"
      });
      closePopupService.register($scope.recommendPopup);
      $scope.recommendPopup.then(function(res) {
        if (!$scope.takeTestNowPopupClose) {
          $state.go('app.performanceEvals');
        }
      });
    }, 100);
  }
  $scope.takeTestNow = function(status) {
    // console.log($scope.relapse);
    // console.log(status);
    if (status) {
      $scope.recommendForPm = "yes";
    } else {
      $scope.recommendForPm = "no";
    }
    $scope.recommendPopup.close();
    $scope.takeTestNowPopupClose = true;
    for(var i = 0; i < $scope.prePMSurveyAnswers.length; i++){
      $scope.prePMSurveyAnswers[i].answer = "no";
			if ($scope.relapse["relapseInfection"+(i+1)] == true){
				$scope.prePMSurveyAnswers[i].answer = "yes";
			}
    }
    // console.log($scope.recommendForPm);
  }
  $scope.recommendationPopup();

  function initialte() {
    var t1; var t2;
    if ($scope.attempts >= 3) {
      $scope.retakeCancelBtnDisabled = true;
    }

    function cloneMessage(result) {
      var clone = {};
      for (var key in result) {
        if (result.hasOwnProperty(key)) //ensure not adding inherited props
          clone[key] = result[key];
      }
      return clone;
    };

    var totalScoreV, totalScoreA;
    var counterTrail = 1;
    var performanceTestResults = $kinvey.DataStore.getInstance('performanceTestResults', $kinvey.DataStoreType.Network);
    var notificationTestList = $kinvey.DataStore.getInstance('notifications', $kinvey.DataStoreType.Network);
    var jsonData = {},
      t = {};
    jsonData.PMType = "Reaction Time";
    jsonData.date = new Date();
    jsonData.raw = [];

    $scope.goToTest = function(test) {
      $ionicScrollDelegate.scrollTop();
      console.log(test);
      if (test == "trial") {
        if (typeof window.ga !== "undefined") {
          window.ga.trackEvent('Reaction Time Screen', 'Trail starts', 'Take A Trail', 1);
        }
        $scope.takeTrial = false;
        $scope.trialShow = false;
        $scope.tapToStartTrial = true;
        $scope.startTestDisplay = true;
        startTrialTest();
        if($scope.attempts == 0 && !$scope.isTrailTaken){
          $scope.saveInitialTrailData();
        }
      } else if (test == "main") {
        if (typeof window.ga !== "undefined") {
          window.ga.trackEvent('Reaction Time Screen', 'Test starts', 'Get Started', 1);
        }
        $scope.takeTrial = true;
        $scope.trialShow = true;
        $scope.show = true;
        startVisualTest();
      }
      $scope.testDisplay = true;
    };

    $scope.saveInitialTrailData = function(){
      $scope.showLoader();
      if($scope.testResultId == ""){
        performanceTestResults.save({
          "date": new Date(),
          "PMType":jsonData.PMType,
          "attempts":0,
          "cycle":$scope.cycle,
          "type":$scope.type,
          "prePMSurveyAnswers":$scope.prePMSurveyAnswers,
          "notificationId":$scope.notificationId,
          "isTrial":false
        }).then(function(entity) {
          console.log("entity ",entity);
          $scope.isTrailTaken = false;
          $scope.testResultId  = entity._id;
          $scope.hideLoader();
        }).catch(function(error) {
          $scope.internalGlitch();
          $state.go('app.performanceEvals');
        });
      }else{
        performanceTestResults.save({
          "_id":$scope.testResultId,
          "date": new Date(),
          "PMType":jsonData.PMType,
          "attempts":0,
          "cycle":$scope.cycle,
          "type":$scope.type,
          "prePMSurveyAnswers":$scope.prePMSurveyAnswers,
          "notificationId":$scope.notificationId,
          "isTrial":false
        }).then(function(entity) {
          console.log("entity ",entity);
          $scope.isTrailTaken = false;
          $scope.testResultId  = entity._id;
          $scope.hideLoader();
        }).catch(function(error) {
          $scope.internalGlitch();
          $state.go('app.performanceEvals');
        });
      }
    };

    $scope.saveFinalTrailData = function(){
      $scope.showLoader();
      performanceTestResults.save({
        "_id":$scope.testResultId,
        "date": new Date(),
        "PMType":jsonData.PMType,
        "attempts":0,
        "cycle":$scope.cycle,
        "type":$scope.type,
        "prePMSurveyAnswers":$scope.prePMSurveyAnswers,
        "notificationId":$scope.notificationId,
        "isTrial":true
      }).then(function(entity) {
        console.log("entity ",entity);
        $scope.isTrailTaken = true;
        $scope.testResultId  = entity._id;
        $scope.hideLoader();
      }).catch(function(error) {
        $scope.internalGlitch();
        $state.go('app.performanceEvals');
      });
    };

    $scope.testStartedKinvey = function() {
      if($scope.attempts==1){
        $scope.setPMNotificationIsShown($scope.notificationId);
         if($scope.relapseTrack.startRelapse){
          $scope.relapseTrackerId = $scope.relapseTrack.kinveyData._id;
        }
      }
      if ($scope.testResultId == "") {
        performanceTestResults.save({
          "date": new Date(),
          "PMType": jsonData.PMType,
          "attempts": $scope.attempts,
          "cycle": $scope.cycle,
          "type": $scope.type,
          "recommendForPm": $scope.recommendForPm,
          "prePMSurveyAnswers":$scope.prePMSurveyAnswers,
          "notificationId": $scope.notificationId,
          "isAborted": true,
          "isShown": $scope.attempts == 3 ? true : false,
          "isPointGiven":$scope.isPointGiven,
          "isTrial":$scope.isTrailTaken,
           "relapseTrackerId":$scope.relapseTrackerId
        }).then(function(entity) {
          console.log("entity ", entity);
          $scope.testResultId = entity._id;
          if($scope.attempts == 1 && $scope.relapseTrack.startRelapse){
            $scope.$emit('updateRelapseTracker',$scope.testResultId);
          }
          if ($scope.attempts >= 3) {
            $scope.updateNotificationCollection();
          }
        }).catch(function(error) {
          $scope.internalGlitch();
          $state.go('app.performanceEvals');
        });
      } else {
        performanceTestResults.save({
          "_id": $scope.testResultId,
          "date": new Date(),
          "PMType": jsonData.PMType,
          "attempts": $scope.attempts,
          "cycle": $scope.cycle,
          "type": $scope.type,
          "recommendForPm": $scope.recommendForPm,
          "prePMSurveyAnswers":$scope.prePMSurveyAnswers,
          "notificationId": $scope.notificationId,
          "isAborted": true,
          "isShown": $scope.attempts == 3 ? true : false,
          "isPointGiven":$scope.isPointGiven,
          "isTrial":$scope.isTrailTaken,
          "relapseTrackerId":$scope.relapseTrackerId
        }).then(function(entity) {
          console.log("entity ", entity);
          $scope.testResultId = entity._id;
          if ($scope.attempts >= 3) {
            $scope.updateNotificationCollection();
          }
        }).catch(function(error) {
          $scope.internalGlitch();
          $state.go('app.performanceEvals');
        });
      }
    }

    $scope.testAudio = function() {
      if (typeof window.ga !== "undefined") {
        window.ga.trackEvent('Reaction Time Screen', 'Play Audio', 'Test Audio', 1);
      }
      var x = document.getElementById("myAudio");
      x.play();
      $scope.showMessage('If you did not hear a sound, check your sound settings.',3000);
    };

    var startTrialTest = function() {
      //console.log($scope.takeTrial);
      $scope.startTrial = function() {
        $scope.tapToStartTrial = false;
        counterTrail = 0;
        t1 = $interval(callAtInterval, 1000);
        $scope.circleBlinksCount = 1;
      };

      function callAtInterval() {
        counterTrail++;
        if (counterTrail == 4) {
          $scope.startVisualTrial = true;
          $interval.cancel(t1);
          t1 = undefined;
          counterTrail = 0;
          t2 = $interval(callAtInterval1, 1000);
        }
      };

      function callAtInterval1() {
        counterTrail++;
        if (counterTrail == 4) {
          $interval.cancel(t2);
          t2 = undefined;
          counterTrail = 0;
          $scope.startVisualTrial = false;
          $scope.circleBlinksCount = $scope.circleBlinksCount + 1;
          if ($scope.circleBlinksCount == 4) {
            $scope.testDisplay = false;
            $scope.showMessage('Please read instructions and try again.', 3000);
          } else {
            t1 = $interval(callAtInterval, 1000);
          }
        }
      };

      $scope.tapToStopTrial = function() {
        $interval.cancel(t1);
        t1 = undefined;
        $interval.cancel(t2);
        t2 = undefined;
        $scope.tapColor = "#fadbbc";
        $timeout(function() {$scope.tapColor=""}, 300);
        $scope.startVisualTrial = false;
        $scope.startTestDisplay = false;
        $scope.cong = true;
        if($scope.attempts == 0 && !$scope.isTrailTaken){
          $scope.saveFinalTrailData();
        }
      };
      $scope.tapToStartVisualTest = function() {
        $scope.show = true;
        $scope.cong = false;
        startVisualTest();
      };
    };

    var startVisualTest = function() {
      $scope.attempts++;
      $scope.testStartedKinvey();
      if ($scope.attempts == 3) {
        $scope.retakeCancelBtnDisabled = true;
      }
      $scope.trialShow = true;
      $scope.resultScreen = false;
      $scope.startTestDisplay = false;
      var correctClick = 0,
        missedClick = 0;
      // var correctText,missedText;
      var updateTime;
      var state, graphics;
      var elapsedTime = 0;
      var circle, createCircle;
      $scope.correctClicks = 0;
      $scope.missedClicks = 0;
      var width = window.innerWidth; //-(((window.innerWidth*2.41)/100)*2)+20
      var height = width * 1.30; //- (window.innerWidth*0.1087)-(window.innerWidth*0.1690)-(window.innerWidth*0.20); //(window.innerHeight*65)/100;
      if (width > 500) {
        width = 600;
        height = width * 0.9266;
      }

      $scope.startTest = function() {
        $scope.showLoader();
        $scope.show = false;
        circle = null;
        correctClick = 0;
        missedClick = 0;
        // correctText = null;
        // missedText = null;
        updateTime = null;
        state = null;
        graphics = null;
        elapsedTime = 0;
        createCircle = null;
        $timeout(function(){
        simpleReaction = new Phaser.Game(width*3, height*3, Phaser.AUTO, 'gameDiv2', { preload: preload, create: create, update: update });
        }, 100);
      };

      $scope.$on('$destroy', function() {
        $scope.unlockOrientation();
        if (Number(correctClick) + Number(missedClick) != 4) {
          $scope.correctClicks = correctClick;
          $scope.missedClicks = missedClick;
          $scope.resultScreen = true;
        }
      });

      function preload() {
        simpleReaction.load.image('small', pathVariable + 'circleForVisual.png');
        simpleReaction.stage.backgroundColor = "#ffffff";
      }

      function create() {
        small = simpleReaction.add.sprite(simpleReaction.world.centerX, ((width*3) / 4.7), 'small');
        small.visible = false;
        small.scale.set(1);
        small.anchor.setTo(0.5, 0);
        small.inputEnabled = true;
        simpleReaction.input.onTap.add(onTap, this);
        updateTime = randomTime.randomTimeOneSix();
        $scope.hideLoader();
        // $timeout(function(){
        //   $scope.hideLoader();
        // }, 200);
        state = 0;
      }

      function onTap(pointer) {
        console.log(simpleReaction);
        if (small.visible) {
          $timeout(function() {simpleReaction.stage.backgroundColor = "#fadbbc";$scope.tapColor = "#fadbbc";}, 10);
          $timeout(function() {simpleReaction.stage.backgroundColor = "#ffffff";$scope.tapColor = "";small.visible = false;}, 300);
          correctClick++;
          t.time = elapsedTime;
          t.type = 'visual';
          t.status = 1;
          jsonData.raw.push(cloneMessage(t));

          updateTime = randomTime.randomTimeOneSix();
          elapsedTime = 0;
          state = 0;
        }
      }

      function update() {
        if ((correctClick + missedClick) >= 20) {
          state = 2;
          simpleReaction.input.enabled = false;
          console.log(correctClick, missedClick);
          totalScoreV = correctClick;
          $scope.correctClicks = correctClick;
          $scope.missedClicks = missedClick;
          $scope.visual = false;
          $scope.showAudio = true;
          perVisual = correctClick / 4;
          simpleReaction.destroy();
          simpleReaction = '';
          startAudioTest();
          $scope.$apply();
        }
        switch (state) { // When circle disappears
          case 0:
            elapsedTime += simpleReaction.time.elapsed;
            if (elapsedTime >= updateTime) {
              small.visible = true;
              elapsedTime = 0;
              state = 1
            }
            break;
          case 1: // when circle appears
            elapsedTime += simpleReaction.time.elapsed;
            if ((correctClick + missedClick) >= 20) {
              console.log(correctClick, missedClick);
            }
            if (elapsedTime >= 3000) {
              small.visible = false;
              updateTime = randomTime.randomTimeOneSix();
              elapsedTime = 0;
              state = 0;
              missedClick++;
              time = 3000;
              t.time = time;
              t.type = 'visual';
              t.status = 0;
              jsonData.raw.push(cloneMessage(t));
            }
            break;
          case 2:
            //circle.kill();
            //simpleReaction.destroy();
            break;
          default:
            state = 0;
            break;
        }
      }
    };

    var startAudioTest = function() {
      $scope.resultScreen = false;
      var correctClick = 0,
      missedClick = 0;
      var correctText, missedText;
      var updateTime;
      var state, graphics;
      var elapsedTime = 0;
      var circle, createCircle;
      $scope.correctClicksAudio = 0;
      $scope.missedClicksAudio = 0;
      var width = window.innerWidth;
      var height = width * 1.30;
      if (width > 500) {
        width = 600;
        height = width * 0.9266;
      }

      $scope.startTestAudio = function() {
        $scope.showLoader();
        $scope.showAudio = false;
        circle = null;
        correctClick = 0;
        missedClick = 0;
        correctText = null;
        missedText = null;
        updateTime = null;
        state = null;
        graphics = null;
        elapsedTime = 0;
        createCircle = null;
        $timeout(function(){
        simpleReaction = new Phaser.Game(width*3, height*3, Phaser.AUTO, 'gameDiv1', { preload: preload, create: create, update: update });
        }, 100);
      };

      function preload() {
        simpleReaction.load.audio('explosion', 'img/phaser/GS_Soundclip1.wav');
        simpleReaction.load.image('small', pathVariable + 'circleForSound.png');
        simpleReaction.stage.backgroundColor = "#ffffff";
      }
      var explosion;
      var text1;

      function create() {
        small = simpleReaction.add.sprite(simpleReaction.world.centerX, ((width*3) / 4.7), 'small');
        // small.scale.set(0.3);
        small.anchor.setTo(0.5, 0);
        small.inputEnabled = false;
        small.width = (window.innerWidth * 0.41)*3;
        small.height = (window.innerWidth * 0.41)*3;

        explosion = simpleReaction.add.audio('explosion');
        simpleReaction.input.onTap.add(onTap, this);
        // simpleReaction.inputEnabled = false;
        // small.events.onInputDown.add(onTap, this);
        $scope.hideLoader();
        updateTime = randomTime.randomTimeOneSix();
        state = 0;
      }

      var keys;

      function playFx() {
        explosion.play();
        small.inputEnabled = true;
      }

      function soundStopped(sound) {
        explosion.stop();
        small.inputEnabled = false;
      }

      function onTap(pointer) {
        if (small.inputEnabled) {
          console.log('tapped');
          $timeout(function() {simpleReaction.stage.backgroundColor = "#fadbbc";$scope.tapColor = "#fadbbc";}, 10);
          $timeout(function() {simpleReaction.stage.backgroundColor = "#ffffff";$scope.tapColor = "";small.inputEnabled = false;}, 300);

          correctClick++;
          // t.time = time;
          t.time = elapsedTime;
          console.log(elapsedTime);
          t.type = 'audio';
          t.status = 1;
          jsonData.raw.push(cloneMessage(t));
          soundStopped();
          updateTime = randomTime.randomTimeOneSix();
          elapsedTime = 0;
          state = 0;
        }
      }

      function update() {
        if ((correctClick + missedClick) >= 20) {
          totalScoreA = correctClick;
          $scope.totalScore = totalScoreA + totalScoreV;
          console.log($scope.totalScore);
          console.log(correctClick, missedClick);
          $scope.correctClicksAudio = correctClick;
          $scope.missedClicksAudio = missedClick;
          perAudio = correctClick / 4;
          $scope.visualScore = 0;
          $scope.audioScore = 0;
          for (var i = 0; i < jsonData.raw.length; i++) {
            if (jsonData.raw[i].type == "visual") {
              $scope.visualScore += jsonData.raw[i].time;
            } else {
              $scope.audioScore += jsonData.raw[i].time;
            }
          }
          $scope.visualScore = parseInt($scope.visualScore / 20);
          $scope.audioScore = parseInt($scope.audioScore / 20);
          $scope.$apply();
          state = 2;
        }


        switch (state) { // When circle disappears
          case 0:
            elapsedTime += simpleReaction.time.elapsed;
            if (elapsedTime >= updateTime) {
              playFx();
              elapsedTime = 0;
              state = 1
            }
            break;
          case 1: // when circle appears
            elapsedTime += simpleReaction.time.elapsed;
            if (elapsedTime >= 3000) {
              // graphics.visible = false;
              soundStopped();
              updateTime = randomTime.randomTimeOneSix();
              console.log(updateTime);
              elapsedTime = 0;
              state = 0;
              missedClick++;
              time = 3000;
              t.time = time;
              t.type = 'audio';
              t.status = 0;
              jsonData.raw.push(cloneMessage(t));
            }
            break;
          case 2:
            // circle.kill();
            soundStopped();
            simpleReaction.destroy();
            simpleReaction = '';
            $scope.resultScreen = true;
            $scope.updateTestResultonKinvey();
            break;
          default:
            state = 0;
            break;
        }
      }
    };

    $scope.updateNotificationCollection = function() {
      var tempdata = [];
      var col = "_id";
      var relatedJson = $scope.notificationId;
      var query = new $kinvey.Query();
      query.equalTo(col, relatedJson);
      notificationTestList.find(query).subscribe(function(data) {
        tempdata = data;
      }, function(error) {}, function(data) {
        tempdata[0].isAck = true;
        notificationTestList.update(tempdata[0]).then(function(data) {
          console.log(data);
        }).catch(function(error) {
          $scope.internalGlitch();
          console.log(error);
        });
      });
    }


    var t1, t2;
    $scope.updateTestResultonKinvey = function(cancel) {
      jsonData.score = {};
      jsonData.score.VC = $scope.correctClicks;
      jsonData.score.VM = $scope.missedClicks;
      jsonData.score.AC = $scope.correctClicksAudio;
      jsonData.score.AM = $scope.missedClicksAudio;
      jsonData.attempts = $scope.attempts;
      $scope.showLoader();
      var addpoints = false;
      if($scope.isPointGiven == false && cancel != true) {
        $scope.isPointGiven = true; addpoints = true;
      }
      if ($scope.attempts == 3) {
        $scope.retakeCancelBtnDisabled = true;
      }
      var kinveObj = {
        "_id": $scope.testResultId,
        "date": jsonData.date,
        "PMType": jsonData.PMType,
        "score1": jsonData.score,
        "attempts": jsonData.attempts,
        "recommendForPm": $scope.recommendForPm,
        "prePMSurveyAnswers":$scope.prePMSurveyAnswers,
        "raw": jsonData.raw,
        "cycle": $scope.cycle,
        "type": $scope.type,
        "visualScore": $scope.visualScore,
        "audioScore": $scope.audioScore,
        "isAborted": cancel == undefined ? false : cancel,
        "isShown": jsonData.attempts == 3 ? true : false,
        "notificationId": $scope.notificationId,
        "isPointGiven":$scope.isPointGiven,
        "isTrial":$scope.isTrailTaken,
        "relapseTrackerId":$scope.relapseTrackerId
      }
      performanceTestResults.save(kinveObj).then(function(entity) {
        console.log("entity ", entity);
        $scope.testResultId = entity._id;
        if (cancel == true) {
          $state.go('app.performanceEvals');
        } else {
          if (addpoints) {
            $scope.$emit('saveGamificationPoints', CONSTANT.gamificationPoints.pmEvaluation.value, CONSTANT.gamificationPoints.pmEvaluation.name);
          }
        }
        $scope.hideLoader();
      }).catch(function(error) {
        localStorage.setItem("unSavedPMData", JSON.stringify(kinveObj));
        localStorage.setItem("unSavedPMPoints", addpoints);
        $scope.internalGlitch();
      });
      console.log(jsonData);
    }

    $scope.retakeTest = function() {
      var template = '<a class="popClose" ng-click="retakeTestPopup.close()"></a><p class="text-center">If problems occurred while taking this test, you can retake it. However, this test is given to gain an accurate measurement of normal functioning, so please don’t re-take it just to improve your score or to practice.</p>';
      if ($scope.attempts == 2) {
        var title = "Are you sure you want to restart the test? You have 1 test remaining.";
        var buttons = [{ text: 'CONTINUE', type: 'button-block button-darkPurple', onTap: function() {
            return true; } }];
      } else {
        var title = "Are you sure you want to restart the test? You have 2 tests remaining.";
        var buttons = [{ text: 'CANCEL', type: 'button-block button-outline button-darkPurple skip', onTap: function() {
            return false; } }, { text: 'RETAKE TEST', type: 'button-block button-darkPurple', onTap: function() {
            return true; } }];
      }
      $scope.retakeTestPopup = $ionicPopup.show({
        title: title,
        template: template,
        scope: $scope,
        buttons: buttons
      });
      closePopupService.register($scope.retakeTestPopup);
      $scope.retakeTestPopup.then(function(res) {
        if (res) {
          //$scope.attempts = $scope.attempts + 1;
          if (small != undefined) {
            small.visible = false;
          }
          $scope.testDisplay = false;
          $scope.visual = true;
          $scope.timerForShow = 0;
          $scope.startVisualTrial = false;
          $scope.showAudio = false;
          $scope.cong = false;
          $scope.resultScreen = false;
          initialte();
        }
      });
    };

    $scope.cancelBtn = function() {
      var pauseScreen = "";
      if (!$scope.trialShow) {
        $scope.onCancelCall();
        return;
      } else {
        if (simpleReaction != undefined && simpleReaction.paused != undefined) {
         simpleReaction.paused = true;        }
      }
      if ($scope.attempts == 2) {
        $scope.cancelTemplate = '<p style="text-align: center;">If you cancel the test, it will count as one of your test takes. You will only have one additional chance to retake the test.</p><p  class="text-center">A test should be cancelled only if something went wrong; please don’t re-take the test just to improve your score, or to practice.</p><a class="popClose" ng-click="cancelTestPopup.close()"></a>';
      } else if ($scope.attempts == 1) {
        $scope.cancelTemplate = '<p style="text-align: center;">If you cancel the test, it will count as one of your test takes. You will only have two additional chances to retake the test.</p><p  class="text-center">A test should be cancelled only if something went wrong; please don’t re-take the test just to improve your score, or to practice.</p><a class="popClose" ng-click="cancelTestPopup.close()"></a>';
      }
      $scope.cancelTestPopup = $ionicPopup.show({
        title: "Are you sure you want to cancel the test?",
        template: $scope.cancelTemplate,
        scope: $scope,
        buttons: [{
          text: 'YES, CANCEL',
          type: 'button-block button-outline button-darkPurple skip',
          onTap: function() {
            return true;
          }
        }, {
          text: 'RETURN TO<br/> THE TEST',
          type: 'button-block button-darkPurple bold',
          onTap: function() {
            return false;
          }
        }]
      });
      closePopupService.register($scope.cancelTestPopup);
      $scope.cancelTestPopup.then(function(res) {
        if (res) {
          if (typeof window.ga !== "undefined") {
            window.ga.trackEvent('Reaction Time Screen - Cancel Popup', 'Performance Metric Screen (Reaction Time)', 'Yes, Cancel ', 1);
          }
          $scope.onCancelCall();
          $state.go('app.performanceEvals');
        } else {
          if (typeof window.ga !== "undefined") {
            window.ga.trackEvent('Reaction Time Screen - Cancel Popup', 'Cancel Pop Up', 'Return to the Test', 1);
          }
          if (simpleReaction != undefined && simpleReaction.paused != undefined) {
            simpleReaction.paused = false;
          }
          /*if (!$scope.trialShow) {
            if (pauseScreen == "t2") {
              t2 = $interval(callAtInterval1, 1000);
            } else if (pauseScreen == "t1") {
              t1 = $interval(callAtInterval, 1000);
            }
          } else {
            if (simpleReaction != undefined && simpleReaction.paused != undefined) {
              simpleReaction.paused = false;
            }
          }*/
        }
      });
    };

    $scope.onCancelCall = function(){
      if (typeof simpleReaction != 'undefined' && simpleReaction != '') {
        simpleReaction.destroy();
        simpleReaction = '';
      }
      if (t1 != undefined) {
        $interval.cancel(t1);
        t1 = undefined;
      } else if (t2 == undefined) {
        $interval.cancel(t2);
        t2 = undefined;
      }
      $scope.testDisplay = false;
      $scope.visual = true;
      $scope.timerForShow = 0;
      $scope.startVisualTrial = false;
      $scope.showAudio = false;
      $scope.cong = false;
      $scope.resultScreen = false;
      $ionicScrollDelegate.scrollTop();
    }
  };
  initialte();

}]);


// Resulted JSON :

// {
//   "PMType": "Reaction Time",
//   "date": "2016-06-29T13:40:15.845Z",
//   "raw": [{"time": 3000, "type": "visual", "status": 0 },
//     {"time": 583.8150000000023, "type": "visual", "status": 0 },
//     {"time": 490.8849999999802, "type": "visual", "status": 0 },
// 	   {"time": 425.1949999999779, "type": "visual", "status": 0 },
//     {"time": 3000, "type": "visual", "status": 0 },
//     {"time": 779.6050000000105, "type": "visual", "status": 0 },
//     {"time": 3000, "type": "visual", "status": 0 },
//     {"time": 3000, "type": "visual", "status": 0 },
//     {"time": 53.48000000001048, "type": "visual", "status": 0 },
//     {"time": 3000, "type": "visual", "status": 0 },
//     {"time": 839.1450000000186, "type": "visual", "status": 0 },
//     {"time": 3000, "type": "visual", "status": 0 } ],
//   "score": {
//     "VC": 4,
//     "VM": 2,
//     "AC": 2,
//     "AM": 4
//   },
//   "attempts": 1
// }
