neuroScienceApp.controller('trailTestCtrl', ['$scope', '$ionicPopup', '$kinvey','$ionicScrollDelegate', '$state', '$interval', 'CONSTANT', 'closePopupService', '$timeout', function($scope, $ionicPopup, $kinvey, $ionicScrollDelegate, $state, $interval, CONSTANT, closePopupService, $timeout) {
  $scope.$on('$ionicView.beforeEnter', function() {
    $scope.setStartCondition();
  });

  $timeout(function() {
    window.ga.trackView('Trail Test');
  }, 1000);

  $scope.setStartCondition = function() {
    $scope.testResult = false;
    //$scope.lockOrientation();
    $scope.showSideMenu = true;
    $scope.DescScreen = true;
    $scope.demoTrail = false;
    $scope.mainTrail = false;
    $scope.demoTestComplete = true;
    $scope.results = '';
    //$scope.attempts = 1;
    $scope.retakeCancelBtnDisabled = false;
    $scope.kinveyId = "";
    $scope.timeCount = 0;
    $scope.elapsedTime = 0;
    $scope.round = 0;
    $scope.round1Complete = false;
    $scope.rawData = [];
    $scope.round1Score = 0;
    $scope.round2Score = 0;
    $scope.ShowRound1Score = 0;
    $scope.ShowRound2Score = 0;
    $scope.testStartTime = 0;
    //$scope.roundWaitTime = 0;
    $scope.trailRound = 1;
    // $scope.mainTestStartTime = 0;
    // $scope.mainTestEndTime = 0;
    $scope.round1Time = 0;
    $scope.round2Time = 0;
  }

  $scope.$on('$destroy', function() {
    $scope.unlockOrientation();
  });

  $scope.$on('$ionicView.enter', function() {
    $timeout(function() {
      $scope.lockOrientation();
    }, 2700);
    if (localStorage.getItem("SymbolTestJsonData")) {
      var symbolJsonData = localStorage.getItem("SymbolTestJsonData");
      console.log(JSON.parse(symbolJsonData));
      $scope.parsedSymbolJsonData = JSON.parse(symbolJsonData);
      $scope.correct = $scope.parsedSymbolJsonData.score1.correct;
      $scope.attempts = $scope.parsedSymbolJsonData.attempts;
      $scope.attempts2 = $scope.parsedSymbolJsonData.attempts2;
      $scope.prePMSurveyAnswers = $scope.parsedSymbolJsonData.prePMSurveyAnswers;
      $scope.testResultId2 = $scope.parsedSymbolJsonData.testResultId2;
      $scope.testResultId = $scope.parsedSymbolJsonData.testResultId;
      $scope.cycle = $scope.parsedSymbolJsonData.cycle;
      $scope.type = $scope.parsedSymbolJsonData.type;
      $scope.notificationId = $scope.parsedSymbolJsonData.notificationId;
      $scope.isPointGiven = $scope.parsedSymbolJsonData.isPointGiven;
      $scope.isTrailTaken1 = $scope.parsedSymbolJsonData.isTrailTaken1;
      $scope.isTrailTaken2 = $scope.parsedSymbolJsonData.isTrailTaken2;
      $scope.isTrailTaken2Round2 = $scope.parsedSymbolJsonData.isTrailTaken2Round2;
			$scope.isTrailTaken2Round1 = $scope.parsedSymbolJsonData.isTrailTaken2Round1;
      $scope.relapseTrackerId = $scope.parsedSymbolJsonData.relapseTrackerId;
      $scope.recommendForPm = $scope.parsedSymbolJsonData.recommendForPm;
      if ($scope.isPointGiven == false) {
        $scope.isPointGiven = true;
        addpoints = true;
      }
      localStorage.removeItem("SymbolTestJsonData");
      var kinveObj = {
        "_id": $scope.parsedSymbolJsonData.testResultId,
        "date": $scope.parsedSymbolJsonData.date,
        "SubPMType": $scope.parsedSymbolJsonData.SubPMType,
        "PMType": $scope.parsedSymbolJsonData.PMType,
        "raw": $scope.parsedSymbolJsonData.raw,
        "cycle": $scope.cycle,
        "type": $scope.type,
        "score1": $scope.parsedSymbolJsonData.score1,
        "attempts": $scope.parsedSymbolJsonData.attempts,
        "recommendForPm": $scope.parsedSymbolJsonData.recommendForPm,
        "prePMSurveyAnswers": $scope.parsedSymbolJsonData.prePMSurveyAnswers,
        "isAborted": false,
        "isShown": $scope.parsedSymbolJsonData.attempts == 3 ? true : false,
        "notificationId": $scope.parsedSymbolJsonData.notificationId,
        "isPointGiven": $scope.isPointGiven,
        "isTrial": $scope.isTrailTaken1,
        "relapseTrackerId": $scope.relapseTrackerId
      }
      performanceTestResults.save(kinveObj).then(function(entity) {
        console.log("entity ", entity);
        localStorage.removeItem("SymbolTestJsonData");
      }).catch(function(error) {
        localStorage.setItem("unSavedPMData", JSON.stringify(kinveObj));
        $scope.internalGlitch();
      });
    }
    $scope.trialDemoTimeout = undefined;
  });

  $scope.totalCircles = [
    {
      x: 197,
      y: 210,
      text: '1'
    },
    {
      x: 263,
      y: 305,
      text: 'A'
    },
    {
      x: 154,
      y: 344,
      text: '2'
    },
    {
      x: 77,
      y: 182,
      text: 'B'
    },
    {
      x: 254,
      y: 115,
      text: '3'
    },
    {
      x: 317,
      y: 212,
      text: 'C'
    },
    {
      x: 305,
      y: 58,
      text: '4'
    },
    {
      x: 357,
      y: 93,
      text: 'D'
    },
    {
      x: 347,
      y: 273,
      text: '5'
    },
    {
      x: 293,
      y: 358,
      text: 'E'
    }
  ];

  $scope.mainTestCircles = [
    {
      x: 141,
      y: 248,
      text: '1'
    },
    {
      x: 129,
      y: 148,
      text: '2'
    },
    {
      x: 318,
      y: 395,
      text: '3'
    },
    {
      x: 82,
      y: 343,
      text: '4'
    },
    {
      x: 180,
      y: 331,
      text: '5'
    },
    {
      x: 48,
      y: 180,
      text: '6'
    },
    {
      x: 127,
      y: 35,
      text: '7'
    },
    {
      x: 335,
      y: 91,
      text: '8'
    },
    {
      x: 288,
      y: 212,
      text: '9'
    },
    {
      x: 247,
      y: 127,
      text: '10'
    },
    {
      x: 235,
      y: 202,
      text: '11'
    },
    {
      x: 315,
      y: 332,
      text: '12'
    }
  ];

  $scope.mainTestCircles2 = [
    {
      x: 249,
      y: 248,
      text: '1'
    },
    {
      x: 261,
      y: 148,
      text: 'A'
    },
    {
      x: 72,
      y: 395,
      text: '2'
    },
    {
      x: 308,
      y: 343,
      text: 'B'
    },
    {
      x: 210,
      y: 331,
      text: '3'
    },
    {
      x: 342,
      y: 180,
      text: 'C'
    },
    {
      x: 263,
      y: 35,
      text: '4'
    },
    {
      x: 55,
      y: 91,
      text: 'D'
    },
    {
      x: 102,
      y: 212,
      text: '5'
    },
    {
      x: 143,
      y: 127,
      text: 'E'
    },
    {
      x: 155,
      y: 202,
      text: '6'
    },
    {
      x: 75,
      y: 332,
      text: 'F'
    }
  ];

  var performanceTestResults = $kinvey.DataStore.getInstance('performanceTestResults', $kinvey.DataStoreType.Network);
  var notificationTestList = $kinvey.DataStore.getInstance('notifications', $kinvey.DataStoreType.Network);
  var totalTestTime = 0;


  var jsonData = {};
  jsonData.PMType = "Symbol Substitution and Trail Making";
  jsonData.SubPMType = "Trail Test";
  jsonData.date = new Date();
  jsonData.errorsMade = 0;

  var color = {
    'blue': '0x4b2d81',
    'orange': '0xf18903',
    'red': '0xff0000',
    'green': "0x4b2d81",
    'white': '0xffffff',
    'grey': "0xefefef"
  };
  var textColor = {
    'blue': '#4b2d81',
    'orange': '#f18903',
    'red': '#ff0000',
    'green': "#4b2d81",
    'white': '#ffffff',
    'grey': "#efefef"
  };

  var state = null, trailTest, trailTestTrail, a = [];

  var state = null, trailTestLower, trailTestTrailLower, aLower = [];

  function startTrailPhaser(drawDiv, test, drawDivLower) {
    $scope.showLoader();
    // trailTest.destroy();
    // trailTestLower.destroy();
    //trailTestTrail = "";
    //trailTestTrailLower = "";
    trailTest = "";
    trailTestLower = "";
    var clickedCircles = [];
    var innerWidth = window.innerWidth;
    var innerHeight = innerWidth;
    var stage = null;
    var stageForLine = null;
    var clicks = 0;
    var firstClickPoint = {},
      secondClickPoint = {},
      j = 0;
    var updateTime = null;

    var graphics = null;
    var graphicsLower = null;

    if (innerWidth > 500) { // For Ipad
      innerWidth = 600;
      innerHeight = 590;
    }
    phaserWidth = (innerWidth * 0.95) - 2; //390
    differenceInWidthPercent = (390 - phaserWidth) / 390;
    differenceInLowerWidthPercent = (390 - (phaserWidth * 3)) / 390;
    phaserheight = (innerHeight * 1.04) - 3; //phaserheight = 500 - (500*differenceInWidthPercent);

    trailTest = new Phaser.Game(phaserWidth, phaserheight, Phaser.AUTO, drawDiv, {
      preload: preload,
      create: create
    }, false, false);

    trailTestLower = new Phaser.Game(phaserWidth * 3, phaserheight * 3, Phaser.AUTO, drawDivLower, {
      preload: preloadLower,
      create: createLower,
      update: update
    }, false, false);


    // pathVariable = 'img/trailTest/backup/';
    pathVariable = getDeviceDimension();

    function getDeviceDimension() {
      if (window.innerWidth < 360) {
        return "img/trailTest/iPhone5/";
      } else if (window.innerWidth >= 360 && window.innerWidth < 400) {
        return "img/trailTest/iPhone6/";
      } else if (window.innerWidth >= 400 && window.innerWidth < 767) {
        return "img/trailTest/iPhone6Plus/";
      } else if (window.innerWidth >= 768) {
        return "img/trailTest/iPad/";
      }
    }

    function preload() {
      trailTest.load.image('1B', pathVariable + 'transparent/1.png');
      trailTest.load.image('1O', pathVariable + 'transparent/1.png');
      trailTest.load.image('1W', pathVariable + 'transparent/1.png');
      trailTest.load.image('2B', pathVariable + 'transparent/1.png');
      trailTest.load.image('2O', pathVariable + 'transparent/1.png');
      trailTest.load.image('2W', pathVariable + 'transparent/1.png');
      trailTest.load.image('3B', pathVariable + 'transparent/1.png');
      trailTest.load.image('3O', pathVariable + 'transparent/1.png');
      trailTest.load.image('3W', pathVariable + 'transparent/1.png');
      trailTest.load.image('4B', pathVariable + 'transparent/1.png');
      trailTest.load.image('4O', pathVariable + 'transparent/1.png');
      trailTest.load.image('4W', pathVariable + 'transparent/1.png');
      trailTest.load.image('5B', pathVariable + 'transparent/1.png');
      trailTest.load.image('5O', pathVariable + 'transparent/1.png');
      trailTest.load.image('5W', pathVariable + 'transparent/1.png');
      trailTest.load.image('6B', pathVariable + 'transparent/1.png');
      trailTest.load.image('6O', pathVariable + 'transparent/1.png');
      trailTest.load.image('6W', pathVariable + 'transparent/1.png');

      trailTest.load.image('7B', pathVariable + 'transparent/1.png');
      trailTest.load.image('7O', pathVariable + 'transparent/1.png');
      trailTest.load.image('7W', pathVariable + 'transparent/1.png');

      trailTest.load.image('8B', pathVariable + 'transparent/1.png');
      trailTest.load.image('8O', pathVariable + 'transparent/1.png');
      trailTest.load.image('8W', pathVariable + 'transparent/1.png');

      trailTest.load.image('9B', pathVariable + 'transparent/1.png');
      trailTest.load.image('9O', pathVariable + 'transparent/1.png');
      trailTest.load.image('9W', pathVariable + 'transparent/1.png');

      trailTest.load.image('10B', pathVariable + 'transparent/1.png');
      trailTest.load.image('10O', pathVariable + 'transparent/1.png');
      trailTest.load.image('10W', pathVariable + 'transparent/1.png');

      trailTest.load.image('11B', pathVariable + 'transparent/1.png');
      trailTest.load.image('11O', pathVariable + 'transparent/1.png');
      trailTest.load.image('11W', pathVariable + 'transparent/1.png');

      trailTest.load.image('12B', pathVariable + 'transparent/1.png');
      trailTest.load.image('12O', pathVariable + 'transparent/1.png');
      trailTest.load.image('12W', pathVariable + 'transparent/1.png');

      trailTest.load.image('AB', pathVariable + 'transparent/1.png');
      trailTest.load.image('AO', pathVariable + 'transparent/1.png');
      trailTest.load.image('AW', pathVariable + 'transparent/1.png');
      trailTest.load.image('BB', pathVariable + 'transparent/1.png');
      trailTest.load.image('BO', pathVariable + 'transparent/1.png');
      trailTest.load.image('BW', pathVariable + 'transparent/1.png');
      trailTest.load.image('CB', pathVariable + 'transparent/1.png');
      trailTest.load.image('CO', pathVariable + 'transparent/1.png');
      trailTest.load.image('CW', pathVariable + 'transparent/1.png');
      trailTest.load.image('DB', pathVariable + 'transparent/1.png');
      trailTest.load.image('DO', pathVariable + 'transparent/1.png');
      trailTest.load.image('DW', pathVariable + 'transparent/1.png');
      trailTest.load.image('EB', pathVariable + 'transparent/1.png');
      trailTest.load.image('EO', pathVariable + 'transparent/1.png');
      trailTest.load.image('EW', pathVariable + 'transparent/1.png');
      trailTest.load.image('FB', pathVariable + 'transparent/1.png');
      trailTest.load.image('FO', pathVariable + 'transparent/1.png');
      trailTest.load.image('FW', pathVariable + 'transparent/1.png');
    }

    function preloadLower() {
      trailTestLower.load.image('1B', pathVariable + '1B.svg');
      trailTestLower.load.image('1O', pathVariable + '1O.svg');
      trailTestLower.load.image('1W', pathVariable + '1W.svg');
      trailTestLower.load.image('2B', pathVariable + '2B.svg');
      trailTestLower.load.image('2O', pathVariable + '2O.svg');
      trailTestLower.load.image('2W', pathVariable + '2W.svg');
      trailTestLower.load.image('3B', pathVariable + '3B.svg');
      trailTestLower.load.image('3O', pathVariable + '3O.svg');
      trailTestLower.load.image('3W', pathVariable + '3W.svg');
      trailTestLower.load.image('4B', pathVariable + '4B.svg');
      trailTestLower.load.image('4O', pathVariable + '4O.svg');
      trailTestLower.load.image('4W', pathVariable + '4W.svg');
      trailTestLower.load.image('5B', pathVariable + '5B.svg');
      trailTestLower.load.image('5O', pathVariable + '5O.svg');
      trailTestLower.load.image('5W', pathVariable + '5W.svg');
      trailTestLower.load.image('6B', pathVariable + '6B.svg');
      trailTestLower.load.image('6O', pathVariable + '6O.svg');
      trailTestLower.load.image('6W', pathVariable + '6W.svg');

      trailTestLower.load.image('7B', pathVariable + '7B.svg');
      trailTestLower.load.image('7O', pathVariable + '7O.svg');
      trailTestLower.load.image('7W', pathVariable + '7W.svg');

      trailTestLower.load.image('8B', pathVariable + '8B.svg');
      trailTestLower.load.image('8O', pathVariable + '8O.svg');
      trailTestLower.load.image('8W', pathVariable + '8W.svg');

      trailTestLower.load.image('9B', pathVariable + '9B.svg');
      trailTestLower.load.image('9O', pathVariable + '9O.svg');
      trailTestLower.load.image('9W', pathVariable + '9W.svg');

      trailTestLower.load.image('10B', pathVariable + '10B.svg');
      trailTestLower.load.image('10O', pathVariable + '10O.svg');
      trailTestLower.load.image('10W', pathVariable + '10W.svg');

      trailTestLower.load.image('11B', pathVariable + '11B.svg');
      trailTestLower.load.image('11O', pathVariable + '11O.svg');
      trailTestLower.load.image('11W', pathVariable + '11W.svg');

      trailTestLower.load.image('12B', pathVariable + '12B.svg');
      trailTestLower.load.image('12O', pathVariable + '12O.svg');
      trailTestLower.load.image('12W', pathVariable + '12W.svg');

      trailTestLower.load.image('AB', pathVariable + 'aB.svg');
      trailTestLower.load.image('AO', pathVariable + 'aO.svg');
      trailTestLower.load.image('AW', pathVariable + 'aW.svg');
      trailTestLower.load.image('BB', pathVariable + 'bB.svg');
      trailTestLower.load.image('BO', pathVariable + 'bO.svg');
      trailTestLower.load.image('BW', pathVariable + 'bW.svg');
      trailTestLower.load.image('CB', pathVariable + 'cB.svg');
      trailTestLower.load.image('CO', pathVariable + 'cO.svg');
      trailTestLower.load.image('CW', pathVariable + 'cW.svg');
      trailTestLower.load.image('DB', pathVariable + 'dB.svg');
      trailTestLower.load.image('DO', pathVariable + 'dO.svg');
      trailTestLower.load.image('DW', pathVariable + 'dW.svg');
      trailTestLower.load.image('EB', pathVariable + 'eB.svg');
      trailTestLower.load.image('EO', pathVariable + 'eO.svg');
      trailTestLower.load.image('EW', pathVariable + 'eW.svg');
      trailTestLower.load.image('FB', pathVariable + 'fB.svg');
      trailTestLower.load.image('FO', pathVariable + 'fO.svg');
      trailTestLower.load.image('FW', pathVariable + 'fW.svg');
    }

    function create() {
      trailTest.stage.backgroundColor = "#ffffff";
      graphics = trailTest.add.graphics(0, 0);
      permissionTotap = true;
      $scope.showLoader();
      $timeout(function() {
        $scope.hideLoader();
      }, 1000);


      if (test == 'trial') {
        if($scope.trailRound == 1){
          var trailAlphabets = [];
          var num = 1;
          var round1TotalCircle = [
            {
              x: 197,
              y: 210,
              text: '1'
            },
            {
              x: 154,
              y: 344,
              text: '2'
            },
            {
              x: 48,
              y: 180,
              text: '3'
            },
            {
              x: 197,
              y: 58,
              text: '4'
            },
            {
              x: 270,
              y: 300,
              text: '5'
            },
            {
              x: 310,
              y: 150,
              text: '6'
            }
          ];
          $scope.totalCircles = round1TotalCircle;
          for (var i = 0; i < $scope.totalCircles.length; i++) {
            var trialX = $scope.totalCircles[i].x - ($scope.totalCircles[i].x * differenceInWidthPercent);
            var trialY = $scope.totalCircles[i].y - ($scope.totalCircles[i].y * differenceInWidthPercent);
            drawCirclesImage(trialX, trialY, num + 'W', $scope.totalCircles[i].text);
            num = num + 1;
          }
        }else{
          var round2TotalCircle = [
            {
              x: 197,
              y: 210,
              text: '1'
            },
            {
              x: 263,
              y: 305,
              text: 'A'
            },
            {
              x: 154,
              y: 344,
              text: '2'
            },
            {
              x: 77,
              y: 182,
              text: 'B'
            },
            {
              x: 254,
              y: 115,
              text: '3'
            },
            {
              x: 317,
              y: 212,
              text: 'C'
            },
            {
              x: 305,
              y: 58,
              text: '4'
            },
            {
              x: 357,
              y: 93,
              text: 'D'
            },
            {
              x: 347,
              y: 273,
              text: '5'
            },
            {
              x: 293,
              y: 358,
              text: 'E'
            }
          ];
          $scope.totalCircles = round2TotalCircle;
          var trailAlphabets = ['A', 'B', 'C', 'D', 'E'];
          var num = 1;
          for (var i = 0; i < $scope.totalCircles.length; i++) {
            var trialX = $scope.totalCircles[i].x - ($scope.totalCircles[i].x * differenceInWidthPercent);
            var trialY = $scope.totalCircles[i].y - ($scope.totalCircles[i].y * differenceInWidthPercent);
            if (isEven(i + 1)) {
              drawCirclesImage(trialX, trialY, trailAlphabets.shift() + 'W', $scope.totalCircles[i].text);
            } else {
              drawCirclesImage(trialX, trialY, num + 'W', $scope.totalCircles[i].text);
              num = num + 1;
            }
          }
        }
        // Image approach
      } else {
        var num = 1;
        var mainAlphabets = ['A', 'B', 'C', 'D', 'E', 'F'];
        if ($scope.round == 1) {
          mainAlphabets = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12']
        }
        for (var i = 0; i < $scope.roundCircles.length; i++) {
          var trialX = $scope.roundCircles[i].x - ($scope.roundCircles[i].x * differenceInWidthPercent);
          var trialY = $scope.roundCircles[i].y - ($scope.roundCircles[i].y * differenceInWidthPercent);
          if ($scope.round == 1) {
            drawCirclesImage(trialX, trialY, num + 'W', $scope.roundCircles[i].text);
            num = num + 1;

          } else if ($scope.round == 2) {
            if (isEven(i + 1)) {
              drawCirclesImage(trialX, trialY, mainAlphabets.shift() + 'W', $scope.roundCircles[i].text);
            } else {
              drawCirclesImage(trialX, trialY, num + 'W', $scope.roundCircles[i].text);
              num = num + 1;
            }
          }
        }

      }
      $timeout(function() {
        state = 1;
      }, 1000);

    }

    function createLower() {
      trailTestLower.stage.backgroundColor = "#EFEFEF";
      graphicsLower = trailTestLower.add.graphics(0, 0);
      permissionTotap = true;

      if (test == 'trial') {
        if($scope.trailRound == 1){
          var trailAlphabets = [];
          var round1TotalCircle = [
            {
              x: 197,
              y: 210,
              text: '1'
            },
            {
              x: 154,
              y: 344,
              text: '2'
            },
            {
              x: 48,
              y: 180,
              text: '3'
            },
            {
              x: 197,
              y: 58,
              text: '4'
            },
            {
              x: 270,
              y: 300,
              text: '5'
            },
            {
              x: 310,
              y: 150,
              text: '6'
            }
          ];
          $scope.totalCircles = round1TotalCircle;
          var num = 1;
          for (var i = 0; i < $scope.totalCircles.length; i++) {
            var trialX = $scope.totalCircles[i].x - ($scope.totalCircles[i].x * differenceInLowerWidthPercent);
            var trialY = $scope.totalCircles[i].y - ($scope.totalCircles[i].y * differenceInLowerWidthPercent);
            drawLowerCirclesImage(trialX, trialY, num + 'W', $scope.totalCircles[i].text);
            num = num + 1;
          }
        }else{
          var trailAlphabets = ['A', 'B', 'C', 'D', 'E'];
          var round2TotalCircle = [
            {
              x: 197,
              y: 210,
              text: '1'
            },
            {
              x: 263,
              y: 305,
              text: 'A'
            },
            {
              x: 154,
              y: 344,
              text: '2'
            },
            {
              x: 77,
              y: 182,
              text: 'B'
            },
            {
              x: 254,
              y: 115,
              text: '3'
            },
            {
              x: 317,
              y: 212,
              text: 'C'
            },
            {
              x: 305,
              y: 58,
              text: '4'
            },
            {
              x: 357,
              y: 93,
              text: 'D'
            },
            {
              x: 347,
              y: 273,
              text: '5'
            },
            {
              x: 293,
              y: 358,
              text: 'E'
            }
          ];
          $scope.totalCircles = round2TotalCircle;
          // Image approach
          var num = 1;
          for (var i = 0; i < $scope.totalCircles.length; i++) {
            var trialX = $scope.totalCircles[i].x - ($scope.totalCircles[i].x * differenceInLowerWidthPercent);
            var trialY = $scope.totalCircles[i].y - ($scope.totalCircles[i].y * differenceInLowerWidthPercent);
            if (isEven(i + 1)) {
              drawLowerCirclesImage(trialX, trialY, trailAlphabets.shift() + 'W', $scope.totalCircles[i].text);
            } else {
              drawLowerCirclesImage(trialX, trialY, num + 'W', $scope.totalCircles[i].text);
              num = num + 1;
            }
          }
        }

       

      }
      else {
        var num = 1;
        var mainAlphabets = ['A', 'B', 'C', 'D', 'E', 'F'];
        if ($scope.round == 1) {
          mainAlphabets = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12']
        }
        for (var i = 0; i < $scope.roundCircles.length; i++) {
          var trialX = $scope.roundCircles[i].x - ($scope.roundCircles[i].x * differenceInLowerWidthPercent);
          var trialY = $scope.roundCircles[i].y - ($scope.roundCircles[i].y * differenceInLowerWidthPercent);
          if ($scope.round == 1) {
            drawLowerCirclesImage(trialX, trialY, num + 'W', $scope.roundCircles[i].text);
            num = num + 1;
          } else if ($scope.round == 2) {
            if (isEven(i + 1)) {
              drawLowerCirclesImage(trialX, trialY, mainAlphabets.shift() + 'W', $scope.roundCircles[i].text);
            } else {
              drawLowerCirclesImage(trialX, trialY, num + 'W', $scope.roundCircles[i].text);
              num = num + 1;
            }
          }
        }
      }
      $timeout(function() {
        $scope.testStartTime = performance.now();
        $scope.hideLoader();
      }, 1000);
      state = 1;
    }

    function drawCirclesImage(x, y, name, type) {
      a[type] = trailTest.add.sprite(x, y, name);
      a[type].scale.set(1);
      a[type].scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
      a[type].anchor.setTo(0.5);
      a[type].inputEnabled = true;
      a[type].type = type;
      a[type].events.onInputDown.add(onTap, this);
    }

    function drawLowerCirclesImage(x, y, name, type) {
      aLower[type] = trailTestLower.add.sprite(x, y, name);
      aLower[type].scale.set(1);
      aLower[type].scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
      aLower[type].anchor.setTo(0.5);
      aLower[type].inputEnabled = true;
      aLower[type].type = type;
      // aLower[type].events.onInputDown.add(onTap, this);
    }

    function isEven(n) {
      return n % 2 == 0;
    }


    function onTap(pointer) {
      $scope.currentTapTime = performance.now();
      console.log(pointer);
      var rawTap = {
        "actual": 0,
        "response": 0,
        "responseTime": $scope.currentTapTime - $scope.previousTapTime,
        "status": 0,
        "round": $scope.round
      }
      $scope.previousTapTime = $scope.currentTapTime;
      if (pointer.type && permissionTotap) {
        if (clickedCircles.indexOf(pointer.type) == -1) {

          // FOR FIRST TIME ONLY
          if (!firstClickPoint.x) {
            rawTap.responseTime = $scope.currentTapTime - $scope.testStartTime;
            if (test == 'trial') {
              // FOR CORRECT SEQUENCE MATCHING
              if (pointer.type == $scope.totalCircles[j].text) {
                firstClickPoint.x = pointer.x;
                firstClickPoint.y = pointer.y;
                clickedCircles.push({
                  'text': pointer.type,
                  'x': pointer.x,
                  'y': pointer.y,
                  'color': color.blue
                });
                drawCirclesImage(pointer.x, pointer.y, pointer.type + 'B', pointer.type);
                drawLowerCirclesImage(pointer.x * 3, pointer.y * 3, pointer.type + 'B', pointer.type);
              }

              // FOR WORNG SEQUENCE MATCHING
              else {
                permissionTotap = false;
                for (var i = 0; i < $scope.totalCircles.length; i++) {
                  if ($scope.totalCircles[i].text == pointer.type) {
                    drawCirclesImage(pointer.x, pointer.y, pointer.type + 'O', pointer.type);
                    drawLowerCirclesImage(pointer.x * 3, pointer.y * 3, pointer.type + 'O', pointer.type);
                    clickedCircles.push({
                      'text': pointer.type,
                      'x': pointer.x,
                      'y': pointer.y,
                      'color': color.red
                    });
                    $scope.elapsedTime = 0;
                    state = 0;
                    break;
                  }
                }
              }
            }
            else {
              //For 1st tap for the Main TEST
              // FOR CORRECT SEQUENCE MATCHING
              if (pointer.type == $scope.roundCircles[j].text) {
                clickedCircles.push({
                  'text': pointer.type,
                  'x': pointer.x,
                  'y': pointer.y,
                  'color': color.blue
                });
                rawTap.actual = $scope.roundCircles[j].text;
                rawTap.response = pointer.type;
                rawTap.status = 1;
                firstClickPoint.x = pointer.x;
                firstClickPoint.y = pointer.y;
                drawCirclesImage(pointer.x, pointer.y, pointer.type + 'B', pointer.type);
                drawLowerCirclesImage(pointer.x * 3, pointer.y * 3, pointer.type + 'B', pointer.type);
                $scope.rawData.push(rawTap);
              }

              // FOR WORNG SEQUENCE MATCHING
              else {
                rawTap.actual = $scope.roundCircles[j].text;
                rawTap.response = pointer.type;
                rawTap.status = 0;
                permissionTotap = false;
                for (var i = 0; i < $scope.roundCircles.length; i++) {
                  if ($scope.roundCircles[i].text == pointer.type) {
                    drawCirclesImage(pointer.x, pointer.y, pointer.type + 'O', pointer.type);
                    drawLowerCirclesImage(pointer.x * 3, pointer.y * 3, pointer.type + 'O', pointer.type);
                    clickedCircles.push({
                      'text': pointer.type,
                      'x': pointer.x,
                      'y': pointer.y,
                      'color': color.red
                    });
                    $scope.elapsedTime = 0;
                    state = 0;
                    break;
                  }
                }
                jsonData.errorsMade = jsonData.errorsMade + 1;
                $scope.rawData.push(rawTap);
              }
            }
          }

          // FOR SECOND AND MOVING FORWARD
          else {
            secondClickPoint.x = pointer.x;
            secondClickPoint.y = pointer.y;
            if (test == 'trial') {
              // FOR CORRECT SEQUENCE MATCHING
              if (pointer.type == $scope.totalCircles[j + 1].text) {
                j = j + 1;
                drawCirclesImage(pointer.x, pointer.y, pointer.type + 'B', pointer.type);
                drawLowerCirclesImage(pointer.x * 3, pointer.y * 3, pointer.type + 'B', pointer.type);
                clickedCircles.push({
                  'text': pointer.type,
                  'x': pointer.x,
                  'y': pointer.y,
                  'color': color.blue
                });
                firstClickPoint = secondClickPoint;
                secondClickPoint = {};
                var lastWord = 'E';
                if($scope.trailRound == '1'){
                  lastWord = '6';
                }
                if ($scope.totalCircles[j].text == lastWord) {
                  $interval.cancel($scope.trialDemoTimeout);
                  $scope.demoTestComplete = false;
                  console.log('End Trail');
                  /*trailTest.destroy();
                  trailTest = '';
                  trailTestLower.destroy();
                  trailTestLower = '';*/
                  if($scope.trailRound == 1){
                    if (!$scope.isTrailTaken2Round1) {
                      $scope.isTrailTaken2Round1 = true;
                      $scope.saveFinalTrailData();
                    }
                  } else {
                    if (!$scope.isTrailTaken2Round2) {
                      $scope.isTrailTaken2Round2 = true;
                      $scope.isTrailTaken2 = true;
                      $scope.saveFinalTrailData();
                    }
                  }
                  
                  $scope.$apply();
                };
              }

              // FOR WORNG SEQUENCE MATCHING
              else {
                clickedAlready = false;
                for (var i = 0; i < clickedCircles.length; i++) {
                  if (pointer.type == clickedCircles[i].text) {
                    clickedAlready = true;
                    break;
                  } else {
                    clickedAlready = false;
                  }
                }
                if (!clickedAlready) {
                  permissionTotap = false;
                  clickedCircles.push({
                    'text': pointer.type,
                    'x': pointer.x,
                    'y': pointer.y,
                    'color': color.red
                  });
                  for (var i = 0; i < $scope.totalCircles.length; i++) {
                    if (clickedCircles.length > 1 && ($scope.totalCircles[i].text == clickedCircles[clickedCircles.length - 2].text)) {
                      drawCirclesImage(pointer.x, pointer.y, pointer.type + 'B', pointer.type);
                      drawLowerCirclesImage(pointer.x * 3, pointer.y * 3, pointer.type + 'B', pointer.type);
                    }
                    if ($scope.totalCircles[i].text == clickedCircles[clickedCircles.length - 1].text) {
                      drawCirclesImage(pointer.x, pointer.y, pointer.type + 'O', pointer.type);
                      drawLowerCirclesImage(pointer.x * 3, pointer.y * 3, pointer.type + 'O', pointer.type);
                    }
                  }
                  $scope.elapsedTime = 0;
                  state = 0;
                }
              }
            }
            else {
              // For 2nd Tap on Main Test
              // FOR CORRECT SEQUENCE MATCHING
              if (pointer.type == $scope.roundCircles[j + 1].text) {
                j = j + 1;
                drawCirclesImage(pointer.x, pointer.y, pointer.type + 'B', pointer.type);
                drawLowerCirclesImage(pointer.x * 3, pointer.y * 3, pointer.type + 'B', pointer.type);
                clickedCircles.push({
                  'text': pointer.type,
                  'x': pointer.x,
                  'y': pointer.y,
                  'color': color.blue
                });

                rawTap.actual = $scope.roundCircles[j].text;
                rawTap.response = pointer.type;
                rawTap.status = 1;

                firstClickPoint = secondClickPoint;
                secondClickPoint = {};

                var lastword = '12';
                if ($scope.round == 2) {
                  lastword = 'F';
                }
                $scope.rawData.push(rawTap);
                if ($scope.roundCircles[j].text == lastword) {
                  if ($scope.round == 1) {
                    $scope.round1Time =  performance.now() - $scope.testStartTime;
                    //$scope.roundWaitTime = performance.now();
                    $scope.round1Complete = true;
                    $scope.testResult = false;
                    $scope.DescScreen = true;
                    $scope.demoTrail = false;
                    $scope.mainTrail = false;
                    $scope.$digest();
                  } else {
                    $scope.round2Time =  performance.now() - $scope.testStartTime;
                    $scope.saveTheFinalResult();
                  }
                }
              }
              // FOR WORNG SEQUENCE MATCHING
              else {
                rawTap.actual = $scope.roundCircles[j + 1].text;
                rawTap.response = pointer.type;
                rawTap.status = 0;
                clickedAlready = false;
                for (var i = 0; i < clickedCircles.length; i++) {
                  if (pointer.type == clickedCircles[i].text) {
                    clickedAlready = true;
                    break;
                  } else {
                    clickedAlready = false;
                  }
                }
                if (!clickedAlready) {
                  clickedCircles.push({
                    'text': pointer.type,
                    'x': pointer.x,
                    'y': pointer.y,
                    'color': color.red
                  });
                  permissionTotap = false;
                  for (var i = 0; i < $scope.roundCircles.length; i++) {
                    if (clickedCircles.length > 1 && ($scope.roundCircles[i].text == clickedCircles[clickedCircles.length - 2].text)) {
                      drawCirclesImage(pointer.x, pointer.y, pointer.type + 'B', pointer.type);
                      drawLowerCirclesImage(pointer.x * 3, pointer.y * 3, pointer.type + 'B', pointer.type);
                    }
                    if ($scope.roundCircles[i].text == clickedCircles[clickedCircles.length - 1].text) {
                      drawCirclesImage(pointer.x, pointer.y, pointer.type + 'O', pointer.type);
                      drawLowerCirclesImage(pointer.x * 3, pointer.y * 3, pointer.type + 'O', pointer.type);
                    }
                  }
                  jsonData.errorsMade = jsonData.errorsMade + 1;
                  $scope.elapsedTime = 0;
                  state = 0;
                }
                $scope.rawData.push(rawTap);
              }
            }
          }
        }
      }
      console.log($scope.rawData);
    }

    $scope.saveTheFinalResult = function() {
      var tempTotalTime = 0;
      tempTotalTime = $scope.round1Time + $scope.round2Time;
      $scope.ShowRound1Score = Math.round($scope.round1Time / 1000);
      $scope.ShowRound2Score = Math.round($scope.round2Time / 1000);

      //Not in use now as we are not calulating time from raw data
      // for (var l = 0; l < $scope.rawData.length; l++) {
      //   if($scope.rawData[l].round == 1){
      //     $scope.round1Score = $scope.round1Score + $scope.rawData[l].responseTime;
      //   } else if($scope.rawData[l].round == 2){
      //     $scope.round2Score = $scope.round2Score + $scope.rawData[l].responseTime;
      //   }
      //   // tempTotalTime = tempTotalTime + $scope.rawData[l].responseTime;
      // }


      // console.log(tempTotalTime, $scope.round1Score, $scope.round2Score, $scope.round1Time, $scope.round2Time);
      // console.log(totalTestTime); // old
      $scope.results = 'Results';
      $scope.testResult = true;
      $scope.DescScreen = false;
      $scope.demoTrail = false;
      $scope.mainTrail = false;
      $scope.totalTimeinterval = Math.round(tempTotalTime / 1000);
      jsonData.totalTimeinterval = tempTotalTime;
      // $scope.totalTimeinterval = Math.round(totalTestTime / 1000); //old
      // jsonData.totalTimeinterval = totalTestTime; //old
      state = 2;
      if ($scope.parsedSymbolJsonData.attempts2) {
        jsonData.attempts2 = $scope.parsedSymbolJsonData.attempts2;
      } else {
        jsonData.attempts2 = 1;
      }
      $scope.updateTestResultonKinvey();
    }


    function update() {
      if (trailTestLower && trailTestLower.time && trailTestLower.time.elapsed < 50) {
        totalTestTime += trailTestLower.time.elapsed;
      }
      if (graphicsLower) {
        graphicsLower.clear();
        if (clickedCircles.length > 1) {
          graphicsLower.clear();
          for (var i = 0; i < clickedCircles.length; i++) {
            if (clickedCircles[i + 1]) {
              graphicsLower.lineStyle(5, clickedCircles[i + 1].color);
              graphicsLower.moveTo(clickedCircles[i].x * 3, clickedCircles[i].y * 3);
              graphicsLower.lineTo(clickedCircles[i + 1].x * 3, clickedCircles[i + 1].y * 3);
            }
          }
        }
      }
      switch (state) {
        case 0:
          if (test == 'trial') {
            if (clickedCircles.length > 0 && (clickedCircles[clickedCircles.length - 1].text != $scope.totalCircles[clickedCircles.length - 1].text)) {
              $scope.elapsedTime += trailTestLower.time.elapsed;
              if ($scope.elapsedTime >= 500) {
                permissionTotap = true;
                for (var i = 0; i < $scope.totalCircles.length; i++) {
                  var trialX = $scope.totalCircles[i].x - ($scope.totalCircles[i].x * differenceInWidthPercent);
                  var trialY = $scope.totalCircles[i].y - ($scope.totalCircles[i].y * differenceInWidthPercent);
                  if ($scope.totalCircles[i].text == clickedCircles[clickedCircles.length - 1].text) {
                    a[$scope.totalCircles[i].text].visible = false;
                    aLower[$scope.totalCircles[i].text].visible = false;
                    drawCirclesImage(trialX, trialY, $scope.totalCircles[i].text + 'W', $scope.totalCircles[i].text);
                    drawLowerCirclesImage(trialX * 3, trialY * 3, $scope.totalCircles[i].text + 'W', $scope.totalCircles[i].text);
                  }
                  if (clickedCircles.length > 1 && ($scope.totalCircles[i].text == clickedCircles[clickedCircles.length - 2].text)) {
                    drawCirclesImage(trialX, trialY, $scope.totalCircles[i].text + 'B', $scope.totalCircles[i].text);
                    drawLowerCirclesImage(trialX * 3, trialY * 3, $scope.totalCircles[i].text + 'B', $scope.totalCircles[i].text);
                  }
                }
                clickedCircles.splice([clickedCircles.length - 1], 1);
                $scope.elapsedTime = 0;
                state = 1;
              }
            }
          } else {
            if (clickedCircles.length > 0 && (clickedCircles[clickedCircles.length - 1].text != $scope.roundCircles[clickedCircles.length - 1].text)) {
              $scope.elapsedTime += trailTestLower.time.elapsed;
              if ($scope.elapsedTime >= 500) {
                permissionTotap = true;
                for (var i = 0; i < $scope.roundCircles.length; i++) {
                  var x = $scope.roundCircles[i].x - ($scope.roundCircles[i].x * differenceInWidthPercent);
                  var y = $scope.roundCircles[i].y - ($scope.roundCircles[i].y * differenceInWidthPercent);
                  if ($scope.roundCircles[i].text == clickedCircles[clickedCircles.length - 1].text) {
                    a[$scope.roundCircles[i].text].visible = false;
                    aLower[$scope.roundCircles[i].text].visible = false;
                    drawCirclesImage(x, y, $scope.roundCircles[i].text + 'W', $scope.roundCircles[i].text);
                    drawLowerCirclesImage(x * 3, y * 3, $scope.roundCircles[i].text + 'W', $scope.roundCircles[i].text);
                  }
                  if (clickedCircles.length > 1 && ($scope.roundCircles[i].text == clickedCircles[clickedCircles.length - 2].text)) {
                    drawCirclesImage(x, y, $scope.roundCircles[i].text + 'B', $scope.roundCircles[i].text);
                    drawLowerCirclesImage(x * 3, y * 3, $scope.roundCircles[i].text + 'B', $scope.roundCircles[i].text);
                  }
                }
                clickedCircles.splice((clickedCircles.length - 1), 1);
                $scope.elapsedTime = 0;
                state = 1;
              }
            }
          }
          break;
        case 1:
          break;
        case 2:
          trailTest.destroy();
          trailTestLower.destroy();
          trailTest = "";
          trailTestLower = "";
          break;
        default:
          state = 0;
          break;
      }
    }
  }

  $scope.updateTestResultonKinvey = function(cancel) {
    $scope.showLoader();
    var addpoints = false;
    $scope.isPointGiven = $scope.parsedSymbolJsonData.isPointGiven;
    if ($scope.isPointGiven == false && cancel != true) {
      $scope.isPointGiven = true;
      addpoints = true;
    }
    if (jsonData.attempts == 3) {
      $scope.retakeCancelBtnDisabled = true;
    }
    var kinveyObj2 = {
      "_id": $scope.testResultId2,
      "date": jsonData.date,
      "PMType": jsonData.PMType,
      "SubPMType": jsonData.SubPMType,
      "totalTimeinterval": jsonData.totalTimeinterval,
      "attempts": $scope.attempts2,
      "cycle": $scope.cycle,
      "raw": $scope.rawData,
      "score1": $scope.round1Time,
      "score2": $scope.round2Time,
      //"roundWaitTime":$scope.roundWaitTime,
      "type": $scope.type,
      "errorsMade": jsonData.errorsMade,
      "recommendForPm": $scope.parsedSymbolJsonData.recommendForPm,
      "isAborted": cancel == undefined ? false : cancel,
      "isShown": $scope.attempts == 3 ? true : false,
      "notificationId": $scope.parsedSymbolJsonData.notificationId,
      "isPointGiven": $scope.isPointGiven,
      "isTrial": $scope.isTrailTaken2,
      "isTrial1": $scope.isTrailTaken2Round1,
      "isTrial2": $scope.isTrailTaken2Round2,
      "relapseTrackerId": $scope.relapseTrackerId
    }
    performanceTestResults.save(kinveyObj2).then(function(entity) {
      $scope.testResultId2 = entity._id;
      if (cancel == true) {
        $state.go('app.performanceEvals');
      } else {
        if (addpoints) {
          $scope.$emit('saveGamificationPoints', CONSTANT.gamificationPoints.pmEvaluation.value, CONSTANT.gamificationPoints.pmEvaluation.name);
        }
      }
      $scope.hideLoader();
      console.log("entity ", entity);
    }).catch(function(error) {
      localStorage.setItem("unSavedPMDataTrail", JSON.stringify(kinveyObj2));
      localStorage.setItem("unSavedPMPoints", addpoints);
      $scope.internalGlitch();
    });
    console.log(jsonData);
    $scope.$apply();
  }

  $scope.takeDemoTrial = function(round) {
    $ionicScrollDelegate.scrollTop();
    $scope.trailRound = round;
    if (typeof window.ga !== "undefined") {
      window.ga.trackEvent('Trail Making Screen', 'Trail starts', 'Take A Trail', 1);
    }
    $scope.demoTestComplete = true;
    $scope.DescScreen = false;
    $scope.demoTrail = true;
    $scope.trialDemoTimeout = $interval(function() {
      $interval.cancel($scope.trialDemoTimeout);
      state = 2;
      $scope.demoTrail = false;
      $scope.DescScreen = true;
      $scope.showMessage('Please read instructions and try again.', 3000);
    }, 30000);
    if(round == 2){
      trailTest.destroy();
      trailTestLower.destroy();
      trailTest = '';
      trailTestLower = '';
      $scope.roundCircles = $scope.mainTestCircles2;
      //$scope.roundWaitTime = performance.now() - $scope.roundWaitTime;
    }
    if (!$scope.isTrailTaken2Round1) {
      $scope.saveInitialTrailData();
    }
    startTrailPhaser('trailTestTrial', 'trial', 'trailTestTrialLower');
  };

  $scope.saveInitialTrailData = function() {
    $scope.showLoader();
    if ($scope.testResultId2 == "") {
      performanceTestResults.save({
        "date": new Date(),
        "PMType": jsonData.PMType,
        "SubPMType": jsonData.SubPMType,
        "attempts": $scope.attempts2,
        "cycle": $scope.cycle,
        "type": $scope.type,
        "notificationId": $scope.notificationId,
        "recommendForPm": $scope.recommendForPm,
        "isTrial": false,
        "isTrial1": $scope.isTrailTaken2Round1,
        "isTrial2": $scope.isTrailTaken2Round2,
        "isAborted": true,
        "isShown": $scope.attempts == 3 ? true : false,
        "isPointGiven": $scope.isPointGiven,
        "relapseTrackerId": $scope.relapseTrackerId
      }).then(function(entity) {
        console.log("entity ", entity);
        $scope.isTrailTaken2 = false;
        $scope.testResultId2 = entity._id;
        $scope.hideLoader();
      }).catch(function(error) {
        $scope.internalGlitch();
        $state.go('app.performanceEvals');
      });
    } else {
      performanceTestResults.save({
        "_id": $scope.testResultId2,
        "date": new Date(),
        "PMType": jsonData.PMType,
        "attempts": $scope.attempts2,
        "cycle": $scope.cycle,
        "type": $scope.type,
        "notificationId": $scope.notificationId,
        "recommendForPm": $scope.recommendForPm,
        "isTrial": false,
        "isTrial1": $scope.isTrailTaken2Round1,
        "isTrial2": $scope.isTrailTaken2Round2,
        "isAborted": true,
        "isShown": $scope.attempts == 3 ? true : false,
        "isPointGiven": $scope.isPointGiven,
        "relapseTrackerId": $scope.relapseTrackerId
      }).then(function(entity) {
        console.log("entity ", entity);
        $scope.isTrailTaken2 = false;
        $scope.testResultId2 = entity._id;
        $scope.hideLoader();
      }).catch(function(error) {
        $scope.internalGlitch();
        $state.go('app.performanceEvals');
      });
    }
  };

  $scope.saveFinalTrailData = function() {
    $scope.showLoader();
    performanceTestResults.save({
      "_id": $scope.testResultId2,
      "date": new Date(),
      "PMType": jsonData.PMType,
      "attempts": $scope.attempts2,
      "cycle": $scope.cycle,
      "type": $scope.type,
      "notificationId": $scope.notificationId,
      "recommendForPm": $scope.recommendForPm,
      "isTrial": $scope.isTrailTaken2,
      "isTrial1": $scope.isTrailTaken2Round1,
      "isTrial2": $scope.isTrailTaken2Round2,
      "isAborted": true,
      "isShown": $scope.attempts == 3 ? true : false,
      "isPointGiven": $scope.isPointGiven,
      "relapseTrackerId": $scope.relapseTrackerId
    }).then(function(entity) {
      console.log("entity ", entity);
      $scope.testResultId2 = entity._id;
      $scope.hideLoader();
    }).catch(function(error) {
      $scope.internalGlitch();
      $state.go('app.performanceEvals');
    });
  };

  $scope.startTrailTest = function() {
    if (typeof window.ga !== "undefined") {
      window.ga.trackEvent('Trail Making Screen', 'Test starts', 'Get Started', 1);
    }
    if ($scope.attempts2 == 3) {
      $scope.retakeCancelBtnDisabled = true;
    }
    $ionicScrollDelegate.scrollTop();
    //$scope.round1Complete = false;
    $scope.demoTestComplete = true;
    $scope.demoTrail = false;
    $scope.round++;
    if ($scope.round == 1) {
      if(trailTest){
        trailTest.destroy();
      }
      if(trailTestLower){
        trailTestLower.destroy();
      }
      trailTest = '';
      trailTestLower = '';
      $scope.roundCircles = $scope.mainTestCircles;
      $scope.showSideMenu = false;
      $scope.DescScreen = false;
      $scope.demoTrail = false;
      $scope.mainTrail = true;
      $scope.rawData = [];
      totalTestTime = 0;
    } else if ($scope.round == 2) {
      $scope.DescScreen = false;
      $scope.demoTrail = false;
      $scope.mainTrail = true;
      if(trailTest){
        trailTest.destroy();
      }
      if(trailTestLower){
        trailTestLower.destroy();
      }
      trailTest = '';
      trailTestLower = '';
      $scope.roundCircles = $scope.mainTestCircles2;
      //$scope.roundWaitTime = performance.now() - $scope.roundWaitTime;
    }
    $timeout(function() {
      startTrailPhaser('trailTest', 'main', 'trailTestLower');
      var c = performance.now();
    },100);

  };


  $scope.testStartedKinvey2 = function() {
    if ($scope.attempts2 == 1) {
      //$scope.setPMNotificationIsShown($scope.notificationId);
      if ($scope.relapseTrack.startRelapse) {
        $scope.relapseTrackerId = $scope.relapseTrack.kinveyData._id;
      }
    }
    if ($scope.testResultId2 == "" || $scope.testResultId2 == undefined) {
      performanceTestResults.save({
        "date": new Date(),
        "PMType": jsonData.PMType,
        "SubPMType": jsonData.SubPMType,
        "attempts": $scope.attempts2,
        "cycle": $scope.cycle,
        "type": $scope.type,
        "notificationId": $scope.notificationId,
        "recommendForPm": $scope.recommendForPm,
        "isAborted": true,
        "isShown": $scope.attempts == 3 ? true : false,
        "isPointGiven": $scope.isPointGiven,
        "isTrial": $scope.isTrailTaken2,
        "isTrial1": $scope.isTrailTaken2Round1,
        "isTrial2": $scope.isTrailTaken2Round2,
        "relapseTrackerId": $scope.relapseTrackerId
      }).then(function(entity) {
        console.log("entity ", entity);
        $scope.testResultId2 = entity._id;
        if ($scope.attempts2 == 1 && $scope.relapseTrack.startRelapse) {
          $scope.$emit('updateRelapseTracker', $scope.testResultId2);
        }
        $scope.hideLoader();
      }).catch(function(error) {
        $scope.internalGlitch();
        console.log("error " + error);
      });
    } else {
      performanceTestResults.save({
        "_id": $scope.testResultId2,
        "date": new Date(),
        "PMType": jsonData.PMType,
        "SubPMType": jsonData.SubPMType,
        "attempts": $scope.attempts2,
        "cycle": $scope.cycle,
        "type": $scope.type,
        "recommendForPm": $scope.recommendForPm,
        "notificationId": $scope.notificationId,
        "isAborted": true,
        "isShown": $scope.attempts == 3 ? true : false,
        "isPointGiven": $scope.isPointGiven,
        "isTrial": $scope.isTrailTaken2,
        "isTrial1": $scope.isTrailTaken2Round1,
        "isTrial2": $scope.isTrailTaken2Round2,
        "relapseTrackerId": $scope.relapseTrackerId
      }).then(function(entity) {
        console.log("entity ", entity);
        $scope.testResultId2 = entity._id;
        $scope.hideLoader();
      }).catch(function(error) {
        $scope.internalGlitch();
      });
    }
  };

  $scope.retakeTest = function() {
    if ($scope.parsedSymbolJsonData.attempts == 3) {
      $state.go('app.performanceEvals');
      return;
    }
    var template = '<a class="popClose" ng-click="retakeTestPopup.close()"></a><p class="text-center">If problems occurred while taking this test, you can retake it. However, this test is given to gain an accurate measurement of normal functioning, so please don’t re-take it just to improve your score or to practice.</p>';
    if ($scope.parsedSymbolJsonData.attempts == 2) {
      var title = "Are you sure you want to restart the test? You have 1 test remaining.";
      var buttons = [{
        text: 'CONTINUE',
        type: 'button-block button-darkPurple',
        onTap: function() {
          return true;
        }
      }];
    } else {
      var title = "Are you sure you want to restart the test? You have 2 tests remaining.";
      var buttons = [{
        text: 'CANCEL',
        type: 'button-block button-outline button-darkPurple skip',
        onTap: function() {
          return false;
        }
      }, {
        text: 'RETAKE TEST',
        type: 'button-block button-darkPurple',
        onTap: function() {
          return true;
        }
      }];
    }
    $scope.retakeTestPopup = $ionicPopup.show({
      title: title,
      template: template,
      scope: $scope,
      buttons: buttons
    });
    closePopupService.register($scope.retakeTestPopup);
    $scope.retakeTestPopup.then(function(res) {
      if (res) {
        if ($scope.parsedSymbolJsonData.attempts >= 4) {
          $state.go('app.performanceEvals');
        } else {
          //$scope.parsedSymbolJsonData.attempts = $scope.parsedSymbolJsonData.attempts + 1;
          $scope.parsedSymbolJsonData.attempts = $scope.attempts;
          $scope.parsedSymbolJsonData.attempts2 = $scope.attempts2;
          $scope.parsedSymbolJsonData.testResultId2 = $scope.testResultId2;
          $scope.parsedSymbolJsonData.testResultId = $scope.testResultId;
          $scope.parsedSymbolJsonData.isTrailTaken2 = $scope.isTrailTaken2;
          $scope.parsedSymbolJsonData.isTrailTaken2Round1 = $scope.isTrailTaken2Round1;
          $scope.parsedSymbolJsonData.isTrailTaken2Round2 = $scope.isTrailTaken2Round2;
          $scope.parsedSymbolJsonData.isTrailTaken1 = $scope.isTrailTaken1;
          localStorage.setItem("SymbolTestJsonData", JSON.stringify($scope.parsedSymbolJsonData));
          //$scope.attempts = $scope.attempts + 1;
          $scope.testResult = false;
          $scope.showSideMenu = true;
          $scope.DescScreen = true;
          $scope.demoTrail = false;
          $scope.mainTrail = false;
          $scope.demoTestComplete = true;
          $scope.results = '';
          $scope.totalTimeinterval = '';
          jsonData.errorsMade = 0;
          $scope.round = 0;
          $scope.round1Complete = false;
          $scope.rawData = [];
          $scope.round1Score = 0;
          $scope.round2Score = 0;
          $scope.ShowRound1Score = 0;
          $scope.ShowRound2Score = 0;
          $state.go('app.symbolDigit');
        }
      }
    });
  };

  $scope.cancelBtn = function() {
    if ($scope.demoTrail) {
      $interval.cancel($scope.trialDemoTimeout);
      $scope.trialDemoTimeout = undefined;
      $scope.setStartCondition();
      if (trailTest != '') {
        trailTest.destroy();
        trailTestLower.destroy();
        trailTest = '';
        trailTestLower = '';
      }
      return;
    }
    trailTestLower.paused = true;
    if ($scope.attempts == 2) {
      $scope.cancelTemplate = '<p style="text-align: center;">If you cancel the test, it will count as one of your test takes. You will only have one additional chance to retake the test.</p><p  class="text-center">A test should be cancelled only if something went wrong; please don’t re-take the test just to improve your score, or to practice.</p><a class="popClose" ng-click="cancelTestPopup.close()"></a>';
    } else if ($scope.attempts == 1) {
      $scope.cancelTemplate = '<p style="text-align: center;">If you cancel the test, it will count as one of your test takes. You will only have two additional chances to retake the test.</p><p  class="text-center">A test should be cancelled only if something went wrong; please don’t re-take the test just to improve your score, or to practice.</p><a class="popClose" ng-click="cancelTestPopup.close()"></a>';
    }
    $scope.cancelTestPopup = $ionicPopup.show({
      title: "Are you sure you want to cancel the test?",
      template: $scope.cancelTemplate,
      scope: $scope,
      buttons: [{
        text: 'YES, CANCEL',
        type: 'button-block button-outline button-darkPurple skip',
        onTap: function() {
          return true;
        }
      }, {
        text: 'RETURN TO<br/> THE TEST',
        type: 'button-block button-darkPurple',
        onTap: function() {
          return false;
        }
      }]
    });
    closePopupService.register($scope.cancelTestPopup);
    $scope.cancelTestPopup.then(function(res) {
      if (res) {
        if (typeof window.ga !== "undefined") {
          window.ga.trackEvent('Trail Making Screen - Cancel Popup', 'Performance Metric Screen (Trail Making)', 'Yes, Cancel', 1);
        }
        if (trailTestLower) {
          // trailTestLower.destroy();
          trailTest.destroy();
          trailTestLower.destroy();
          trailTest = '';
          trailTestLower = '';
        }
        //$scope.attempts = 0;
        // $scope.testResult = false;
        // $scope.showSideMenu = true;
        // $scope.DescScreen = true;
        // $scope.demoTrail = false;
        // $scope.mainTrail = false;
        // $scope.demoTestComplete = true;
        // $scope.results = '';
        // $scope.totalTimeinterval = '';
        // jsonData.errorsMade = 0;
        //$scope.updateTestResultonKinvey(true);
        $state.go('app.performanceEvals');
      } else {
        if (typeof window.ga !== "undefined") {
          window.ga.trackEvent('Trail Making Screen - Cancel Popup', 'Cancel Pop Up', 'Return to the Test', 1);
        }
        trailTestLower.paused = false;
      }
    });
  }
}]);
