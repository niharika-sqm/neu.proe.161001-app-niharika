neuroScienceApp.controller('singlePerformanceResultCtrl', ['$scope','$state','$ionicScrollDelegate','$timeout', function($scope,$state,$ionicScrollDelegate,$timeout){
	$scope.$on('$ionicView.afterEnter',function(){
		$ionicScrollDelegate.scrollTop();
	});

	$timeout(function(){
    window.ga.trackView('Performance Metrics Result Screen');
  }, 1000);

	$scope.init = function(){
		if($scope.PMtypeListResult){
			$scope.PMtypeListResultSymbol = [];
			$scope.PMtypeListResultTrail = [];		
			console.log($scope.PMtypeListResult);
			for (var j = 0; j < $scope.PMtypeListResult.length; j++) {
		    if($scope.PMtypeListResult[j].SubPMType == "Symbol Test") {
	        $scope.PMtypeListResultSymbol.push($scope.PMtypeListResult[j]);
		    }else if($scope.PMtypeListResult[j].SubPMType == "Trail Test"){
		    	$scope.PMtypeListResultTrail.push($scope.PMtypeListResult[j]);
		    }
			}
			$scope.strongHand = "Right hand";
			$scope.weekHand = "Left hand";
			if($scope.performanceUser.info.personalInformation.preferHand == "left"){
      	$scope.strongHand = "Left hand";
				$scope.weekHand = "Right hand";
      } 
			console.log($scope.PMtypeListResultSymbol, $scope.PMtypeListResultTrail,$scope.performanceUser.info.personalInformation.preferHand);
		}
	}
	$scope.init();

}]);