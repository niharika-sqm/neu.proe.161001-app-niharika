neuroScienceApp.controller('gonogoCtrl', ['$scope','randomTime', function($scope,randomTime){
	
	$scope.showTestButton = true;

	// Phaser Variable
	var gonogo;
	var correctClick = 0, missedClick=0;
	var correctText,missedText, updateTime, state;
	var elapsedTime = 0;
	var solidClick=0, solidUnclick=0, patternClick=0, patternUnclick=0, solidClicks,patternClicks;
	var showAgainText;


	$scope.startTest = function(){
		gonogo = new Phaser.Game(window.innerWidth, window.innerHeight-40, Phaser.AUTO, 'gonogoDiv', { preload: preload, create: create, update: update });
		$scope.showTestButton = false;
	}


	function preload(){
		gonogo.load.image('solid', 'img/phaser/solidCircle.png');
		gonogo.load.image('pattern', 'img/phaser/patternCircle.png');
		gonogo.load.image('tapagain', 'img/phaser/tapagain.png');
	}

	function create(){
		gonogo.stage.backgroundColor = "#ffffff";
		sphere = gonogo.add.sprite(gonogo.world.centerX, gonogo.world.centerY, 'solid');
    sphere.anchor.set(0.5);
    sphere.exists = false;
    sphere.height = 300;
    sphere.width = 300;

    showAgain = gonogo.add.sprite(gonogo.world.centerX, gonogo.world.centerY, 'tapagain');
    showAgain.anchor.set(0.5);
    showAgain.exists = false;


    gonogo.input.onTap.add(onTap, this);


    updateTime = randomTime.randomTimeOneSix();
		console.log(updateTime)

		solidClicks = gonogo.add.text(110, 10, 'Solid Click: '+solidClick+',  Solid Unclick: '+solidUnclick, { font: "14px Arial", fill: "#000", align: "center" });
    solidClicks.anchor.setTo(0.5, 0.5);

    patternClicks = gonogo.add.text(125, 30, 'Pattern Click: '+patternClick+',  Pattern Unclick: '+patternUnclick, { font: "14px Arial", fill: "#000", align: "center" });
    patternClicks.anchor.setTo(0.5, 0.5);

		correctText = gonogo.add.text(60, 50, 'Correct Score: '+correctClick, { font: "14px Arial", fill: "#000", align: "center" });
    correctText.anchor.setTo(0.5, 0.5);
    missedText = gonogo.add.text(60, 70, 'Missed Score: '+missedClick, { font: "14px Arial", fill: "#000", align: "center" });
    missedText.anchor.setTo(0.5, 0.5);


    state = 0;
	}


	function onTap(pointer) {
		console.log(pointer,sphere.key,sphere.exists);
		console.log(showAgain.key,showAgain.exists);
		if(showAgain.key == "tapagain" && showAgain.exists){
			next();
			showAgain.exists = false;
		}
		if(sphere.exists){
			if(sphere.key=="solid"){
				solidClick++;
				sphere.exists = false;
				solidClicks.setText('Solid Click: '+solidClick+',  Solid Unclick: '+solidUnclick);
				state = 2;
			}else{
				patternClick++;
				patternClicks.setText('Pattern Click: '+patternClick+',  Pattern Unclick: '+patternUnclick);
				sphere.exists = false;
				state = 2;
			}
		}
	}
	
	function next(){
		updateTime = randomTime.randomTimeOneSix();
		console.log(updateTime);
		elapsedTime = 0;
		state = 0;
	}

	function update(){
		/*if((correctClick + missedClick)>=6){
			console.log(correctClick,missedClick);
			state = 2;
		}*/

		switch(state){ // When circle disappears
			case 0:
				elapsedTime += gonogo.time.elapsed;
				if(elapsedTime >= updateTime){
					if(randomTime.randomTimeOneTwo() == 1){
						sphere.loadTexture('solid');
						sphere.exists = true;
					}else{
						sphere.loadTexture('pattern');
						sphere.exists = true;
					}
					elapsedTime = 0;
					state = 1
				}
				correctText.setText('Correct Score: ' + Number(solidClick+patternUnclick));
				missedText.setText('Missed Score: ' + Number(solidUnclick+patternClick));
				break;
			case 1: // when circle appears 
				elapsedTime += gonogo.time.elapsed;
				
				if(elapsedTime >= 4000){
					if(sphere.exists && sphere.key=="solid"){
						solidUnclick++;
						solidClicks.setText('Solid Click: '+solidClick+',  Solid Unclick: '+solidUnclick);
					}
					if(sphere.exists && sphere.key=="pattern"){
						patternUnclick++;
						patternClicks.setText('Pattern Click: '+patternClick+',  Pattern Unclick: '+patternUnclick);
					}
					sphere.exists = false;
					// updateTime = randomTime.randomTimeOneSix();
					// console.log(updateTime);
					elapsedTime = 0;
					updateTime = 0;
					state = 2;
				}

				correctText.setText('Correct Score: ' + Number(solidClick+patternUnclick));
				missedText.setText('Missed Score: ' + Number(solidUnclick+patternClick));
				break;
			case 2:
				correctText.setText('Correct Score: ' + Number(solidClick+patternUnclick));
				missedText.setText('Missed Score: ' + Number(solidUnclick+patternClick));
				showAgain.exists = true;
				// gonogo.destroy();

				break;
			default:
				state = 0;
				break;
		}
	}

}]);