neuroScienceApp.controller('ishiharaTestCtrl', ['$scope', '$state', '$interval', '$ionicPopup', '$kinvey', '$ionicScrollDelegate', '$timeout', '$q', '$stateParams', 'CONSTANT','closePopupService', function($scope, $state, $interval, $ionicPopup, $kinvey, $ionicScrollDelegate, $timeout, $q, $stateParams, CONSTANT,closePopupService) {
  $scope.$on("$ionicView.beforeEnter", function() {
    $scope.tap = false;
    $scope.started = true;
    $scope.results = true;
    $scope.BtnDisabledIshiharaTest = false;
    $scope.bumperImage = false;
    $scope.idontseeText = "I do not see anything";
    $scope.idontseebutton = false;
    var resultsRight = [],
        resultsLeft = [],
        imagesCpy = [];
    if ($stateParams.test != null) {
      $scope.notificationId = $stateParams.test.notificationId;
      $scope.attempts = $stateParams.test.attempts;
      // $scope.attempts = 1;
      $scope.testResultId = $stateParams.test.testResultId;
      $scope.cycle = $stateParams.test.cycle;
      $scope.type = $stateParams.test.type;
      $scope.isPointGiven = $stateParams.test.isPointGiven;
      $stateParams.test.relapseTrackerId == undefined ? $scope.relapseTrackerId = "" : $scope.relapseTrackerId = $stateParams.test.relapseTrackerId;
      delete $stateParams.test;
    }
    $scope.retakeCancelBtnDisabled = false;
    $scope.processingNumber = -1; // this variable is used to prevent double value pushed into answer array
  });

  $timeout(function(){
    window.ga.trackView('Color Vision Test');
  }, 1000);

  $scope.$on('$ionicView.afterEnter', function() {
    $scope.lockOrientation();
  });

  $scope.$on('$destroy',function(){
    $scope.unlockOrientation();
  });

  $scope.$on("$ionicView.enter", function() {

    $scope.relapse = {
      relapseInfection1: null,
      relapseInfection2: null,
      relapseInfection3: null,
      relapseInfection4: null,
    };

    $scope.prePMSurveyAnswers = [
    	{"question":"Did you take your medications as usual?", "answer":null},
    	{"question":"Did you get a typical amount of sleep last night?", "answer":null},
    	{"question":"Do you have 6-8 minutes in a low distraction environment to focus on today's tests?", "answer":null},
    	{"question":"Do you feel any of these factors could impact your performance test today?", "answer":null}
    ];

    $scope.recommendationPopup = function() {
      $scope.recommendForPm = '';
      var button = '<div class="popBodyInput orange"><div class="item item-input item-borderless"><div class="radioButtonGroup"><label class="questionLabel ">Did you take your medications as usual?</label>' +

        '<div class="radioButtonWrapper yes"><input class="radioButton" type="radio" name="preRelapse1" ng-value=true ng-model="relapse.relapseInfection1"><label class="radioLabel">  Yes </label></div>' +

        '<div class="radioButtonWrapper no"><input class="radioButton" type="radio" name="preRelapse1" ng-value=false ng-model="relapse.relapseInfection1"><label class="radioLabel">  No </label></div></div></div>' +

        '<div class="item item-input item-borderless"><div class="radioButtonGroup"><label class="questionLabel ">Did you get a typical amount of sleep last night?</label>' +

        '<div class="radioButtonWrapper yes"><input class="radioButton" type="radio" name="preRelapse2" ng-value=true ng-model="relapse.relapseInfection2"><label class="radioLabel">  Yes </label></div>' +

        '<div class="radioButtonWrapper no"><input class="radioButton" type="radio" name="preRelapse2" ng-value=false ng-model="relapse.relapseInfection2"><label class="radioLabel">  No </label></div></div></div>' +

        '<div class="item item-input item-borderless"><div class="radioButtonGroup"><label class="questionLabel ">Do you have 6-8 minutes in a low distraction environment to focus on today' + "'s " + 'tests?</label>' +

        '<div class="radioButtonWrapper yes"><input class="radioButton" type="radio" name="preRelapse3" ng-value=true ng-model="relapse.relapseInfection3"><label class="radioLabel">  Yes </label></div>' +

        '<div class="radioButtonWrapper no"><input class="radioButton" type="radio" name="preRelapse3" ng-value=false ng-model="relapse.relapseInfection3"><label class="radioLabel">  No </label></div></div></div>' +

        '<div class="item item-input item-borderless"><div class="radioButtonGroup"><label class="questionLabel ">Do you feel any of these factors could impact your performance test today?</label>' +

        '<div class="radioButtonWrapper yes"><input class="radioButton" type="radio" name="preRelapse4" ng-value=true ng-model="relapse.relapseInfection4"><label class="radioLabel">  Yes </label></div>' +

        '<div class="radioButtonWrapper no"><input class="radioButton" type="radio" name="preRelapse4" ng-value=false ng-model="relapse.relapseInfection4"><label class="radioLabel">  No </label></div></div></div></div>' +

        '<div ng-show="relapse.relapseInfection1!=null && relapse.relapseInfection2!=null && relapse.relapseInfection3!=null && relapse.relapseInfection4!=null"><div ng-show="relapse.relapseInfection1==true && relapse.relapseInfection2==true && relapse.relapseInfection3==true && relapse.relapseInfection4==false"><div class="height20"></div><div class="popup-buttons" ><button class="button button-block button-darkPurple font40" ng-click="takeTestNow(true)">GREAT! LET' + "'" + 'S START TODAY' + "'" + 'S TASKS.</button></div><div class="height20"></div>' +

        '</div>' +

        '<div ng-show="!(relapse.relapseInfection1==true && relapse.relapseInfection2==true && relapse.relapseInfection3==true && relapse.relapseInfection4==false)"><p class="text-center italicFont smallText">You'+"'ve"+' indicated that some factors may affect your performance on today' + "'" + 's tasks.</p><div class="popup-buttons" ><button class="button button-block button-outline button-darkPurple font40 skip" ng-click="recommendPopup.close()">TAKE TEST <br/>LATER</button>' +

        '<div class="height10"></div><button class="button button-block button-multiline button-darkPurple font40" ng-click="takeTestNow(false)">I' + "'" + 'D RATHER TAKE <br/>THE TEST NOW</button></div></div></div></div>';

      var template = '<p class="text-center">Performance can be influenced by many factors, including health, medical treatments, and life events, even having a ' + '"bad day".' +'</p><a class="popClose" ng-click="recommendPopup.close()"></a><p class="text-center">We want you to do the best you can on the performance measures.  So tell us if today is a typical day for you by answering the questions below.</p>' + button;
      $timeout(function() {
        $scope.recommendPopup = $ionicPopup.show({
          title: "Performance Metric Testing",
          template: template,
          scope: $scope,
          cssClass: "pmStartPop"
        });
        closePopupService.register($scope.recommendPopup);
        $scope.recommendPopup.then(function(res) {
          if (!$scope.takeTestNowPopupClose) {
            $state.go('app.performanceEvals');
          }
        });
      }, 100);
    }
    $scope.takeTestNow = function(status) {
      // console.log($scope.relapse);
      // console.log(status);
      if (status) {
        $scope.recommendForPm = "yes";
      } else {
        $scope.recommendForPm = "no";
      }
      $scope.recommendPopup.close();
      $scope.takeTestNowPopupClose = true;

      for(var i = 0; i < $scope.prePMSurveyAnswers.length; i++){
        $scope.prePMSurveyAnswers[i].answer = "no";
  			if ($scope.relapse["relapseInfection"+(i+1)] == true){
  				$scope.prePMSurveyAnswers[i].answer = "yes";
  			}
      }

      // console.log($scope.recommendForPm);
    }
    $scope.recommendationPopup();
    var result = {},t;
    var game = "";
    var uptoTime = 0;
    var tcountforPause = "";
    $scope.imgValue = { value: '' };
    $scope.imgDisabled = true;
    var performanceTestResults = $kinvey.DataStore.getInstance('performanceTestResults', $kinvey.DataStoreType.Network);
    var notificationTestList = $kinvey.DataStore.getInstance('notifications', $kinvey.DataStoreType.Network);

    $scope.numbers = [0,1,2,3,4,5,6,7,8,9];
    //$scope.attempts = 1;
    var jsonData = {};
    jsonData.PMType = "Color Vision";
    $scope.submitButtonActive = false;

    $scope.changeInput = function($event, value) {
      console.log('hi', value, $event);
      value.trim();
      value = value.replace(/[^0-9]/g, '');
      $scope.imgValue.value = value;
      if ($scope.imgValue.value) {
        $scope.submitButtonActive = true;
      } else {
        $scope.submitButtonActive = false;
      }
    };

    $scope.backspace = function(){
      var length = $scope.imgValue.value.length;
      if(length == 0) {
        return;
      }
      if($scope.imgValue.value == $scope.idontseeText){
        $scope.imgValue.value = '';
        $scope.idontseebutton = false;
        /*if ($scope.eyeSide == 'Right') {
          resultsRight.splice(resultsRight.length-1, 1);
        }else{
          resultsLeft.splice(resultsLeft.length-1, 1);
        }*/
      }else{
        $scope.imgValue.value = $scope.imgValue.value.slice(0, length-1);
      }
      length = $scope.imgValue.value.length;
      if(length == 0){
        $scope.submitButtonActive = false;
      }
    }

    $scope.iSawThis = function(value){
      $scope.submitButtonActive = true;
      if ($scope.imgValue.value) {
        if($scope.imgValue.value.length == 2 || $scope.imgValue.value == $scope.idontseeText){
          return;
        }
        $scope.imgValue.value += (value).toString();
      } else {
        $scope.imgValue.value = (value).toString();
      }
    }

    $scope.cancelBtn = function() {
      if(!$scope.imgDisabled){
        console.log(uptoTime);
        $interval.cancel(t);
        t = undefined;
        $interval.cancel(tcountforPause);
        tcountforPause = undefined;
        if ($scope.image.value == 1) {
          game.paused = true;
        }
      }
      if ($scope.attempts == 2) {
        $scope.cancelTemplate = '<p class="text-center">If you cancel the test, it will count as one of your test takes. You will only have one additional chance to retake the test.</p><p class="text-center">A test should be cancelled only if something went wrong; please don’t re-take the test just to improve your score, or to practice.</p><a class="popClose" ng-click="cancelTestPopup.close()"></a>';
      } else if ($scope.attempts == 1) {
        $scope.cancelTemplate = '<p style="text-align: center;">If you cancel the test, it will count as one of your test takes. You will only have two additional chances to retake the test.</p><p  class="text-center">A test should be cancelled only if something went wrong; please don’t re-take the test just to improve your score, or to practice.</p><a class="popClose" ng-click="cancelTestPopup.close()"></a>';
      }
      $scope.cancelTestPopup = $ionicPopup.show({
        title: "Are you sure you want to cancel the test?",
        template: $scope.cancelTemplate,
        scope: $scope,
        buttons: [{
          text: 'YES, CANCEL',
          type: 'button-block button-outline button-darkPurple skip',
          onTap: function() {
            return true;
          }
        }, {
          text: 'RETURN TO<br/> THE TEST',
          type: 'button-block button-darkPurple',
          onTap: function() {
            return false;
          }
        }]
      });
      closePopupService.register($scope.cancelTestPopup);
      $scope.cancelTestPopup.then(function(res) {
        if (res) {
          if (typeof window.ga !== "undefined") {
            window.ga.trackEvent('Color Vision Screen - Cancel Popup', 'Performance Metric Screen (Color Vision)', 'Yes, Cancel ', 1);
          }
          $interval.cancel(t);
          //$scope.updateTestResultonKinvey(true);
          //initiate();
          if(!$scope.imgDisabled && $scope.image.value == 1){
            game.destroy();
          }
          $state.go('app.performanceEvals');
        } else {
          if (typeof window.ga !== "undefined") {
            window.ga.trackEvent('Color Vision Screen - Cancel Popup', 'Cancel Pop Up', 'Return to the Test', 1);
          }
          if(!$scope.imgDisabled){
            console.log(uptoTime);
            if ($scope.image.value == 1) {
              game.paused = false;
            }
            $scope.cancelResume();
          }
        }
      });
    };

    $scope.cancelResume = function(argument) {
      t = $interval(function() {
        if (!$scope.imgValue.value) {
          if ($scope.image.value == 0) {
            $scope.unableSee(false);
            return;
          }
          $scope.unableSee();
        } else {
          $scope.submitTest();
        }
      }, 10000 - (uptoTime * 1000));
      tcountforPause = $interval(function() {
        if (uptoTime >= 10) {
          $interval.cancel(tcountforPause);
          tcountforPause = undefined;
        } else {
          uptoTime++;
          console.log(uptoTime);
        }
      }, 1000);
    }

    var initiate = function() {

      if ($scope.attempts >= 3) {
        $scope.BtnDisabledIshiharaTest = true;
      }
      var rightcorrect = 0,
        rightmissed = 0,
        leftcorrect = 0,
        leftmissed = 0,
        intervalForBumperImageTrueResult = undefined,
        intervalForBumperImageFalseResult = undefined,
        intervalForBumperImageUnableSeeResult = undefined;

        resultsRight = [],
        resultsLeft = [],
        imagesCpy = [];

      $scope.ishiharaResult = {};
      $scope.ishiharaResult.rightCorrect = 0;
      $scope.ishiharaResult.rightMissed = 0;
      $scope.ishiharaResult.leftCorrect = 0;
      $scope.ishiharaResult.leftMissed = 0;
      $scope.ishiharaResult.resultsLeft = [];
      $scope.ishiharaResult.resultsRight = [];

      $scope.tap = false;
      $scope.started = true;
      $scope.results = true;
      $scope.imgValue = {

      };

      var arrayPoints = [{ "id": 1, "x": 100, "y": 303 }, { "id": 2, "x": 140, "y": 300 }, { "id": 3, "x": 180, "y": 280 }, { "id": 4, "x": 210, "y": 248 }, { "id": 5, "x": 242, "y": 215 }, { "id": 6, "x": 275, "y": 183 }, { "id": 7, "x": 320, "y": 174 }, { "id": 8, "x": 366, "y": 178 }, { "id": 9, "x": 409, "y": 197 }, { "id": 10, "x": 426, "y": 235 }, { "id": 11, "x": 409, "y": 278 }, { "id": 12, "x": 374, "y": 303 }, { "id": 13, "x": 336, "y": 318 }, { "id": 14, "x": 292, "y": 336 }, { "id": 15, "x": 248, "y": 355 }, { "id": 16, "x": 206, "y": 384 }, { "id": 17, "x": 168, "y": 418 }, { "id": 18, "x": 178, "y": 458 }, { "id": 19, "x": 217, "y": 470 }, { "id": 20, "x": 254, "y": 484 }, { "id": 21, "x": 288, "y": 518 }, { "id": 22, "x": 334, "y": 530 }, { "id": 23, "x": 366, "y": 491 }, { "id": 24, "x": 368, "y": 439 }, { "id": 25, "x": 389, "y": 398 }, { "id": 26, "x": 440, "y": 393 }, { "id": 27, "x": 489, "y": 396 }, { "id": 28, "x": 530, "y": 407 }, { "id": 29, "x": 575, "y": 393 }, { "id": 30, "x": 612, "y": 366 }, { "id": 31, "x": 655, "y": 345 }];
      var canvasWidth = window.innerWidth - (window.innerWidth * 0.24);
      var canvasHeight = window.innerWidth * 0.5958;
      $scope.showCanvasPlate = false;
      var mainState = {
        preload: function() {
          game.load.image('solid', 'img/ishihara/11_740x740.png');
          game.stage.backgroundColor = '#FFFFFF';
        },
        create: function() {
          renderingArray = [];
          nextWayPoint = 0;
          distanceVector = { x: 0, y: 0 };
          distanceVectorMagnitude = 0;
          graphics = game.add.graphics(0, 0); // TODO
          sphere = game.add.sprite(game.world.centerX, game.world.centerY, 'solid');
          imageScaleRatio = (window.innerWidth * 0.5958) / sphere.width;
          sphere.scale.set(imageScaleRatio);
          sphere.anchor.set(0.5);
          extraSpaceInImageX = (game.world.width - sphere.width) / 2; //33.5
          game.input.onUp.add(actions.onMouseUp, this);
          game.input.onDown.add(actions.onMouseDown, this);
        },
        update: function() {
          elapsedTime += game.time.elapsed;
          totalTimeInSeconds = parseInt(game.time.totalElapsedSeconds());
          if (arrayPoints.length > nextWayPoint) {
            distanceVector.x = ((arrayPoints[nextWayPoint].x * imageScaleRatio) + extraSpaceInImageX) - game.input.x;
            distanceVector.y = arrayPoints[nextWayPoint].y * imageScaleRatio - game.input.y;
            distanceVectorMagnitude = Math.sqrt((distanceVector.x * distanceVector.x) + (distanceVector.y * distanceVector.y));
          }
          // console.log(window.innerWidth*0.0410);
          if (distanceVectorMagnitude < window.innerWidth * 0.0411) { //17
            if (nextWayPoint <= arrayPoints.length - 1) {
              renderingArray.push(arrayPoints[nextWayPoint]);
              renderingArray[nextWayPoint].lostTrack = false;
              nextWayPoint++;
            }
          } else if (distanceVectorMagnitude > window.innerWidth * 0.08) { //32
            console.log("divert from path", distanceVectorMagnitude);
            if (renderingArray.length) {
              renderingArray[nextWayPoint - 1].lostTrack = true;
              // console.log(renderingArray[nextWayPoint-1].id,renderingArray[nextWayPoint-1].lostTrack);
            }
          }

          // TODO
          /*graphics.clear();
          for(var i=0;i<arrayPoints.length;i++){
            actions.drawCircle(graphics, 0xff0000, window.innerWidth*0.0411,((arrayPoints[i].x*imageScaleRatio)+ extraSpaceInImageX),arrayPoints[i].y*imageScaleRatio)
          }
          if(renderingArray.length>1){
            for(var i=renderingArray.length-1;i>0;i--){
              graphics.lineStyle(2, 0x000000);
              graphics.moveTo((renderingArray[i-1].x*imageScaleRatio)+extraSpaceInImageX, renderingArray[i-1].y*imageScaleRatio);
              graphics.lineTo((renderingArray[i].x*imageScaleRatio)+extraSpaceInImageX, renderingArray[i].y*imageScaleRatio);
            }
          }*/

        }
      };
      var actions = {
        onMouseUp: function() {
          if (arrayPoints.length == renderingArray.length) {
            console.log("correct Trace");
            $timeout(function() {
              $scope.imgValue.value = 1
            });
          } else {
            console.log("incorrect Trace")
          }
          // console.log(game.input.x,game.input.y)
        },
        onMouseDown: function() {
          $timeout(function() {
            $scope.submitButtonActive = true;
          });
          // console.log(game.input.x,game.input.y)
        },
        drawCircle: function(_graphics, color, radius, x, y) {
          _graphics.beginFill(color, 1);
          _graphics.drawCircle(x, y, radius);
          return _graphics;
        },
        restart: function() {
          game.paused = false;
          game.state.start('main');
          elapsedTime = 0;
        },
        finishGame: function() {
          // $timeout(function(){
          //  $scope.gameFinished();
          // });
          gameFinished = true;
          game.destroy();
        }
      }

      $scope.eyeSide = "Right";
      $scope.oppEyeSide = "left";

      var t0, t1;
      var images = [{
        "value": 12,
        "path": "img/ishihara/1_740x740.png"
      }, {
        "value": 8,
        "path": "img/ishihara/2_740x740.png"
      }, {
        "value": 5,
        "path": "img/ishihara/3_740x740.png"
      }, {
        "value": 29,
        "path": "img/ishihara/4_740x740.png"
      }, {
        "value": 74,
        "path": "img/ishihara/5_740x740.png"
      }, {
        "value": 7,
        "path": "img/ishihara/6_740x740.png"
      }, {
        "value": 45,
        "path": "img/ishihara/7_740x740.png"
      }, {
        "value": 2,
        "path": "img/ishihara/8_740x740.png"
      }, {
        "value": 0,
        "path": "img/ishihara/9_740x740.png"
      }, {
        "value": 16,
        "path": "img/ishihara/10_740x740.png"
      }, {
        "value": 1,
        "path": "img/ishihara/11_740x740.png"
      }];
      imagesCpy = images.slice(0);

      $scope.testStartedKinvey = function() {
        if($scope.attempts==1){
          $scope.setPMNotificationIsShown($scope.notificationId);
           if($scope.relapseTrack.startRelapse){
            $scope.relapseTrackerId = $scope.relapseTrack.kinveyData._id;
          }
        }
        if ($scope.testResultId == "") {
          performanceTestResults.save({
            "date": new Date(),
            "PMType": jsonData.PMType,
            "attempts": $scope.attempts,
            "cycle": $scope.cycle,
            "type": $scope.type,
            "recommendForPm": $scope.recommendForPm,
            "prePMSurveyAnswers":$scope.prePMSurveyAnswers,
            "notificationId": $scope.notificationId,
            "isAborted": true,
            "isShown": $scope.attempts == 3 ? true : false,
            "isPointGiven":$scope.isPointGiven,
            "relapseTrackerId":$scope.relapseTrackerId
          }).then(function(entity) {
            console.log("entity " + entity);
            $scope.testResultId = entity._id;
            if($scope.attempts == 1 && $scope.relapseTrack.startRelapse){
              $scope.$emit('updateRelapseTracker',$scope.testResultId);
            }
            if ($scope.attempts >= 3) {
              $scope.updateNotificationCollection();
            }
          }).catch(function(error) {
            $scope.internalGlitch();
            $state.go('app.performanceEvals');
          });
        } else {
          performanceTestResults.save({
            "_id": $scope.testResultId,
            "date": new Date(),
            "PMType": jsonData.PMType,
            "attempts": $scope.attempts,
            "cycle": $scope.cycle,
            "type": $scope.type,
            "recommendForPm": $scope.recommendForPm,
            "prePMSurveyAnswers":$scope.prePMSurveyAnswers,
            "notificationId": $scope.notificationId,
            "isAborted": true,
            "isShown": $scope.attempts == 3 ? true : false,
            "isPointGiven":$scope.isPointGiven,
            "relapseTrackerId":$scope.relapseTrackerId
          }).then(function(entity) {
            console.log("entity " + entity);
            $scope.testResultId = entity._id;
            if ($scope.attempts >= 3) {
              $scope.updateNotificationCollection();
            }
          }).catch(function(error) {
            $scope.internalGlitch();
            $state.go('app.performanceEvals');
          });
        }
      }

      $scope.getStarted = function() {
        if (typeof window.ga !== "undefined") {
          window.ga.trackEvent('Color Vision Screen', 'Test starts', 'Get Started', 1);
        }
        $ionicScrollDelegate.scrollTop();
        $scope.attempts++;
        $scope.testStartedKinvey();
        if ($scope.attempts == 3) {
          $scope.retakeCancelBtnDisabled = true;
        }
        $scope.started = false;
      };

      $scope.startTest = function() {
        $scope.isProcessing = true;
        $scope.imgDisabled = false;
        t0 = performance.now();
        if (images.length != 0) {
          $scope.tap = true;
          $scope.submitButtonActive = false;
          var getImage = images[Math.floor(Math.random() * images.length)];
          var i = images.indexOf(getImage);
          images.splice(i, 1);
          if (getImage.value == 1) {
            console.log("draw");
            $scope.showCanvasPlate = true;
            startCanvasPlate();
            $timeout(function() {
              $scope.isProcessing = false;
            }, 500);
          } else {
            $timeout(function() {
              $scope.isProcessing = false;
            }, 100);
            $scope.showCanvasPlate = false;
          }
          $scope.image = getImage;
          $interval.cancel(t);
          t = $interval(function() {
            $interval.cancel(t);
            $scope.isProcessing = false;
            $scope.submitTest();
            /*if (!$scope.imgValue.value) {
              if ($scope.image.value == 0) {
                $scope.unableSee(false);
                return;
              }
              $scope.unableSee();
            } else {
              $scope.submitTest();
            }*/
          }, 10000);
          uptoTime = 0;
          $interval.cancel(tcountforPause);
          tcountforPause = $interval(function() {
            if (uptoTime >= 10) {
              $interval.cancel(tcountforPause);
              tcountforPause = undefined;
            } else {
              uptoTime++;
              console.log(uptoTime);
            }
          }, 1000);
        } else {
          $scope.showCanvasPlate = false;
          $scope.imgDisabled = true;
          $scope.eyeSide = "Left";
          $scope.oppEyeSide = "right";
          images = imagesCpy;
          $scope.tap = false;
          $scope.submitButtonActive = false;
          $interval.cancel(t);
          t = undefined;
          if (imagesCpy.length == 0) {
            $scope.updateTestResultonKinvey();
          }
          $scope.isProcessing = false;
        }
      };
      $scope.updateTestResultonKinvey = function(cancel) {
        // reset value check variable
        $scope.processingNumber = -1;
        $scope.started = false;
        $scope.ishiharaResult.rightCorrect = rightcorrect;
        $scope.ishiharaResult.rightMissed = rightmissed;
        $scope.ishiharaResult.leftCorrect = leftcorrect;
        $scope.ishiharaResult.leftMissed = leftmissed;
        $scope.ishiharaResult.resultsLeft = resultsLeft;
        $scope.ishiharaResult.resultsRight = resultsRight;
        $scope.results = false;
        jsonData.date = new Date();
        jsonData.PMType = "Color Vision";
        jsonData.attempts = $scope.attempts;
        jsonData.score = {};
        jsonData.score.RC = $scope.ishiharaResult.rightCorrect;
        jsonData.score.RM = $scope.ishiharaResult.rightMissed;
        jsonData.score.LC = $scope.ishiharaResult.leftCorrect;
        jsonData.score.LM = $scope.ishiharaResult.leftMissed;
        jsonData.raw = resultsLeft.concat(resultsRight);
        $scope.showLoader();
        var addpoints = false;
        if($scope.isPointGiven == false && cancel != true) {
          $scope.isPointGiven = true; addpoints = true;
        }
        if ($scope.attempts == 3) {
          $scope.retakeCancelBtnDisabled = true;
        }
        var kinveObj = {
          _id: $scope.testResultId,
          "date": jsonData.date,
          "PMType": jsonData.PMType,
          "attempts": jsonData.attempts,
          "cycle": $scope.cycle,
          "recommendForPm": $scope.recommendForPm,
          "prePMSurveyAnswers":$scope.prePMSurveyAnswers,
          "type": $scope.type,
          "score1": jsonData.score,
          "raw": jsonData.raw,
          "isAborted": cancel == undefined ? false : cancel,
          "isShown": $scope.attempts == 3 ? true : false,
          "notificationId": $scope.notificationId,
          "isPointGiven":$scope.isPointGiven,
          "relapseTrackerId":$scope.relapseTrackerId
        }
        performanceTestResults.save(kinveObj).then(function(entity) {
          console.log("entity ", entity);
          $scope.testResultId = entity._id;
          if (cancel == true) {
            $state.go('app.performanceEvals');
          } else {
            if (addpoints) {
              $scope.$emit('saveGamificationPoints', CONSTANT.gamificationPoints.pmEvaluation.value, CONSTANT.gamificationPoints.pmEvaluation.name);
            }
          }
          $scope.hideLoader();
        }).catch(function(error) {
          localStorage.setItem("unSavedPMData", JSON.stringify(kinveObj));
          localStorage.setItem("unSavedPMPoints", addpoints);
          $scope.internalGlitch();
        });

        console.log(jsonData);
      }

      $scope.updateNotificationCollection = function() {
        var tempdata = [];
        var col = "_id";
        var relatedJson = $scope.notificationId;
        var query = new $kinvey.Query();
        query.equalTo(col, relatedJson);
        notificationTestList.find(query).subscribe(function(data) {
          tempdata = data;
        }, function(error) {}, function(data) {
          tempdata[0].isAck = true;
          notificationTestList.update(tempdata[0]).then(function(data) {
            console.log(data);
          }).catch(function(error) {
            $scope.internalGlitch();
            console.log(error);
          });
        });
      }

      $scope.submitTest = function() {
        // if image value is preocessing in that case or same answer is send twice in both case ignore response
        if($scope.isProcessing || $scope.processingNumber == $scope.image.value){
          return;
        }
        // Image which is processing
        $scope.processingNumber = $scope.image.value;
        $scope.isProcessing = true;
        $ionicScrollDelegate.scrollTop();
        t1 = performance.now();
        /*if(typeof(cordova)!= "undefined"){
          cordova.plugins.Keyboard.close();
        }*/
        if($scope.idontseebutton){
          //$scope.isProcessing = false;
          $scope.imgValue.value = '';
          $interval.cancel(t);
          t = undefined;
          $interval.cancel(tcountforPause);
          tcountforPause = undefined;
          /*console.log(JSON.stringify(resultsLeft));
          console.log(JSON.stringify(resultsRight));*/
          if ($scope.eyeSide == 'Right') {
            if ($scope.image.value == 0) {
              console.log("aagya R");
              rightcorrect++;
              result.value = $scope.image.value;
              result.typed = $scope.imgValue.value;
              result.time = t1 - t0;
              result.status = 1;
              result.eye = 'Right';
              resultsRight.push(cloneMessage(result));
            } else {
              console.log("aagya R");
              rightmissed++;
              result.value = $scope.image.value;
              result.typed = $scope.imgValue.value;
              result.time = t1 - t0;
              result.status = 2;
              result.eye = 'Right';
              if ($scope.image.value == 1) {
                result.tracePoints = renderingArray;
              }else{
                if(result.tracePoints){
                  delete result.tracePoints;
                }
              }
              resultsRight.push(cloneMessage(result));
            }
          } else {
            if ($scope.image.value == 0) {
              console.log("aagya L");
              leftcorrect++;
              result.value = $scope.image.value;
              result.typed = $scope.imgValue.value;
              result.time = t1 - t0;
              result.status = 1;
              result.eye = 'Left';
              resultsLeft.push(cloneMessage(result));
            } else {
              console.log("aagya L");
              leftmissed++;
              result.value = $scope.image.value;
              result.typed = $scope.imgValue.value;
              result.time = t1 - t0;
              result.status = 2;
              result.eye = 'Left';
              if ($scope.image.value == 1) {
                result.tracePoints = renderingArray;
              }else{
                if(result.tracePoints){
                  delete result.tracePoints;
                }
              }
              resultsLeft.push(cloneMessage(result));
            }
          }
          $scope.bumperImage = true;
          $scope.isProcessing = false;
          return;
        }
        else{
          if ($scope.image.value != 0 && $scope.image.value == $scope.imgValue.value) {
            if ($scope.eyeSide == 'Right') {
              rightcorrect++;
              result.value = $scope.image.value;
              result.typed = $scope.imgValue.value;
              result.time = t1 - t0;
              result.status = 1;
              result.eye = 'Right';
              if ($scope.image.value == 1) {
                result.tracePoints = renderingArray;
              }else{
                if(result.tracePoints){
                  delete result.tracePoints;
                }
              }
              resultsRight.push(cloneMessage(result));
            } else {
              leftcorrect++;
              result.value = $scope.image.value;
              result.typed = $scope.imgValue.value;
              result.time = t1 - t0;
              result.status = 1;
              result.eye = 'Left';
              if ($scope.image.value == 1) {
                result.tracePoints = renderingArray;
              }else{
                if(result.tracePoints){
                  delete result.tracePoints;
                }
              }
              resultsLeft.push(cloneMessage(result));
            }
            if ($scope.image.value == 1) {
              actions.finishGame();
            }
            $scope.imgValue.value = '';
            $interval.cancel(t);
            t = undefined;
            $interval.cancel(tcountforPause);
            tcountforPause = undefined;
            // $scope.startTest();
            // $scope.tapToContinue();
          } else if ($scope.image.value == 0 || $scope.image.value != $scope.imgValue.value) {
            if ($scope.eyeSide == 'Right') {
              console.log("aagya R");
              rightmissed++;
              result.value = $scope.image.value;
              result.typed = $scope.imgValue.value;
              result.time = t1 - t0;
              result.status = 0;
              result.eye = 'Right';
              if ($scope.image.value == 1) {
                result.tracePoints = renderingArray;
              }else{
                if(result.tracePoints){
                  delete result.tracePoints;
                }
              }
              resultsRight.push(cloneMessage(result));
            } else {
              console.log("aagya L");
              leftmissed++;
              result.value = $scope.image.value;
              result.typed = $scope.imgValue.value;
              result.time = t1 - t0;
              result.status = 0;
              result.eye = 'Left';
              if ($scope.image.value == 1) {
                result.tracePoints = renderingArray;
              }else{
                if(result.tracePoints){
                  delete result.tracePoints;
                }
              }
              resultsLeft.push(cloneMessage(result));
            }
            if ($scope.image.value == 1) {
              actions.finishGame();
            }
            $scope.imgValue.value = '';
            $interval.cancel(t);
            t = undefined;
            $interval.cancel(tcountforPause);
            tcountforPause = undefined;
            // $scope.startTest();
            // $scope.tapToContinue();
          }
        }
        $scope.bumperImage = true;
        $scope.isProcessing = false;
      };

      $scope.tapToContinue = function() {
        if($scope.isProcessing){
          return;
        }
        // reset variable of image processing for next image
        $scope.imgValue.value = "";
        $scope.processingNumber = -1;
        $scope.startTest();
        $scope.bumperImage = false;
        $scope.idontseebutton = false;
      }

      $scope.unableSee = function(noSeeImageBtnStatus) {
        if($scope.isProcessing){
          return;
        }
        // $scope.isProcessing = true;
        $ionicScrollDelegate.scrollTop();
        t1 = performance.now();
        /*if(typeof(cordova)!= "undefined"){
          cordova.plugins.Keyboard.close();
        }*/
        if ($scope.image.value == 1) {
          actions.finishGame();
        }
        $scope.imgValue.value = '';
        // $interval.cancel(t);
        // t = undefined;
        // $interval.cancel(tcountforPause);
        // tcountforPause = undefined;
        if(!noSeeImageBtnStatus){
          // the button is not pressed
          $scope.bumperImage = true;
          $scope.isProcessing = false;
        }else{
          // the button is pressed
          $scope.submitButtonActive = true;
          $scope.idontseebutton = true;
          $scope.imgValue.value = angular.copy($scope.idontseeText);
        }
        // $scope.startTest();
        // $scope.tapToContinue();
      };

      function cloneMessage(result) {
        var clone = {};
        for (var key in result) {
          if (result.hasOwnProperty(key)) //ensure not adding inherited props
            clone[key] = result[key];
        }
        return clone;
      };

      function startCanvasPlate() {
        game = new Phaser.Game(canvasWidth, canvasHeight, Phaser.AUTO, 'gameDiv1');
        game.state.add('main', mainState);
        actions.restart();
      }
    };
    initiate();


    $scope.retakeTest = function() {
      if ($scope.attempts == 3) {
        $state.go('app.performanceEvals');
        return;
      }
      var template = '<a class="popClose" ng-click="retakeTestPopup.close()"></a><p class="text-center">If problems occurred while taking this test, you can retake it. However, this test is given to gain an accurate measurement of normal functioning, so please don’t re-take it just to improve your score or to practice.</p>';
      if ($scope.attempts == 2) {
        var title = "Are you sure you want to restart the test? You have 1 test remaining.";
        var buttons = [{
          text: 'CONTINUE',
          type: 'button-block button-darkPurple',
          onTap: function() {
            return true;
          }
        }];
      } else {
        var title = "Are you sure you want to restart the test? You have 2 tests remaining.";
        var buttons = [{
          text: 'CANCEL',
          type: 'button-block button-outline button-darkPurple skip',
          onTap: function() {
            return false;
          }
        }, {
          text: 'RETAKE TEST',
          type: 'button-block button-darkPurple',
          onTap: function() {
            return true;
          }
        }];
      }
      $scope.retakeTestPopup = $ionicPopup.show({
        title: title,
        template: template,
        scope: $scope,
        buttons: buttons
      });
      closePopupService.register($scope.retakeTestPopup);
      $scope.retakeTestPopup.then(function(res) {
        if (res) {
          //$scope.attempts = $scope.attempts + 1;
          initiate();
        }
      });
    };

    $scope.changeScroll = function() {
      if (ionic.Platform.isAndroid()) {
        $timeout(function() {
          $ionicScrollDelegate.$getByHandle('autoFocusDisable').scrollTo(0, 150, false);
        }, 651);
        $timeout(function() {
          $ionicScrollDelegate.$getByHandle('autoFocusDisable').scrollTo(0, 150, true);
        }, 2000);
      } else {
        $timeout(function() {
          $ionicScrollDelegate.$getByHandle('autoFocusDisable').scrollTo(0, 150, true);
        }, 250);
      }
    }
  });
}]);


// Resulted JSON :

// {
//   "date": "2016-06-16T10:57:58.811Z",
//   "PMType": "Color Vision",
//   "attempts": 1,
//   "score": {"RC": 1, "RM": 1, "LC": 1, "LM": 1 },
//   "raw": [{"value": 8, "typed": "", "time": 1520.824999999997, "status": 2, "eye": "Left"},
//      {"value": 12, "typed": 12, "time": 2879.3650000000052, "status": 1, "eye": "Left"},
//      {"value": 12, "typed": 1, "time": 4671.800000000003, "status": 0, "eye": "Right"},
//      {"value": 8, "typed": 8, "time": 2903.1899999999987, "status": 1, "eye": "Right"}
//     ]
// }
