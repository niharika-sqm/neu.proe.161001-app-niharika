neuroScienceApp.controller('towerOfHanoiCtrl', ['$scope','$state','$timeout','$ionicPopup','$kinvey','$interval','$q','$stateParams','CONSTANT','$ionicScrollDelegate','closePopupService', function($scope,$state,$timeout,$ionicPopup,$kinvey,$interval,$q,$stateParams,CONSTANT,$ionicScrollDelegate,closePopupService){
/*	function interval(func, wait, times){
		var interv = function(w, t){
			return function(){
				if(typeof t === "undefined" || t-- > 0){
					setTimeout(interv, w);
					try{
						func.call(null);
					}
					catch(e){
						t = 0;
						throw e.toString();
					}
				}
			};
		}(wait, times);

		setTimeout(interv, wait);
	};*/

	// var width = window.innerWidth-(((window.innerWidth*2.41)/100)*2)-3;

	$timeout(function(){
    window.ga.trackView('Tower Test');
  }, 1000);

	$scope.$on('$ionicView.afterEnter',function(){
		$scope.lockOrientation();
	});

	$scope.$on('$destroy',function(){
		$scope.unlockOrientation();
	});

	var windowInnerWidth = window.innerWidth;
	if(windowInnerWidth > 500){
		windowInnerWidth = 600;
	}
	var width = (windowInnerWidth*0.95)-2;
	var height = (windowInnerWidth*1.04)-1; //(window.innerHeight*65)/100;
	$scope.hideLoader();
	$scope.showGetStarted = true;
	$scope.showStartTest = false;
	$scope.showMainTest = false;
	$scope.showResultScreen = false;
	$scope.timeLeft = null;
	$scope.numberofMoves = 0;
	$scope.showTrialResult = false;
	$scope.comingFromTrial = false;
	$scope.firstMove = false;
	$scope.rawData = [];
	$scope.retakeCancelBtnDisabled = false;
	if($stateParams.test != null){
		$scope.notificationId = $stateParams.test.notificationId;
		$scope.attempts = $stateParams.test.attempts;
		$scope.testResultId = $stateParams.test.testResultId;
		$scope.cycle = $stateParams.test.cycle;
		$scope.type = $stateParams.test.type;
		$scope.isPointGiven = $stateParams.test.isPointGiven;
		$scope.isTrailTaken = $stateParams.test.isTrailTaken;
		$stateParams.test.relapseTrackerId == undefined ? $scope.relapseTrackerId = "" : $scope.relapseTrackerId = $stateParams.test.relapseTrackerId;
		delete $stateParams.test;
	}
	var towerTestResult = $kinvey.DataStore.getInstance('performanceTestResults', $kinvey.DataStoreType.Network);
	var notificationTestList = $kinvey.DataStore.getInstance('notifications', $kinvey.DataStoreType.Network);
	$scope.isFlickerCount=0;
	var t1 = "";
	$scope.showSideMenu = true;
	$scope.testName = "Tower Tasks";

	var roundJson = [
		{
			id:0,
			finalPegPosition:2,
			discCount:2,
			timeToComplete:30400,
			minmumMoves:3,
			disc1Position:1,
			disc2Position:0
		},{
			id:1,
			finalPegPosition:1,
			discCount:3,
			timeToComplete:90400,
			minmumMoves:3,
			disc1Position:0,
			disc2Position:0,
			disc3Position:1
		},{
			id:2,
			finalPegPosition:2,
			discCount:3,
			timeToComplete:90400,
			minmumMoves:4,
			disc1Position:0,
			disc2Position:0,
			disc3Position:1
		},{
			id:3,
			finalPegPosition:0,
			discCount:3,
			timeToComplete:120400,
			minmumMoves:7,
			disc1Position:0,
			disc2Position:0,
			disc3Position:1
		},{
			id:4,
			finalPegPosition:1,
			discCount:4,
			timeToComplete:180400,
			minmumMoves:9,
			disc1Position:2,
			disc2Position:2,
			disc3Position:2,
			disc4Position:0
		},{
			id:5,
			finalPegPosition:2,
			discCount:4,
			timeToComplete:270400,
			minmumMoves:13,
			disc1Position:1,
			disc2Position:2,
			disc3Position:0,
			disc4Position:0
		}
	];

	$scope.relapse = {
		relapseInfection1:null,
		relapseInfection2:null,
		relapseInfection3:null,
		relapseInfection4:null,
	};

	$scope.prePMSurveyAnswers = [
		{"question":"Did you take your medications as usual?", "answer":null},
		{"question":"Did you get a typical amount of sleep last night?", "answer":null},
		{"question":"Do you have 6-8 minutes in a low distraction environment to focus on today's tests?", "answer":null},
		{"question":"Do you feel any of these factors could impact your performance test today?", "answer":null}
	];

  $scope.recommendForPm = '';
	var button ='<div class="popBodyInput orange"><div class="item item-input item-borderless"><div class="radioButtonGroup"><label class="questionLabel ">Did you take your medications as usual?</label>'+

		'<div class="radioButtonWrapper yes"><input class="radioButton" type="radio" name="preRelapse1" ng-value=true ng-model="relapse.relapseInfection1"><label class="radioLabel">  Yes </label></div>' +

		'<div class="radioButtonWrapper no"><input class="radioButton" type="radio" name="preRelapse1" ng-value=false ng-model="relapse.relapseInfection1"><label class="radioLabel">  No </label></div></div></div>'+

		'<div class="item item-input item-borderless"><div class="radioButtonGroup"><label class="questionLabel ">Did you get a typical amount of sleep last night?</label>'+

		'<div class="radioButtonWrapper yes"><input class="radioButton" type="radio" name="preRelapse2" ng-value=true ng-model="relapse.relapseInfection2"><label class="radioLabel">  Yes </label></div>' +

		'<div class="radioButtonWrapper no"><input class="radioButton" type="radio" name="preRelapse2" ng-value=false ng-model="relapse.relapseInfection2"><label class="radioLabel">  No </label></div></div></div>'+

		'<div class="item item-input item-borderless"><div class="radioButtonGroup"><label class="questionLabel ">Do you have 6-8 minutes in a low distraction environment to focus on today'+"'s "+ 'tests?</label>'+

		'<div class="radioButtonWrapper yes"><input class="radioButton" type="radio" name="preRelapse3" ng-value=true ng-model="relapse.relapseInfection3"><label class="radioLabel">  Yes </label></div>' +

		'<div class="radioButtonWrapper no"><input class="radioButton" type="radio" name="preRelapse3" ng-value=false ng-model="relapse.relapseInfection3"><label class="radioLabel">  No </label></div></div></div>'+

		'<div class="item item-input item-borderless"><div class="radioButtonGroup"><label class="questionLabel ">Do you feel any of these factors could impact your performance test today?</label>'+

		'<div class="radioButtonWrapper yes"><input class="radioButton" type="radio" name="preRelapse4" ng-value=true ng-model="relapse.relapseInfection4"><label class="radioLabel">  Yes </label></div>' +

		'<div class="radioButtonWrapper no"><input class="radioButton" type="radio" name="preRelapse4" ng-value=false ng-model="relapse.relapseInfection4"><label class="radioLabel">  No </label></div></div></div></div>'+

		'<div ng-show="relapse.relapseInfection1!=null && relapse.relapseInfection2!=null && relapse.relapseInfection3!=null && relapse.relapseInfection4!=null"><div ng-show="relapse.relapseInfection1==true && relapse.relapseInfection2==true && relapse.relapseInfection3==true && relapse.relapseInfection4==false"><div class="height20"></div><div class="popup-buttons" ><button class="button button-block button-darkPurple font40" ng-click="takeTestNow(true)">GREAT! LET'+"'"+'S START TODAY'+"'"+'S TASKS.</button></div><div class="height20"></div>'+

		'</div>'+

		'<div ng-show="!(relapse.relapseInfection1==true && relapse.relapseInfection2==true && relapse.relapseInfection3==true && relapse.relapseInfection4==false)"><p class="text-center italicFont smallText">You'+"'ve" +' indicated that some factors may affect your performance on today'+"'"+'s tasks.</p><div class="popup-buttons" ><button class="button button-block button-outline button-darkPurple font40 skip" ng-click="recommendPopup.close()">TAKE TEST <br/>LATER</button>'+

		'<div class="height10"></div><button class="button button-block button-multiline button-darkPurple font40" ng-click="takeTestNow(false)">I'+"'"+'D RATHER TAKE <br/>THE TEST NOW</button></div></div></div></div>';

	var template = '<p class="text-center">Performance can be influenced by many factors, including health, medical treatments, and life events, even having a '+'"bad day".'+'</p><a class="popClose" ng-click="recommendPopup.close()"></a><p class="text-center">We want you to do the best you can on the performance measures.  So tell us if today is a typical day for you by answering the questions below.</p>'+ button;

	$timeout(function() {
		$scope.recommendPopup = $ionicPopup.show({
			title:"Performance Metric Testing",
			template:template,
			scope: $scope,
			cssClass: "pmStartPop"
		});
		closePopupService.register($scope.recommendPopup);
		$scope.recommendPopup.then(function(res){
			if(!$scope.takeTestNowPopupClose){
				$state.go('app.performanceEvals');
			}
		});
	}, 100);

	$scope.testStartedKinvey = function(){
    if($scope.attempts==1){
	    $scope.setPMNotificationIsShown($scope.notificationId);
	     if($scope.relapseTrack.startRelapse){
	      $scope.relapseTrackerId = $scope.relapseTrack.kinveyData._id;
	    }
	  }
		if($scope.testResultId == ""){
			towerTestResult.save({
        "date": new Date(),
		    "PMType":$scope.testName,
        "attempts":$scope.attempts,
        "cycle":$scope.cycle,
        "type":$scope.type,
        "recommendForPm":$scope.recommendForPm,
				"prePMSurveyAnswers":$scope.prePMSurveyAnswers,
        "notificationId":$scope.notificationId,
        "isAborted": true,
        "isShown":$scope.attempts==3 ? true : false,
        "isPointGiven":$scope.isPointGiven,
        "isTrial":$scope.isTrailTaken,
        "relapseTrackerId":$scope.relapseTrackerId
		    }).then(function(entity) {
	        console.log("entity "+entity);
	        $scope.testResultId  = entity._id;
	        if($scope.attempts == 1 && $scope.relapseTrack.startRelapse){
           	$scope.$emit('updateRelapseTracker',$scope.testResultId);
        	}
	        if($scope.attempts>=3){
	        	$scope.updateNotificationCollection();
	        }
		    }).catch(function(error) {
		       $scope.internalGlitch();
		       $state.go('app.performanceEvals');
		    });
		}
		else{
			towerTestResult.save({
				"_id":$scope.testResultId,
        "date": new Date(),
				"PMType":$scope.testName,
        "attempts":$scope.attempts,
        "cycle":$scope.cycle,
        "type":$scope.type,
        "recommendForPm":$scope.recommendForPm,
				"prePMSurveyAnswers":$scope.prePMSurveyAnswers,
        "notificationId":$scope.notificationId,
        "isAborted": true,
        "isShown":$scope.attempts==3 ? true : false,
        "isPointGiven":$scope.isPointGiven,
        "isTrial":$scope.isTrailTaken,
        "relapseTrackerId":$scope.relapseTrackerId
		    }).then(function(entity) {
		        console.log("entity "+entity);
		        $scope.testResultId  = entity._id;
		        if($scope.attempts>=3){
		        	$scope.updateNotificationCollection();
		        }
		    }).catch(function(error) {
		       $scope.internalGlitch();
		       $state.go('app.performanceEvals');
		    });
		}
	}

	$scope.takeTestNow = function(status) {
		// console.log($scope.relapse);
		// console.log(status);
		if(status){
			$scope.recommendForPm = "yes";
		}else{
			$scope.recommendForPm = "no";
		}
		$scope.recommendPopup.close();
		$scope.takeTestNowPopupClose = true;

		for(var i = 0; i < $scope.prePMSurveyAnswers.length; i++){
			$scope.prePMSurveyAnswers[i].answer = "no";
			if ($scope.relapse["relapseInfection"+(i+1)] == true){
				$scope.prePMSurveyAnswers[i].answer = "yes";
			}
		}
		// console.log($scope.recommendForPm);
	}

	var mainState = {
		preload:function(){
			// Load all of the images
			game.load.image('small', 'img/towerOfHanoi/s_disc.png');
			game.load.image('medium', 'img/towerOfHanoi/m_disc.png');
			game.load.image('large', 'img/towerOfHanoi/l_disc.png');
			game.load.image('xlarge', 'img/towerOfHanoi/xl_disc.png');
			game.load.image('peg', 'img/towerOfHanoi/peg.png');
			game.load.image('stack', 'img/towerOfHanoi/stack.png');
			game.load.image('stackBlue', 'img/towerOfHanoi/stackBlue.png');
			game.load.image('youCannotPlace','img/towerOfHanoi/youCannotPlace.png');
			game.stage.backgroundColor = '#EFEFEF';
		},
		create:function(){
			// This function is called after the preload function
			// Here we set up the game, display sprites, input, etc.
			blinking = false;
			numberofMoves = 0;
			elapsedTime = 0;
			game.time.elapsed = 0;
			selected = null;
			state = 1;
			numberOfFinishTap = null;
			gameFinished = false;
			thenCallMouseUp = false;
			originalPostion = {};
			game.input.onUp.add(actions.onMouseUp, this);
			// bounds = new Phaser.Rectangle(10, 10, width-10, height-20);
			// var graphics = game.add.graphics(bounds.x, bounds.y);
			// graphics.beginFill(0x000077);
			// graphics.drawRect(0, 0, bounds.width, bounds.height);
			landscape.createRedBlocks();
			landscape.createDisks();
		},
		update:function(){
			elapsedTime += game.time.elapsed;
			totalTimeInSeconds = parseInt(game.time.totalElapsedSeconds());

			if (selected != null) {
				selected.x = game.input.x;
				selected.y = game.input.y;
			}
			switch(state){
				case 0:
					if(numberOfFinishTap > 1){
						actions.changeTheRound();
						break;
					}
					if(landscape.checkForCorrectness()){
						actions.changeTheRound();
					}else{
						state = 1;
					}
					break;
				case 1:
					if($scope.timeLeft!=0){
						actions.updateTimer();
					}
					if($scope.round>5){
						actions.finishGame();
					}
					break;
			}
		}
	};
	var actions = {
		select:function(disk,data,updateFrom){
			dragStart = true;
			if(blinking){
				$interval.cancel(t1);
				t1 = undefined;
				stackBlue.visible=false;
				stackHere.visible=true;
				blinking = false;
			}
			if(actions.checkForDragging(disk)){
				disk.input.draggable = true;
			}else{
				disk.input.draggable = false;
				return;
			}
			selected = disk;

			if(updateFrom == 'peg'){
				thenCallMouseUp = true;
			}else{
				thenCallMouseUp = false;
				originalPostion.x = selected.x;
				originalPostion.y = selected.y;
				originalPostion.peg = selected.peg;
			}
		},
		pegSelect:function(peg){
			if(peg.key == 'peg1'){
				if(peg1[peg1.length-1]){
					actions.pegClickUpdateDisk(peg1[peg1.length-1])
				}
			}else if(peg.key == 'peg2'){
				if(peg2[peg2.length-1]){
					actions.pegClickUpdateDisk(peg2[peg2.length-1])
				}
			}else if(peg.key == 'peg3'){
				if(peg3[peg3.length-1]){
					actions.pegClickUpdateDisk(peg3[peg3.length-1])
				}
			}
		},
		pegClickUpdateDisk:function(lastdiskOnPeg){
			if(lastdiskOnPeg == 'small'){
				originalPostion.x = small.x;
				originalPostion.y = small.y;
				originalPostion.peg = small.peg;
				small.x = game.input.x;
				small.y = game.input.y;
				actions.select(small,null,'peg');
			}else if(lastdiskOnPeg == 'medium'){
				originalPostion.x = medium.x;
				originalPostion.y = medium.y;
				originalPostion.peg = medium.peg;
				medium.x = game.input.x;
				medium.y = game.input.y;
				actions.select(medium,null,'peg');
			}else if(lastdiskOnPeg == 'large'){
				originalPostion.x = large.x;
				originalPostion.y = large.y;
				originalPostion.peg = large.peg;
				large.x = game.input.x;
				large.y = game.input.y;
				actions.select(large,null,'peg');
			}else if(lastdiskOnPeg == 'xlarge'){
				originalPostion.x = xlarge.x;
				originalPostion.y = xlarge.y;
				originalPostion.peg = xlarge.peg;
				xlarge.x = game.input.x;
				xlarge.y = game.input.y;
				actions.select(xlarge,null,'peg');
			}
		},
		onMouseUp:function(){
			if(thenCallMouseUp && selected){
				landscape.fixLocation(selected);
			}
		},
		unselect:function(){
			if (selected != null) {
				selected = null;
			}
		},
		checkForDragging:function(disk){
			// Only those will get return true when it has to move.
			if(disk.peg == 0 && peg1.length>1 && peg1.indexOf(disk.key) < (peg1.length-1)){
				return false;
			}else if(disk.peg == 1 && peg2.length>1 && peg2.indexOf(disk.key) < (peg2.length-1)){
				return false;
			}else if(disk.peg == 2 && peg3.length>1 && peg3.indexOf(disk.key) < (peg3.length-1)){
				return false;
			}else{
				return true;
			}
		},
		restart: function() {
			numberofMoves = 0;
			$scope.numberofMoves = numberofMoves;
			console.log('restart');
			game.paused = false;
			game.state.start('main');
			elapsedTime = 0;
		},
		changeTheRound:function(){
			// console.log("CORRECT Move");
			state = 1;
			if($scope.round == 5){
				$scope.showStartTest = false;
			}
			actions.checkFinalRoundCorrectness();
			$timeout(function(){
				if($scope.round != 0){
					$scope.showMainTest = false;
					$scope.rawData[$scope.round-1].totalNumberOfMoves = $scope.numberofMoves;
					console.log($scope.rawData);
				}
				$scope.round = $scope.round+1;
				$scope.firstMove = false;
				if($scope.round<5 && !gameFinished){
					game.paused = true;
				}
			}, 200);
			elapsedTime = 0;
		},
		updateTimer:function(){
			if($scope.round<=5 && $scope.showMainTest){
				if($scope.round == 0){
					var timeToComplete = roundJson[0].timeToComplete;
				}else{
					var timeToComplete = roundJson[$scope.round].timeToComplete;
				}
				$timeout(function(){
					if($scope.timeLeft != 0){
						$scope.timeLeft = parseInt((timeToComplete/1000) - elapsedTime/1000);
						//console.log($scope.timeLeft);
					}
				});
			}
		},
		finishGame:function(){
			$timeout(function(){
				$scope.gameFinished();
			});
			gameFinished = true;
			game.destroy();
			game = '';
		},
		finishTrialGame:function(){
			$timeout(function(){
				$scope.gameTrialFinished();
			});
			gameFinished = true;
			game.destroy();
			game = '';
		},
		updateCompletionResult : function(){
			if($scope.showMainTest){
				if($scope.rawData && $scope.rawData[$scope.round-1] && Object.keys($scope.rawData[$scope.round-1]).length > 0){
					$scope.rawData[$scope.round-1].correctCompleted = 1;
					$scope.rawData[$scope.round-1].completionTime = elapsedTime;
					if(numberOfFinishTap >0){
						$scope.rawData[$scope.round-1].finishTappedBeforeComplete = 'no';
					}
				}else{
					$scope.rawData[$scope.round-1] = {"correctCompleted":1,"completionTime":elapsedTime};
					if(numberOfFinishTap >0){
						$scope.rawData[$scope.round-1].finishTappedBeforeComplete = 'no';
					}
				}
			}
		},
		updateNotCompletionResult:function(){
			if($scope.showMainTest){
				if($scope.rawData && $scope.rawData[$scope.round-1] && Object.keys($scope.rawData[$scope.round-1]).length > 0){
					$scope.rawData[$scope.round-1].correctCompleted = 0;
					if(numberOfFinishTap >0){
						$scope.rawData[$scope.round-1].finishTappedBeforeComplete = 'yes';
					}
				}else{
					$scope.rawData[$scope.round-1] = {"correctCompleted":0};
					if(numberOfFinishTap >0){
						$scope.rawData[$scope.round-1].finishTappedBeforeComplete = 'yes';
					}
				}
			}
		},
		updateTimeForFirstMove : function(){
			if(!gameFinished && $scope.round != 0){
				if($scope.rawData && $scope.rawData[$scope.round-1] && Object.keys($scope.rawData[$scope.round-1]).length > 0){
					$scope.rawData[$scope.round-1].timeForFirstMove = elapsedTime;
				}else{
					$scope.rawData[$scope.round-1] = {"timeForFirstMove":elapsedTime};
				}
			}
		},
		updateRulesViolation:function(num){
			if($scope.showMainTest && !gameFinished && $scope.round != 0){
				if($scope.rawData && $scope.rawData[$scope.round-1] && Object.keys($scope.rawData[$scope.round-1]).length > 0){
					if($scope.rawData[$scope.round-1].rulesViolationCount){
						$scope.rawData[$scope.round-1].rulesViolationCount = $scope.rawData[$scope.round-1].rulesViolationCount+1;
					}else{
						$scope.rawData[$scope.round-1].rulesViolationCount = num;
					}
				}else{
					$scope.rawData[$scope.round-1] = {"rulesViolationCount":num};
				}
			}
		},
		updateFirstFinishTapTime:function(){
			console.log(numberOfFinishTap)
			if(numberOfFinishTap == 1){
				if($scope.rawData && $scope.rawData[$scope.round-1] && Object.keys($scope.rawData[$scope.round-1]).length > 0){
					$scope.rawData[$scope.round-1].firstFinishTap = elapsedTime;
				}else{
					$scope.rawData[$scope.round-1] = {"firstFinishTap":elapsedTime};
				}
			}else if(numberOfFinishTap == 2){
				if($scope.rawData && $scope.rawData[$scope.round-1] && Object.keys($scope.rawData[$scope.round-1]).length > 0){
					$scope.rawData[$scope.round-1].secondFinishTap = elapsedTime;
				}else{
					$scope.rawData[$scope.round-1] = {"secondFinishTap":elapsedTime};
				}
			}
		},
		changeStackImage:function(){
			t1 = $interval(function() {
				stackBlue.visible=!stackBlue.visible;
				stackHere.visible=!stackHere.visible;
				blinking = true;
			},1000);
		},
		checkFinalRoundCorrectness:function(){
			if($scope.round == 0){
				if(peg3.length == 2){
					$scope.showTrialResult = true;
					$scope.showStartTest = true;
					if($scope.attempts == 0 && !$scope.isTrailTaken){
						$scope.saveFinalTrailData();
					}
				}else{
					$scope.showTrialResult = false;
					actions.finishTrialGame()
				}
			}else if($scope.round == 1){
				if(peg2.length == 3){
					actions.updateCompletionResult();
				}else{
					actions.updateNotCompletionResult();
				}
			}else if($scope.round == 2){
				if(peg3.length == 3){
					actions.updateCompletionResult()
				}else{
					actions.updateNotCompletionResult();
				}
			}else if($scope.round == 3){
				if(peg1.length == 3){
					actions.updateCompletionResult();
				}else{
					actions.updateNotCompletionResult();
				}
			}else if($scope.round == 4){
				if(peg2.length == 4){
					actions.updateCompletionResult();
				}else{
					actions.updateNotCompletionResult();
				}
			}else if($scope.round == 5){
				if(peg3.length == 4){
					actions.updateCompletionResult();
				}else{
					actions.updateNotCompletionResult();
				}
			}
		}
	};
	var landscape = {
		createRedBlocks:function(){
			gameCenterX = game.world.centerX;
			gameCenterY = game.world.centerY;
			gameLeftX = (gameCenterX/2)-20;
			gameRightX = (gameCenterX + (gameCenterX/2)+ 20);
			// gameDownY = (gameCenterY + (gameCenterY/2) - 80);
			gameDownY = windowInnerWidth*0.446

			red1 = game.add.sprite(gameLeftX, gameDownY, 'peg');
			red2 = game.add.sprite(gameCenterX, gameDownY, 'peg');
			red3 = game.add.sprite(gameRightX, gameDownY, 'peg');
			red1.width = windowInnerWidth*0.244;
			red2.width = windowInnerWidth*0.244;
			red3.width = windowInnerWidth*0.244;
			red1.height = windowInnerWidth*0.488;
			red2.height = windowInnerWidth*0.488;
			red3.height = windowInnerWidth*0.488;


			red1.anchor.setTo(0.5);
			red2.anchor.setTo(0.5);
			red3.anchor.setTo(0.5);
			landscape.enableInputOnPeg(red1,'peg1');
			landscape.enableInputOnPeg(red2,'peg2');
			landscape.enableInputOnPeg(red3,'peg3');
		},
		createDisks:function(){
			// diskTotalHeight = 35*0.3;
			diskHeight = windowInnerWidth*0.0281;
      diskSmallWidth = windowInnerWidth*0.0893;
      diskDifference = windowInnerWidth*0.0525;
      // diskStartY = gameDownY+(red1.height/2) + 0.6 -(red1.height*0.133)-diskTotalHeight/2;
      diskStartY = windowInnerWidth*0.658 - (diskHeight/2);
      CentersBtwLeftPegs = (gameCenterX-gameLeftX)-red1.width;
      CentersBtwRightPegs = (gameRightX-gameCenterX)-red1.width;
      pegHalfWidth = red1.width/2;

      youCannotPlace = game.add.sprite(gameCenterX, ((game.world._height - game.world.centerY)/2), 'youCannotPlace');
			if(window.innerWidth>=360 && window.innerWidth<400){
				youCannotPlace.width = windowInnerWidth*0.545;
			}else{
				youCannotPlace.width = windowInnerWidth*0.51;
			}
			youCannotPlace.height = windowInnerWidth*0.08;
			youCannotPlace.anchor.setTo(0.5);
			youCannotPlace.visible = false;

			if($scope.round == 0 &&	roundJson[0].discCount == 2){
				medium = game.add.sprite(gameLeftX, diskStartY, 'medium');
				small = game.add.sprite(gameCenterX, diskStartY, 'small');

				/*medium.scale.set(0.3);
				small.scale.set(0.3);*/
				small.width = diskSmallWidth;
				small.height = diskHeight;
				medium.width = diskSmallWidth + diskDifference;
				medium.height = diskHeight;

				medium.anchor.setTo(0.5);
				small.anchor.setTo(0.5);
			}
			else if(roundJson[$scope.round].discCount == 3){
				large = game.add.sprite(gameCenterX, diskStartY, 'large');
				medium = game.add.sprite(gameLeftX, diskStartY, 'medium');
				small = game.add.sprite(gameLeftX, diskStartY-diskHeight, 'small');

				/*large.scale.set(0.3);
				medium.scale.set(0.3);
				small.scale.set(0.3);*/
				small.width = diskSmallWidth;
				small.height = diskHeight;
				medium.width = diskSmallWidth + diskDifference;
				medium.height = diskHeight;
				large.width = diskSmallWidth + (2*diskDifference);
				large.height = diskHeight;

				large.anchor.setTo(0.5);
				medium.anchor.setTo(0.5);
				small.anchor.setTo(0.5);
			}else if(roundJson[$scope.round].discCount == 4 && $scope.round == 4){
				small = game.add.sprite(gameRightX, diskStartY-(diskHeight*2), 'small');
				medium = game.add.sprite(gameRightX, diskStartY-diskHeight, 'medium');
				large = game.add.sprite(gameRightX, diskStartY, 'large');
				xlarge = game.add.sprite(gameLeftX, diskStartY, 'xlarge');

				/*small.scale.set(0.3);
				medium.scale.set(0.3);
				large.scale.set(0.3);
				xlarge.scale.set(0.3);*/
				small.width = diskSmallWidth;
				small.height = diskHeight;
				medium.width = diskSmallWidth + diskDifference;
				medium.height = diskHeight;
				large.width = diskSmallWidth + (2*diskDifference);
				large.height = diskHeight;
				xlarge.width = diskSmallWidth + (3*diskDifference);
				xlarge.height = diskHeight;

				small.anchor.setTo(0.5);
				medium.anchor.setTo(0.5);
				large.anchor.setTo(0.5);
				xlarge.anchor.setTo(0.5);

			}else if(roundJson[$scope.round].discCount == 4 && $scope.round == 5){
				small = game.add.sprite(gameCenterX, diskStartY, 'small');
				medium = game.add.sprite(gameRightX, diskStartY, 'medium');
				large = game.add.sprite(gameLeftX, diskStartY-diskHeight, 'large');
				xlarge = game.add.sprite(gameLeftX, diskStartY, 'xlarge');

				/*small.scale.set(0.3);
				medium.scale.set(0.3);
				large.scale.set(0.3);
				xlarge.scale.set(0.3);*/
				small.width = diskSmallWidth;
				small.height = diskHeight;
				medium.width = diskSmallWidth + diskDifference;
				medium.height = diskHeight;
				large.width = diskSmallWidth + (2*diskDifference);
				large.height = diskHeight;
				xlarge.width = diskSmallWidth + (3*diskDifference);
				xlarge.height = diskHeight;

				small.anchor.setTo(0.5);
				medium.anchor.setTo(0.5);
				large.anchor.setTo(0.5);
				xlarge.anchor.setTo(0.5);
			}

			if($scope.round == 0){
				stackHere = game.add.sprite(gameRightX, windowInnerWidth*0.812, 'stack');
				stackBlue = game.add.sprite(gameRightX, windowInnerWidth*0.812, 'stackBlue');
			}else if(roundJson[$scope.round].finalPegPosition == 0){
				stackHere = game.add.sprite(gameLeftX, windowInnerWidth*0.812, 'stack');
				stackBlue = game.add.sprite(gameLeftX, windowInnerWidth*0.812, 'stackBlue');
			}else if(roundJson[$scope.round].finalPegPosition == 1){
				stackHere = game.add.sprite(gameCenterX, windowInnerWidth*0.812, 'stack');
				stackBlue = game.add.sprite(gameCenterX, windowInnerWidth*0.812, 'stackBlue');
			}else if(roundJson[$scope.round].finalPegPosition == 2){
				stackHere = game.add.sprite(gameRightX, windowInnerWidth*0.812, 'stack');
				stackBlue = game.add.sprite(gameRightX, windowInnerWidth*0.812, 'stackBlue');
			}

			// stackHere.scale.set(0.28);
			stackHere.width = windowInnerWidth*0.21;
			stackHere.height = windowInnerWidth*0.15;

			stackHere.anchor.setTo(0.5);

			stackBlue.width = windowInnerWidth*0.21;
			stackBlue.height = windowInnerWidth*0.15;
			// stackBlue.scale.set(0.28);
			stackBlue.anchor.setTo(0.5);
			stackBlue.visible = false	;

			landscape.arrangeDiskOnPeg();

			if($scope.round == 0){
				landscape.assign_values(small,0,roundJson[0].disc1Position);
				landscape.assign_values(medium,1,roundJson[0].disc2Position);
			}else if(roundJson[$scope.round].discCount == 3){
				landscape.assign_values(small,0,roundJson[$scope.round].disc1Position);
				landscape.assign_values(medium,1,roundJson[$scope.round].disc2Position);
				landscape.assign_values(large,2,roundJson[$scope.round].disc3Position);
			}else if(roundJson[$scope.round].discCount == 4){
				landscape.assign_values(small,0,roundJson[$scope.round].disc1Position);
				landscape.assign_values(medium,1,roundJson[$scope.round].disc2Position);
				landscape.assign_values(large,2,roundJson[$scope.round].disc3Position);
				landscape.assign_values(xlarge,3,roundJson[$scope.round].disc4Position);
			}
		},
		enableInputOnPeg:function(peg,key){
			peg.key = key;
			peg.inputEnabled = true;
			peg.events.onInputDown.add(actions.pegSelect, this);
		},
		arrangeDiskOnPeg:function(){
			if($scope.round == 0){
				peg1 = ['medium'];
				peg2 = ['small'];
				peg3 = [];
			}	else if($scope.round == 1 || $scope.round == 2 || $scope.round == 3){
				peg1 = ['medium','small'];
				peg2 = ['large'];
				peg3 = [];
			}else if($scope.round == 4){
				peg1 = ['xlarge'];
				peg2 = [];
				peg3 = ['large','medium','small'];
			}else if($scope.round == 5){
				peg1 = ['xlarge','large'];
				peg2 = ['small'];
				peg3 = ['medium'];
			}
		},
		assign_values:function(disk, type, peg){
			disk.type = type;
			disk.peg = peg;
			disk.inputEnabled = true;
			disk.input.enableDrag(true);
			disk.input.enableSnap(87, 1, false, true);
			disk.events.onDragStop.add(landscape.fixLocation);
			disk.events.onDragUpdate.add(landscape.updateLocation);

			disk.events.onInputDown.add(actions.select, this);
			disk.input.useHandCursor = true;
			// disk.input.boundsRect = bounds;
		},
		updateLocation:function(disk){
			if((disk.x <10 || disk.x> game.world.width-10) && dragStart){
				console.log(disk.x);
				dragStart = false;
				disk.input.draggable = false;
				landscape.fixLocation(disk);
			}
		},
		fixLocation:function(disk){
			if (disk.x > -100 && disk.x < (gameLeftX+pegHalfWidth + (CentersBtwLeftPegs/2))) { // peg1
				if(landscape.checkPosition(0)){
					disk.x = gameLeftX;

					if(selected.peg == 0){
						peg1.pop();
					}else if(selected.peg == 1){
						peg2.pop();
					}else if(selected.peg == 2){
						peg3.pop();
					}

					if(peg1.length == 0){
						disk.y = diskStartY;
						selected.peg = 0;
						peg1.push(disk.key);
					}else{
						disk.y = diskStartY - (diskHeight*peg1.length);
						selected.peg = 0;
						peg1.push(disk.key);
					}

					if(originalPostion.peg != selected.peg){
						if(!$scope.firstMove){
							actions.updateTimeForFirstMove();
							$scope.firstMove = true;
						}
						numberofMoves = numberofMoves+1;
						$scope.numberofMoves = numberofMoves;
					}
					actions.unselect();
				}else{
					numberofMoves = numberofMoves+1;
					$scope.numberofMoves = numberofMoves;
					actions.updateRulesViolation(1);
					selected.x = originalPostion.x;
					selected.y = originalPostion.y;
					actions.unselect();
					youCannotPlace.visible = true;
					$timeout(function(){
					youCannotPlace.visible = false;
					},3000);
					//$scope.showMessage('You cannot place a larger disc onto a smaller one.',3000);
				}
			}
			else if (disk.x > (gameCenterX-pegHalfWidth -(CentersBtwLeftPegs/2)) && disk.x < (gameCenterX+pegHalfWidth+(CentersBtwRightPegs/2))) { // peg2
				if(landscape.checkPosition(1)){
					disk.x = gameCenterX;

					if(selected.peg == 0){
						peg1.pop();
					}else if(selected.peg == 1){
						peg2.pop();
					}else if(selected.peg == 2){
						peg3.pop();
					}

					if(peg2.length == 0){
						disk.y = diskStartY;
						selected.peg = 1;
						peg2.push(disk.key);
					}else{
						disk.y = diskStartY - (diskHeight*peg2.length);
						selected.peg = 1;
						peg2.push(disk.key);
					}

					if(originalPostion.peg != selected.peg){
						if(!$scope.firstMove){
							actions.updateTimeForFirstMove();
							$scope.firstMove = true;
						}
						numberofMoves = numberofMoves+1;
						$scope.numberofMoves = numberofMoves;
					}
					actions.unselect();
				}else{
					numberofMoves = numberofMoves+1;
					$scope.numberofMoves = numberofMoves;
					actions.updateRulesViolation(1);
					selected.x = originalPostion.x;
					selected.y = originalPostion.y;
					actions.unselect();
					youCannotPlace.visible = true;
					$timeout(function(){
					youCannotPlace.visible = false;
					},3000);
					//$scope.showMessage('You cannot place a larger disc onto a smaller one.',3000);
				}
			}
			else if (disk.x > (gameRightX-pegHalfWidth-(CentersBtwRightPegs/2)) && disk.x < 800) { // peg3
				if(landscape.checkPosition(2)){
					disk.x = gameRightX;

					if(selected.peg == 0){
						peg1.pop();
					}else if(selected.peg == 1){
						peg2.pop();
					}else if(selected.peg == 2){
						peg3.pop();
					}

					if(peg3.length == 0){
						disk.y = diskStartY;
						selected.peg = 2;
						peg3.push(disk.key);
					}else{
						disk.y = diskStartY - (diskHeight*peg3.length);
						selected.peg = 2;
						peg3.push(disk.key);
					}
					if(originalPostion.peg != selected.peg){
						if(!$scope.firstMove){
							actions.updateTimeForFirstMove();
							$scope.firstMove = true;
						}
						numberofMoves = numberofMoves+1;
						$scope.numberofMoves = numberofMoves;
					}
					actions.unselect();
				}else{
					numberofMoves = numberofMoves+1;
					$scope.numberofMoves = numberofMoves;
					actions.updateRulesViolation(1);
					selected.x = originalPostion.x;
					selected.y = originalPostion.y;
					actions.unselect();
					youCannotPlace.visible = true;
					$timeout(function(){
					youCannotPlace.visible = false;
					},3000);
					//$scope.showMessage('You cannot place a larger disc onto a smaller one.',3000);
				}
			}
		},
		checkPosition:function(dropedPeg){
			// Find the proper landing spot for the disk.
			// If the disks below it are smaller, don't allow the drop.
			if(selected.key == "small"){
				return true;
			}else if(selected.key == "medium"){
				if(dropedPeg == small.peg){
					return false;
				}
				return true;
			}else if(selected.key == "large"){
				if(dropedPeg == small.peg){
					return false;
				}else if(dropedPeg == medium.peg){
					return false;
				}
				return true;
			}else if(selected.key == 'xlarge'){
				if(dropedPeg == small.peg){
					return false;
				}else if(dropedPeg == medium.peg){
					return false;
				}else if(dropedPeg == large.peg){
					return false;
				}
				return true;
			}
		},
		checkForCorrectness:function(){
			if($scope.round == 0){
				selectedRoundJson = roundJson[0];
			}else{
				selectedRoundJson = roundJson[$scope.round];
			}
			if(peg1.length == selectedRoundJson.discCount && selectedRoundJson.finalPegPosition == 0){
				return true;
			}else if(peg2.length == selectedRoundJson.discCount && selectedRoundJson.finalPegPosition == 1){
				return true;
			}else if(peg3.length == selectedRoundJson.discCount && selectedRoundJson.finalPegPosition == 2){
				return true;
			}else {
				return false;
			}
		}
	};

	$scope.$watch(function(){
		return $scope.timeLeft;
	},function(newValue,oldValue){
		if(newValue <= 0 && newValue != null){
			actions.changeTheRound();
			state = 1;
		}
	});

	$scope.gameFinished = function(){
		$scope.showGetStarted = false;
		$scope.showMainTest = false;
		$scope.showStartTest = false;
		$scope.showResultScreen = true;
		$scope.numberOfCorrect = angular.copy($scope.rawData.reduce(function(a, b) {return (a + b.correctCompleted);}, 0));
		$scope.updateKinveyData(false);
	}

	$scope.gameTrialFinished = function(){
		$scope.showGetStarted = true;
		$scope.showStartTest = false;
		$scope.showMainTest = false;
		$scope.showResultScreen = false;
		$scope.timeLeft = null;
		$scope.numberofMoves = 0;
		$scope.showTrialResult = false;
		$scope.comingFromTrial = false;
		$scope.firstMove = false;
		$scope.showSideMenu = true;
		$scope.showMessage('Please read instructions and try again.');
	}

	$scope.updateKinveyData = function(resetDataStatus,cancel){
		$scope.showLoader();
		console.log("update the data on kinvey");
		console.log($scope.rawData,$scope.recommendForPm, $scope.attempts)
		if(!$scope.numberOfCorrect){
			$scope.numberOfCorrect = angular.copy($scope.rawData.reduce(function(a, b) {return (a + b.correctCompleted);}, 0));
		}
		var addpoints = false;
    if($scope.isPointGiven == false && cancel != true) {
      $scope.isPointGiven = true; addpoints = true;
    }
    if($scope.attempts == 3){
  		$scope.retakeCancelBtnDisabled = true;
  		//$scope.updateNotificationCollection();
  	}
  	 var kinveObj = {
			"_id" : $scope.testResultId,
      "date": new Date(),
      "PMType":$scope.testName,
      "score1":$scope.numberOfCorrect,
      "attempts":$scope.attempts,
      "cycle":$scope.cycle,
      "type":$scope.type,
      "raw":$scope.rawData,
      "recommendForPm":$scope.recommendForPm,
			"prePMSurveyAnswers":$scope.prePMSurveyAnswers,
      "isAborted":cancel == undefined ? false : cancel,
      "isShown":$scope.attempts==3 ? true : false,
			"notificationId":$scope.notificationId,
			"isPointGiven":$scope.isPointGiven,
			"isTrial":$scope.isTrailTaken,
			"relapseTrackerId":$scope.relapseTrackerId
    }
		towerTestResult.save(kinveObj).then(function(entity) {
    	$scope.resetDataVarables(resetDataStatus);
    	if(cancel == true){
   			$state.go('app.performanceEvals');
     	}
     	else{
     		if(addpoints){
					$scope.$emit('saveGamificationPoints',CONSTANT.gamificationPoints.pmEvaluation.value,CONSTANT.gamificationPoints.pmEvaluation.name);
		    }
     	}
     	$scope.hideLoader();
	    console.log("entity",entity);
    }).catch(function(error) {
      $scope.internalGlitch();
      localStorage.setItem("unSavedPMData", JSON.stringify(kinveObj));
      localStorage.setItem("unSavedPMPoints", addpoints);
    });

	}

	$scope.updateNotificationCollection = function(){
		var tempdata = [];
		var col = "_id";
		var relatedJson = $scope.notificationId;
		var query = new $kinvey.Query();
	  query.equalTo(col, relatedJson);
		notificationTestList.find(query).subscribe(function(data) {
	    tempdata = data;
    }, function(error) {}, function(data) {
    	tempdata[0].isAck = true;
    	notificationTestList.update(tempdata[0]).then(function(data) {
    		console.log(data);
	    }).catch(function(error) {
        console.log(error);
        $scope.internalGlitch();
      });
    });
	}

	$scope.finishTapped = function(){
		if($scope.round == 0){
			console.log('finishTapped');
			state = 0;
			numberOfFinishTap = numberOfFinishTap+1;
		}else{
			state = 0;
			numberOfFinishTap = numberOfFinishTap+1;
			actions.updateFirstFinishTapTime();
		}
		if(numberOfFinishTap == null || numberOfFinishTap < 2){
			actions.changeStackImage();
		}else{
			$interval.cancel(t1);
			t1 = undefined;
			stackBlue.visible=false;
			stackHere.visible=true;
			blinking = false;
		}
	}

	$scope.gettingStarted = function(){
		if (typeof window.ga !== "undefined") {
      window.ga.trackEvent('Tower Task Screen', 'Test starts', 'Get Started', 1);
    }
		$ionicScrollDelegate.scrollTop();
		$scope.attempts++;
    $scope.testStartedKinvey();
    if($scope.attempts == 3){
  		$scope.retakeCancelBtnDisabled = true;
  	}
  	$scope.isTrailRunning = false;
		$scope.showGetStarted = false;
		$scope.showMainTest = false;
		$scope.showStartTest = true;
		$scope.showResultScreen = false;
		$scope.comingFromTrial = false;
		$scope.numberOfCorrect = 0;
		$scope.round = 1;
		$scope.showSideMenu = false;
	}

	$scope.startTrialTest = function(){
		if (typeof window.ga !== "undefined") {
      window.ga.trackEvent('Tower Task Screen', 'Trail starts', 'Take A Trail', 1);
    }
		$scope.round = 0;
		$scope.isTrailRunning = true;
		$scope.comingFromTrial = true;
		$scope.showGetStarted = false;
		$scope.showStartTest = false;
		$scope.showSideMenu = false;
		$scope.startMainTest();
		if($scope.attempts == 0 && !$scope.isTrailTaken){
      $scope.saveInitialTrailData();
    }
	}

  $scope.saveInitialTrailData = function(){
    $scope.showLoader();
    if($scope.testResultId == ""){
      towerTestResult.save({
        "date": new Date(),
        "PMType":$scope.testName,
        "attempts":0,
        "cycle":$scope.cycle,
        "type":$scope.type,
				"prePMSurveyAnswers":$scope.prePMSurveyAnswers,
        "notificationId":$scope.notificationId,
        "isTrial":false
      }).then(function(entity) {
        console.log("entity ",entity);
        $scope.isTrailTaken = false;
        $scope.testResultId  = entity._id;
        $scope.hideLoader();
      }).catch(function(error) {
        $scope.internalGlitch();
        $state.go('app.performanceEvals');
      });
    }else{
      towerTestResult.save({
        "_id":$scope.testResultId,
        "date": new Date(),
        "PMType":$scope.testName,
        "attempts":0,
        "cycle":$scope.cycle,
        "type":$scope.type,
				"prePMSurveyAnswers":$scope.prePMSurveyAnswers,
        "notificationId":$scope.notificationId,
        "isTrial":false
      }).then(function(entity) {
        console.log("entity ",entity);
        $scope.isTrailTaken = false;
        $scope.testResultId  = entity._id;
        $scope.hideLoader();
      }).catch(function(error) {
        $scope.internalGlitch();
        $state.go('app.performanceEvals');
      });
    }
  };

  $scope.saveFinalTrailData = function(){
		$scope.showLoader();
		towerTestResult.save({
			"_id":$scope.testResultId,
      "date": new Date(),
      "PMType":$scope.testName,
      "attempts":0,
      "cycle":$scope.cycle,
      "type":$scope.type,
			"prePMSurveyAnswers":$scope.prePMSurveyAnswers,
      "notificationId":$scope.notificationId,
      "isTrial":true
    }).then(function(entity) {
      console.log("entity ",entity);
      $scope.isTrailTaken = true;
      $scope.testResultId  = entity._id;
      $scope.hideLoader();
    }).catch(function(error) {
      $scope.internalGlitch();
    	$state.go('app.performanceEvals');
    });
	};

	$scope.startMainTestAfterTrial = function(){
		if (typeof window.ga !== "undefined") {
      window.ga.trackEvent('Tower Task Screen', 'Test starts', 'Get Started', 1);
    }
		$scope.showStartTest = true;
		$scope.showMainTest = false;
		$interval.cancel(t1);
		t1 = undefined;
		$scope.attempts++;
		$scope.isTrailRunning = false;
    $scope.testStartedKinvey();
    if($scope.attempts == 3){
  		$scope.retakeCancelBtnDisabled = true;
  	}
		// $scope.startMainTest();
	}

	$scope.startMainTest = function(){
		$interval.cancel(t1);
		t1 = undefined;
		$scope.isFlickerCount=0;
		$scope.showTrialResult = false;
		$scope.showMainTest = true;
		$scope.timeLeft = null;

		if($scope.round ==  0 && $scope.comingFromTrial){
			game = new Phaser.Game(width, height, Phaser.AUTO, 'gameDiv1');
			game.state.add('main', mainState);
		}
		if($scope.round == 1 && !$scope.comingFromTrial){
			game = new Phaser.Game(width, height, Phaser.AUTO, 'gameDiv1');
			game.state.add('main', mainState);
		}
		actions.restart();
	}

	$scope.resetDataVarables = function(resetDataStatus){
		if(resetDataStatus){
			$scope.rawData = [];
		}
		$scope.timeLeft = null;
	}

	$scope.retakeTest = function(){
		if($scope.attempts == 3){
			$state.go('app.performanceEvals');
			return;
		}
		var template = '<a class="popClose" ng-click="retakeTestPopup.close()"></a><p class="text-center">If problems occurred while taking this test, you can retake it. However, this test is given to gain an accurate measurement of normal functioning, so please don’t re-take it just to improve your score or to practice.</p>';
		if($scope.attempts == 2){
			var title = "Are you sure you want to restart the test? You have 1 test remaining.";
			var buttons = [{text: 'CONTINUE', type: 'button-block button-darkPurple', onTap: function() {return true; } }];
		}
		else{
			var title = "Are you sure you want to restart the test? You have 2 tests remaining.";
			var buttons = [{text: 'CANCEL', type: 'button-block button-outline button-darkPurple skip', onTap: function() {return false; } },{text: 'RETAKE TEST', type: 'button-block button-darkPurple', onTap: function() {return true; } }];
		}
		$scope.retakeTestPopup = $ionicPopup.show({
			title:title,
      template:template,
      scope: $scope,
      buttons: buttons
		});
		closePopupService.register($scope.retakeTestPopup);
		$scope.retakeTestPopup.then(function(res){
			if(res){
				// $scope.attempts = $scope.attempts + 1;
				$scope.rawData = [];
				$scope.showGetStarted = true;
				$scope.showStartTest = false;
				$scope.showMainTest = false;
				$scope.showResultScreen = false;
				$scope.timeLeft = null;
				$scope.numberofMoves = 0;
				$scope.showTrialResult = false;
				$scope.comingFromTrial = false;
				$scope.firstMove = false;
				$scope.showSideMenu = true;
  		}
		});
	}

	$scope.cancelBtn = function () {
		if($scope.showMainTest){
			game.paused = true;
		}
		if($scope.round == 0 || $scope.isTrailRunning){
			$scope.onCancelCall();
			return;
		}
		if ($scope.attempts == 2) {
      $scope.cancelTemplate = '<p style="text-align: center;">If you cancel the test, it will count as one of your test takes. You will only have one additional chance to retake the test.</p><p  class="text-center">A test should be cancelled only if something went wrong; please don’t re-take the test just to improve your score, or to practice.</p><a class="popClose" ng-click="cancelTestPopup.close()"></a>';
    } else if ($scope.attempts == 1) {
      $scope.cancelTemplate = '<p style="text-align: center;">If you cancel the test, it will count as one of your test takes. You will only have two additional chances to retake the test.</p><p  class="text-center">A test should be cancelled only if something went wrong; please don’t re-take the test just to improve your score, or to practice.</p><a class="popClose" ng-click="cancelTestPopup.close()"></a>';
    }
		$scope.cancelTestPopup = $ionicPopup.show({
			title:"Are you sure you want to cancel the test?",
			template:$scope.cancelTemplate,
			scope: $scope,
			buttons: [{
				text: 'YES, CANCEL',
				type: 'button-block button-outline button-darkPurple skip',
				onTap: function() {
					return true;
				}
			},{
				text: 'RETURN TO<br/> THE TEST',
				type: 'button-block button-darkPurple',
				onTap: function() {
					return false;
				}
			}]
		});
		closePopupService.register($scope.cancelTestPopup);
		$scope.cancelTestPopup.then(function(res){
			if(res){
				if (typeof window.ga !== "undefined") {
          window.ga.trackEvent('Tower Task Screen - Cancel Popup', 'Performance Metric Screen (Tower Task)', 'Yes, Cancel', 1);
        }
				$scope.onCancelCall();
				if(t1!=undefined){
					$interval.cancel(t1);
					t1 = undefined;
				}
				$state.go('app.performanceEvals');
			}else{
				if (typeof window.ga !== "undefined") {
          window.ga.trackEvent('Tower Task Screen - Cancel Popup', 'Cancel Pop Up', 'Return to the Test', 1);
        }
				if($scope.showMainTest){
					game.paused = false;
				}
			}
		});
	}

	$scope.onCancelCall = function(){
		// Want to submit the data on cancel
		if($scope.round > 0 && !$scope.isTrailRunning){
			$scope.updateKinveyData(true,true);
		}
		// Cannot refresh the data want to submit it.
		// $scope.rawData = [];
		// $scope.timeLeft = null;
		$scope.showGetStarted = true;
		$scope.showStartTest = false;
		$scope.showMainTest = false;
		$scope.showResultScreen = false;
		$scope.numberofMoves = 0;
		$scope.showTrialResult = false;
		$scope.comingFromTrial = false;
		$scope.firstMove = false;
		$scope.attempts = $scope.attempts + 1;
		if(typeof game != 'undefined' && game != ''){
			game.destroy();
			game = '';
		}
		$ionicScrollDelegate.scrollTop();
	}

	$scope.goBackToPmPage = function(){
		if (typeof window.ga !== "undefined") {
      window.ga.trackEvent('Tower Task Screen','Performance Metric Screen (Tower Task)', 'Return', 1);
    }
		$scope.resetDataVarables(true);
		$state.go('app.performanceEvals');
	}
}]);



// Resulted JSON :

// {
//   "PMType": "Tower Tasks",
//   "date": "2016-07-29T13:40:15.845Z",
//   "raw": [{"timeForFirstMove": 1802, "firstFinishTap": 4695, "correctCompleted": 1, "completionTime": 4712, "finishTappedBeforeComplete": "no", "totalNumberOfMoves": 3 },
//  {"timeForFirstMove": 4838, "rulesViolationCount": 1, "firstFinishTap": 10801, "correctCompleted": 1, "completionTime": 10817, "finishTappedBeforeComplete": "no", "totalNumberOfMoves": 5 },
//  {"firstFinishTap": 1318, "secondFinishTap": 1818, "correctCompleted": 0, "finishTappedBeforeComplete": "yes", "totalNumberOfMoves": 0 },
//  {"timeForFirstMove": 2810, "firstFinishTap": 29249, "secondFinishTap": 33794, "correctCompleted": 1, "completionTime": 33810, "finishTappedBeforeComplete": "no", "totalNumberOfMoves": 15 },
//  {"rulesViolationCount": 3, "timeForFirstMove": 8666, "correctCompleted": 0, "totalNumberOfMoves": 57 }]
//   "score1": 4,
//   "attempts": 1,
// 	 "recommendStatus":"NO"
// }
