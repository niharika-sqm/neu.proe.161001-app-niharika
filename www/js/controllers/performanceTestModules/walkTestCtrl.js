neuroScienceApp.controller('walkTestCtrl', ['$scope', '$state', '$interval', '$kinvey', '$ionicScrollDelegate', '$ionicPopup', '$stateParams', '$q', 'CONSTANT','closePopupService','$timeout', function($scope, $state, $interval, $kinvey, $ionicScrollDelegate, $ionicPopup, $stateParams, $q, CONSTANT,closePopupService,$timeout) {

  $timeout(function(){
    window.ga.trackView('Walk Test');
  }, 1000);

  $scope.$on('$ionicView.beforeEnter', function() {
    $scope.showWalkTest = true;
    $scope.showSideMenu = true;
    $scope.started = true;
    $scope.testCounter = 1;
    $scope.log = 0;
    if ($stateParams.test != null) {
      $scope.notificationId = $stateParams.test.notificationId;
      $scope.attempts = $stateParams.test.attempts;
      $scope.testResultId = $stateParams.test.testResultId;
      $scope.cycle = $stateParams.test.cycle;
      $scope.type = $stateParams.test.type;
      $scope.isPointGiven = $stateParams.test.isPointGiven;
      $scope.isTrailTaken = $stateParams.test.isTrailTaken;
      $stateParams.test.relapseTrackerId == undefined ? $scope.relapseTrackerId = "" : $scope.relapseTrackerId = $stateParams.test.relapseTrackerId;
      delete $stateParams.test;
    }
    $scope.Results = {};
    $scope.yes = 'button-lightGray button-outline';
    $scope.no = 'button-lightGray button-outline';
    $scope.rawData = [];
    $scope.retakeCancelBtnDisabled = false;
  });

  $scope.$on('$ionicView.afterEnter', function() {
    $scope.lockOrientation();
  });

  $scope.$on('$destroy',function(){
    $scope.unlockOrientation();
  });

  $scope.relapse = {
    relapseInfection1: null,
    relapseInfection2: null,
    relapseInfection3: null,
    relapseInfection4: null,
  };

  $scope.prePMSurveyAnswers = [
  	{"question":"Did you take your medications as usual?", "answer":null},
  	{"question":"Did you get a typical amount of sleep last night?", "answer":null},
  	{"question":"Do you have 6-8 minutes in a low distraction environment to focus on today's tests?", "answer":null},
  	{"question":"Do you feel any of these factors could impact your performance test today?", "answer":null}
  ];

  $scope.raw = [];
  $scope.rawData = {};
  $scope.rawStartTime = 0;
  $scope.rawPauseTime = 0;
  $scope.rawPauseTimeFirstCheck = 0;
  $scope.totalWalkR1 = 0;
  $scope.totalWalkR2 = 0;
  $scope.totalPauseR1 = 0;
  $scope.totalPauseR2 = 0;

  $scope.recommendationPopup = function() {
    $scope.recommendForPm = '';
    var button = '<div class="popBodyInput orange"><div class="item item-input item-borderless"><div class="radioButtonGroup"><label class="questionLabel ">Did you take your medications as usual?</label>' +

      '<div class="radioButtonWrapper yes"><input class="radioButton" type="radio" name="preRelapse1" ng-value=true ng-model="relapse.relapseInfection1"><label class="radioLabel">  Yes </label></div>' +

      '<div class="radioButtonWrapper no"><input class="radioButton" type="radio" name="preRelapse1" ng-value=false ng-model="relapse.relapseInfection1"><label class="radioLabel">  No </label></div></div></div>' +

      '<div class="item item-input item-borderless"><div class="radioButtonGroup"><label class="questionLabel ">Did you get a typical amount of sleep last night?</label>' +

      '<div class="radioButtonWrapper yes"><input class="radioButton" type="radio" name="preRelapse2" ng-value=true ng-model="relapse.relapseInfection2"><label class="radioLabel">  Yes </label></div>' +

      '<div class="radioButtonWrapper no"><input class="radioButton" type="radio" name="preRelapse2" ng-value=false ng-model="relapse.relapseInfection2"><label class="radioLabel">  No </label></div></div></div>' +

      '<div class="item item-input item-borderless"><div class="radioButtonGroup"><label class="questionLabel ">Do you have 6-8 minutes in a low distraction environment to focus on today' + "'s " + 'tests?</label>' +

      '<div class="radioButtonWrapper yes"><input class="radioButton" type="radio" name="preRelapse3" ng-value=true ng-model="relapse.relapseInfection3"><label class="radioLabel">  Yes </label></div>' +

      '<div class="radioButtonWrapper no"><input class="radioButton" type="radio" name="preRelapse3" ng-value=false ng-model="relapse.relapseInfection3"><label class="radioLabel">  No </label></div></div></div>' +

      '<div class="item item-input item-borderless"><div class="radioButtonGroup"><label class="questionLabel ">Do you feel any of these factors could impact your performance test today?</label>' +

      '<div class="radioButtonWrapper yes"><input class="radioButton" type="radio" name="preRelapse4" ng-value=true ng-model="relapse.relapseInfection4"><label class="radioLabel">  Yes </label></div>' +

      '<div class="radioButtonWrapper no"><input class="radioButton" type="radio" name="preRelapse4" ng-value=false ng-model="relapse.relapseInfection4"><label class="radioLabel">  No </label></div></div></div></div>' +

      '<div ng-show="relapse.relapseInfection1!=null && relapse.relapseInfection2!=null && relapse.relapseInfection3!=null && relapse.relapseInfection4!=null"><div ng-show="relapse.relapseInfection1==true && relapse.relapseInfection2==true && relapse.relapseInfection3==true && relapse.relapseInfection4==false"><div class="height20"></div><div class="popup-buttons" ><button class="button button-block button-darkPurple font40" ng-click="takeTestNow(true)">GREAT! LET' + "'" + 'S START TODAY' + "'" + 'S TASKS.</button></div><div class="height20"></div>' +

      '</div>' +

      '<div ng-show="!(relapse.relapseInfection1==true && relapse.relapseInfection2==true && relapse.relapseInfection3==true && relapse.relapseInfection4==false)"><p class="text-center italicFont smallText">You'+"'ve"+' indicated that some factors may affect your performance on today' + "'" + 's tasks.</p><div class="popup-buttons" ><button class="button button-block button-outline button-darkPurple font40 skip" ng-click="recommendPopup.close()">TAKE TEST <br/>LATER</button>' +

      '<div class="height10"></div><button class="button button-block button-multiline button-darkPurple font40" ng-click="takeTestNow(false)">I' + "'" + 'D RATHER TAKE <br/>THE TEST NOW</button></div></div></div></div>';

    var template = '<p class="text-center">Performance can be influenced by many factors, including health, medical treatments, and life events, even having a ' + '"bad day".' +'</p><a class="popClose" ng-click="recommendPopup.close()"></a><p class="text-center">We want you to do the best you can on the performance measures.  So tell us if today is a typical day for you by answering the questions below.</p>' + button;
    $timeout(function() {
      $scope.recommendPopup = $ionicPopup.show({
        title: "Performance Metric Testing",
        template: template,
        scope: $scope,
        cssClass: "pmStartPop"
      });
      closePopupService.register($scope.recommendPopup);
      $scope.recommendPopup.then(function(res) {
        if (!$scope.takeTestNowPopupClose) {
          $state.go('app.performanceEvals');
        }
      });
    }, 100);
  }
  $scope.takeTestNow = function(status) {
    // console.log($scope.relapse);
    // console.log(status);
    if (status) {
      $scope.recommendForPm = "yes";
    } else {
      $scope.recommendForPm = "no";
    }
    $scope.recommendPopup.close();
    $scope.takeTestNowPopupClose = true;

    for(var i = 0; i < $scope.prePMSurveyAnswers.length; i++){
      $scope.prePMSurveyAnswers[i].answer = "no";
			if ($scope.relapse["relapseInfection"+(i+1)] == true){
				$scope.prePMSurveyAnswers[i].answer = "yes";
			}
    }

    // console.log($scope.recommendForPm);
  }
  $scope.recommendationPopup();

  $scope.walkTestData = {};
  $scope.assistanceOptions = [{ id: 1, text: "One cane", checked: false }, { id: 2, text: "Two canes", checked: false }, { id: 3, text: "Walker", checked: false }, { id: 4, text: "Supported by another person", checked: false }, { id: 5, text: "Supported by the wall", checked: false }, { id: 6, text: "Other", checked: false }];
  $scope.optionSingleSelect = null;
  $scope.typeSingleSelect = null;
  $scope.kinveyid = "";
  $scope.openSingleSelect = function() {
    $scope.typeSingleSelect = 'assistance';
    $scope.optionSingleSelect = angular.copy($scope.assistanceOptions);
    if ($scope.walkTestData[$scope.typeSingleSelect]) {
      $scope.$emit('updateSingleSelectedArray', $scope.walkTestData[$scope.typeSingleSelect]);
    }else{
      $scope.$emit('emptySingleSelectedArray');
    }
    var singleSelectPopup = $ionicPopup.show({
      title: 'What assistance will you use?',
      templateUrl: 'templates/genericTemplate/singleSelectPopup.html',
      cssClass: "selectBoxPopup",
      scope: $scope,
      buttons: [{
        text: 'Cancel',
        type: 'button-block button-outline button-darkPurple font56',
        onTap: function() {
          return false;
        }
      }, {
        text: 'Okay',
        type: 'button-block button-outline button-darkPurple font56 bold',
        onTap: function() {
          return true;
        }
      }]
    });
    closePopupService.register(singleSelectPopup);
    singleSelectPopup.then(function(res) {
      console.log($scope.singleSelection, res);
      if (res) {
        if($scope.singleSelection.text == "Two canes" || $scope.singleSelection.text == "Walker"){
          $scope.openTwoCanesPopup($scope.singleSelection,$scope.typeSingleSelect);
        }
        else{
          $scope.walkTestData[$scope.typeSingleSelect] = $scope.singleSelection;
        }
        $scope.$emit('emptySingleSelectedArray');
        console.log($scope.walkTestData);
      }
    });
  }

  $scope.openTwoCanesPopup = function(values,index) {
    $scope.twoCanesSelectPopup = $ionicPopup.show({
      title: values.text,
      template: '<p style="text-align: center;">Since both of your hands will be busy, make sure another person is there to record your time.</p><a class="popClose" ng-click="twoCanesSelectPopup.close()"></a>',
      scope: $scope,
      buttons: [{
        text: 'Cancel',
        type: 'button-block button-outline button-darkPurple font56',
        onTap: function() {
          return false;
        }
      }, {
        text: 'Okay',
        type: 'button-block button-outline button-darkPurple font56 bold',
        onTap: function() {
          return true;
        }
      }]
    });
    closePopupService.register($scope.twoCanesSelectPopup);
    $scope.twoCanesSelectPopup.then(function(res) {
      if (!res) {
        $scope.openSingleSelect();
      }
      else{
        $scope.walkTestData[index] = values;
      }
    });
  }

  var performanceTestResults = $kinvey.DataStore.getInstance('performanceTestResults', $kinvey.DataStoreType.Network);
  var notificationTestList = $kinvey.DataStore.getInstance('notifications', $kinvey.DataStoreType.Network);

  $scope.assistance = "";
  $scope.walkTestData.PMType = "Timed 25ft Walk";
  $scope.walkTestData.date = new Date();
  var t, timePerNow1, timePerNow2, timePerNow;
  $scope.log = 0;
  $scope.reset = true;
  $scope.logbtn = true;
  $scope.timerWithTimeout = 0;
  $scope.startontap = true;
  $scope.assistanceSelect = false;
  $scope.yes = 'button-lightGray button-outline';
  $scope.no = 'button-lightGray button-outline';

  $scope.taptostart = function() {
    if ($scope.startontap && $scope.timerWithTimeout != 18000) {
      $scope.startTimerWithTimeout();
      $scope.startRecTime4Raw();
    } else {
      $scope.stopTimerWithTimeout();
      $scope.startRecPauseTime4Raw();
    }
    $scope.startontap = !$scope.startontap;
  };

  $scope.startRecTime4Raw = function(){
    $scope.rawStartTime = performance.now();
    if($scope.rawPauseTimeFirstCheck > 0){
      var tempWtPausetime = performance.now();
      $scope.rawPauseTime = tempWtPausetime - $scope.rawPauseTime;
      $scope.rawData = {"time":$scope.rawPauseTime, "type":"pause", "round": $scope.testCounter}
      $scope.raw.push($scope.rawData);
      $scope.rawData = {};
      $scope.rawPauseTime = 0;
      console.log($scope.raw);
    }
  };

  $scope.startRecPauseTime4Raw = function(){
    $scope.rawPauseTime = performance.now();
    $scope.rawPauseTimeFirstCheck++;
    var tempWtRuntime = performance.now();
    $scope.rawStartTime = tempWtRuntime - $scope.rawStartTime;
    $scope.rawData = {"time":$scope.rawStartTime, "type":"walk", "round": $scope.testCounter}
    $scope.raw.push($scope.rawData);
    $scope.rawData = {};
    $scope.rawStartTime = 0;
    console.log($scope.raw);
  };

  $scope.getStarted = function(formData) {
    if (typeof window.ga !== "undefined") {
      window.ga.trackEvent('Timed 25ft Walk Screen', 'Test starts', 'Get Started', 1);
    }
    if ($scope.walkTestData.selectAssistance == false || ($scope.walkTestData.selectAssistance == true && $scope.walkTestData.assistance != undefined && $scope.walkTestData.assistance != "")) {
      $scope.resetTimerWithTimeout();
      $scope.attempts++;
      console.log("Count = " + $scope.attempts);
      $scope.testStartedKinvey();
      // if ($scope.attempts == 3) {
      //   $scope.retakeCancelBtnDisabled = true;
      // }
      $ionicScrollDelegate.scrollTop();
      $scope.started = false;
      $scope.showSideMenu = false;
      formData.$submitted = false;
    }
  };

  $scope.testStartedKinvey = function() {
    if($scope.attempts==1){
      $scope.setPMNotificationIsShown($scope.notificationId);
       if($scope.relapseTrack.startRelapse){
        $scope.relapseTrackerId = $scope.relapseTrack.kinveyData._id;
      }
    }
    var arrayAssistance = ""
    if ($scope.walkTestData.assistance) {
      arrayAssistance = JSON.parse(angular.toJson($scope.walkTestData.assistance));
    } else {
      arrayAssistance = "";
    }
    if ($scope.testResultId == "") {
      performanceTestResults.save({
        "date": $scope.walkTestData.date,
        "assistance": arrayAssistance,
        "PMType": $scope.walkTestData.PMType,
        "attempts": $scope.attempts,
        "cycle": $scope.cycle,
        "type": $scope.type,
        "recommendForPm": $scope.recommendForPm,
        "prePMSurveyAnswers":$scope.prePMSurveyAnswers,
        "notificationId": $scope.notificationId,
        "isAborted": true,
        "isShown": false,
        "isPointGiven":$scope.isPointGiven,
        "relapseTrackerId":$scope.relapseTrackerId
      }).then(function(entity) {
        console.log("entity ",entity);
        $scope.testResultId = entity._id;
        if($scope.attempts == 1 && $scope.relapseTrack.startRelapse){
           $scope.$emit('updateRelapseTracker',$scope.testResultId);
        }
        // if ($scope.attempts >= 3) {
        //   $scope.updateNotificationCollection();
        // }
      }).catch(function(error) {
      	$scope.internalGlitch();
        $state.go('app.performanceEvals');
      });
    } else {
      performanceTestResults.save({
        "_id": $scope.testResultId,
        "assistance": arrayAssistance,
        "date": $scope.walkTestData.date,
        "PMType": $scope.walkTestData.PMType,
        "attempts": $scope.attempts,
        "cycle": $scope.cycle,
        "type": $scope.type,
        "recommendForPm": $scope.recommendForPm,
        "prePMSurveyAnswers":$scope.prePMSurveyAnswers,
        "notificationId": $scope.notificationId,
        "isAborted": true,
        "isShown": false,
        "isPointGiven":$scope.isPointGiven,
        "relapseTrackerId":$scope.relapseTrackerId
      }).then(function(entity) {
        console.log("entity ", entity);
        $scope.testResultId = entity._id;
        // if ($scope.attempts >= 3) {
        //   $scope.updateNotificationCollection();
        // }
      }).catch(function(error) {
      	$scope.internalGlitch();
        $state.go('app.performanceEvals');
      });
    }
  }

  $scope.goToTestList = function() {
    if (typeof window.ga !== "undefined") {
      window.ga.trackEvent('Timed 25ft Walk Screen','Performance Metric Screen (Timed 25ft Walk)', 'Return', 1);
    }
    $state.go('app.performanceEvals');
  }

  $scope.showAssistance = function(assistance, formData) {
    $scope.walkTestData.selectAssistance = assistance;
    formData.$submitted = false;
    if (assistance == true) {
      $scope.assistanceSelect = true;
      $scope.yes = 'button-orange';
      $scope.no = 'button-lightGray button-outline';
    } else if (assistance == false) {
      $scope.walkTestData.assistance = "";
      $scope.assistanceSelect = false;
      $scope.no = 'button-orange';
      $scope.yes = 'button-lightGray button-outline';
    }
  }

  $scope.startTimerWithTimeout = function() {
    timePerNow1 = performance.now();
    t = $interval(callAtInterval, 10);
    $scope.reset = false;
    $scope.logbtn = true;
    $scope.log = 0;
  };

  function callAtInterval() {
    $scope.timerWithTimeout++;
    if ($scope.timerWithTimeout == 18000) {
      $scope.startontap = false;
      $scope.stopTimerWithTimeout();
    }
  }

  $scope.resetTimerWithTimeout = function() {
    var take = 0;
    if (arguments.length > 0) {
        take = arguments[0];

        if(take == 1){
          $scope.raw = [];
          $scope.rawData = {};
          $scope.rawPauseTimeFirstCheck = 0;
          $scope.rawStartTime = 0;
          $scope.rawPauseTime = 0;
        }else if(take == 2){
        var tempRaw = [];
        tempRaw = JSON.parse(angular.toJson($scope.raw));
        var runLoop = true;
        var whileLoopIndex = 0;
        while(runLoop){
          if(whileLoopIndex > tempRaw.length - 1){
            runLoop = false;
            break;
          }
          if(tempRaw[whileLoopIndex].round == take){
            tempRaw.splice(whileLoopIndex,1);
          }else{
            whileLoopIndex++;
          }
        }
        $scope.raw = JSON.parse(angular.toJson(tempRaw));
        $scope.rawData = {};
        $scope.rawPauseTimeFirstCheck = 0;
        $scope.rawStartTime = 0;
        $scope.rawPauseTime = 0;
      }

      console.log($scope.raw);
    }

    $scope.startontap = true;
    $scope.log = 0;
    $scope.reset = true;
    $scope.logbtn = true;
    $scope.timerWithTimeout = 0;
    $interval.cancel(t);
    t = undefined;
  }

  $scope.retake1 = function() {
    $scope.testCounter = 1;
    $scope.showWalkTest = true;
    $scope.log1 = 0;
    $scope.log2 = 0;
    $scope.reset = true;
    $scope.logbtn = true;
    $scope.timerWithTimeout = 0;
    $scope.startontap = true;
    $scope.raw = [];
    $scope.rawData = {};
    $scope.rawPauseTimeFirstCheck = 0;
    $scope.rawStartTime = 0;
    $scope.rawPauseTime = 0;
    $interval.cancel(t);
  }

  $scope.stopTimerWithTimeout = function() {
    timePerNow2 = performance.now();
    $interval.cancel(t);
    $scope.logbtn = false;
    if ($scope.timerWithTimeout == 18000) {
      $scope.logTimerWithTimeout();
    }
  }

  $scope.logTimerWithTimeout = function(cancel) {
    $ionicScrollDelegate.scrollTop();
    $scope.rawPauseTimeFirstCheck = 0;
    console.log(timePerNow = timePerNow2 - timePerNow1);
    console.log($scope.timerWithTimeout);
    $interval.cancel(t);
    $scope.logbtn = false;
    cancel == true ? "" : $scope.showWalkTest = false;
    $scope.walkTestData.attempts = $scope.attempts;
    if ($scope.testCounter == 2 || cancel) {
      $scope.showLoader();
      $scope.testCounter++;

      for(var k = 0; k < $scope.raw.length; k++){
        if($scope.raw[k].round == 1){
          // if($scope.raw[k].type == "walk"){
            $scope.totalWalkR1 = $scope.totalWalkR1 + $scope.raw[k].time;
          // } else
          if($scope.raw[k].type == "pause"){
            $scope.totalPauseR1 = $scope.totalPauseR1 + $scope.raw[k].time;
          }
        }else if($scope.raw[k].round == 2){
          // if($scope.raw[k].type == "walk"){
            $scope.totalWalkR2 = $scope.totalWalkR2 + $scope.raw[k].time;
          // } else
          if($scope.raw[k].type == "pause"){
            $scope.totalPauseR2 = $scope.totalPauseR2 + $scope.raw[k].time;
          }
        }
      }

      $scope.log2 = $scope.timerWithTimeout;
      $scope.log = $scope.timerWithTimeout;
      $scope.Results.second = parseInt($scope.timerWithTimeout);
      console.log($scope.log1, $scope.log2);
      $scope.walkTestData.score2 = timePerNow2 - timePerNow1;
      // $scope.logResult = parseInt(($scope.Results.first + $scope.Results.second) / 2);
      $scope.logResult = parseInt((($scope.totalWalkR1 + $scope.totalWalkR2) - ($scope.totalPauseR1 + $scope.totalPauseR2)) / 2);
      $scope.logResult = parseInt($scope.logResult / 10);

      var arrayAssistance = ""
      if ($scope.walkTestData.assistance) {
        arrayAssistance = JSON.parse(angular.toJson($scope.walkTestData.assistance));
      } else {
        arrayAssistance = "";
      }
      var addpoints = false;
      if($scope.isPointGiven == false && cancel != true) {
        $scope.isPointGiven = true; addpoints = true;
      }
      // if ($scope.attempts == 3) {
      //   $scope.retakeCancelBtnDisabled = true;
      // }


      var kinveObj = {
        "_id": $scope.testResultId,
        "date": $scope.walkTestData.date,
        "PMType": $scope.walkTestData.PMType,
        "assistance": arrayAssistance,
        // "score1": $scope.walkTestData.score1,
        // "score2": $scope.walkTestData.score2,
        "score1": $scope.totalWalkR1,
        "score2": $scope.totalWalkR2,
        "score1PauseTime":$scope.totalPauseR1,
        "score2PauseTime":$scope.totalPauseR2,
        "average": $scope.logResult,
        "cycle": $scope.cycle,
        "type": $scope.type,
        "raw":$scope.raw,
        "attempts": $scope.attempts,
        "recommendForPm": $scope.recommendForPm,
        "prePMSurveyAnswers":$scope.prePMSurveyAnswers,
        "isAborted": cancel == undefined ? false : cancel,
        "isShown": false,
        "notificationId": $scope.notificationId,
        "isPointGiven":$scope.isPointGiven,
        "relapseTrackerId":$scope.relapseTrackerId
      }
      performanceTestResults.save(kinveObj).then(function(entity) {
        console.log("entity " + entity);
        $scope.testResultId = entity._id;
        if (cancel == true) {
          $state.go('app.performanceEvals');
        } else {
          if (addpoints) {
            $scope.$emit('saveGamificationPoints', CONSTANT.gamificationPoints.pmEvaluation.value, CONSTANT.gamificationPoints.pmEvaluation.name);
          }
        }
        $scope.hideLoader();
      }).catch(function(error) {
        localStorage.setItem("unSavedPMData", JSON.stringify(kinveObj));
        localStorage.setItem("unSavedPMPoints", addpoints);
        $scope.internalGlitch();
      });
    } else {
      $scope.log = parseInt($scope.timerWithTimeout / 100);
      $scope.log1 = parseInt($scope.timerWithTimeout / 100);
      $scope.Results.first = parseInt($scope.timerWithTimeout);
      $scope.walkTestData.score1 = timePerNow2 - timePerNow1;
      $scope.testCounter = 2;
      $scope.showWalkTest = true;
      $scope.log = 0;
      $scope.reset = true;
      $scope.logbtn = true;
      $scope.timerWithTimeout = 0;
      $scope.startontap = true;
    }
    console.log($scope.walkTestData);
  }

  $scope.updateNotificationCollection = function() {
    var tempdata = [];
    var col = "_id";
    var relatedJson = $scope.notificationId;
    var query = new $kinvey.Query();
    query.equalTo(col, relatedJson);
    notificationTestList.find(query).subscribe(function(data) {
      tempdata = data;
    }, function(error) {}, function(data) {
      tempdata[0].isAck = true;
      notificationTestList.update(tempdata[0]).then(function(data) {
        console.log(data);
      }).catch(function(error) {
        $scope.internalGlitch();
        console.log(error);
      });
    });
  }

  $scope.showSelectValue = function(assistance) {
    console.log(assistance);
    $scope.walkTestData.assistance = assistance;
  }

  $scope.reloadState = function() {
    //$ionicScrollDelegate.resize();
    $scope.testCounter = 1;
    $scope.showSideMenu = false;
    $scope.showWalkTest = true;
    $scope.started = true;
    $scope.log1 = 0;
    $scope.log2 = 0;
    $scope.reset = true;
    $scope.logbtn = true;
    $scope.timerWithTimeout = 0;
    $scope.startontap = true;
    $scope.showSideMenu = true;
    $scope.walkTestData = {};
    $scope.walkTestData.PMType = "Timed 25ft Walk";
    $scope.walkTestData.date = new Date();
    $scope.walkTestData.assistance = "";
    $scope.assistanceSelect = false;
    $scope.raw = [];
    $scope.rawPauseTimeFirstCheck = 0;
    $scope.rawData = {};
    $scope.rawStartTime = 0;
    $scope.rawPauseTime = 0;
    $scope.totalWalkR1 = 0;
    $scope.totalWalkR2 = 0;
    $scope.totalPauseR1 = 0;
    $scope.totalPauseR2 = 0;
    $scope.yes = 'button-lightGray button-outline';
    $scope.no = 'button-lightGray button-outline';
    /*var template = '<a class="popClose" ng-click="retakeTestPopup.close()"></a><p>If problems occurred while taking this test, you can retake it. However this test is given to gain an accurate measurement of your functioning, so try to avoid excessive retakes. Only your latest test will be recorded in your results.</p>';
    if ($scope.attempts == 2) {
      var title = "Are you sure you want to restart the test? You have 1 test remaining.";
      $scope.retakeTestPopup = $ionicPopup.show({
        title: title,
        template: template,
        scope: $scope,
        buttons: [{
          text: 'CONTINUE',
          type: 'button-block button-darkPurple font40',
          onTap: function() {
            return true;
          }
        }]
      });
    } else {
      var title = "Are you sure you want to restart the test? You have 2 tests remaining.";
      $scope.retakeTestPopup = $ionicPopup.show({
        title: title,
        template: template,
        scope: $scope,
        buttons: [{
          text: 'CANCEL',
          type: 'button-block button-outline button-darkPurple skip font40',
          onTap: function() {
            return false;
          }
        }, {
          text: 'RETAKE TEST',
          type: 'button-block button-darkPurple font40',
          onTap: function() {
            return true;
          }
        }]
      });
    }
    closePopupService.register($scope.retakeTestPopup);
    $scope.retakeTestPopup.then(function(res) {
      if (res) {
        $scope.testCounter = 1;
        $scope.showSideMenu = false;
        $scope.showWalkTest = true;
        $scope.started = true;
        $scope.log1 = 0;
        $scope.log2 = 0;
        $scope.reset = true;
        $scope.logbtn = true;
        $scope.timerWithTimeout = 0;
        $scope.startontap = true;
        $scope.showSideMenu = true;
        $scope.walkTestData = {};
        $scope.walkTestData.PMType = "Timed 25ft Walk";
        $scope.walkTestData.date = new Date();
        $scope.assistanceSelect = false;
        $scope.yes = 'button-lightGray button-outline';
        $scope.no = 'button-lightGray button-outline';
      }
    });*/
  }
  $scope.cancelBtn = function() {
    // if ($scope.attempts == 2) {
    //   $scope.cancelTemplate = '<p style="text-align: center;">If you cancel the test, it will count as one of your test takes. You will only have one additional chance to retake the test.</p><a class="popClose" ng-click="cancelTestPopup.close()"></a>';
    // } else if ($scope.attempts == 1) {
    //   $scope.cancelTemplate = '<p style="text-align: center;">If you cancel the test, it will count as one of your test takes. You will only have two additional chance to retake the test.</p><a class="popClose" ng-click="cancelTestPopup.close()"></a>';
    // }
    $scope.cancelTemplate = '<p style="text-align: center;">If you cancel the test, it will count as one of your test takes.</p><p  class="text-center">Please do not retake the test to practice or improve your score. This option is only available if something went wrong during the test.</p><a class="popClose" ng-click="cancelTestPopup.close()"></a>';
    $scope.taptostart();
    $scope.cancelTestPopup = $ionicPopup.show({
      title: "Are you sure you want to cancel the test?",
      template: $scope.cancelTemplate,
      scope: $scope,
      buttons: [{
        text: 'YES, CANCEL',
        type: 'button-block button-outline button-darkPurple skip',
        onTap: function() {
          return true;
        }
      }, {
        text: 'RETURN TO<br/> THE TEST',
        type: 'button-block button-darkPurple',
        onTap: function() {
          return false;
        }
      }]
    });
    closePopupService.register($scope.cancelTestPopup);
    $scope.cancelTestPopup.then(function(res) {
      if (res) {
        //$scope.attempts > 3 ?  $state.go('app.performanceEvals'): "";
        //$scope.logTimerWithTimeout(true);
        $state.go('app.performanceEvals');
      } else {
        $scope.taptostart();
      }
    });
  }
}]);



// Resulted JSON :

// {
//   "PMType": "Timed 25ft Walk",
//   "date": "2016-06-17T11:09:10.442Z",
//   "assistance": "Supported by another person",
//   "score1": 5367.125000000002,
// 	 "score2":5366.27832,
//   "attempts": 1,
//   "raw":[{"score1":5367.125000,"score2":5366.27832,"complete":cancel}]
// }
