neuroScienceApp.controller('fingerTapCtrl',['$scope','$state','$interval', '$ionicScrollDelegate', '$ionicPopup', '$kinvey','CONSTANT','$timeout','$q','$stateParams','closePopupService', function($scope,$state,$interval,$ionicScrollDelegate,$ionicPopup,$kinvey,CONSTANT,$timeout,$q,$stateParams,closePopupService) {

	$timeout(function(){
    window.ga.trackView('Finger Tap Test');
  }, 1000);

	$scope.getInitialState = function(){
		$scope.testDisplayFingerTap = false;
		$scope.startTestDetails = true;
		$scope.Result = [];
		$scope.pauseTest = false;
		$scope.roundInfo = {
			sno:1,
			dominant:'',
			nonDominant:'',
			currentHand:'Dominant hand',
		};
		$scope.stage = 0;
		$scope.totalCount = 1;
		$scope.timeCount = 3;
		$scope.isTapping = false;
		$scope.score = {};
		if($stateParams.test != null){
			$scope.notificationId = $stateParams.test.notificationId;
			$scope.attempts = $stateParams.test.attempts;
			$scope.testResultId = $stateParams.test.testResultId;
			$scope.cycle = $stateParams.test.cycle;
			$scope.type = $stateParams.test.type;
			$scope.isPointGiven = $stateParams.test.isPointGiven;
			$scope.isTrailTaken = $stateParams.test.isTrailTaken;
			$stateParams.test.relapseTrackerId == undefined ? $scope.relapseTrackerId = "" : $scope.relapseTrackerId = $stateParams.test.relapseTrackerId;
			delete $stateParams.test;
		}
		$scope.retakeCancelBtnDisabled = false;
		$scope.timeIntervalStart="";
		$scope.testTrail = "";
		$scope.startGame=false;
		$scope.strongHand = "Right hand";
		$scope.weekHand = "Left hand";
		$scope.numberofRound = 3;
	}

	$scope.findDormantHand = function(){
		var promise = new $q(function(resolve) {
      resolve($kinvey.User.getActiveUser());
    });
    promise.then(function(user) {
      if (user) {
        return user.me();
      }
    }).then(function(user) {
      if (user) {
        console.log(user);
        if(user.data.info.personalInformation.preferHand == "left"){
        	$scope.strongHand = "Left hand";
					$scope.weekHand = "Right hand";
        }
      }
    }).catch(function(error) {
      $scope.internalGlitch();
      console.log(error);
    });
	}

	$scope.$on('$ionicView.beforeEnter',function(){
		$scope.getInitialState();
		$scope.recommendationPopup();
		$scope.findDormantHand();
	});

	$scope.$on('$ionicView.afterEnter',function(){
		$scope.lockOrientation();
	});

	$scope.$on('$destroy',function(){
		$scope.unlockOrientation();
	});

	$scope.relapse = {
		relapseInfection1: null,
		relapseInfection2: null,
		relapseInfection3: null,
		relapseInfection4: null,
	};

	$scope.prePMSurveyAnswers = [
		{"question":"Did you take your medications as usual?", "answer":null},
		{"question":"Did you get a typical amount of sleep last night?", "answer":null},
		{"question":"Do you have 6-8 minutes in a low distraction environment to focus on today's tests?", "answer":null},
		{"question":"Do you feel any of these factors could impact your performance test today?", "answer":null}
	];

	$scope.recommendationPopup = function() {
		$scope.recommendForPm = '';
		var button = '<div class="popBodyInput orange"><div class="item item-input item-borderless"><div class="radioButtonGroup"><label class="questionLabel ">Did you take your medications as usual?</label>' +

			'<div class="radioButtonWrapper yes"><input class="radioButton" type="radio" name="preRelapse1" ng-value=true ng-model="relapse.relapseInfection1"><label class="radioLabel">  Yes </label></div>' +

			'<div class="radioButtonWrapper no"><input class="radioButton" type="radio" name="preRelapse1" ng-value=false ng-model="relapse.relapseInfection1"><label class="radioLabel">  No </label></div></div></div>' +

			'<div class="item item-input item-borderless"><div class="radioButtonGroup"><label class="questionLabel ">Did you get a typical amount of sleep last night?</label>' +

			'<div class="radioButtonWrapper yes"><input class="radioButton" type="radio" name="preRelapse2" ng-value=true ng-model="relapse.relapseInfection2"><label class="radioLabel">  Yes </label></div>' +

			'<div class="radioButtonWrapper no"><input class="radioButton" type="radio" name="preRelapse2" ng-value=false ng-model="relapse.relapseInfection2"><label class="radioLabel">  No </label></div></div></div>' +

			'<div class="item item-input item-borderless"><div class="radioButtonGroup"><label class="questionLabel ">Do you have 6-8 minutes in a low distraction environment to focus on today' + "'s " + 'tests?</label>' +

			'<div class="radioButtonWrapper yes"><input class="radioButton" type="radio" name="preRelapse3" ng-value=true ng-model="relapse.relapseInfection3"><label class="radioLabel">  Yes </label></div>' +

			'<div class="radioButtonWrapper no"><input class="radioButton" type="radio" name="preRelapse3" ng-value=false ng-model="relapse.relapseInfection3"><label class="radioLabel">  No </label></div></div></div>' +

			'<div class="item item-input item-borderless"><div class="radioButtonGroup"><label class="questionLabel ">Do you feel any of these factors could impact your performance test today?</label>' +

			'<div class="radioButtonWrapper yes"><input class="radioButton" type="radio" name="preRelapse4" ng-value=true ng-model="relapse.relapseInfection4"><label class="radioLabel">  Yes </label></div>' +

			'<div class="radioButtonWrapper no"><input class="radioButton" type="radio" name="preRelapse4" ng-value=false ng-model="relapse.relapseInfection4"><label class="radioLabel">  No </label></div></div></div></div>' +

			'<div ng-show="relapse.relapseInfection1!=null && relapse.relapseInfection2!=null && relapse.relapseInfection3!=null && relapse.relapseInfection4!=null"><div ng-show="relapse.relapseInfection1==true && relapse.relapseInfection2==true && relapse.relapseInfection3==true && relapse.relapseInfection4==false"><div class="height20"></div><div class="popup-buttons" ><button class="button button-block button-darkPurple font40" ng-click="takeTestNow(true)">GREAT! LET' + "'" + 'S START TODAY' + "'" + 'S TASKS.</button></div><div class="height20"></div>' +

			'</div>' +

			'<div ng-show="!(relapse.relapseInfection1==true && relapse.relapseInfection2==true && relapse.relapseInfection3==true && relapse.relapseInfection4==false)"><p class="text-center italicFont smallText">You'+"'ve"+' indicated that some factors may affect your performance on today' + "'" + 's tasks.</p><div class="popup-buttons" ><button class="button button-block button-outline button-darkPurple font40 skip" ng-click="recommendPopup.close()">TAKE TEST <br/>LATER</button>' +

			'<div class="height10"></div><button class="button button-block button-multiline button-darkPurple font40" ng-click="takeTestNow(false)">I' + "'" + 'D RATHER TAKE <br/>THE TEST NOW</button></div></div></div></div>';

		var template = '<p class="text-center">Performance can be influenced by many factors, including health, medical treatments, and life events, even having a ' + '"bad day".' +'</p><a class="popClose" ng-click="recommendPopup.close()"></a><p class="text-center">We want you to do the best you can on the performance measures.  So tell us if today is a typical day for you by answering the questions below.</p>' + button;
		$timeout(function() {
			$scope.recommendPopup = $ionicPopup.show({
				title: "Performance Metric Testing",
				template: template,
				scope: $scope,
				cssClass: "pmStartPop"
			});
			closePopupService.register($scope.recommendPopup);
			$scope.recommendPopup.then(function(res) {
				if (!$scope.takeTestNowPopupClose) {
					$state.go('app.performanceEvals');
				}
			});
		}, 100);
	}

	$scope.takeTestNow = function(status) {
		// console.log($scope.relapse);
		// console.log(status);
		if (status) {
			$scope.recommendForPm = "yes";
		} else {
			$scope.recommendForPm = "no";
		}
		$scope.recommendPopup.close();
		$scope.takeTestNowPopupClose = true;
		for(var i = 0; i < $scope.prePMSurveyAnswers.length; i++){
			$scope.prePMSurveyAnswers[i].answer = "no";
			if ($scope.relapse["relapseInfection"+(i+1)] == true){
				$scope.prePMSurveyAnswers[i].answer = "yes";
			}
		}
		// console.log($scope.recommendForPm);
	}

	$scope.testName = "Finger Tapping";
	var countdown,t,t1,fingerTap;

	$scope.retakeTest = function () {
		if($scope.attempts == 3){
			$state.go('app.performanceEvals');
			return;
		}
		var template = '<a class="popClose" ng-click="retakeTestPopup.close()"></a><p class="text-center">If problems occurred while taking this test, you can retake it. However, this test is given to gain an accurate measurement of normal functioning, so please don’t re-take it just to improve your score or to practice.</p>';
		if($scope.attempts == 2){
			var title = "Are you sure you want to restart the test? You have 1 test remaining.";
			var buttons = [{text: 'CONTINUE', type: 'button-block button-darkPurple', onTap: function() {return true; } }];
		}
		else{
			var title = "Are you sure you want to restart the test? You have 2 tests remaining.";
			var buttons = [{text: 'CANCEL', type: 'button-block button-outline button-darkPurple skip', onTap: function() {return false; } },{text: 'RETAKE TEST', type: 'button-block button-darkPurple', onTap: function() {return true; } }];
		}
		$scope.retakeTestPopup = $ionicPopup.show({
      title:title,
      template:template,
      scope: $scope,
      buttons: buttons
  	});
  	closePopupService.register($scope.retakeTestPopup);
  	$scope.retakeTestPopup.then(function(res){
    	if(res){
				$scope.resetFingerTap();
    	}
 	 	});
	};

	$scope.cancelBtn = function() {
	  $scope.cancelTemplate = "";
	  if ($scope.testTrail == "test") {
	    if ($scope.attempts == 2) {
	      $scope.cancelTemplate = '<p style="text-align: center;">If you cancel the test, it will count as one of your test takes. You will only have one additional chance to retake the test. </p><p  class="text-center">A test should be cancelled only if something went wrong; please don’t re-take the test just to improve your score, or to practice.</p><a class="popClose" ng-click="cancelTestPopup.close()"></a>';
	    } else if ($scope.attempts == 1) {
	      $scope.cancelTemplate = '<p style="text-align: center;">If you cancel the test, it will count as one of your test takes. You will only have two additional chances to retake the test. </p><p  class="text-center">A test should be cancelled only if something went wrong; please don’t re-take the test just to improve your score, or to practice.</p><a class="popClose" ng-click="cancelTestPopup.close()"></a>';
	    }
	  } else {
	  	$interval.cancel($scope.timeIntervalStart);
			$scope.timeIntervalStart = undefined;
	  	fingerTap.destroy();
	  	$scope.resetFingerTap();
	    $scope.getInitialState();
	    return;
	  }
	  if($scope.startGame){
	  	$scope.pauseTestTrail();
	  }
	  $scope.cancelTestPopup = $ionicPopup.show({
	    title: "Are you sure you want to cancel the test?",
	    template: $scope.cancelTemplate,
	    scope: $scope,
	    buttons: [{
	      text: 'YES, CANCEL',
	      type: 'button-block button-outline button-darkPurple skip',
	      onTap: function() {
	        return true;
	      }
	    }, {
	      text: 'RETURN TO<br>THE TEST',
	      type: 'button-block button-darkPurple',
	      onTap: function() {
	        return false;
	      }
	    }]
	  });
	  closePopupService.register($scope.cancelTestPopup);
	  $scope.cancelTestPopup.then(function(res) {
	    if (res) {
	    	if (typeof window.ga !== "undefined") {
          window.ga.trackEvent('Finger Tapping Screen - Cancel Popup', 'Performance Metric Screen (Finger Tapping)', 'Yes, Cancel ', 1);
        }
	      //$scope.updateTestResultScreen(true);
	      $state.go('app.performanceEvals');
	    } else {
	    	if (typeof window.ga !== "undefined") {
          window.ga.trackEvent('Finger Tapping Screen - Cancel Popup', 'Cancel Pop Up', 'Return to the Test', 1);
        }
	    	if($scope.startGame){
	  			$scope.resumeTestTrail();
	  		}
	    }
	  });
	};


	function initiate() {
		var performanceTestResults = $kinvey.DataStore.getInstance('performanceTestResults', $kinvey.DataStoreType.Network);
		var notificationTestList = $kinvey.DataStore.getInstance('notifications', $kinvey.DataStoreType.Network);
		var jsonData = {};
		var scoreRaw = {"type": $scope.strongHand,"taps": 0};
		jsonData.PMType = $scope.testName;
		jsonData.date = new Date();
		jsonData.attempts = $scope.attempts;
		jsonData.score = {"ADH": 0,"ANDH": 0};
		jsonData.raw = [];
  	var clicks = 0;
  	$scope.tapped = 0;
  	var isTest = "";
  	var begin = false;
  	$scope.showTestButton = false;

		$scope.startTest = function (bol) {
			$scope.showLoader();
			$ionicScrollDelegate.scrollTop();
			isTest = bol;
			$scope.tapped = 0;
			$scope.testDisplayFingerTap = true;
			circleDraw('fingerTapDiv',bol);
			$scope.roundInfo.currentHand = 	$scope.strongHand;
			if(bol) {
				if (typeof window.ga !== "undefined") {
          window.ga.trackEvent('Finger Tapping Screen', 'Test starts', 'Get Started', 1);
        }
				$scope.attempts++;
				$scope.timeCount = 10;
				$scope.testStartedKinvey();
				if($scope.attempts == 3){
		      $scope.retakeCancelBtnDisabled = true;
		    }
		    $scope.testTrail = "test";
			}
			else{
				if (typeof window.ga !== "undefined") {
          window.ga.trackEvent('Finger Tapping Screen', 'Trail starts', 'Take A Trail', 1);
        }
				$scope.testTrail = "demo";
				if($scope.attempts == 0 && !$scope.isTrailTaken){
					$scope.saveInitialTrailData();
				}
			}
		};

		$scope.saveInitialTrailData = function(){
			$scope.showLoader();
			if($scope.testResultId == ""){
				performanceTestResults.save({
	        "date": new Date(),
	        "PMType":jsonData.PMType,
	        "attempts":0,
	        "cycle":$scope.cycle,
	        "type":$scope.type,
					"prePMSurveyAnswers":$scope.prePMSurveyAnswers,
	        "notificationId":$scope.notificationId,
	        "isTrial":false
		    }).then(function(entity) {
	        console.log("entity ",entity);
	        $scope.isTrailTaken = false;
	        $scope.testResultId  = entity._id;
	        $scope.hideLoader();
		    }).catch(function(error) {
		      $scope.internalGlitch();
	      	$state.go('app.performanceEvals');
		    });
		  }else{
		  	performanceTestResults.save({
		  		"_id":$scope.testResultId,
	        "date": new Date(),
	        "PMType":jsonData.PMType,
	        "attempts":0,
	        "cycle":$scope.cycle,
	        "type":$scope.type,
					"prePMSurveyAnswers":$scope.prePMSurveyAnswers,
	        "notificationId":$scope.notificationId,
	        "isTrial":false
		    }).then(function(entity) {
	        console.log("entity ",entity);
	        $scope.isTrailTaken = false;
	        $scope.testResultId  = entity._id;
	        $scope.hideLoader();
		    }).catch(function(error) {
		      $scope.internalGlitch();
	      	$state.go('app.performanceEvals');
		    });
		  }
		};

		$scope.saveFinalTrailData = function(){
			$scope.showLoader();
			performanceTestResults.save({
				"_id":$scope.testResultId,
        "date": new Date(),
        "PMType":jsonData.PMType,
        "attempts":0,
        "cycle":$scope.cycle,
        "type":$scope.type,
				"prePMSurveyAnswers":$scope.prePMSurveyAnswers,
        "notificationId":$scope.notificationId,
        "isTrial":true
	    }).then(function(entity) {
        console.log("entity ",entity);
        $scope.isTrailTaken = true;
        $scope.testResultId  = entity._id;
        $scope.hideLoader();
	    }).catch(function(error) {
	      $scope.internalGlitch();
      	$state.go('app.performanceEvals');
	    });
		};

		$scope.testStartedKinvey = function(){
	    if($scope.attempts==1){
		    $scope.setPMNotificationIsShown($scope.notificationId);
		     if($scope.relapseTrack.startRelapse){
		      $scope.relapseTrackerId = $scope.relapseTrack.kinveyData._id;
		    }
		  }
			if($scope.testResultId == ""){
				performanceTestResults.save({
	        "date": new Date(),
	        "PMType":jsonData.PMType,
	        "attempts":$scope.attempts,
	        "cycle":$scope.cycle,
	        "type":$scope.type,
					"prePMSurveyAnswers":$scope.prePMSurveyAnswers,
	        "recommendForPm":$scope.recommendForPm,
	        "notificationId":$scope.notificationId,
	        "isAborted": true,
	        "isShown":$scope.attempts==3 ? true : false,
	        "isPointGiven":$scope.isPointGiven,
	        "isTrial":$scope.isTrailTaken,
	        "relapseTrackerId":$scope.relapseTrackerId
		    }).then(function(entity) {
	        console.log("entity ",entity);
	        $scope.testResultId  = entity._id;
	        $scope.hideLoader();
	        if($scope.attempts == 1 && $scope.relapseTrack.startRelapse){
	           $scope.$emit('updateRelapseTracker',$scope.testResultId);
	        }
	        if($scope.attempts>=3){
	        	$scope.updateNotificationCollection()
	        }
		    }).catch(function(error) {
		      $scope.internalGlitch();
        	$state.go('app.performanceEvals');
		    });
			}
			else{
				performanceTestResults.save({
					"_id":$scope.testResultId,
					"date": new Date(),
			    "PMType":jsonData.PMType,
	        "attempts":$scope.attempts,
	        "cycle":$scope.cycle,
	        "type":$scope.type,
	        "recommendForPm":$scope.recommendForPm,
					"prePMSurveyAnswers":$scope.prePMSurveyAnswers,
	        "notificationId":$scope.notificationId,
	        "isAborted": true,
	        "isShown":$scope.attempts==3 ? true : false,
	        "isPointGiven":$scope.isPointGiven,
	        "isTrial":$scope.isTrailTaken,
	        "relapseTrackerId":$scope.relapseTrackerId
		    }).then(function(entity) {
	        console.log("entity ",entity);
	        $scope.testResultId  = entity._id;
	        $scope.hideLoader();
	        if($scope.attempts>=3){
	        	$scope.updateNotificationCollection();
	        }
		    }).catch(function(error) {
		      $scope.internalGlitch();
        	$state.go('app.performanceEvals');
		    });
			}
		}

		$scope.beginTestFromTail = function(bol){
			$ionicScrollDelegate.scrollTop();
			$scope.distroyPhaser();
			$scope.resetFingerTap();
			$scope.startTest(true);
		}

		$scope.resetFingerTap = function(){
			$scope.stage = 0;
			$scope.timeCount = 3;
			isTest = true;
			$scope.pauseTest = false;
			$scope.testDisplayFingerTap = false;
			$scope.startTestDetails = true;
			_tapInsideText.visible = true;
			clicks = 0;
			$scope.totalCount = 1;
			jsonData = {};
			scoreRaw = {"type": $scope.strongHand,"taps": 0};
			jsonData.PMType = $scope.testName;
			jsonData.date = new Date();
			jsonData.attempts = $scope.attempts;
			jsonData.score = {"ADH": 0,"ANDH": 0};
			jsonData.raw = [];
	  	$scope.isTapping = false;
	  	$scope.score = {};
  		begin = false;
  		$scope.showTestButton = false;
		}

		$scope.distroyPhaser = function(){
			fingerTap.destroy();
		}

		function endTrail() {
			begin = false;
			$interval.cancel($scope.timeIntervalStart);
			$scope.timeIntervalStart = undefined;
			begin = false;
			if(clicks>0){
				_useEitherHandText.visible = false;
				_congoText.visible = true;
				$scope.stage++;
				$scope.showTestButton = true;
				if($scope.attempts == 0 && !$scope.isTrailTaken){
					$scope.saveFinalTrailData();
				}
			}
			else{
				$scope.resetFingerTap();
				$scope.showMessage('Please read instructions and try again.');
				$scope.distroyPhaser();
			}
			console.log("stage = " + $scope.stage);
		}

		function callATimmer(){
			$scope.timeCount--;
		}

		function endTest() {
			begin = false;
			$interval.cancel($scope.timeIntervalStart);
			$scope.timeIntervalStart = undefined;
			if($scope.totalCount % 2 == 1){
				scoreRaw = {"type": "Dominant hand","taps": clicks};
				jsonData.score.ADH += clicks;
				_roundText1.setText("Round " + ($scope.stage +1)+" of "+$scope.numberofRound+" - ");
				_roundText1.x = roundXplacemnt
				_roundText2.setText($scope.weekHand);
				_roundText2.x = dominantXPlacement;
			}
			else{
				scoreRaw = {"type": "Non-Dominant hand","taps": clicks};
				jsonData.score.ANDH += clicks;
				$scope.stage++;
				_roundText1.setText("Round " + ($scope.stage +1) +" of "+$scope.numberofRound+" - ");
				_roundText1.x = roundXplacemnt
				_roundText2.setText($scope.strongHand);
				_roundText2.x = nonDominantXPlacement;
				console.log("stage = " + $scope.stage);
			}
			jsonData.raw.push(scoreRaw);
			$scope.totalCount++;
			clicks = 0;
			if($scope.stage == $scope.numberofRound) {
				$scope.updateTestResultScreen();
			}
			else{ $scope.pauseTest = true;}
		}

		$scope.startPauseTest = function(){
			$scope.timeCount = 10;
			_tapInsideText.visible = true;
			begin = false; $scope.isTapping = false;
			$interval.cancel($scope.timeIntervalStart);
			$scope.timeIntervalStart = undefined;
			click=0;
			$timeout(function(){
				$scope.pauseTest = false;
				fingerTap.input.enabled= true;
			}, 200);
		}

		$scope.pauseTestTrail = function () {
			fingerTap.paused = true;
			$interval.cancel($scope.timeIntervalStart);
			$scope.timeIntervalStart = undefined;
		}

		$scope.resumeTestTrail = function () {
			fingerTap.paused = false;
			$scope.timeIntervalStart = $interval(callATimmer, 1000);
		}

		function circleDraw(tapDiv,test) {
			$scope.showLoader();
			clicks = 0;
			roundXplacemnt = (window.innerWidth*0.128)*3;
			dominantXPlacement = (window.innerWidth*0.530)*3; //(window.innerWidth*0.423)*3;
			nonDominantXPlacement = (window.innerWidth*0.530)*3;
			fontSize = (window.innerWidth*0.045)*3; // 56px
			fontSizeRound = (window.innerWidth*0.060)*3; // 56px
			goFontSize = (window.innerWidth*.144)*3;
			/*var width = ((window.innerWidth * 97)/100)*3;
			var height = ((window.innerWidth*65)/100)*3;*/
			var width = "";
			var height = "";
			if(window.innerWidth < 768){
				width = ((window.innerWidth * 97)/100)*3;
				height = ((window.innerWidth*65)/100)*3;
			}else{
				var width = ((window.innerWidth * 97)/100)*3;
				var height = ((window.innerWidth*72)/100)*3;
			}
			fingerTap = new Phaser.Game(width, height, Phaser.AUTO, tapDiv, { preload: preload, create: create , update:update});

			function preload() {
			    //fingerTap.load.image('small', 'img/tapAreaBg.png');
			    fingerTap.stage.backgroundColor = "#ffffff";
			}

			function create() {
				fingerTap.input.onTap.add(onTap, this);
				if(window.innerWidth < 768){
					_useEitherHandText = fingerTap.add.text(width/2, (height * 0.25), 'You can use either hand for the trial', { font: fontSize+"px Ubuntu-Bold", fill: "#f18903", align: "center" });
					_useEitherHandText.anchor.setTo(0.5, 0.5);
	       	_roundText1 = fingerTap.add.text(roundXplacemnt, (height * 0.25), 'Round '+$scope.roundInfo.sno +" of "+$scope.numberofRound+' - ', { font: fontSizeRound+"px Ubuntu-Bold",fill: "#f18903"});
	       	// _roundText1.anchor.setTo(0.5);
	       	_roundText2 = fingerTap.add.text(dominantXPlacement, (height * 0.25), $scope.roundInfo.currentHand, { font: fontSizeRound+"px Ubuntu-Bold",fill: "#454545"});
	       	// _roundText2.anchor.setTo(0.5);
					_tapInsideText = fingerTap.add.text((width/2), (height * 0.60), 'Tap inside the dotted area to begin', { font:  fontSize+"px Ubuntu",fill: "#454545", align: "center"});
					_tapInsideText.anchor.setTo(0.5, 0.5);
					_goText = fingerTap.add.text(width/2, (height * 0.60), 'GO!', { font: goFontSize+"px Ubuntu-Bold",fill: "#454545", align: "center"});
					_goText.anchor.setTo(0.5, 0.5);
					_goText.visible = false;
					_congoText = fingerTap.add.text(width/2, (height * 0.60), 'Congratulations! ', { font: fontSize+"px Ubuntu-Bold",fill: "#92c83e", align: "center",fontStyle:"italic"});
					_congoText.anchor.setTo(0.5, 0.5);
					_congoText.visible = false;
				}else{
					_useEitherHandText = fingerTap.add.text(width/2, (height * 0.20), 'You can use either hand for the trial', { font: fontSize+"px Ubuntu-Bold", fill: "#f18903", align: "center" });
					_useEitherHandText.anchor.setTo(0.5, 0.5);
	       	_roundText1 = fingerTap.add.text(roundXplacemnt, (height * 0.20), 'Round '+$scope.roundInfo.sno +" of "+$scope.numberofRound+' - ', { font: fontSizeRound+"px Ubuntu-Bold",fill: "#f18903"});
	       	// _roundText1.anchor.setTo(0.5);
	       	_roundText2 = fingerTap.add.text(dominantXPlacement, (height * 0.20), $scope.roundInfo.currentHand, { font: fontSizeRound+"px Ubuntu-Bold",fill: "#454545"});
	       	// _roundText2.anchor.setTo(0.5);
					_tapInsideText = fingerTap.add.text((width/2), (height * 0.45), 'Tap inside the dotted area to begin', { font:  fontSize+"px Ubuntu",fill: "#454545", align: "center"});
					_tapInsideText.anchor.setTo(0.5, 0.5);
					_goText = fingerTap.add.text(width/2, (height * 0.45), 'GO!', { font: goFontSize+"px Ubuntu-Bold",fill: "#454545", align: "center"});
					_goText.anchor.setTo(0.5, 0.5);
					_goText.visible = false;
					_congoText = fingerTap.add.text(width/2, (height * 0.45), 'Congratulations! ', { font: fontSize+"px Ubuntu-Bold",fill: "#92c83e", align: "center",fontStyle:"italic"});
					_congoText.anchor.setTo(0.5, 0.5);
					_congoText.visible = false;
				}
       	if(isTest){
          _roundText2.visible=true;
          _roundText1.visible = true;
          _useEitherHandText.visible = false;
       	}
       	else{
          _roundText2.visible=false;
          _roundText1.visible = false;
          _useEitherHandText.visible = true;
       	}
        $timeout(function(){
          $scope.hideLoader();
        }, 1000);
       	$scope.stage = 0;
     	}

			function onTap(pointer) {
				if(isTest){
					switch(begin){
						case false: begin = true; $scope.isTapping = true; console.log("tap"); break;
						case true : clicks++;  $timeout(function() {$scope.tapped = clicks;});  console.log(clicks);
					}
				}
				else if(isTest == false){
					switch($scope.stage){
						case 0: begin = true; break;
						case 1: clicks++;$timeout(function() {$scope.tapped = clicks;}); console.log(clicks);break;
					}
				}
			}

			function update(){
			 if(isTest == false){
					if(begin == true && $scope.stage==0 && fingerTap.input.enabled){
						fingerTap.input.enabled= false;
						begin = false;
						_tapInsideText.visible = false;
						_goText.visible = true;
						click=0;
						$interval.cancel($scope.timeIntervalStart);
						$scope.timeIntervalStart = undefined;
						$timeout(function(){
							_goText.visible = false;
							$scope.timeCount = 3;
							$scope.timeIntervalStart = $interval(callATimmer, 1000);
							$scope.stage = 1;
							begin = true;
							$scope.startGame=true;
							if(!fingerTap.input.enabled) fingerTap.input.enabled = true;
						}, 1000);
					}
					else if(begin == true && $scope.stage==1 && ($scope.timeCount <= 0 || clicks > 4) && fingerTap.input.enabled){
						fingerTap.input.enabled= false;
						$scope.startGame=false;
						//$scope.tapped = 0;
						endTrail();
					}
				}
				else if(isTest == true){
					if(begin == true && $scope.isTapping && fingerTap.input.enabled){
						fingerTap.input.enabled= false;
						begin = false;
						$scope.isTapping = false;
						console.log("ready");
						_tapInsideText.visible = false;
						_roundText2.visible = true;
						_roundText1.visible = true;
						_goText.visible = true;
						click=0;
						$interval.cancel($scope.timeIntervalStart);
						$scope.timeIntervalStart = undefined;
						$scope.timeCount = 10;
						$scope.timeIntervalStart = $interval(callATimmer, 1000);
						$scope.startGame=true;
						$timeout(function(){
							_goText.visible = false;
							begin = true;
							if(!fingerTap.input.enabled) fingerTap.input.enabled = true;
						}, 1000);
					}
					else if(begin == true && $scope.timeCount <= 0 && fingerTap.input.enabled){
						console.log("hand change");
						fingerTap.input.enabled= false;
						$scope.startGame=false;
						$scope.tapped = 0;
						endTest();
					}
				}
			}
		};

		$scope.updateTestResultScreen = function(cancel) {
			$scope.showLoader();
			$scope.distroyPhaser();
			jsonData.score.ADH = Math.round(jsonData.score.ADH/$scope.numberofRound);
			jsonData.score.ANDH = Math.round(jsonData.score.ANDH/$scope.numberofRound);
			$scope.score.ADH = jsonData.score.ADH;
			$scope.score.ANDH = jsonData.score.ANDH;
			$scope.fingerTap = true;
			$scope.startTestDetails = false;
			jsonData.attempts = $scope.attempts;
			var addpoints = false;
      if($scope.isPointGiven == false && cancel != true) {
        $scope.isPointGiven = true; addpoints = true;
      }
      if(jsonData.attempts == 3){
     		$scope.retakeCancelBtnDisabled = true;
     		//$scope.updateNotificationCollection();
      }
      var kinveObj = {
				"_id"  : $scope.testResultId,
        "date": jsonData.date,
        "PMType":jsonData.PMType,
        "score1":jsonData.score,
        "attempts":jsonData.attempts,
        "recommendForPm":$scope.recommendForPm,
				"prePMSurveyAnswers":$scope.prePMSurveyAnswers,
        "cycle":$scope.cycle,
        "type":$scope.type,
        "raw":jsonData.raw,
        "isAborted":cancel == undefined ? false : cancel,
        "isShown":jsonData.attempts==3 ? true : false,
        "notificationId":$scope.notificationId,
        "isPointGiven":$scope.isPointGiven,
        "isTrial":$scope.isTrailTaken,
        "relapseTrackerId":$scope.relapseTrackerId
	    }
			performanceTestResults.save(kinveObj).then(function(entity) {
        console.log(entity);
        $scope.testResultId = entity._id;
       	if(cancel == true){
       		$state.go('app.performanceEvals');
       	}
       	else{
       		if(addpoints){
						$scope.$emit('saveGamificationPoints',CONSTANT.gamificationPoints.pmEvaluation.value,CONSTANT.gamificationPoints.pmEvaluation.name);
        	}
       	}
       	$scope.hideLoader();
	    }).catch(function(error) {
	    	localStorage.setItem("unSavedPMData", JSON.stringify(kinveObj));
        localStorage.setItem("unSavedPMPoints", addpoints);
	      $scope.internalGlitch();
	    });
		}

		$scope.updateNotificationCollection = function(){
			var tempdata = [];
			var col = "_id";
			var relatedJson = $scope.notificationId;
			var query = new $kinvey.Query();
	    query.equalTo(col, relatedJson);
			notificationTestList.find(query).subscribe(function(data) {
	    	tempdata = data;
	    }, function(error) {}, function(data) {
	    	tempdata[0].isAck = true;
	    	notificationTestList.update(tempdata[0]).then(function(data) {
	    		console.log(data);
		    }).catch(function(error) {
	        console.log(error);
	      });
	    });
		}
	};

	initiate();

}]);




// {
//   "PMType": "Finger Tapping",
//   "date": "2016-06-29T13:40:15.845Z",
//   "score1": {
//     "ADH": 4,
//     "ANDH": 2
//   },
//   "attempts": 1,
//   "raw": [{"type":"dominant","taps":0},
//     { "type": "nonDominant", "taps": 0 },
//     { "type": "dominant", "taps": 0 },
// 	   {"type": "nonDominant", "taps": 0 }]
// }
