neuroScienceApp.controller('performanceEvaluationCtrl',['$scope','$state','$kinvey','$q','CONSTANT','$timeout', function($scope,$state,$kinvey,$q,CONSTANT,$timeout) {
	$scope.$on('$ionicView.beforeEnter',function(){
		$scope.getTestListforUser();
		$scope.showLoader();
		$scope.PMtypeFilterResults = {};
		$scope.testList={};
		$scope.takenTestList = [];
		$scope.newTestList = [];
		$scope.performanceUser={};
		//$scope.unlockOrientation();
		ionic.Platform.fullScreen();
	});

	$timeout(function(){
    window.ga.trackView('Performance Metrics Landing Screen');
  }, 1000);

  $scope.replaceTrackingOnPM = function(){
    if (typeof window.ga !== "undefined") {
      window.ga.trackEvent('Performance Metrics','Start / Stop Relapse (Performance Metrics)', 'Relapse', 1);
    }
    $scope.replaceTracking();
  }

	$scope.savePMunSavedData();
	$scope.goToTest = function (state,item) {
		if(CONSTANT.appVersion < item.version){
			$scope.appVersionPopup("Please update the app in order to access new features");
			return;
		}	
		console.log($scope.notificationsListData);
		var stateParam = {"notificationId":"","testResultId":"","attempts":0,"attempts2":0,"cycle":"","type":item.type,"isPointGiven":false,"questionLists":"","isTrailTaken":false,"isTrailTaken2":false, "isTrailTaken2Round2":false, "isTrailTaken2Round1":false};
		for(var i=0;i<$scope.notificationsListData.length;i++){
			if(item.type == $scope.notificationsListData[i].notificationAttributes.pms){	
				stateParam.cycle = $scope.notificationsListData[i].notificationAttributes.cycle
				stateParam.notificationId = $scope.notificationsListData[i]._id;
				break;
			}
		}
		if(item.type == "pm_4" && item.questionLists){
			if(stateParam.cycle % 2){ 
				stateParam.questionLists = item.questionLists.odd;
				stateParam.questionListsVersion = "odd";
			}else{ 
				stateParam.questionLists = item.questionLists.even;
				stateParam.questionListsVersion = "even";
			}
		}
		for(var i=0;i<$scope.performanceIncomplete.length;i++){
			if(item.type == "pm_1"){
				if(stateParam.notificationId == $scope.performanceIncomplete[i].notificationId && $scope.performanceIncomplete[i].SubPMType == "Symbol Test"){
					stateParam.testResultId = $scope.performanceIncomplete[i]._id;
					stateParam.attempts = $scope.performanceIncomplete[i].attempts;		
					stateParam.relapseTrackerId = $scope.performanceIncomplete[i].relapseTrackerId;
					$scope.performanceIncomplete[i].isTrial == true ? stateParam.isTrailTaken1 = true:stateParam.isTrailTaken1 = false;
				}
				if(stateParam.notificationId == $scope.performanceIncomplete[i].notificationId && $scope.performanceIncomplete[i].SubPMType == "Trail Test"){
					stateParam.attempts2 = $scope.performanceIncomplete[i].attempts;	
					stateParam.testResultId2 = $scope.performanceIncomplete[i]._id;
					stateParam.isPointGiven = $scope.performanceIncomplete[i].isPointGiven;
					$scope.performanceIncomplete[i].isTrial == true ? stateParam.isTrailTaken2 = true : stateParam.isTrailTaken2 = false;
					$scope.performanceIncomplete[i].isTrial1 == true ? stateParam.isTrailTaken2Round1 = true : stateParam.isTrailTaken2Round1 = false;
					$scope.performanceIncomplete[i].isTrial2 == true ? stateParam.isTrailTaken2Round2 = true : stateParam.isTrailTaken2Round2 = false;
				}
			}
			else{
				if(stateParam.notificationId == $scope.performanceIncomplete[i].notificationId){
					stateParam.testResultId = $scope.performanceIncomplete[i]._id;
					stateParam.attempts = $scope.performanceIncomplete[i].attempts;
					stateParam.isPointGiven = $scope.performanceIncomplete[i].isPointGiven;
					stateParam.relapseTrackerId = $scope.performanceIncomplete[i].relapseTrackerId;
					$scope.performanceIncomplete[i].isTrial == true ? stateParam.isTrailTaken = true:stateParam.isTrailTaken = false;
					break;
				}
			}	
		}
		if(stateParam.attempts > 0 && item.type != "pm_1"){
			stateParam.isTrailTaken = true;
		}else if(item.type == "pm_1"){
			if(stateParam.attempts > 0){
				stateParam.isTrailTaken1 = true;
			}
			/*if(stateParam.attempts2 > 0)
			{
				stateParam.isTrailTaken2 = true;
			}	*/
		}

		$state.go(state,{test: stateParam})
	}

	$scope.getTestListforUser = function(){
		var promise = new $q(function(resolve) {
			resolve($kinvey.User.getActiveUser());
		});
		promise.then(function(user) {
			if (user) {
				return user.me();
			}
			return user;
		}).then(function(user) {
			$scope.performanceUser = user.data;
			$scope.getRelapseEntryData(user._id);
			$scope.checkPMResultFromKinvey();
			// $scope.getDataFromKinvey();
		}).catch(function(error) {
				$scope.internalGlitch();
			  console.log(error);
		});
	}
	
	var performanceTestResults = $kinvey.DataStore.getInstance('performanceTestResults', $kinvey.DataStoreType.Network);
	var performanceTestList = $kinvey.DataStore.getInstance('performanceTestList', $kinvey.DataStoreType.Network);
	var notificationTestList = $kinvey.DataStore.getInstance('notifications', $kinvey.DataStoreType.Network);
	var ackNotificationSetTrue = []; var updateResultPMResult=[];
	$scope.performanceResultsArray = [];
	$scope.performanceTestListData = [];
	$scope.notificationsListData = [];
	$scope.performanceIncomplete=[];
	var notificationList = [];
	var tempData = [];
	var pmPointLength = 0;

	$scope.checkPMResultFromKinvey = function(){
		$scope.showLoader();
		var col = "_acl";
		var relatedJson = {"creator":$scope.performanceUser._id};
		var query = new $kinvey.Query();
			query.equalTo(col, relatedJson);
			performanceTestResults.find(query).subscribe(function(data) {
				tempData = data	    	      
			}, function(error) {
					$scope.internalGlitch();
			}, function(data) {
				for(var i=0;i<tempData.length;i++){
					if(!tempData[i].isShown && moment().diff(tempData[i]._kmd.ect,'days') >= 3 && tempData[i].attempts > 0){
						ackNotificationSetTrue.push(tempData[i].notificationId);
						tempData[i].isShown = true;
						updateResultPMResult.push(tempData[i]);
						$scope.performanceResultsArray.push(tempData[i]);
					}
					else if(tempData[i].isShown){
						$scope.performanceResultsArray.push(tempData[i]);
					}  	
					else{
						$scope.performanceIncomplete.push(tempData[i]);
					}
					if(tempData[i].isPointGiven == true && tempData[i].cycle == 1){ // PMbattery Point to all-test and cycle is 1
						pmPointLength++;
					}	
				}
				if(updateResultPMResult.length > 0){
					$scope.updateCollectionNotification();
				}
				else{
					$scope.getNotificationsFromKiney();
				}
			});
	}

	$scope.updateCollectionNotification = function(){
		console.log("Notification Updated");
		var col = "_id";
		var relatedJson = ackNotificationSetTrue;
		var query = new $kinvey.Query();
			query.contains(col, relatedJson);
		notificationTestList.find(query).subscribe(function(data) {
			$scope.updateNotification = data;
		}, function(error) {
			$scope.internalGlitch();
		}, function(data) {
			for(var i=0;i<$scope.updateNotification.length;i++){
				$scope.updateNotification[i].isAck = true;
			}
			notificationTestList.update($scope.updateNotification).then(function(data) {
			$scope.updateCollectionPMResults();
			}).catch(function(error) {
					$scope.internalGlitch();
					console.log(error);
				});
		});
	}

	$scope.updateCollectionPMResults = function(){
		console.log("PM Result Updated");
		performanceTestResults.update(updateResultPMResult).then(function(data) {
			console.log(data)
			$scope.getNotificationsFromKiney();
		}).catch(function(error) {
			$scope.internalGlitch();
			console.log(error);
		});
	}

	$scope.getNotificationsFromKiney = function(){
		var query = new $kinvey.Query();
		query.equalTo("_acl", {"creator":$scope.performanceUser._id});
		var query2 = new $kinvey.Query();
		query2.equalTo("isAck",false).equalTo('isSent', true).equalTo('isActive',true);;
		query.and(query2);
		query2.equalTo("notificationType","PM");
		query.and(query2);
		console.log(updateResultPMResult);
		notificationTestList.find(query).subscribe(function(data) {
		tempData = data;
		}, function(error) {
			$scope.internalGlitch();
			console.log(error);
		}, function(data) {
			console.log(tempData,notificationList);
			$scope.notificationsListData = tempData
			for(var i=0;i<tempData.length;i++){
				notificationList.push(tempData[i].notificationAttributes.pms)
			}
			$scope.getPMTestListfromKinvey();
		});
	}

	$scope.getPMTestListfromKinvey = function(){
		var col = "type";
		var relatedJson = notificationList;
		var query = new $kinvey.Query();
		query.contains(col, relatedJson);
		performanceTestList.find(query).subscribe(function(data) {
			tempData = data;
		}, function(error) {
			$scope.internalGlitch();
			console.log(error);
		}, function(data) {
			for(var i=0;i<$scope.performanceIncomplete.length;i++){
				for(var j=0;j<tempData.length;j++){
					if(tempData[j].type == $scope.performanceIncomplete[i].type && $scope.performanceIncomplete[i].attempts > 0){
						tempData[j].lastDate = moment($scope.performanceIncomplete[i]._kmd.lmt).format("MM/DD/YYYY");
						break;
					}	
				}
			}
			for(var i=0;i<tempData.length;i++){
				if(tempData[i].lastDate){
					$scope.takenTestList.push(tempData[i]);
				}else{
					$scope.newTestList.push(tempData[i]);
				}
			}
			console.log($scope.performanceResultsArray);
			$scope.filterDataFromKinvey();
			$scope.hideLoader();
		});
	}

	/*$scope.filterTestListFromKinvey = function(kinveyData) {
		if($scope.performanceUser.testList != undefined){
				for(var i=0;i<$scope.performanceUser.testList.length; i++){
					for(var j=0;j<kinveyData.length; j++){
						if(kinveyData[j]._id == $scope.performanceUser.testList[i].id && $scope.performanceUser.testList[i].isActive == false)
							kinveyData[j].isActive = false;
					}
				}  		
		}
		console.log(kinveyData);
		$scope.testList=kinveyData;
	}*/


	$scope.filterDataFromKinvey = function() {
		$scope.kinveyDataArr = angular.copy($scope.performanceResultsArray);
		var PMType = [{"name":"Timed 25ft Walk","imageName":"walkTest"}, {"name":"Word Match and Memory","imageName":"languageMemoryTest"}, {"name":"Symbol Substitution and Trail Making","imageName":"symbolDigit"}, {"name":"Color Vision","imageName":"ishiharaTest"}, {"name":"Reaction Time","imageName":"reactionTest"}, {"name":"Finger Tapping","imageName":"fingerTap"}, {"name":"Tower Tasks","imageName":"towerOfHanoi"} ];
		for(var i = 0; i < PMType.length; i++) {
			for (var j = $scope.kinveyDataArr.length-1; j >= 0; j--) {
				if ($scope.kinveyDataArr[j].PMType == PMType[i].name) {
					$scope.PMtypeFilterResults[PMType[i].name] = $scope.kinveyDataArr[j];
					$scope.PMtypeFilterResults[PMType[i].name].imageName = PMType[i].imageName;
					break;
				}
			}
		}
		
		/*var pmBatteryScore = 0;
		if($scope.kinveyDataArr.length == 8 && moment().diff(moment($scope.performanceUser.personalInfoCompTime),'days') <8 && !$scope.performanceUser.points.fullBatteryPMs){
			for(var i=0;i<$scope.kinveyDataArr.length;i++){
				if($scope.kinveyDataArr[i].cycle == 1 && $scope.kinveyDataArr[i].isAborted == false){
					pmBatteryScore++;
				}
			}
		}

		if(pmBatteryScore == 8){
			$scope.$emit('saveGamificationPoints',CONSTANT.gamificationPoints.fullPMOneWeek.value,CONSTANT.gamificationPoints.fullPMOneWeek.name);
		}*/

		if(pmPointLength == 8 && moment().diff(moment($scope.performanceUser.personalInfoCompTime),'days') <8 && !$scope.performanceUser.points.fullBatteryPMs){
			$scope.$emit('saveGamificationPoints',CONSTANT.gamificationPoints.fullPMOneWeek.value,CONSTANT.gamificationPoints.fullPMOneWeek.name);
		}
		console.log($scope.PMtypeFilterResults)
	}

	$scope.getKeyData = function(testType) {
		$scope.PMtypeListResult = [];
		for (var j = 0; j < $scope.kinveyDataArr.length; j++) {
			if ($scope.kinveyDataArr[j].PMType == testType.key) {
				$scope.PMtypeListResult.push($scope.kinveyDataArr[j]);
			}
		}
		$state.go('app.performanceEvals.singlePerformanceResult');
	}
}]);
