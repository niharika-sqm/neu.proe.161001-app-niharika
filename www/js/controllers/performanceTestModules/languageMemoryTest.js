neuroScienceApp.controller('languageMemoryTestCtrl', ['$scope', '$state', '$interval', '$kinvey', '$ionicScrollDelegate', '$ionicPopup', 'CONSTANT', '$q', '$stateParams','closePopupService','$timeout', function($scope, $state, $interval, $kinvey, $ionicScrollDelegate, $ionicPopup, CONSTANT, $q, $stateParams,closePopupService,$timeout) {

  $timeout(function(){
    window.ga.trackView('Language Memory Test');
  }, 1000);

  $scope.part1 = true;
  $scope.startmemory = false;
  $scope.Description = true;
  $scope.TrialTest = false;
  $scope.startmemory2 = false;
  var timeDelayForMemoryPart2 = 240; // 240 seconds (4 mins) 
  // timer for word memory test 1 min wait time before start of memory test part 2
  var memoryTestTimer = 60+timeDelayForMemoryPart2; // add timeDelayForMemoryPart2 in this (1min + 4min  = 5mins)

  // This variable is used to check if the memory test is time out or not.
  $scope.isTimeout = false;  //intial value will be false;

  $scope.$on('$ionicView.enter', function() {
    $scope.recommendation();
    if ($stateParams.test != null) {
      $scope.notificationId = $stateParams.test.notificationId;
      $scope.attempts = $stateParams.test.attempts;
      $scope.testResultId = $stateParams.test.testResultId;
      $scope.cycle = $stateParams.test.cycle;
      $scope.type = $stateParams.test.type;
      $scope.isPointGiven = $stateParams.test.isPointGiven;
      $scope.isTrailTaken = $stateParams.test.isTrailTaken;
      $stateParams.test.relapseTrackerId == undefined ? $scope.relapseTrackerId = "" : $scope.relapseTrackerId = $stateParams.test.relapseTrackerId;
      $scope.questionLists = $stateParams.test.questionLists;
      $scope.wordListVersion = $stateParams.test.questionListsVersion;
      delete $stateParams.test;
    }
    $scope.kinveyId = "";
    $scope.result = '';
    $scope.yesCorrectClicks = 0;
    $scope.yesCorrectClickslang = 0;
    $scope.noCorrectClicks = 0;
    $scope.noCorrectClickslang = 0;
    $scope.yesIncorrectClicks = 0;
    $scope.yesIncorrectClickslang = 0;
    $scope.noIncorrectClicks = 0;
    $scope.noIncorrectClickslang = 0;
    $scope.category_text_counter = 0;
    $scope.category_counter = 0;
    $scope.memory_counter = 0;
    $scope.memory_type_counter = 0;
    $scope.startLang = true;
    $scope.timer = 0;
    $scope.raw = {};
    $scope.trialEndCount = 4;
    $scope.trialCounts = 0;
    if($scope.questionLists){
      $scope.languageWordsTest = $scope.questionLists.languageWordsTest;
      $scope.languageWords = $scope.questionLists.languageWords;
      $scope.memoryWords = JSON.parse(JSON.stringify($scope.questionLists.memoryWords));
      $scope.languageWordslists = Object.keys($scope.languageWords);
    }
    $scope.showBumperImage = false;
  });

  $scope.$on('$ionicView.afterEnter', function() {
    $scope.lockOrientation();
  });

  $scope.$on('$destroy',function(){
    $scope.unlockOrientation();
    if (angular.isDefined($scope.memoryTimeInterval)) {
      $interval.cancel($scope.memoryTimeInterval);
    }
  });

  var timeout, time1, time2, trialTimeoutlang, timeoutP2;

  var performanceTestResults = $kinvey.DataStore.getInstance('performanceTestResults', $kinvey.DataStoreType.Network);
  var notificationTestList = $kinvey.DataStore.getInstance('notifications', $kinvey.DataStoreType.Network);

  var jsonData = {};
  jsonData.PMType = "Word Match and Memory";
  jsonData.date = new Date();
  jsonData.attempts = 0;

  $scope.takeTrial = function() {
    if (typeof window.ga !== "undefined") {
      window.ga.trackEvent('Word Match and Memory Screen', 'Trail starts', 'Take A Trail', 1);
    }
    $ionicScrollDelegate.scrollTop();
    $scope.TrialTest = true;
    $scope.Description = false;
    $scope.category_name = 'Animals';
    $scope.startTrialTest = true;
    $scope.falseClick = false;
    $scope.trial = true;
    if($scope.attempts == 0 && !$scope.isTrailTaken){
      $scope.saveInitialTrailData();
    }
  };

  $scope.saveInitialTrailData = function(){
    $scope.showLoader();
    if($scope.testResultId == ""){
      performanceTestResults.save({
        "date": new Date(),
        "PMType":jsonData.PMType,
        "attempts":0,
        "cycle":$scope.cycle,
        "type":$scope.type,
        "prePMSurveyAnswers":$scope.prePMSurveyAnswers,
        "notificationId":$scope.notificationId,
        "isTrial":false,
        "wordListVersion":$scope.wordListVersion
      }).then(function(entity) {
        console.log("entity ",entity);
        $scope.isTrailTaken = false;
        $scope.testResultId  = entity._id;
        $scope.hideLoader();
      }).catch(function(error) {
        $scope.internalGlitch();
        $state.go('app.performanceEvals');
      });
    }else{
      performanceTestResults.save({
        "_id":$scope.testResultId,
        "date": new Date(),
        "PMType":jsonData.PMType,
        "attempts":0,
        "cycle":$scope.cycle,
        "type":$scope.type,
        "prePMSurveyAnswers":$scope.prePMSurveyAnswers,
        "notificationId":$scope.notificationId,
        "isTrial":false,
        "wordListVersion":$scope.wordListVersion
      }).then(function(entity) {
        console.log("entity ",entity);
        $scope.isTrailTaken = false;
        $scope.testResultId  = entity._id;
        $scope.hideLoader();
      }).catch(function(error) {
        $scope.internalGlitch();
        $state.go('app.performanceEvals');
      });
    }
  };

  $scope.saveFinalTrailData = function(){
    $scope.showLoader();
    performanceTestResults.save({
      "_id":$scope.testResultId,
      "date": new Date(),
      "PMType":jsonData.PMType,
      "attempts":0,
      "cycle":$scope.cycle,
      "type":$scope.type,
      "prePMSurveyAnswers":$scope.prePMSurveyAnswers,
      "notificationId":$scope.notificationId,
      "isTrial":true,
      "wordListVersion":$scope.wordListVersion
    }).then(function(entity) {
      console.log("entity ",entity);
      $scope.isTrailTaken = true;
      $scope.testResultId  = entity._id;
      $scope.hideLoader();
    }).catch(function(error) {
      $scope.internalGlitch();
      $state.go('app.performanceEvals');
    });
  };

  $scope.startTrial = function() {
    if (typeof window.ga !== "undefined") {
      window.ga.trackEvent('Word Match and Memory Screen', 'Test starts', 'Get Started', 1);
    }
    $scope.startTrialTest = false;
    $scope.trialCounts = 0;
    $scope.category_text = $scope.languageWordsTest.Animals[$scope.category_text_counter];
    trialTimeoutlang = $interval(function() {
      $interval.cancel(trialTimeoutlang);
      trialTimeoutlang = null;
      $scope.part1 = true;
      $scope.startmemory = false;
      $scope.Description = true;
      $scope.TrialTest = false;
      $scope.startLang = true;
      $scope.startmemory2 = false;
      $scope.category_text_counter = 0;
      $scope.showMessage('Please read instructions and try again.', 3000);
    }, 30000);
  };

  $scope.afterAnswerRefreshVariable = function() {
    $scope.startTrialTest = false;
    console.log($scope.languageWordsTest.Animals.length, $scope.category_text_counter);
    if ($scope.languageWordsTest.Animals.length == $scope.category_text_counter) {
      $scope.category_text_counter = 0;
      $scope.category_text = $scope.languageWordsTest.Animals[$scope.category_text_counter];
    } else {
      $scope.category_text = $scope.languageWordsTest.Animals[$scope.category_text_counter];
    }
  }

  $scope.submitTextTrial = function(result) {
    if (result == $scope.languageWordsTest.Animals[$scope.category_text_counter].status) {
      console.log(result, $scope.languageWordsTest.Animals[$scope.category_text_counter].status);
      $scope.category_text_counter = $scope.category_text_counter + 1;

      $scope.trialCounts++;
      if($scope.trialCounts == $scope.trialEndCount){
        $interval.cancel(trialTimeoutlang);
        trialTimeoutlang = null;
        $scope.congratulationsMsg = 'Congratulations';
        $scope.trial = false;
        if($scope.attempts == 0 && !$scope.isTrailTaken){
          $scope.saveFinalTrailData();
        }
      }else{
        $scope.afterAnswerRefreshVariable();
      }
    } else {
      console.log(result, $scope.languageWordsTest.Animals[$scope.category_text_counter].status);
      $scope.category_text_counter = $scope.category_text_counter + 1;
      if (result == true) {
        $scope.falseMsg = "This word is not associated with<br>the category 'Animals'";
      } else {
        $scope.falseMsg = "This word is associated<br>with the category 'Animals'";
      }

      $scope.falseClick = true;
      timeout = $interval(function() {
        $interval.cancel(timeout);
        timeout = null;
        $scope.falseClick = false;
        $scope.afterAnswerRefreshVariable();
      }, 3000);
    }
  };

  $scope.testStartedKinvey = function() {
    if($scope.attempts==1){
    $scope.setPMNotificationIsShown($scope.notificationId);
      if($scope.relapseTrack.startRelapse){
      $scope.relapseTrackerId = $scope.relapseTrack.kinveyData._id;
      }
    }
    if ($scope.testResultId == "") {
      performanceTestResults.save({
        "date": jsonData.date,
        "PMType": jsonData.PMType,
        "attempts": $scope.attempts,
        "cycle": $scope.cycle,
        "type": $scope.type,
        "recommendForPm": $scope.recommendForPm,
        "prePMSurveyAnswers":$scope.prePMSurveyAnswers,
        "notificationId": $scope.notificationId,
        "isAborted": true,
        "isShown": true,
        "isPointGiven":$scope.isPointGiven,
        "isTrial":$scope.isTrailTaken,
        "relapseTrackerId":$scope.relapseTrackerId,
        "isTimeout":$scope.isTimeout,
        "wordListVersion":$scope.wordListVersion
      }).then(function(entity) {
        console.log("entity " + entity);
        $scope.testResultId = entity._id;
        if($scope.attempts == 1 && $scope.relapseTrack.startRelapse){
           $scope.$emit('updateRelapseTracker',$scope.testResultId);
        }
        $scope.updateNotificationCollection();
      }).catch(function(error) {
        $scope.internalGlitch();
        $state.go('app.performanceEvals');
      });
    }else{
      performanceTestResults.save({
        "_id":$scope.testResultId,
        "date": jsonData.date,
        "PMType": jsonData.PMType,
        "attempts": $scope.attempts,
        "cycle": $scope.cycle,
        "type": $scope.type,
        "recommendForPm": $scope.recommendForPm,
        "prePMSurveyAnswers":$scope.prePMSurveyAnswers,
        "notificationId": $scope.notificationId,
        "isAborted": true,
        "isShown": true,
        "isPointGiven":$scope.isPointGiven,
        "isTrial":$scope.isTrailTaken,
        "relapseTrackerId":$scope.relapseTrackerId,
        "isTimeout":$scope.isTimeout,
        "wordListVersion":$scope.wordListVersion
      }).then(function(entity) {
        console.log("entity " + entity);
        $scope.testResultId = entity._id;
        if($scope.attempts == 1 && $scope.relapseTrack.startRelapse){
           $scope.$emit('updateRelapseTracker',$scope.testResultId);
        }
        $scope.updateNotificationCollection();
      }).catch(function(error) {
        $scope.internalGlitch();
        $state.go('app.performanceEvals');
      });
    }
  }

  $scope.startTest = function() {
    $ionicScrollDelegate.scrollTop();
    $scope.language = true;
    $scope.category_text_counter = 0;
    $scope.Description = false;
    $scope.TrialTest = false;
    $scope.startLang = true;
    $interval.cancel(trialTimeoutlang);
    if ($scope.category_counter == 0) {
      $scope.attempts++;
      $scope.testStartedKinvey();
      /*if ($scope.attempts == 3) {
        $scope.retakeBtnDisabled = true;
      }*/
      //$scope.category_lang_name = 'Desserts';
      $scope.category_lang_name = $scope.languageWordslists[0];
    } else if ($scope.category_counter == 1) {
      //$scope.category_lang_name = 'Vegetables';
      $scope.category_lang_name = $scope.languageWordslists[1];
    } else if ($scope.category_counter == 2) {
      //$scope.category_lang_name = 'Bathroom Items';
      $scope.category_lang_name = $scope.languageWordslists[2];
    } else {
      $scope.language = false;
      $scope.results = false;
    }
  };

  $scope.startLangTest = function() {
    time1 = performance.now();
    if ($scope.category_text_counter == 12) {
      // if($scope.category_text_counter == 2){
      $scope.category_counter = $scope.category_counter + 1;
      $scope.startTest();
    } else {
      $scope.startLang = false;
      $scope.category_lang_text = $scope.languageWords[$scope.category_lang_name][$scope.category_text_counter];
    }
  };

  $scope.submitText = function(result, testType) {
    time2 = performance.now();
    if (testType == 'lang') {
      $scope.languageWords[$scope.category_lang_name][$scope.category_text_counter].responseTime = time2 - time1;
      console.log($scope.languageWords[$scope.category_lang_name][$scope.category_text_counter].responseTime);
      if (result == $scope.languageWords[$scope.category_lang_name][$scope.category_text_counter].status) {
        if ($scope.category_lang_name == $scope.languageWordslists[0]) {
          $scope.languageWords[$scope.category_lang_name][$scope.category_text_counter].responseType = result;
          $scope.languageWords[$scope.category_lang_name][$scope.category_text_counter].result = 'correct';
          $scope.pushToRaw('languageP1', $scope.languageWords[$scope.category_lang_name][$scope.category_text_counter]);
        } else if ($scope.category_lang_name == $scope.languageWordslists[1]) {
          $scope.languageWords[$scope.category_lang_name][$scope.category_text_counter].responseType = result;
          $scope.languageWords[$scope.category_lang_name][$scope.category_text_counter].result = 'correct';
          $scope.pushToRaw('languageP2', $scope.languageWords[$scope.category_lang_name][$scope.category_text_counter]);
        } else if ($scope.category_lang_name == $scope.languageWordslists[2]) {
          $scope.languageWords[$scope.category_lang_name][$scope.category_text_counter].responseType = result;
          $scope.languageWords[$scope.category_lang_name][$scope.category_text_counter].result = 'correct';
          $scope.pushToRaw('languageP3', $scope.languageWords[$scope.category_lang_name][$scope.category_text_counter]);
        };
        if (result == true) {
          $scope.yesCorrectClickslang = $scope.yesCorrectClickslang + 1;
        } else {
          $scope.noCorrectClickslang = $scope.noCorrectClickslang + 1;
        }
        $scope.category_text_counter = $scope.category_text_counter + 1;
        $scope.startLangTest();
      } else {
        if ($scope.category_lang_name == $scope.languageWordslists[0]) {
          $scope.languageWords[$scope.category_lang_name][$scope.category_text_counter].responseType = result;
          $scope.languageWords[$scope.category_lang_name][$scope.category_text_counter].result = 'inCorrect';
          $scope.pushToRaw('languageP1', $scope.languageWords[$scope.category_lang_name][$scope.category_text_counter]);
        } else if ($scope.category_lang_name == $scope.languageWordslists[1]) {
          $scope.languageWords[$scope.category_lang_name][$scope.category_text_counter].responseType = result;
          $scope.languageWords[$scope.category_lang_name][$scope.category_text_counter].result = 'inCorrect';
          $scope.pushToRaw('languageP2', $scope.languageWords[$scope.category_lang_name][$scope.category_text_counter]);
        } else if ($scope.category_lang_name == $scope.languageWordslists[2]) {
          $scope.languageWords[$scope.category_lang_name][$scope.category_text_counter].responseType = result;
          $scope.languageWords[$scope.category_lang_name][$scope.category_text_counter].result = 'inCorrect';
          $scope.pushToRaw('languageP3', $scope.languageWords[$scope.category_lang_name][$scope.category_text_counter]);
        };
        if (result == true) {
          $scope.yesIncorrectClickslang = $scope.yesIncorrectClickslang + 1;
        } else {
          $scope.noIncorrectClickslang = $scope.noIncorrectClickslang + 1;
        }
        $scope.category_text_counter = $scope.category_text_counter + 1
        $scope.falseClickLang = true;
        $scope.falseClickLang = false;
        $interval.cancel(timeout);
        $scope.startLangTest();
      }
    } else {
      $scope.memoryWords[$scope.memory_counter].responseTime = time2 - time1;
      console.log($scope.memoryWords[$scope.memory_counter].responseTime);
      if (result == $scope.memoryWords[$scope.memory_counter].status) {
        if ($scope.category_memo_name == 'memoryP1') {
          $scope.memoryWords[$scope.memory_counter].responseType = result;
          $scope.memoryWords[$scope.memory_counter].result = 'correct';
          $scope.pushToRaw('memoryP1', $scope.memoryWords[$scope.memory_counter]);
        } else if ($scope.category_memo_name == 'memoryP2') {
          $scope.memoryWords[$scope.memory_counter].responseType = result;
          $scope.memoryWords[$scope.memory_counter].result = 'correct';
          $scope.pushToRaw('memoryP2', $scope.memoryWords[$scope.memory_counter]);
        }
        if (result == true) {
          $scope.yesCorrectClicks = $scope.yesCorrectClicks + 1;
        } else {
          $scope.noCorrectClicks = $scope.noCorrectClicks + 1;
        }
        $scope.memory_counter = $scope.memory_counter + 1;
        $scope.startMemoryTest();
      } else {
        if ($scope.category_memo_name == 'memoryP1') {
          $scope.memoryWords[$scope.memory_counter].responseType = result;
          $scope.memoryWords[$scope.memory_counter].result = 'inCorrect';
          $scope.pushToRaw('memoryP1', $scope.memoryWords[$scope.memory_counter]);
        } else if ($scope.category_memo_name == 'memoryP2') {
          $scope.memoryWords[$scope.memory_counter].responseType = result;
          $scope.memoryWords[$scope.memory_counter].result = 'inCorrect';
          $scope.pushToRaw('memoryP2', $scope.memoryWords[$scope.memory_counter]);
        }
        if (result == true) {
          $scope.yesIncorrectClicks = $scope.yesIncorrectClicks + 1;
        } else {
          $scope.noIncorrectClicks = $scope.noIncorrectClicks + 1;
        }
        $scope.memory_counter = $scope.memory_counter + 1;
        $scope.falseClickLang = true;
        $scope.falseClickLang = false;
        $scope.startMemoryTest();
      }
    }
  };

  $scope.showBumper = function() {
    $scope.startmemory2 = true;
    $scope.showBumperImage = true;
  }

  $scope.startMemoryTest = function() {
    $scope.showBumperImage = false;
    time1 = performance.now();
    if ($scope.startmemory == false) {
      $scope.startmemory = true;
      $scope.category_memo_name = 'memoryP1';
    }
    if ($scope.memory_counter == 48) {
      // if($scope.memory_counter == 2){
      if ($scope.category_memo_name == 'memoryP1') {
        $scope.startmemory2 = false
        $scope.memory_counter = 0;
        $scope.category_memo_name = 'memoryP2';
        $scope.startMemoryP2();
        $scope.memmoryTestTimer();
      } else {
        $scope.updateResultonKinvey();
      }
    } else {
      /* Cancel Timer of 1 min wait time before starting of memory test part 2*/
      if($scope.category_memo_name == 'memoryP2' && $scope.memory_counter == 0) {
        document.removeEventListener('resume',onAppResumeBetweenMemoryP2,false);
        document.removeEventListener("pause", onAppPauseBetweenMemoryP2, false);
        if (angular.isDefined($scope.memoryTimeInterval)) {
          $interval.cancel($scope.memoryTimeInterval);
        }
        //$scope.memmoryTestTimer();
      }

      $scope.category_memo_text = $scope.memoryWords[$scope.memory_counter];
    }
  };

  $scope.memmoryTestTimer = function(){
    $scope.timeTakenForMemoryTestP2 = 0;
    localStorage.setMemoryPart2Timer = Date.now() + memoryTestTimer*1000;
    document.addEventListener("resume", onAppResumeBetweenMemoryP2, false);
    document.addEventListener("pause", onAppPauseBetweenMemoryP2, false);
    $scope.startTimerPart2();
  }

  $scope.startTimerPart2 = function(){
    $scope.memoryTimeInterval = $interval(function() {
      $scope.timeTakenForMemoryTestP2++
      if($scope.timeTakenForMemoryTestP2 == memoryTestTimer){
        if (angular.isDefined($scope.memoryTimeInterval)) {
          $interval.cancel($scope.memoryTimeInterval);
        }
        $scope.updateResultonKinvey();
      }
    },1000);
  }

  function onAppPauseBetweenMemoryP2(){
    if (angular.isDefined($scope.memoryTimeInterval)) {
      $interval.cancel($scope.memoryTimeInterval);
    }
  }

  function onAppResumeBetweenMemoryP2(){
    if(!localStorage.setMemoryPart2Timer){
      return;
    }
    var current = Date.now();
    var expected = Number(localStorage.setMemoryPart2Timer);
    var difference = expected - current;
    $scope.timeTakenForMemoryTestP2 = memoryTestTimer - Math.round(difference/1000);
    //console.log($scope.timer,difference);
    if(difference <= 0){
      $scope.timeTakenForMemoryTestP2 = memoryTestTimer;
      localStorage.removeItem('setMemoryPart2Timer');
      if (angular.isDefined($scope.memoryTimeInterval)) {
        $interval.cancel($scope.memoryTimeInterval);
      }
      document.removeEventListener('resume',onAppResumeBetweenMemoryP2,false);
      document.removeEventListener("pause", onAppPauseBetweenMemoryP2, false);
      $scope.updateResultonKinvey();
    }
    else{
      $scope.startTimerPart2();
    }
  }

  $scope.updateResultonKinvey = function(cancel) {
    $scope.showBumperImage = false;
    $scope.results = true;
    $scope.result = 'Results';
    $scope.percentCorrect = Math.round((($scope.yesCorrectClicks + $scope.noCorrectClicks) / 96) * 100);
    $scope.percentCorrectlang = Math.round((($scope.yesCorrectClickslang + $scope.noCorrectClickslang) / 36) * 100);
    console.log($scope.yesCorrectClicks, $scope.noCorrectClicks, $scope.yesIncorrectClicks, $scope.noIncorrectClicks, $scope.percentCorrect);

    // check if test is timeout or not
    // if 5 min (4min + 1min) is over then test is consider as timeout
    if($scope.timeTakenForMemoryTestP2 >= memoryTestTimer){
      $scope.isTimeout = true;
    }else{
      $scope.isTimeout = false;
    }
    jsonData.raw = $scope.raw;
    jsonData.score = {
      yesCorrectClicks: $scope.yesCorrectClicks,
      noCorrectClicks: $scope.noCorrectClicks,
      yesIncorrectClicks: $scope.yesIncorrectClicks,
      noIncorrectClicks: $scope.noIncorrectClicks,
      percentCorrect: $scope.percentCorrect,
      yesCorrectClickslang: $scope.yesCorrectClickslang,
      noCorrectClickslang: $scope.noCorrectClickslang,
      yesIncorrectClickslang: $scope.yesIncorrectClickslang,
      noIncorrectClickslang: $scope.noIncorrectClickslang,
      percentCorrectlang: $scope.percentCorrectlang,
      waitTimeMemoryP2:$scope.timeTakenForMemoryTestP2 - timeDelayForMemoryPart2
      //timeTakenForMemoryTestP2: $scope.timeTakenForMemoryTestP2
    }
    jsonData.attempts = $scope.attempts;
    console.log(jsonData);
    $scope.showLoader();
    var addpoints = true;
     var kinveObj = {
      _id: $scope.testResultId,
      "date": jsonData.date,
      "PMType": jsonData.PMType,
      "raw": jsonData.raw,
      "score1": jsonData.score,
      "attempts": jsonData.attempts,
      "cycle": $scope.cycle,
      "type": $scope.type,
      "recommendForPm": $scope.recommendForPm,
      "prePMSurveyAnswers":$scope.prePMSurveyAnswers,
      "isAborted": cancel == undefined ? false : cancel,
      "isShown": true,
      "notificationId": $scope.notificationId,
      "isPointGiven":addpoints,
      "isTrial":$scope.isTrailTaken,
      "relapseTrackerId":$scope.relapseTrackerId,
      "isTimeout":$scope.isTimeout,
      "wordListVersion":$scope.wordListVersion
    }
    performanceTestResults.save(kinveObj).then(function(entity) {
      $scope.testResultId = entity._id;
      console.log(entity);
      if (cancel == true) {
        $state.go('app.performanceEvals');
      } else {
        $scope.$emit('saveGamificationPoints', CONSTANT.gamificationPoints.pmEvaluation.value, CONSTANT.gamificationPoints.pmEvaluation.name);
      }
      $scope.hideLoader();
    }).catch(function(error) {
      localStorage.setItem("unSavedPMData", JSON.stringify(kinveObj));
      localStorage.setItem("unSavedPMPoints", addpoints);
      $scope.internalGlitch();
    });

  }

  $scope.updateNotificationCollection = function() {
    var tempdata = [];
    var col = "_id";
    var relatedJson = $scope.notificationId;
    var query = new $kinvey.Query();
    query.equalTo(col, relatedJson);
    notificationTestList.find(query).subscribe(function(data) {
      tempdata = data;
    }, function(error) {}, function(data) {
      tempdata[0].isAck = true;
      notificationTestList.update(tempdata[0]).then(function(data) {
        console.log(data);
      }).catch(function(error) {
        $scope.internalGlitch();
        console.log(error);
      });
    });
  }

  $scope.startMemoryP2 = function() {
    $scope.memoryWords = JSON.parse(JSON.stringify($scope.questionLists.memoryWords));
    console.log($scope.startmemory2, 'startmemory2');
    $scope.part1 = false;
    var now = new Date().getTime(),
    _240_sec_from_now = new Date(now + timeDelayForMemoryPart2*1000);
    localStorage.setMemoryDelayTime = Date.now() + timeDelayForMemoryPart2*1000;
    document.addEventListener("resume", onResume, false);

    function onResume(){
      if(!localStorage.setMemoryDelayTime){
        return;
      }
      var current = Date.now();
      var expected = Number(localStorage.setMemoryDelayTime);
      var difference = expected - current;
      $scope.timer = timeDelayForMemoryPart2 - Math.round(difference/1000);
      //console.log($scope.timer,difference);
      if(difference <= 0){
        localStorage.removeItem('setMemoryDelayTime');
        $interval.cancel(timeoutP2);
        timeoutP2 = undefined;
        $scope.showBumper();
        document.removeEventListener('resume',onResume,false);
      }
    }

    cordova.plugins.notification.local.schedule({
      id: 1,
      "title":"MS Care Connect",
      text: "You can now complete the memory portion of the test!",
      at: _240_sec_from_now,
      icon: 'res://ic_notify.png'
    });
    cordova.plugins.notification.local.on("click", function(notification) {
      // console.log("local notofication triger");
      $interval.cancel(timeoutP2);
      timeoutP2 = undefined;
      $scope.showBumper();
    });
    timeoutP2 = $interval(function() {
      $scope.timer = $scope.timer + 1;
      // console.log($scope.timer);
      if ($scope.timer >= timeDelayForMemoryPart2) {
        $interval.cancel(timeoutP2);
        timeoutP2 = undefined;
        $scope.showBumper();
        document.removeEventListener('resume',onResume,false);
      }
    }, 1000);
  }

  $scope.pushToRaw = function(test, item) {
    $scope.test = test;
    if ($scope.raw[$scope.test]) {
      $scope.raw[$scope.test].push(JSON.parse(JSON.stringify(item)));
    } else {
      $scope.raw[$scope.test] = [];
      $scope.raw[$scope.test].push(JSON.parse(JSON.stringify(item)));
    }
  };

  /*$scope.retakeTest = function() {
      $scope.retakeTestPopup = $ionicPopup.show({
        title:"Are you sure you want to restart the test?",
        template:'<a class="popClose" ng-click="retakeTestPopup.close()"></a><p>This performance metric helps to assess how MS may be affecting your cognitive (thinking), sensory, and motor (movement) functioning. If problems occurred while taking this test, you can retake it. However this test is given to gain an accurate measurement of your functioning, so try to avoid excessive retakes. You can complete the test up to 2 times. Only your latest test will be recorded in your results.</p>',
        scope: $scope,
        buttons: [{
          text: 'CANCEL',
          type: 'button-block button-outline button-darkPurple skip font40',
          onTap: function() {
            return false;
          }
        },{
          text: 'RETAKE TEST',
          type: 'button-block button-darkPurple font40',
          onTap: function() {
            return true;
          }
        }]
      });
      $scope.retakeTestPopup.then(function(res){
        if(res){
          jsonData.attempts = jsonData.attempts + 1;
          $scope.result = '';
          $scope.yesCorrectClicks = 0;
          $scope.yesCorrectClickslang = 0;
          $scope.noCorrectClicks = 0;
          $scope.noCorrectClickslang = 0;
          $scope.yesIncorrectClicks = 0;
          $scope.yesIncorrectClickslang = 0;
          $scope.noIncorrectClicks = 0;
          $scope.noIncorrectClickslang = 0;
          $scope.part1 = true;
          $scope.startmemory = false;
          $scope.Description = true;
          $scope.TrialTest = false;
          $scope.category_text_counter = 0;
          $scope.category_counter = 0;
          $scope.memory_counter = 0;
          $scope.memory_type_counter = 0;
          $scope.startLang = true;
          $scope.startmemory2 == false;
          $scope.timer = 0;
          $scope.raw = {};
        }
    });
  };*/

  $scope.cancelBtn = function() {
    if($scope.TrialTest){
      $scope.onCancelCall();
      return;
    }
    if (timeoutP2) {
      $interval.cancel(timeoutP2);
      timeoutP2 = undefined;
    }
    $scope.cancelTestPopup = $ionicPopup.show({
      title: "Are you sure you want to cancel the test?",
      template: '<p style="text-align: center;">If you cancel the test, it will count as one of your test takes. You will only have one additional chance to retake the test.</p><p  class="text-center">PLEASE do not retake the test to practice or improve your score. This option is only available if something went wrong during the test.</p><a class="popClose" ng-click="cancelTestPopup.close()"></a>',
      scope: $scope,
      buttons: [{
        text: 'YES, CANCEL',
        type: 'button-block button-outline button-darkPurple skip',
        onTap: function() {
          return true;
        }
      }, {
        text: 'RETURN TO<br/> THE TEST',
        type: 'button-block button-darkPurple',
        onTap: function() {
          return false;
        }
      }]
    });
    closePopupService.register($scope.cancelTestPopup);
    $scope.cancelTestPopup.then(function(res) {
      if (res) {
        if (typeof window.ga !== "undefined") {
          window.ga.trackEvent('Word Match and Memory Screen - Cancel Popup', 'Performance Metric Screen (Word Match and Memory)', 'Yes, Cancel ', 1);
        }
        $scope.onCancelCall();
        $state.go('app.performanceEvals');
      } else {
        if (typeof window.ga !== "undefined") {
          window.ga.trackEvent('Word Match and Memory Screen - Cancel Popup', 'Cancel Pop Up', 'Return to the Test', 1);
        }
        if ($scope.timer > 0) {
          $scope.startMemoryP2();
        }
      }
    });
  };

  $scope.onCancelCall = function(){
    $interval.cancel(trialTimeoutlang);
    $interval.cancel(timeout);
    $interval.cancel(timeoutP2);
    $scope.result = '';
    $scope.yesCorrectClicks = 0;
    $scope.yesCorrectClickslang = 0;
    $scope.noCorrectClicks = 0;
    $scope.noCorrectClickslang = 0;
    $scope.yesIncorrectClicks = 0;
    $scope.yesIncorrectClickslang = 0;
    $scope.noIncorrectClicks = 0;
    $scope.noIncorrectClickslang = 0;
    $scope.part1 = true;
    $scope.startmemory = false;
    $scope.Description = true;
    $scope.TrialTest = false;
    $scope.category_text_counter = 0;
    $scope.category_counter = 0;
    $scope.memory_counter = 0;
    $scope.memory_type_counter = 0;
    $scope.startLang = true;
    $scope.startmemory2 == false;
    $scope.timer = 0;
    $scope.raw = {};
    $ionicScrollDelegate.scrollTop();
    $scope.trialCounts = 0;
  }

  $scope.relapse = {
    relapseInfection1: null,
    relapseInfection2: null,
    relapseInfection3: null,
    relapseInfection4: null,
  };

  $scope.prePMSurveyAnswers = [
    {"question":"Did you take your medications as usual?", "answer":null},
    {"question":"Did you get a typical amount of sleep last night?", "answer":null},
    {"question":"Do you have 6-8 minutes in a low distraction environment to focus on today's tests?", "answer":null},
    {"question":"Do you feel any of these factors could impact your performance test today?", "answer":null}
  ];

  $scope.recommendation = function() {
    $scope.recommendForPm = '';
    var button = '<div class="popBodyInput orange"><div class="item item-input item-borderless"><div class="radioButtonGroup"><label class="questionLabel ">Did you take your medications as usual?</label>' +

      '<div class="radioButtonWrapper yes"><input class="radioButton" type="radio" name="preRelapse1" ng-value=true ng-model="relapse.relapseInfection1"><label class="radioLabel">  Yes </label></div>' +

      '<div class="radioButtonWrapper no"><input class="radioButton" type="radio" name="preRelapse1" ng-value=false ng-model="relapse.relapseInfection1"><label class="radioLabel">  No </label></div></div></div>' +

      '<div class="item item-input item-borderless"><div class="radioButtonGroup"><label class="questionLabel ">Did you get a typical amount of sleep last night?</label>' +

      '<div class="radioButtonWrapper yes"><input class="radioButton" type="radio" name="preRelapse2" ng-value=true ng-model="relapse.relapseInfection2"><label class="radioLabel">  Yes </label></div>' +

      '<div class="radioButtonWrapper no"><input class="radioButton" type="radio" name="preRelapse2" ng-value=false ng-model="relapse.relapseInfection2"><label class="radioLabel">  No </label></div></div></div>' +

      '<div class="item item-input item-borderless"><div class="radioButtonGroup"><label class="questionLabel ">Do you have 6-8 minutes in a low distraction environment to focus on today' + "'s " + 'tests?</label>' +

      '<div class="radioButtonWrapper yes"><input class="radioButton" type="radio" name="preRelapse3" ng-value=true ng-model="relapse.relapseInfection3"><label class="radioLabel">  Yes </label></div>' +

      '<div class="radioButtonWrapper no"><input class="radioButton" type="radio" name="preRelapse3" ng-value=false ng-model="relapse.relapseInfection3"><label class="radioLabel">  No </label></div></div></div>' +

      '<div class="item item-input item-borderless"><div class="radioButtonGroup"><label class="questionLabel ">Do you feel any of these factors could impact your performance test today?</label>' +

      '<div class="radioButtonWrapper yes"><input class="radioButton" type="radio" name="preRelapse4" ng-value=true ng-model="relapse.relapseInfection4"><label class="radioLabel">  Yes </label></div>' +

      '<div class="radioButtonWrapper no"><input class="radioButton" type="radio" name="preRelapse4" ng-value=false ng-model="relapse.relapseInfection4"><label class="radioLabel">  No </label></div></div></div></div>' +

      '<div ng-show="relapse.relapseInfection1!=null && relapse.relapseInfection2!=null && relapse.relapseInfection3!=null && relapse.relapseInfection4!=null"><div ng-show="relapse.relapseInfection1==true && relapse.relapseInfection2==true && relapse.relapseInfection3==true && relapse.relapseInfection4==false"><div class="height20"></div><div class="popup-buttons" ><button class="button button-block button-darkPurple font40" ng-click="takeTestNow(true)">GREAT! LET' + "'" + 'S START TODAY' + "'" + 'S TASKS.</button></div><div class="height20"></div>' +

      '</div>' +

      '<div ng-show="!(relapse.relapseInfection1==true && relapse.relapseInfection2==true && relapse.relapseInfection3==true && relapse.relapseInfection4==false)"><p class="text-center italicFont smallText">You' + "'ve" + ' indicated that some factors may affect your performance on today' + "'" + 's tasks.</p><div class="popup-buttons" ><button class="button button-block button-outline button-darkPurple font40 skip" ng-click="recommendPopup.close()">TAKE TEST <br/>LATER</button>' +

      '<div class="height10"></div><button class="button button-block button-multiline button-darkPurple font40" ng-click="takeTestNow(false)">I' + "'" + 'D RATHER TAKE <br/>THE TEST NOW</button></div></div></div></div>';

    var template = '<p class="text-center">Performance can be influenced by many factors, including health, medical treatments, and life events, even having a ' + '"bad day".' + '</p><a class="popClose" ng-click="recommendPopup.close()"></a><p class="text-center">We want you to do the best you can on the performance measures.  So tell us if today is a typical day for you by answering the questions below.</p>' + button;
    $timeout(function() {
      $scope.recommendPopup = $ionicPopup.show({
        title: "Performance Metric Testing",
        template: template,
        scope: $scope,
        cssClass: "pmStartPop"
      });
      closePopupService.register($scope.recommendPopup);
      $scope.recommendPopup.then(function(res) {
        if (!$scope.takeTestNowPopupClose) {
          $state.go('app.performanceEvals');
        }
      });
    }, 100);

    $scope.takeTestNow = function(status) {
      if (status) {
        $scope.recommendForPm = "yes";
      } else {
        $scope.recommendForPm = "no";
      }
      $scope.recommendPopup.close();
      $scope.takeTestNowPopupClose = true;

      for(var i = 0; i < $scope.prePMSurveyAnswers.length; i++){
        $scope.prePMSurveyAnswers[i].answer = "no";
        if ($scope.relapse["relapseInfection"+(i+1)] == true){
          $scope.prePMSurveyAnswers[i].answer = "yes";
        }
      }

      // console.log($scope.recommendForPm);
    }

  };

}]);

// Resulted Json

// {
//   "PMType": "Word Match and Memory",
//   "date": "2016-07-15T09:30:36.268Z",
//   "raw": {
//     "languageP1": [
//       {"id": 13, "text": "Beans", "category": "Desserts", "status": false, "responseTime": 1623.9500000000007, "responseType": false, "result": "correct"}
//     ],
//     "languageP2": [
//       {"id": 1, "text": "Beans", "category": "Vegetables", "status": true, "responseTime": 1032.795000000002, "responseType": false, "result": "inCorrect"}
//     ],
//     "languageP3": [
//       {"id": 25, "text": "Beans", "category": "Bathroom Items", "status": false, "responseTime": 1144.4200000000055, "responseType": false, "result": "correct"}
//     ],
//     "memoryP1": [
//       {"id": 1, "text": "Fish", "status": false, "responseTime": 1022.1950000000361, "responseType": false, "result": "correct"},
//       {"id": 2, "text": "Carrots", "status": false, "responseTime": 495.2950000000128, "responseType": false, "result": "correct"}
//     ],
//     "memoryP2": [
//       {"id": 1, "text": "Fish", "status": false, "responseTime": 1022.1950000000361, "responseType": false, "result": "correct"},
//       {"id": 2, "text": "Carrots", "status": false, "responseTime": 495.2950000000128, "responseType": false, "result": "correct"}
//     ]
//   },
//   "score": {"yesCorrectClicks": 1, "noCorrectClicks": 71, "yesIncorrectClicks": 0, "noIncorrectClicks": 23, "percentCorrect": 75, "yesCorrectClickslang": 0, "noCorrectClickslang": 24, "yesIncorrectClickslang": 0, "noIncorrectClickslang": 12, "percentCorrectlang": 67 }
// }
