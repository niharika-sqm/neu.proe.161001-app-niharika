neuroScienceApp.controller('symbolDigitCtrl', ['$scope', '$state', '$interval', '$ionicPopup', '$kinvey', 'CONSTANT', '$q', '$stateParams','$ionicScrollDelegate','closePopupService','$timeout', function($scope, $state, $interval, $ionicPopup, $kinvey, CONSTANT, $q, $stateParams,$ionicScrollDelegate,closePopupService,$timeout) {

	$timeout(function(){
    window.ga.trackView('Symbol Substitution Test');
  }, 1000);

	$scope.$on('$ionicView.beforeEnter', function() {
		$scope.getStarted = true;
		$scope.symbolTest = true;
		if (localStorage.getItem("SymbolTestJsonData")) {
			var symbolJsonData = localStorage.getItem("SymbolTestJsonData");
			$scope.parsedSymbolJsonData = JSON.parse(symbolJsonData);
			localStorage.removeItem("SymbolTestJsonData");
			if ($scope.parsedSymbolJsonData.attempts) {
				$scope.attempts = $scope.parsedSymbolJsonData.attempts;
				$scope.attempts2 = $scope.parsedSymbolJsonData.attempts2;
				$scope.notificationId = $scope.parsedSymbolJsonData.notificationId;
				$scope.testResultId = $scope.parsedSymbolJsonData.testResultId;
				$scope.testResultId2 = $scope.parsedSymbolJsonData.testResultId2;
				$scope.cycle = $scope.parsedSymbolJsonData.cycle;
				$scope.type = $scope.parsedSymbolJsonData.type;
				$scope.isPointGiven = $scope.parsedSymbolJsonData.isPointGiven;
				$scope.isTrailTaken2 = $scope.parsedSymbolJsonData.isTrailTaken2;
				$scope.isTrailTaken2Round2 = $scope.parsedSymbolJsonData.isTrailTaken2Round2;
				$scope.isTrailTaken2Round1 = $scope.parsedSymbolJsonData.isTrailTaken2Round1;
				$scope.isTrailTaken1 = $scope.parsedSymbolJsonData.isTrailTaken1;
				$scope.relapseTrackerId = $scope.parsedSymbolJsonData.relapseTrackerId;
			} else {
				$scope.attempts = 0;
				$scope.attempts2 = 0;
			}
		} else {
			if ($stateParams.test != null) {
				$scope.notificationId = $stateParams.test.notificationId;
				$scope.attempts = $stateParams.test.attempts;
				$scope.attempts2 = $stateParams.test.attempts2;
				$scope.testResultId = $stateParams.test.testResultId;
				$scope.testResultId2 = $stateParams.test.testResultId2;
				$scope.cycle = $stateParams.test.cycle;
				$scope.type = $stateParams.test.type;
				$scope.isPointGiven = $stateParams.test.isPointGiven;
				$scope.isTrailTaken2 = $stateParams.test.isTrailTaken2;
				$scope.isTrailTaken2Round2 = $stateParams.test.isTrailTaken2Round2;
				$scope.isTrailTaken2Round1 = $stateParams.test.isTrailTaken2Round1;
				$scope.isTrailTaken1 = $stateParams.test.isTrailTaken1;
				$stateParams.test.relapseTrackerId == undefined ? $scope.relapseTrackerId = "" : $scope.relapseTrackerId = $stateParams.test.relapseTrackerId;
				//delete $stateParams.test;
			}
		}
		$scope.recommendationPopup();
	});

	$scope.$on('$ionicView.afterEnter', function() {
		$scope.lockOrientation();
		$scope.hideLoader();
	});

	$scope.$on('$destroy',function(){
    $scope.unlockOrientation();
  });

	$scope.$on('$ionicView.enter', function() {
		$scope.BtnDisabledSymbolTest = false;
		initiate();
	});

	var t,t2, trialTimeout, changeClassInterval, t1,trialTimeoutCounter;

	$scope.relapse = {
		relapseInfection1: null,
		relapseInfection2: null,
		relapseInfection3: null,
		relapseInfection4: null,
	};

	$scope.prePMSurveyAnswers = [
		{"question":"Did you take your medications as usual?", "answer":null},
		{"question":"Did you get a typical amount of sleep last night?", "answer":null},
		{"question":"Do you have 6-8 minutes in a low distraction environment to focus on today's tests?", "answer":null},
		{"question":"Do you feel any of these factors could impact your performance test today?", "answer":null}
	];

	$scope.startSymbolSubstitutionTest = function() {
		if (typeof window.ga !== "undefined") {
      window.ga.trackEvent('Symbol Substitution  / Trail Making Screen', 'Symbol Substitution Test', 'Continue', 1);
    }
		$scope.symbolTest = false;
	};

	$scope.recommendationPopup = function() {

		$scope.recommendForPm = '';
		var button = '<div class="popBodyInput orange"><div class="item item-input item-borderless"><div class="radioButtonGroup"><label class="questionLabel ">Did you take your medications as usual?</label>' +

			'<div class="radioButtonWrapper yes"><input class="radioButton" type="radio" name="preRelapse1" ng-value=true ng-model="relapse.relapseInfection1"><label class="radioLabel">  Yes </label></div>' +

			'<div class="radioButtonWrapper no"><input class="radioButton" type="radio" name="preRelapse1" ng-value=false ng-model="relapse.relapseInfection1"><label class="radioLabel">  No </label></div></div></div>' +

			'<div class="item item-input item-borderless"><div class="radioButtonGroup"><label class="questionLabel ">Did you get a typical amount of sleep last night?</label>' +

			'<div class="radioButtonWrapper yes"><input class="radioButton" type="radio" name="preRelapse2" ng-value=true ng-model="relapse.relapseInfection2"><label class="radioLabel">  Yes </label></div>' +

			'<div class="radioButtonWrapper no"><input class="radioButton" type="radio" name="preRelapse2" ng-value=false ng-model="relapse.relapseInfection2"><label class="radioLabel">  No </label></div></div></div>' +

			'<div class="item item-input item-borderless"><div class="radioButtonGroup"><label class="questionLabel ">Do you have 6-8 minutes in a low distraction environment to focus on today' + "'s " + 'tests?</label>' +

			'<div class="radioButtonWrapper yes"><input class="radioButton" type="radio" name="preRelapse3" ng-value=true ng-model="relapse.relapseInfection3"><label class="radioLabel">  Yes </label></div>' +

			'<div class="radioButtonWrapper no"><input class="radioButton" type="radio" name="preRelapse3" ng-value=false ng-model="relapse.relapseInfection3"><label class="radioLabel">  No </label></div></div></div>' +

			'<div class="item item-input item-borderless"><div class="radioButtonGroup"><label class="questionLabel ">Do you feel any of these factors could impact your performance test today?</label>' +

			'<div class="radioButtonWrapper yes"><input class="radioButton" type="radio" name="preRelapse4" ng-value=true ng-model="relapse.relapseInfection4"><label class="radioLabel">  Yes </label></div>' +

			'<div class="radioButtonWrapper no"><input class="radioButton" type="radio" name="preRelapse4" ng-value=false ng-model="relapse.relapseInfection4"><label class="radioLabel">  No </label></div></div></div></div>' +

			'<div ng-show="relapse.relapseInfection1!=null && relapse.relapseInfection2!=null && relapse.relapseInfection3!=null && relapse.relapseInfection4!=null"><div ng-show="relapse.relapseInfection1==true && relapse.relapseInfection2==true && relapse.relapseInfection3==true && relapse.relapseInfection4==false"><div class="height20"></div><div class="popup-buttons" ><button class="button button-block button-darkPurple font40" ng-click="takeTestNow(true)">GREAT! LET' + "'" + 'S START TODAY' + "'" + 'S TASKS.</button></div><div class="height20"></div>' +

			'</div>' +

			'<div ng-show="!(relapse.relapseInfection1==true && relapse.relapseInfection2==true && relapse.relapseInfection3==true && relapse.relapseInfection4==false)"><p class="text-center italicFont smallText">You'+"'ve"+' indicated that some factors may affect your performance on today' + "'" + 's tasks.</p><div class="popup-buttons" ><button class="button button-block button-outline button-darkPurple font40 skip" ng-click="recommendPopup.close()">TAKE TEST <br/>LATER</button>' +

			'<div class="height10"></div><button class="button button-block button-multiline button-darkPurple font40" ng-click="takeTestNow(false)">I' + "'" + 'D RATHER TAKE <br/>THE TEST NOW</button></div></div></div></div>';

		var template = '<p class="text-center">Performance can be influenced by many factors, including health, medical treatments, and life events, even having a ' + '"bad day".' +'</p><a class="popClose" ng-click="recommendPopup.close()"></a><p class="text-center">We want you to do the best you can on the performance measures.  So tell us if today is a typical day for you by answering the questions below.</p>' + button;
		$timeout(function() {
			$scope.recommendPopup = $ionicPopup.show({
				title: "Performance Metric Testing",
				template: template,
				scope: $scope,
				cssClass: "pmStartPop"
			});
			closePopupService.register($scope.recommendPopup);
			$scope.recommendPopup.then(function(res) {
				if (!$scope.takeTestNowPopupClose) {
					$state.go('app.performanceEvals');
				}
			});
		}, 100);
		$scope.takeTestNow = function(status) {
			console.log($scope.relapse);
			console.log(status);
			if (status) {
				$scope.recommendForPm = "yes";
			} else {
				$scope.recommendForPm = "no";
			}
			$scope.recommendPopup.close();

			for(var i = 0; i < $scope.prePMSurveyAnswers.length; i++){
				var tempAns = "no";
				if ($scope.relapse["relapseInfection"+(i+1)] == true){
					tempAns = "yes"
				}
				$scope.prePMSurveyAnswers[i].answer = tempAns;
			}

			$scope.takeTestNowPopupClose = true;
			console.log($scope.recommendForPm);
		}
	}

	$scope.cancelBtn = function() {
		if($scope.trialRunning){
			$scope.onCancelCall();
			return;
		}
		else{
			if(t!=undefined){
				$interval.cancel(t);
				t = undefined;
			}
			if(t2!=undefined){
				$interval.cancel(t2);
				t2 = undefined;
			}
		}
		if ($scope.attempts == 2) {
      $scope.cancelTemplate = '<p style="text-align: center;">If you cancel the test, it will count as one of your test takes. You will only have one additional chance to retake the test.</p><p  class="text-center">A test should be cancelled only if something went wrong; please don’t re-take the test just to improve your score, or to practice.</p><a class="popClose" ng-click="cancelTestPopup.close()"></a>';
    } else if ($scope.attempts == 1) {
      $scope.cancelTemplate = '<p style="text-align: center;">If you cancel the test, it will count as one of your test takes. You will only have two additional chances to retake the test.</p><p  class="text-center">A test should be cancelled only if something went wrong; please don’t re-take the test just to improve your score, or to practice.</p><a class="popClose" ng-click="cancelTestPopup.close()"></a>';
    }
		$scope.cancelTestPopup = $ionicPopup.show({
			title: "Are you sure you want to cancel the test?",
			template: $scope.cancelTemplate,
			scope: $scope,
			buttons: [{
				text: 'YES, CANCEL',
				type: 'button-block button-outline button-darkPurple skip',
				onTap: function() {
					return true;
				}
			}, {
				text: 'RETURN TO<br/> THE TEST',
				type: 'button-block button-darkPurple',
				onTap: function() {
					return false;
				}
			}]
		});
		closePopupService.register($scope.cancelTestPopup);
		$scope.cancelTestPopup.then(function(res) {
			if (res) {
				if (typeof window.ga !== "undefined") {
			          window.ga.trackEvent('Symbol Substitution Screen', 'Symbol Substitution Screen - Cancel Popup', 'Yes, Cancel', 1);
			        }
				//initiate();
				$scope.onCancelCall();
				$state.go('app.performanceEvals');
			}
			else{
				if (typeof window.ga !== "undefined") {
			          window.ga.trackEvent('Symbol Substitution Screen', 'Return to the Test', '', 1);
			        }
				if($scope.takeTrial){
					$scope.startTrialTimeout(30000 - ($scope.countToCountdown * 1000));
				}
				else if($scope.countToCountdown){
					console.log($scope.countToCountdown);
					$scope.callTimer(90000 - ($scope.countToCountdown * 1000));
				}

			}
		});
	};

	$scope.onCancelCall = function(){
		$interval.cancel(t);
		$interval.cancel(trialTimeout);
		$interval.cancel(changeClassInterval);
		$interval.cancel(trialTimeoutCounter);
		$interval.cancel(t1);
		$scope.getStarted = true;
		$scope.symbolTest = true;
		$ionicScrollDelegate.scrollTop();
	}

	$scope.retakeTest = function() {
		if ($scope.attempts == 3) {
			$state.go('app.performanceEvals');
			return;
		}
		var template = '<a class="popClose" ng-click="retakeTestPopup.close()"></a><p class="text-center">If problems occurred while taking this test, you can retake it. However, this test is given to gain an accurate measurement of normal functioning, so please don’t re-take it just to improve your score or to practice.</p>';
		if ($scope.attempts == 2) {
			var title = "Are you sure you want to restart the test? You have 1 test remaining.";
			var buttons = [{ text: 'CONTINUE', type: 'button-block button-darkPurple', onTap: function() {
					return true; } }];
		} else {
			var title = "Are you sure you want to restart the test? You have 2 tests remaining.";
			var buttons = [{ text: 'CANCEL', type: 'button-block button-outline button-darkPurple skip', onTap: function() {
					return false; } }, { text: 'RETAKE TEST', type: 'button-block button-darkPurple', onTap: function() {
					return true; } }];
		}
		$scope.retakeTestPopup = $ionicPopup.show({
			title: title,
			template: template,
			scope: $scope,
			buttons: buttons
		});
		closePopupService.register($scope.retakeTestPopup);
		$scope.retakeTestPopup.then(function(res) {
			if (res) {
				$scope.getStarted = true;
				$scope.symbolTest = true;
				$scope.attempts = $scope.attempts + 1;
				$scope.attempts2 = $scope.attempts2+1;
				initiate();
				// $state.go('app.performanceEvals');
			}
		});
	};


	var initiate = function() {
		if ($scope.attempts >= 3) {
			$scope.BtnDisabledSymbolTest = true;
		}

		$scope.showTest1 = '';
		$scope.showTest2 = 'hide';
		$scope.showTest3 = 'hide';
		$scope.showTest4 = 'hide';
		$scope.showTest5 = 'hide';
		$scope.show1 = '';
		$scope.show2 = 'hide';
		$scope.show3 = 'hide';
		$scope.show4 = 'hide';
		$scope.show5 = 'hide';
		$scope.correct = 0;

		// changes
		var passed = 0;
		var imageTest, newArrTest = [],
			newArrTestKey = [];
		// end Changes

		var performanceTestResults = $kinvey.DataStore.getInstance('performanceTestResults', $kinvey.DataStoreType.Network);
		var notificationTestList = $kinvey.DataStore.getInstance('notifications', $kinvey.DataStoreType.Network);
		var jsonData = {},
			result = {},
			results = [];
		jsonData.PMType = "Symbol Substitution and Trail Making";
		jsonData.SubPMType = "Symbol Test";
		jsonData.SubPMTypeSecond = "Trail Test";
		jsonData.date = new Date();
		// jsonData.attempts = $scope.attempts;
		jsonData.score1 = {};
		jsonData.score1.correct = 0;
		jsonData.score1.incorrect = 0;
		var timePerNow1, timePerNow2, timePerNow, timeValue = false;
		var arr = [{
			symbol: "img/symbol/Symbol_1_500x500.png",
			value: 1
		}, {
			symbol: 'img/symbol/Symbol_2_500x500.png',
			value: 2
		}, {
			symbol: 'img/symbol/Symbol_7_500x500.png',
			value: 3
		}, {
			symbol: 'img/symbol/Symbol_4_500x500.png',
			value: 4
		}, {
			symbol: 'img/symbol/Symbol_5_500x500.png',
			value: 5
		}, {
			symbol: 'img/symbol/Symbol_6_500x500.png',
			value: 6
		}, {
			symbol: 'img/symbol/Symbol_3_500x500.png',
			value: 7
		}, {
			symbol: 'img/symbol/Symbol_8_500x500.png',
			value: 8
		}, {
			symbol: 'img/symbol/Symbol_9_500x500.png',
			value: 9
		}];

		$scope.sequence = [3, 2, 6, 2, 3, 1, 6, 2, 3, 8, 6, 4, 1, 2, 3, 6, 7, 1, 4, 5, 1, 8, 9, 5, 2, 4, 9, 1, 5, 8, 3, 7, 4, 1, 9, 3, 1, 8, 2, 6, 1, 2, 8, 6, 9, 7, 5, 4, 6, 1, 7, 8, 5, 4, 6, 9, 1, 8, 3, 4, 9, 7, 3, 5, 2, 6, 7, 9, 3, 4, 6, 1, 7, 2, 9, 3, 8, 6, 5, 1, 3, 5, 9, 7, 4, 9, 5, 8, 2, 7, 3, 2, 1, 4, 6, 8, 3, 2, 6, 1, 3, 2, 6, 7, 9, 4, 8, 1, 5, 7];

		$scope.trailSequence = [3, 2, 6, 2, 3, 1, 6, 2, 3, 8, 6, 4, 1, 2, 3, 6, 7, 1, 4, 5, 1, 8, 9, 5, 2, 4, 9, 1, 5, 8, 3, 7, 4, 1, 9, 3, 1, 8, 2, 6, 1, 2, 8, 6, 9, 7, 5, 4, 6, 1, 7, 8, 5, 4, 6, 9, 1, 8, 3, 4, 9, 7, 3, 5, 2, 6, 7, 9, 3, 4, 6, 1, 7, 2, 9, 3, 8, 6, 5, 1, 3, 5, 9, 7, 4, 9, 5, 8, 2, 7, 3, 2, 1, 4, 6, 8, 3, 2, 6, 1, 3, 2, 6, 7, 9, 4, 8, 1, 5, 7];

		var newarr = arr.slice(0);

		$scope.trialRun = function() {
			if (typeof window.ga !== "undefined") {
	      window.ga.trackEvent('Symbol Substitution Screen', 'Trail starts', 'Take A Trail', 1);
	    }
			$scope.symbolArrCounter = $scope.trailSequence.shift();
			$scope.takeTrial = true;
			$scope.trialRunning = true;
			$scope.getStarted = false;
			$scope.startTest = true;
			passed = 0;
			$scope.newArrTestKey = newarr.slice(0);
			if($scope.attempts == 0 && !$scope.isTrailTaken1){
				$scope.saveInitialTrailData();
			}
		}

		$scope.saveInitialTrailData = function(){
			$scope.showLoader();
			if($scope.testResultId == ""){
				performanceTestResults.save({
	        "date": new Date(),
	        "PMType": jsonData.PMType,
					"SubPMType": jsonData.SubPMType,
	        "attempts":0,
	        "cycle":$scope.cycle,
					"prePMSurveyAnswers":$scope.prePMSurveyAnswers,
	        "type":$scope.type,
	        "notificationId":$scope.notificationId,
	        "isTrial":false
		    }).then(function(entity) {
	        console.log("entity ",entity);
	        $scope.isTrailTaken1 = false;
	        $scope.testResultId  = entity._id;
	        $scope.saveInitialTrailData2();
		    }).catch(function(error) {
		      $scope.internalGlitch();
	      	$state.go('app.performanceEvals');
		    });
		  }else{
		  	performanceTestResults.save({
		  		"_id":$scope.testResultId,
	        "date": new Date(),
	        "PMType": jsonData.PMType,
					"SubPMType": jsonData.SubPMType,
					"prePMSurveyAnswers":$scope.prePMSurveyAnswers,
	        "attempts":0,
	        "cycle":$scope.cycle,
	        "type":$scope.type,
	        "notificationId":$scope.notificationId,
	        "isTrial":false
		    }).then(function(entity) {
	        console.log("entity ",entity);
	        $scope.isTrailTaken1 = false;
	        $scope.testResultId  = entity._id;
	        $scope.saveInitialTrailData2();
		    }).catch(function(error) {
		      $scope.internalGlitch();
	      	$state.go('app.performanceEvals');
		    });
		  }
		};

		$scope.saveInitialTrailData2 = function(){
			$scope.showLoader();
			if($scope.testResultId2 == ""){
				performanceTestResults.save({
	        "date": new Date(),
	        "PMType": jsonData.PMType,
					"SubPMType": jsonData.SubPMTypeSecond,
	        "attempts":$scope.attempts2,
	        "cycle":$scope.cycle,
					"prePMSurveyAnswers":$scope.prePMSurveyAnswers,
	        "type":$scope.type,
	        "notificationId":$scope.notificationId,
					"isTrial":$scope.isTrailTaken2,
					"isTrial1":$scope.isTrailTaken2Round1,
					"isTrial2":$scope.isTrailTaken2Round2
		    }).then(function(entity) {
	        console.log("entity ",entity);
	        $scope.testResultId2  = entity._id;
	        $scope.hideLoader();
		    }).catch(function(error) {
		      $scope.internalGlitch();
	      	$state.go('app.performanceEvals');
		    });
		  }else{
		  	performanceTestResults.save({
		  		"_id":$scope.testResultId2,
	        "date": new Date(),
	        "PMType": jsonData.PMType,
					"SubPMType": jsonData.SubPMTypeSecond,
	        "attempts":$scope.attempts2,
	        "cycle":$scope.cycle,
					"prePMSurveyAnswers":$scope.prePMSurveyAnswers,
	        "type":$scope.type,
	        "notificationId":$scope.notificationId,
					"isTrial":$scope.isTrailTaken2,
					"isTrial1":$scope.isTrailTaken2Round1,
					"isTrial2":$scope.isTrailTaken2Round2
		    }).then(function(entity) {
	        console.log("entity ",entity);
	        $scope.testResultId2  = entity._id;
	        $scope.hideLoader();
		    }).catch(function(error) {
		      $scope.internalGlitch();
	      	$state.go('app.performanceEvals');
		    });
		  }
		};

		$scope.saveFinalTrailData = function(){
			$scope.showLoader();
			performanceTestResults.save({
				"_id":$scope.testResultId,
        "date": new Date(),
        "PMType": jsonData.PMType,
				"SubPMType": jsonData.SubPMType,
        "attempts":$scope.attempts2,
				"prePMSurveyAnswers":$scope.prePMSurveyAnswers,
        "cycle":$scope.cycle,
        "type":$scope.type,
        "notificationId":$scope.notificationId,
        "isTrial":true
	    }).then(function(entity) {
        console.log("entity ",entity);
        $scope.isTrailTaken1 = true;
        $scope.testResultId  = entity._id;
        $scope.hideLoader();
	    }).catch(function(error) {
	      $scope.internalGlitch();
      	$state.go('app.performanceEvals');
	    });
		};

		function cloneMessage(result) {
			var clone = {};
			for (var key in result) {
				if (result.hasOwnProperty(key)) //ensure not adding inherited props
					clone[key] = result[key];
			}
			return clone;
		};

		$scope.pickRandomElementTest = function(value) {
			if (value != 0) {
				// console.log($scope.show5,value , $scope.div.value);
				if ($scope.showTest5 == 'hide') {
					return;
				}
				// console.log(value , $scope.div.value);
				$scope.startTest = false;
				if (value == $scope.div.value) {
					passed++;
					console.log('true', passed);
				} else {
					passed = 0;
					console.log('false');
				}
			} else {
				$scope.startTest = false;
				$scope.showTest1 = '';
				$scope.showTest2 = 'hide';
				$scope.showTest3 = 'hide';
				$scope.showTest4 = 'hide';
				$scope.showTest5 = 'hide';
				$scope.startTrialTimeout(30000);
				$scope.trailCounter = 0;
				changeClassInterval = $interval(changeClass, 1010);
				function changeClass() {
					if ($scope.showTest1 != 'hide') {
						$scope.showTest1 = 'hide';
						$scope.showTest2 = '';
					} else if ($scope.showTest2 != 'hide') {
						$scope.showTest2 = 'hide';
						$scope.showTest3 = '';
					} else if ($scope.showTest3 != 'hide') {
						$scope.showTest3 = 'hide';
						$scope.showTest4 = '';
					} else if ($scope.showTest4 != 'hide') {
						$scope.showTest4 = 'hide';
						$scope.showTest5 = '';
						$interval.cancel(changeClassInterval);
					}
				}
			}
			$scope.symbolArrCounter = $scope.trailSequence.shift();
			var randomElement = newarr[$scope.symbolArrCounter];
			//$scope.symbolArrCounter = $scope.symbolArrCounter + 1;
			console.log(randomElement);
			/*if ($scope.symbolArrCounter == 9) {
				$scope.symbolArrCounter = 0;
			}*/
			$scope.div = randomElement;
			var x = newarr.indexOf(randomElement);
			// newarr.splice(x,1);
			if (passed == 5) {
				$scope.stopSymbolTestTrial();
			}
		}

		$scope.startTrialTimeout = function(time) {
			trialTimeout = $interval(function() {
				if (passed != 5) {
					$scope.showMessage('Please read instructions and try again.', 3000);
					$scope.getStarted = true;
					// $scope.symbolTest = true;
					$interval.cancel(trialTimeout);
					trialTimeoutCounter = undefined;
					initiate();
				}
			}, time);

			trialTimeoutCounter = $interval(function() {
				$scope.trailCounter++;
				if ($scope.trailCounter > 31) {
					$interval.cancel(trialTimeoutCounter);
					trialTimeoutCounter = undefined;
				}
			}, 1000);
		};

		$scope.stopSymbolTestTrial = function() {
			$interval.cancel(t);
			$interval.cancel(trialTimeout);
			t = undefined;
			$scope.startSymbolTest(0);
			if($scope.attempts == 0 && !$scope.isTrailTaken1){
        $scope.saveFinalTrailData();
      }
		}

		var image, newArr = [],
			newArrKey = [];
		$scope.startSymbolTest = function(start) {
			console.log(newarr);
			$scope.takeTrial = false;
			$scope.getStarted = false;
			$scope.newArrKey = arr.slice(0);
			console.log(start);
			if (start == 0) {
				$scope.showCong = true;
				$scope.startTest = false;
			} else {
				if (typeof window.ga !== "undefined") {
		      window.ga.trackEvent('Symbol Substitution Screen', 'Test starts', 'Get Started', 1);
		    }
		    $scope.trialRunning = false;
				$scope.showCong = false;
				$scope.startTest = false;
				$scope.attempts++;
				$scope.attempts2++;
				if($scope.attempts == 3){
		      $scope.retakeCancelBtnDisabled = true;
		    }
				$scope.testStartedKinvey();
			}
		}

		$scope.testStartedKinvey = function() {
			$scope.showLoader();
			if($scope.attempts==1){
		    $scope.setPMNotificationIsShown($scope.notificationId);
		     if($scope.relapseTrack.startRelapse){
		      $scope.relapseTrackerId = $scope.relapseTrack.kinveyData._id;
		    }
		  }
			if ($scope.testResultId == "") {
				performanceTestResults.save({
					"date": new Date(),
					"PMType": jsonData.PMType,
					"SubPMType": jsonData.SubPMType,
					"attempts": $scope.attempts,
					"cycle": $scope.cycle,
					"type": $scope.type,
					"recommendForPm": $scope.recommendForPm,
					"prePMSurveyAnswers":$scope.prePMSurveyAnswers,
					"notificationId": $scope.notificationId,
					"isAborted": true,
					"isShown": $scope.attempts == 3 ? true : false,
					"isPointGiven":$scope.isPointGiven,
					"isTrial":$scope.isTrailTaken1,
					"relapseTrackerId":$scope.relapseTrackerId
				}).then(function(entity) {
					console.log("entity " , entity);
					$scope.testResultId = entity._id;
					if($scope.attempts == 1 && $scope.relapseTrack.startRelapse){
	          $scope.$emit('updateRelapseTracker',$scope.testResultId);
	        }
	        if ($scope.attempts >= 3) {
						$scope.updateNotificationCollection();
					}else{
						$scope.hideLoader();
					}
					$scope.testStartedKinvey2();
				}).catch(function(error) {
					$scope.internalGlitch();
					$state.go('app.performanceEvals');
				});
			} else {
				performanceTestResults.save({
					"_id": $scope.testResultId,
					"date": new Date(),
					"PMType": jsonData.PMType,
					"SubPMType": jsonData.SubPMType,
					"attempts": $scope.attempts,
					"cycle": $scope.cycle,
					"type": $scope.type,
					"recommendForPm": $scope.recommendForPm,
					"prePMSurveyAnswers":$scope.prePMSurveyAnswers,
					"notificationId": $scope.notificationId,
					"isAborted": true,
					"isShown": $scope.attempts == 3 ? true : false,
					"isPointGiven":$scope.isPointGiven,
					"isTrial":$scope.isTrailTaken1,
					"relapseTrackerId":$scope.relapseTrackerId
				}).then(function(entity) {
					console.log("entity " , entity);
					$scope.testResultId = entity._id;
					if ($scope.attempts >= 3) {
						$scope.updateNotificationCollection();
					}else{
						$scope.hideLoader();
					}
					$scope.testStartedKinvey2();
				}).catch(function(error) {
					$scope.internalGlitch();
					$state.go('app.performanceEvals');
				});
			}
		}

		$scope.testStartedKinvey2 = function() {
			if($scope.attempts==1){
		    //$scope.setPMNotificationIsShown($scope.notificationId);
		     if($scope.relapseTrack.startRelapse){
		      $scope.relapseTrackerId = $scope.relapseTrack.kinveyData._id;
		    }
		  }
			if ($scope.testResultId2 == "" || $scope.testResultId2 == undefined) {
				performanceTestResults.save({
					"date": new Date(),
					"PMType": jsonData.PMType,
					"SubPMType": jsonData.SubPMTypeSecond,
					"attempts": $scope.attempts2,
					"cycle": $scope.cycle,
					"type": $scope.type,
					"recommendForPm": $scope.recommendForPm,
					"prePMSurveyAnswers":$scope.prePMSurveyAnswers,
					"notificationId": $scope.notificationId,
					"isAborted": true,
					"isShown": $scope.attempts2 == 3 ? true : false,
					"isPointGiven":$scope.isPointGiven,
					"isTrial":$scope.isTrailTaken2,
					"isTrial1":$scope.isTrailTaken2Round1,
					"isTrial2":$scope.isTrailTaken2Round2,
					"relapseTrackerId":$scope.relapseTrackerId
				}).then(function(entity) {
					console.log("entity " , entity);
					$scope.testResultId2 = entity._id;
	        if($scope.attempts2 == 1 && $scope.relapseTrack.startRelapse){
	          $scope.$emit('updateRelapseTracker',$scope.testResultId2);
	        }
					if ($scope.attempts2 >= 3) {
						$scope.updateNotificationCollection();
					}
					else{
						$scope.hideLoader();
					}
				}).catch(function(error) {
					$scope.internalGlitch();
					console.log("error " + error);
				});
			} else {
				var tempData = {};
				var query = new $kinvey.Query();
				query.equalTo("_id", $scope.testResultId2);
				performanceTestResults.find(query).subscribe(function(data) {
					tempData = data
				}, function(error) {
						$scope.internalGlitch();
				}, function(data) {
						tempData = tempData[0]
						tempData.attempts = $scope.attempts2;
						tempData.date = new Date();
						tempData.recommendForPm = $scope.recommendForPm;
						tempData.prePMSurveyAnswers = $scope.prePMSurveyAnswers;
						tempData.isPointGiven = $scope.isPointGiven;
						tempData.relapseTrackerId = $scope.relapseTrackerId;
						tempData.isTrial = $scope.isTrailTaken2;
						tempData.isTrial1 = $scope.isTrailTaken2Round1,
						tempData.isTrial2 = $scope.isTrailTaken2Round2,
						tempData.isAborted = (tempData.isAborted == undefined ? true : tempData.isAborted);
						tempData.isShown = $scope.attempts2 == 3 ? true : false;
						performanceTestResults.save(tempData).then(function(entity) {
						console.log("entity ", entity);
						$scope.testResultId2 = entity._id;
						if ($scope.attempts2 >= 3) {
							$scope.updateNotificationCollection();
						}
						else{
							$scope.hideLoader();
						}
					}).catch(function(error) {
						$scope.internalGlitch();
					});
				});


			}
		}

		$scope.updateNotificationCollection = function() {
			var tempdata = [];
			var col = "_id";
			var relatedJson = $scope.notificationId;
			var query = new $kinvey.Query();
			query.equalTo(col, relatedJson);
			notificationTestList.find(query).subscribe(function(data) {
				tempdata = data;
			}, function(error) {}, function(data) {
				tempdata[0].isAck = true;
				notificationTestList.update(tempdata[0]).then(function(data) {
					$scope.hideLoader();
					console.log(data);
					$scope.hideLoader();
				}).catch(function(error) {
					console.log(error);
					$scope.internalGlitch();
				});
			});
		}

		$scope.startMainTestAfterTrail = function() {
			// $scope.attempts++;
			// $scope.testStartedKinvey();
			// $scope.pickRandomElement(0);
			$scope.startSymbolTest();
		}

		$scope.callTimer = function (time) {
			console.log(time);
			t = $interval(function() {
					$interval.cancel(t);
					t = undefined;
					$scope.stopSymbolTest();
			}, time);
			t2 = $interval(function() {
				console.log($scope.countToCountdown); //pause test timmer
				$scope.countToCountdown++;
				if($scope.countToCountdown >= 90){
					$interval.cancel(t2);
					t2 = undefined;
				}
			}, 1000);
		}

		$scope.pickRandomElement = function(value) {
			// To begin the 90 sec countdown.
			if(value == 0) {
				$scope.countToCountdown = 0;
			}
			if (!timeValue) {
				timePerNow1 = performance.now();
				$scope.callTimer(90000)
			}
			timeValue = true; // make the variable stop for again start the countdown.

			if (value != 0) {
				console.log($scope.show5);
				if ($scope.show5 == 'hide') {
					return;
				}
				timePerNow2 = performance.now();
				timePerNow = timePerNow2 - timePerNow1;
				if (timeValue) {
					timePerNow1 = performance.now();
				}
				console.log('timePerNow', timePerNow);
				result.actual = $scope.div.value;
				result.response = value;
				if (value == $scope.div.value) {
					console.log('true');
					jsonData.score1.correct = jsonData.score1.correct + 1;
					$scope.correct = jsonData.score1.correct;
					result.responseTime = timePerNow;
					result.status = 1;
				} else {
					console.log('false');
					jsonData.score1.incorrect = jsonData.score1.incorrect + 1;
					result.responseTime = timePerNow;
					result.status = 0;
				}
				results.push(cloneMessage(result));
			} else {
				$scope.startTest = true;
				$scope.show2 = 'hide';
				$scope.show3 = 'hide';
				$scope.show4 = 'hide';
				$scope.show5 = 'hide';
				t1 = $interval(changeClass, 1000);

				function changeClass() {
					if ($scope.show1 != 'hide') {
						$scope.show1 = 'hide';
						$scope.show2 = '';
					} else if ($scope.show2 != 'hide') {
						$scope.show2 = 'hide';
						$scope.show3 = '';
					} else if ($scope.show3 != 'hide') {
						$scope.show3 = 'hide';
						$scope.show4 = '';
					} else if ($scope.show4 != 'hide') {
						$scope.show4 = 'hide';
						$scope.show5 = '';
						$interval.cancel(t1);
					}
				}
			}

			// TODO : Modify this part with new logic.
			// var randomElement = arr[Math.floor(Math.random() * arr.length)];
			// $scope.div = randomElement;
			// var x = arr.indexOf(randomElement);
			// arr.splice(x, 1);

			$scope.div = getSymbol($scope.sequence[0]);
			$scope.sequence.shift();
		}
		function getSymbol(value) {
	    	for(var i=0; i<arr.length; i++){
	    		if(arr[i].value == value){
	    			return arr[i];
	    		}
	    	}
		}

		function cloneMessage(result) {
			var clone = {};
			for (var key in result) {
				if (result.hasOwnProperty(key)) //ensure not adding inherited props
					clone[key] = result[key];
			}
			return clone;
		};

		$scope.stopSymbolTest = function() {
			// $scope.symbolTest = true;
			jsonData.raw = results;
			jsonData.attempts = $scope.attempts;
			console.log(jsonData);
			jsonData.recommendForPm = $scope.recommendForPm;
			jsonData.prePMSurveyAnswers = $scope.prePMSurveyAnswers;
			jsonData.notificationId = $scope.notificationId;
			jsonData.attempts = $scope.attempts;
			jsonData.attempts2 = $scope.attempts2;
			jsonData.testResultId = $scope.testResultId;
			jsonData.prePMSurveyAnswers = $scope.prePMSurveyAnswers;
			jsonData.testResultId2 = $scope.testResultId2;
			jsonData.cycle = $scope.cycle;
			jsonData.type = $scope.type;
			jsonData.isPointGiven=$scope.isPointGiven;
			jsonData.relapseTrackerId=$scope.relapseTrackerId;
			jsonData.isTrailTaken1=$scope.isTrailTaken1;
			jsonData.isTrailTaken2=$scope.isTrailTaken2;
			jsonData.isTrailTaken2Round1 = $scope.isTrailTaken2Round1;
			jsonData.isTrailTaken2Round2 = $scope.isTrailTaken2Round2;
			localStorage.setItem("SymbolTestJsonData", JSON.stringify(jsonData));
			$state.go('app.trailTest');
		}
	};

}]);



// Resulted Json

// {
//   "PMType": "Symbol Substitution and Trail Making",
//   "date": "2016-06-20T11:33:06.565Z",
//   "attempts": 1,
//   "score1": {
//     "correct": 1,
//     "incorrect": 2
//   },
//   "raw": [
//     {
//       "actual": 4,
//       "response": 5,
//       "responseTime": 5.8400000000001455,
//       "status": 0
//     },
//     {
//       "actual": 8,
//       "response": 7,
//       "responseTime": 0.4750000000003638,
//       "status": 0
//     },
//     {
//       "actual": 1,
//       "response": 1,
//       "responseTime": 0.4149999999990541,
//       "status": 1
//     }
//   ]
// }
