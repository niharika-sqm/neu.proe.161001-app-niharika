neuroScienceApp.controller('heatAlertsCtrl', ['$scope', '$state','$ionicPopup','$kinvey','$q','utility','CONFIG','CONSTANT','closePopupService','$timeout', function ($scope, $state,$ionicPopup,$kinvey,$q,utility,CONFIG,CONSTANT,closePopupService,$timeout) {

  $timeout(function(){
    window.ga.trackView('Heat Alert Landing Screen');
  }, 1000);

  var notificationHistory = $kinvey.DataStore.getInstance('notificationHistory', $kinvey.DataStoreType.Network);
  var notifications = $kinvey.DataStore.getInstance('notifications', $kinvey.DataStoreType.Network);  
  $scope.getMyHeatAlert = function(){
    $scope.showLoader(); 
    $scope.activeUserInfo = null;
    $scope.heatSence = {"temperatureMax":"","apparentTemperatureMax":"","humidity":"","active":false,"city":"","state":""};
    var promise = new $q(function(resolve) {resolve($kinvey.User.getActiveUser().me());}); 
    var countHealtAlert = 0;
    promise.then(function(user){
      if(user.data.heatAlerts !=undefined) {
        $scope.heatAlerts = user.data.heatAlerts;
        $scope.heatSence.state = user.data.heatAlerts.state;
        $scope.heatSence.city = user.data.heatAlerts.city;
        $scope.temperatureOptions[0].checked = true;
        if(user.data.heatAlerts.maxTemperature && user.data.heatAlerts.maxTemperature!=""){ 
          $scope.temperatureOptions[user.data.heatAlerts.maxTemperature.id-1].checked = true;
        }
        return user;
      }
      else{
        $scope.hideLoader();
        return user;
      }   
    }).then(function(user){
      /*call forcast service*/
      $scope.user = user;
      $scope.notifications = false;
      if($scope.user.data.notificationSettings == undefined){
        $scope.notifications = true;
      }
      else if($scope.user.data.notificationSettings.heatAlerts == true){
        $scope.notifications = true;
      }
      $scope.getNotificationNumberFromNotification(user);
      if(user!=undefined && user.data.heatAlerts && user.data.heatAlerts.latitude!="" && user.data.heatAlerts.longitude!=""){
        CONFIG.heatAlerts.latitude = user.data.heatAlerts.latitude;
        CONFIG.heatAlerts.longitude = user.data.heatAlerts.longitude;
        $scope.getCityCoordinate($scope.heatAlerts.city+", "+$scope.heatAlerts.state,true);
      }
      else{
        $scope.hideLoader(); 
      }     
    }).catch(function(error){
      $scope.internalGlitch();
    });
  }

  $scope.heatAlertsInit = function(){
    // $scope.unlockOrientation();
    $scope.showLoader();
    $scope.heatAlertfirst = false;
    $scope.heatAlertChange = false;
    $scope.getLocationForUser = false;
    $scope.temperatureOptions = [{id:1,text:"NO ALERT SET",checked:true}]
    for(var i=50; i<=130; i++){
      $scope.temperatureOptions.push({id:i-48,text:i,checked:false});
    }
    $scope.heatAlerts = {"maxTemperature":"","latitude":"","longitude":""};
    $scope.heatNotificationNumber = 0;
    var heatData=[];
    $scope.getMyHeatAlert();
  }

  $scope.getNotificationNumberFromNotification = function(user){
    var query = new $kinvey.Query();
    var col = "_acl";
    var relatedJson = {"creator":user.data._id};
    query.equalTo(col, relatedJson);
    var query1= new $kinvey.Query();
    query1.equalTo("notificationType", "HEATALERT");
    query2= new $kinvey.Query();
    query2.equalTo("isSent", true);
    query.and(query1.and(query2));
    var col = "_kmd"
    notifications.find(query).subscribe(function(data) {      
      heatData = data;      
    }, function(error) {
      $scope.internalGlitch();
    }, function(data) {
      if(heatData.length>0)$scope.heatNotificationNumber += heatData.length;
      $scope.getNotificationNumberFromNotificationHistory(user)
    });
  }

  $scope.getNotificationNumberFromNotificationHistory = function(user){
    var query = new $kinvey.Query();
    var col = "_acl";
    var relatedJson = {"creator":user.data._id};
    query.equalTo(col, relatedJson);
    var query1= new $kinvey.Query();
    query1.equalTo("notificationType", "HEATALERT");
    query.and(query1);
    var col = "_kmd"
    //var relatedJson = {"lmt":moment().startOf('isoweek').format("YYYY-MM-DD") + "T00:00:00.000Z"}; // monday date 
    var query2 = new $kinvey.Query();
    query2.greaterThanOrEqualTo("_kmd.lmt", moment().startOf('isoweek').format("YYYY-MM-DD") + "T00:00:00.000Z");   
    //relatedJson = {"lmt":moment(moment(), "DD-MM-YYYY").add(1, 'days').format("DD-MM-YYYY") + "T00:00:00.000Z"}; // Tomorrow date (12:00 AM)
    var query3 = new $kinvey.Query();
    query3.lessThanOrEqualTo("_kmd.lmt", moment(moment(), "YYYY-MM-DD").add(1, 'days').format("YYYY-MM-DD") + "T00:00:00.000Z");
    query2.and(query3);
    query.and(query2);
    notificationHistory.find(query).subscribe(function(data) {      
      heatData = data;      
    }, function(error) {
      $scope.internalGlitch();
    }, function(data) {
      if(heatData.length>0)$scope.heatNotificationNumber += heatData.length;
    });
  }

  $scope.heatAlertsInit();

  document.addEventListener("deviceready", onDeviceReady, false);
  function onDeviceReady() {
    console.log("navigator.geolocation works well");
  }
   $scope.getCityName = function(){
    var latlng = new google.maps.LatLng( $scope.heatAlerts.latitude, $scope.heatAlerts.longitude);
    var geocoderlatLong;
    geocoderlatLong = new google.maps.Geocoder();
    geocoderlatLong.geocode(
        {'latLng': latlng}, 
        function(results, status) {
          if (status == google.maps.GeocoderStatus.OK) {
            if (results[0]) {
                var add= results[0].formatted_address ;
                var  value=add.split(",");
                count=value.length;
                $scope.heatAlerts.state = value[count-2].replace(/[0-9]/g, '');
                $scope.heatAlerts.state = $scope.heatAlerts.state.trim();
                $scope.heatAlerts.city = value[count-3];
                $scope.heatAlerts.city = $scope.heatAlerts.city.trim();
                $scope.heatSence.state = $scope.heatAlerts.state;
                $scope.heatSence.city = $scope.heatAlerts.city; 
                $scope.getCityCoordinate($scope.heatAlerts.city+", "+$scope.heatAlerts.state);                 
            }
            else {
                $scope.hideLoader();
                alert("address not found");
            }
          }
           else {
              //alert("Geocoder failed due to: " + status);
              $scope.internalGlitch();
          }
          //$scope.getTempFromForecast();
        }
    );  
  }

  $scope.getCityCoordinate = function(address,first){
    var geocoderForCity;
    geocoderForCity = new google.maps.Geocoder();
    geocoderForCity.geocode({
    'address': address
    }, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
        $scope.heatAlerts.cityLat = results[0].geometry.location.lat();
        $scope.heatAlerts.cityLng = results[0].geometry.location.lng();
        $scope.checkLatLongAtKinvey(first);
      } else {
        console.log("Something got wrong " + status);
      }
    });
  }

  $scope.checkLatLongAtKinvey = function(first){
    var user = $kinvey.User.getActiveUser();
    var userObj = {"id":$scope.user._id,"maxTempSet":""}
    if($scope.heatAlerts.maxTemperature && first != true && $scope.notifications){
      var remove =false;
      var add = true;
      if($scope.heatAlerts.maxTemperature.id == 1) {
        userObj.maxTempSet = "";
        remove =true;
        add = false;
      }
      else{
        userObj.maxTempSet = $scope.heatAlerts.maxTemperature.text;
        remove =false;
        add = true;
      }    
      var promise = $kinvey.CustomEndpoint.execute('latLongApi', {
        latitude: $scope.heatAlerts.cityLat,
        longitude:$scope.heatAlerts.cityLng,
        checkLatLong: true,
        remove:remove,
        add:add,
        user:userObj
      });
      promise.then(function(response) { 
        if(response.maxTemp == undefined){
          $scope.updateHeatAlertonKinvey("temperature");
        } 
        else if(response.maxTemp && response.maxTemp!=""){
          $scope.heatSence.temperatureMax = response.maxTemp;
          $scope.heatSence.apparentTemperatureMax = response.maxTempApparent;
          $scope.heatSence.humidity = response.humidity;
          $scope.heatSence.active = true;
          $scope.heatAlerts.temperatureMax = response.maxTemp;
          $scope.heatAlerts.apparentTemperatureMax = response.maxTempApparent;
          $scope.heatAlerts.humidity = response.humidity;
          if(first == undefined){
            $scope.updateHeatAlertonKinvey("cordinates");
          }
          else{
            $scope.updateHeatAlertonKinvey("temperature");
          }
        }
      }).catch(function(err) {
        $scope.hideLoader();  
      });
    }
    else{
      var promise = $kinvey.CustomEndpoint.execute('latLongApi', {
        latitude: $scope.heatAlerts.cityLat,
        longitude:$scope.heatAlerts.cityLng,
        checkLatLong: true
      });
      promise.then(function(response) {  
        if(response.maxTemp && response.maxTemp!=""){
          $scope.heatSence.temperatureMax = response.maxTemp;
          $scope.heatSence.apparentTemperatureMax = response.maxTempApparent;
          $scope.heatSence.humidity = response.humidity;
          $scope.heatSence.active = true;
          $scope.heatAlerts.temperatureMax = response.maxTemp;
          $scope.heatAlerts.apparentTemperatureMax = response.maxTempApparent;
          $scope.heatAlerts.humidity = response.humidity;
          $scope.updateHeatAlertonKinvey("cordinates");
        }
      }).catch(function(err) {
        $scope.updateHeatAlertonKinvey("cordinates");
        $scope.hideLoader();  
      });
    }
  }

  function geolocationSuccess(position) {
    $scope.heatAlerts.latitude = position.coords.latitude;
    $scope.heatAlerts.longitude = position.coords.longitude;
    CONFIG.heatAlerts.latitude = position.coords.latitude;
    CONFIG.heatAlerts.longitude = position.coords.longitude;
    $scope.getCityName();
  }

  function geolocationError(error) {
    if(error.code == 3 && error.message == "Timeout expired" && countHealtAlert<4){
      countHealtAlert++;
      navigator.geolocation.getCurrentPosition(geolocationSuccess, geolocationError,{timeout: 5000, enableHighAccuracy: false});
    }
    else{
      $scope.$emit('emptySingleSelectedArray');
      if($scope.heatAlerts.maxTemperature!=undefined && $scope.heatAlerts.maxTemperature!=""){
        $scope.temperatureOptions[$scope.heatAlerts.maxTemperature.id-1].checked = false;
        delete($scope.heatAlerts.maxTemperature);
      }  
      $scope.$apply();
      $scope.hideLoader();
    }
  }

  $scope.getLocation = function(){ 
    if (typeof window.ga !== "undefined") {
      window.ga.trackEvent('Heat Alert - Update Location', 'Get user current location and save', 'Update Location', 1);
    }
    cordova.plugins.locationAccuracy.request(function(){
        console.log("Successfully made request to invoke native Location Services dialog");
        $scope.showLoader();
        $scope.locationForHeatAlert();
    }, function(error){
      if(error == "Location services is already enabled"){
          $scope.locationForHeatAlert();
        }
        else{
          console.error("Accuracy request failed: error code="+error.code+"; error message="+error.message);
          if(error.code !== cordova.plugins.locationAccuracy.ERROR_USER_DISAGREED){
            if(window.confirm("Failed to automatically set Location Mode to 'High Accuracy'. Would you like to switch to the Location Settings page and do this manually?")){
              cordova.plugins.diagnostic.switchToLocationSettings();
            }
          }else{
            geolocationError(error);
          }
        }
    }, cordova.plugins.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY);
  }

  $scope.locationForHeatAlert = function(){
    navigator.geolocation.getCurrentPosition(geolocationSuccess, geolocationError,{timeout: 5000, enableHighAccuracy: false});
    countHealtAlert = 1;
  }

  $scope.openSingleSelect = function(options, popupType, screenType, title, index, key) {
    $scope.typeSingleSelect = popupType;
    $scope.optionSingleSelect = options;
    $scope.screenTypeSingleSelect = screenType;
    $scope.index = index;
    $scope.key = key;       
    var singleSelectPopup = $ionicPopup.show({
      title: title,
      templateUrl: 'templates/genericTemplate/singleSelectPopup.html',
      cssClass: "selectBoxPopup",
      scope: $scope,
      buttons: [{
        text: 'Cancel',
        type: 'button-block button-outline button-darkPurple',
        onTap: function() {
          return false;
        }
      }, {
        text: 'Okay',
        type: 'button-block button-outline button-darkPurple',
        onTap: function() {
          return true;
        }
      }]
    });
    closePopupService.register(singleSelectPopup);
    singleSelectPopup.then(function(res) {
      console.log($scope.singleSelection, res);
      if (res) {
        if (typeof window.ga !== "undefined") {
          window.ga.trackEvent('Heat Alert - Current Temperature Alert Set To', 'Save alert value and close pop up', 'Save', 1);
        }
        $scope.heatAlerts.maxTemperature = $scope.singleSelection;
        $scope.$emit('emptySingleSelectedArray');
        for (var i = 0; i < $scope.optionSingleSelect.length; i++) {
          if($scope.optionSingleSelect[i].id == $scope.heatAlerts.maxTemperature.id) {
            $scope.optionSingleSelect[i].checked = true;
          }
          else{
            $scope.optionSingleSelect[i].checked = false;
          }
        }
        if($scope.heatAlerts.maxTemperature.id != 1){
          $scope.heatAlertChange = true;
        }
        if($scope.heatAlerts.latitude == "" || $scope.heatAlerts.longitude == "" ){
          $scope.getLocation();
        }
        else{
          $scope.checkLatLongAtKinvey("temperature");
        }       
      } else {
        if (typeof window.ga !== "undefined") {
          window.ga.trackEvent('Heat Alert - Current Temperature Alert Set To', 'Save alert value and close pop up', 'Cancel', 1);
        }
        $scope.$emit('emptySingleSelectedArray');
        // for (var i = 0; i < $scope.optionSingleSelect.length; i++) {
        //   if($scope.optionSingleSelect[i].checked == true) {
        //     $scope.optionSingleSelect[i].checked = false;
        //     break;
        //   }
        // }
      } 
    });
  }
  var usersDS = new $kinvey.UserStore();

  $scope.updateHeatAlertonKinvey = function(type){
    $scope.showLoader();
    var promise = new $q(function(resolve) {
      resolve($kinvey.User.getActiveUser().me());
    });
    promise.then(function(user) {
      return user;
    }).then(function(user) {
      if(type == "cordinates") {
        $scope.heatAlerts.entryDate = moment().format("YYYY-MM-DD");
      }       
      if(($scope.heatAlertChange == true && user.data.heatAlerts == undefined)){
        $scope.heatAlertfirst = true;
      }
      else if($scope.heatAlertChange == true && user.data.heatAlerts.isPoint == undefined ){
        $scope.heatAlertfirst = true;
      }
      else{
        $scope.heatAlertfirst = false;
      }
      user.data.heatAlerts = JSON.parse(angular.toJson($scope.heatAlerts)); 
      if($scope.heatAlertfirst == true) {
        user.data.heatAlerts.isPoint = true;
      }
      usersDS.update(user.data)
      .then(function (user) {
         return user;
      }).then(function(user) {
        if(user){
          $scope.hideLoader();
          if($scope.heatAlertfirst){
            $scope.$emit('saveGamificationPoints',CONSTANT.gamificationPoints.activateWeatherAlerts.value,CONSTANT.gamificationPoints.activateWeatherAlerts.name);
          }
          $scope.heatAlerts = user.heatAlerts;
          console.log(JSON.stringify(user));
        }
      }).catch(function (error) {
        console.log(error)
      });
    }).catch(function(error){
      console.log(error);
      $scope.hideLoader();
      $scope.internalGlitch();
    });
  }
  
  $scope.getTempFromForecast = function(){
    utility.forecast()
    .then(function(data){
      $scope.hideLoader();
      if(data!=undefined){
        $scope.heatSence.temperatureMax = data.daily.data[0].temperatureMax;
        $scope.heatSence.apparentTemperatureMax = data.daily.data[0].apparentTemperatureMax;
        $scope.heatSence.humidity = data.daily.data[0].humidity;
        $scope.heatSence.active = true;
        $scope.heatAlerts.temperatureMax = data.daily.data[0].temperatureMax;
        $scope.heatAlerts.apparentTemperatureMax = data.daily.data[0].apparentTemperatureMax;
        $scope.heatAlerts.humidity = data.daily.data[0].humidity;
        $scope.updateHeatAlertonKinvey("cordinates"); 
      }
      $scope.hideLoader();
      
    },function(data){
      $scope.internalGlitch();
      $scope.hideLoader();
    });
  }
  
}]);