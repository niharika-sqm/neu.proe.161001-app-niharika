neuroScienceApp.controller('profileMsHistoryCtrl', ['$scope', '$state', '$ionicPopup', '$kinvey', '$ionicScrollDelegate','closePopupService','$timeout', function($scope, $state, $ionicPopup, $kinvey, $ionicScrollDelegate,closePopupService,$timeout) {

  $timeout(function(){
    window.ga.trackView('Profile Settings - MS History');
  }, 1000);

  //$scope.accordian.current = "";
  $scope.accordian.current = "mshistory1";
  $scope.numMSRelapsesLastYearOptions = [];
  var personalInfoHistory = $kinvey.DataStore.getInstance('personalInfoHistory', $kinvey.DataStoreType.Network);
  $scope.profileMsHistoryInit = function() {
    $scope.infoRegistration.msHistoryA = angular.copy($scope.activeUserInfo.info.msHistoryA);
    $scope.infoRegistration.msHistoryB = angular.copy($scope.activeUserInfo.info.msHistoryB);
    // debugger
    // if ($scope.activeUserInfo.info.msHistoryB.dateOfLastRelapse != undefined && $scope.activeUserInfo.info.msHistoryB.dateOfLastRelapse != null) {
    //   $scope.dateOfLastRelapse.year = parseInt(moment(new Date($scope.activeUserInfo.info.msHistoryB.dateOfLastRelapse)).format("YYYY"));
    //   $scope.dateOfLastRelapse.month = parseInt(moment(new Date($scope.activeUserInfo.info.msHistoryB.dateOfLastRelapse)).format("MM"));
    // }
    if ($scope.infoRegistration.msHistoryB.numMSRelapsesLifeTime && $scope.infoRegistration.msHistoryB.numMSRelapsesLifeTime.range != undefined) {
      $scope.numMSRelapsesLastYearOptions = [];
      for (var i = 0; i <= $scope.infoRegistration.msHistoryB.numMSRelapsesLifeTime.range; i++) {
        $scope.numMSRelapsesLastYearOptions.push({ id: i + 1, text: i, checked: false });
      }
    }
    $scope.errorOfLastRelapse = {};
    $scope.popupValidate = false;
    $scope.dateError = false;
  }

  $scope.$on('updateRelapsesLastYearOptions', function(event, data) {
    $scope.numMSRelapsesLastYearOptions = [];
    for (var i = 0; i <= data; i++) {
      $scope.numMSRelapsesLastYearOptions.push({
        id: i + 1,
        text: i,
        checked: false
      });
    }
  });

  $scope.profileMsHistoryInit();

  //Part2

  $scope.resetHistoryPart2 = function() {
    if (typeof window.ga !== "undefined") {
      window.ga.trackEvent('Profile Settings - MS History and Status', 'Profile Settings Screen (MS History and Status)', 'Cancel', 1);
    }
    form2.$submitted = false;
    //$scope.infoRegistration.msHistoryB.overallAbility = angular.copy($scope.activeUserInfo.info.msHistoryB.overallAbility);
    $ionicScrollDelegate.scrollTop();
    $scope.initialState();
    $state.go('app.profileSettings');
  }

  $scope.openOverallAbility = function(detail, title, cssClass) {
    $scope.overallAbilityDetail = detail;
    $scope.currentOverallAbility = title;
    $scope.overallAbilityPopup = $ionicPopup.show({
      title: title.toUpperCase(),
      templateUrl: 'templates/authenticationModules/registrationModule/overallAbilityPopup.html',
      cssClass: "appStart " + cssClass,
      scope: $scope
    });
    closePopupService.register($scope.overallAbilityPopup);
  }

  if($scope.infoRegistration.msHistoryB && $scope.infoRegistration.msHistoryB.overallAbility){
    $scope.previousOverallValue = $scope.infoRegistration.msHistoryB.overallAbility;
  }

  $scope.confirmOverallAbility = function(){
    $scope.previousOverallValue = $scope.currentOverallAbility;
    $scope.infoRegistration.msHistoryB.overallAbility = $scope.currentOverallAbility;
    $scope.overallAbilityPopup.close();
  }

  $scope.cancelOverallAbility = function(){
    $scope.infoRegistration.msHistoryB.overallAbility = $scope.previousOverallValue;
    $scope.overallAbilityPopup.close();
  }

  var form2 = "";
  $scope.submitHistoryPart2 = function(formData) {
    if (typeof window.ga !== "undefined") {
      window.ga.trackEvent('Profile Settings - Personal and Account Info', 'Update and Profile Settings Screen (MS History and Status)', 'Update', 1);
    }
    form2 = formData;
    if (formData.$valid) {
      $scope.showLoader();
      var overallAbilityHistoryObj = {"to":{},"from":{}};
      var usersDS = new $kinvey.UserStore();
      var user = $kinvey.User.getActiveUser();
      if (user) {
        if(user.data.info.msHistoryB.overallAbility != $scope.infoRegistration.msHistoryB.overallAbility){
          overallAbilityHistoryObj = {"to":{},"from":{}};
          overallAbilityHistoryObj.to.overallAbility = $scope.infoRegistration.msHistoryB.overallAbility;
          overallAbilityHistoryObj.to.date = new Date();
          overallAbilityHistoryObj.from.overallAbility = user.data.info.msHistoryB.overallAbility;
          overallAbilityHistoryObj.from.date = user.data.personalInfoCompTime;
        }else{
          overallAbilityHistoryObj = "";
        }

        user.data.info.msHistoryB.overallAbility = $scope.infoRegistration.msHistoryB.overallAbility;
        usersDS.update(user.data)
          .then(function(user) {
            if(overallAbilityHistoryObj == ""){
              $scope.hideLoader();
              $scope.$emit('reloadAllProfileSettings', user);
              $ionicScrollDelegate.scrollTop();
              $state.go('app.profileSettings');
            }
            else{
              $scope.updateOverallAbilityHistory(overallAbilityHistoryObj,user._id);
            }
            console.log(JSON.stringify(user));
          }).catch(function(error) {
            if(error && error.message == "errorOccurred"){
              $scope.infoEmptyToast();
            }else{
              console.log(error);
              $scope.internalGlitch();
            } 
          });
      }
    }
  }



  $scope.updateOverallAbilityHistory = function(overallAbilityHistoryObj,userId){
    console.log("OverallAbility Updating");
    var query = new $kinvey.Query();
    query.contains("_acl", {"creator":userId});
    personalInfoHistory.find(query).subscribe(function(data) {
      $scope.userHistoryInfo = data;
    }, function(error) {
      $scope.internalGlitch();
    }, function(data) {
      if($scope.userHistoryInfo && $scope.userHistoryInfo.length>0){
        if($scope.userHistoryInfo[0].overallAbilityHistory && $scope.userHistoryInfo[0].overallAbilityHistory.length>0){
          overallAbilityHistoryObj.from.date = $scope.userHistoryInfo[0].overallAbilityHistory[$scope.userHistoryInfo[0].overallAbilityHistory.length-1].to.date;
        }
        else {
          $scope.userHistoryInfo[0].overallAbilityHistory = [];
        }
        $scope.userHistoryInfo[0].overallAbilityHistory.push(overallAbilityHistoryObj);
        personalInfoHistory.update($scope.userHistoryInfo).then(function(data) {
          $scope.hideLoader();
          $scope.$emit('reloadAllProfileSettings');
          $ionicScrollDelegate.scrollTop();
          console.log("OverallAbility Updated");
          $state.go('app.profileSettings');
        }).catch(function(error) {
          $scope.internalGlitch();
          console.log(error);
        });
      }else{
        var createArray = [];
        createArray.push(overallAbilityHistoryObj);
        personalInfoHistory.save({"overallAbilityHistory":createArray}).then(function(entity) {
          $scope.hideLoader();
          $scope.$emit('reloadAllProfileSettings');
          $ionicScrollDelegate.scrollTop();
          console.log("OverallAbility Updated");
          $state.go('app.profileSettings');
        }).catch(function(error) {
          $scope.internalGlitch();
          console.log(error);
        });
      }   
    });
  }

  //Part1

  $scope.resetHistoryPart1 = function() {
    form1.$submitted = false;
    if (typeof window.ga !== "undefined") {
      window.ga.trackEvent('Profile Settings - MS History and Status', 'Profile Settings Screen (MS History and Status)', 'Cancel', 1);
    }
    var dummy = $scope.infoRegistration.msHistoryB.overallAbility;
    $scope.profileMsHistoryInit();
    $scope.infoRegistration.msHistoryB.overallAbility = dummy;
    for(var i=0;i<$scope.familyMemberRelationshipOptions.length;i++){
      if($scope.familyMemberRelationshipOptions[i].checked == true) $scope.familyMemberRelationshipOptions[i].checked = false;
    }
    $ionicScrollDelegate.scrollTop();
    $scope.initialState();
    $state.go('app.profileSettings');
  }

  $scope.setWindowScroll = function(){
    $ionicScrollDelegate.resize();
  }


  $scope.openRelapseInfoPopup = function() {
    $scope.relapsePopup = $ionicPopup.show({
      title: "<div class='relapseHistoryicon' ><i class='icon'></i> </div> Relapse?",
      templateUrl: 'templates/authenticationModules/registrationModule/relapsePopup.html',
      cssClass: "appStart",
      scope: $scope
    });
    closePopupService.register($scope.relapsePopup);
  }

  $scope.changeDateOfLastRelapse = function() {
    $scope.errorOfLastRelapse.date = false;
    $scope.dateError = false;
  }

  $scope.$watch(
    function(scope) {
      return $scope.infoRegistration.msHistoryB.numMSRelapsesLastYear },
    function(newValue, oldValue) {
      if ($scope.infoRegistration.msHistoryB.numMSRelapsesLastYear && $scope.infoRegistration.msHistoryB.numMSRelapsesLastYear.text == '0') {
        $scope.dateError = false;
      }
    }
  );

  var form1 = "";
  $scope.submitHistoryPart1 = function(formData) {
    if (typeof window.ga !== "undefined") {
      window.ga.trackEvent('Profile Settings - Personal and Account Info', 'Update and Profile Settings Screen (MS History and Status)', 'Update', 1);
    }
    form1 = formData;
    var info = $scope.infoRegistration.msHistoryB;
    if (formData.month && (formData.month.$error.required || formData.year.$error.required)) {
      $scope.dateError = true;
    }
    // debugger
    // if ($scope.dateOfLastRelapse.month) {
    //   $scope.dateOfLastRelapse.month = "0" + $scope.dateOfLastRelapse.month;
    // }
    // if (info.numMSRelapsesLastYear && info.numMSRelapsesLastYear.text == '0') {
    //   if (moment($scope.dateOfLastRelapse.year + ' ' + ($scope.dateOfLastRelapse.month), "YYYY MM", true).isValid() && moment($scope.dateOfLastRelapse.year + ' ' + ($scope.dateOfLastRelapse.month), "YYYY MM", true).diff(moment()) < 0) {
    //     //$scope.dateOfLastRelapse.month++;
    //     $scope.infoRegistration.msHistoryB.dateOfLastRelapse = moment($scope.dateOfLastRelapse.year + ' ' + ($scope.dateOfLastRelapse.month), "YYYY MM");
    //     $scope.dateError = false;
    //     $scope.errorOfLastRelapse.date = false;
    //   } else {
    //     $scope.errorOfLastRelapse.date = true;
    //     return;
    //   }
    // } else {
    //   $scope.infoRegistration.msHistoryB.dateOfLastRelapse = null;
    // }
    $scope.popupValidate = true;
    // if (!info.numMSRelapsesLifeTime || !info.numMSRelapsesLastYear || ((info.numMSRelapsesLastYear && info.numMSRelapsesLastYear.text == '0') && !info.dateOfLastRelapse)) {
    //   console.log("novalidated");
    //   return;
    // } else {
    //   $scope.popupValidate = true;
    // }
    if (formData.$valid && $scope.popupValidate) {
      $scope.showLoader();
      var usersDS = new $kinvey.UserStore();
      var currentDiagnosisHistoryObj = { "to": {}, "from": {} };
      var user = $kinvey.User.getActiveUser();
      if (user) {
        $scope.infoRegistration.msHistoryA = JSON.parse(angular.toJson($scope.infoRegistration.msHistoryA));
        $scope.infoRegistration.msHistoryB = JSON.parse(angular.toJson($scope.infoRegistration.msHistoryB));
        user.data.info.msHistoryA.ageAtDiagnosis = $scope.infoRegistration.msHistoryA.ageAtDiagnosis;
        user.data.info.msHistoryA.ageOfFirstSymptoms = $scope.infoRegistration.msHistoryA.ageOfFirstSymptoms;
        // Current Diagnosis check
        if (user.data.info.msHistoryA.currentDiagnosis && user.data.info.msHistoryA.currentDiagnosis.text != $scope.infoRegistration.msHistoryA.currentDiagnosis.text) {
          currentDiagnosisHistoryObj = { "to": {}, "from": {} };
          currentDiagnosisHistoryObj.to.currentDiagnosis = $scope.infoRegistration.msHistoryA.currentDiagnosis;
          currentDiagnosisHistoryObj.to.date = new Date();
          currentDiagnosisHistoryObj.from.currentDiagnosis = user.data.info.msHistoryA.currentDiagnosis;
          currentDiagnosisHistoryObj.from.date = user.data.personalInfoCompTime;
        } else {
          currentDiagnosisHistoryObj = "";
        }
        user.data.info.msHistoryA.currentDiagnosis = $scope.infoRegistration.msHistoryA.currentDiagnosis;
        user.data.info.msHistoryA.familyHistoryOfMS = $scope.infoRegistration.msHistoryA.familyHistoryOfMS;
        user.data.info.msHistoryA.initialDiagnosis = $scope.infoRegistration.msHistoryA.initialDiagnosis;
        user.data.info.msHistoryB.dateOfLastRelapse = $scope.infoRegistration.msHistoryB.dateOfLastRelapse;
        user.data.info.msHistoryB.numMSRelapsesLastYear = $scope.infoRegistration.msHistoryB.numMSRelapsesLastYear;
        user.data.info.msHistoryB.numMSRelapsesLifeTime = $scope.infoRegistration.msHistoryB.numMSRelapsesLifeTime;
        user.data.info.msHistoryB.steroidsUsedInLastRelapse = $scope.infoRegistration.msHistoryB.steroidsUsedInLastRelapse;
        if ($scope.infoRegistration.msHistoryA.familyHistoryOfMS.status == "yes" && $scope.infoRegistration.msHistoryA.familyMemberRelationship) {
          user.data.info.msHistoryA.familyMemberRelationship = JSON.parse(angular.toJson($scope.infoRegistration.msHistoryA.familyMemberRelationship));
        }
        usersDS.update(user.data)
          .then(function(user) {
            if(currentDiagnosisHistoryObj == ""){
              $scope.hideLoader();
              $scope.$emit('reloadAllProfileSettings', user);
              $ionicScrollDelegate.scrollTop();
              $state.go('app.profileSettings');
              console.log(JSON.stringify(user));
            }
            else{
              $scope.updateCurrentDiagnosisHistory(currentDiagnosisHistoryObj,user._id);
            }
          }).catch(function(error) {
            if(error && error.message == "errorOccurred"){
              $scope.infoEmptyToast();
            }else{
              console.log(error);
              $scope.internalGlitch();
            } 
          });
      }
    }
  }

  $scope.updateCurrentDiagnosisHistory = function(currentDiagnosisHistoryObj,userId){
    console.log("Current Diagnosis Updating");
    var query = new $kinvey.Query();
    query.contains("_acl", {"creator":userId});
    personalInfoHistory.find(query).subscribe(function(data) {
      $scope.userHistoryInfo = data;
    }, function(error) {
      $scope.internalGlitch();
    }, function(data) {
      if($scope.userHistoryInfo && $scope.userHistoryInfo.length>0){
        if($scope.userHistoryInfo[0].currentDiagnosisHistory && $scope.userHistoryInfo[0].currentDiagnosisHistory.length>0){
          currentDiagnosisHistoryObj.from.date = $scope.userHistoryInfo[0].currentDiagnosisHistory[$scope.userHistoryInfo[0].currentDiagnosisHistory.length-1].to.date;
        }
        else {
          $scope.userHistoryInfo[0].currentDiagnosisHistory = [];
        }
        $scope.userHistoryInfo[0].currentDiagnosisHistory.push(currentDiagnosisHistoryObj);
        personalInfoHistory.update($scope.userHistoryInfo).then(function(data) {
          $scope.hideLoader();
          $scope.$emit('reloadAllProfileSettings');
          $ionicScrollDelegate.scrollTop();
          console.log("Current Diagnosis Updated");
          $state.go('app.profileSettings');
        }).catch(function(error) {
          $scope.internalGlitch();
          console.log(error);
        });
      }else{
        var createArray = [];
        createArray.push(currentDiagnosisHistoryObj);
        personalInfoHistory.save({"currentDiagnosisHistory":createArray}).then(function(entity) {
          $scope.hideLoader();
          $scope.$emit('reloadAllProfileSettings');
          $ionicScrollDelegate.scrollTop();
          console.log("Current Diagnosis Updated");
          $state.go('app.profileSettings');
        }).catch(function(error) {
          $scope.internalGlitch();
          console.log(error);
        });
      }   
    });
  }

  $scope.navigateTo = function(){
    if (typeof window.ga !== "undefined") {
      window.ga.trackEvent('Profile Settings - MS History and Status', 'Profile Settings Screen (MS History and Status)', 'Back', 1);
    }
    $scope.initialState();
    $state.go('app.profileSettings');
  }

}]);
