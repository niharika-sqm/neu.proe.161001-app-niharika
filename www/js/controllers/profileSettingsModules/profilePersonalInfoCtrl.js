neuroScienceApp.controller('profilePersonalInfoCtrl', ['$scope', '$state', '$q', '$kinvey', '$ionicScrollDelegate', '$ionicHistory', '$timeout', function($scope, $state, $q, $kinvey, $ionicScrollDelegate, $ionicHistory, $timeout) {
  $ionicScrollDelegate.resize();

  $timeout(function(){
    window.ga.trackView('Profile Settings - Personal Information');
  }, 1000);
  
  //personal
  $scope.alreadyExist = false;
  //$scope.accordian.current = "";
  $scope.accordian.current = "personal1";
  var personalInfoHistory = $kinvey.DataStore.getInstance('personalInfoHistory', $kinvey.DataStoreType.Network);
  $scope.personalInfoInit = function() {
    $scope.personalInfo = {
      "firstName": angular.copy($scope.activeUserInfo.first_name),
      "lastName": angular.copy($scope.activeUserInfo.lastname),
      "datedob": { 'month': "", "date": "", "year": "" },
      "completedate": ""
    };
    $scope.personalInfo.datedob.month = angular.copy(moment($scope.activeUserInfo.info.personalInformation.dob).format("MM"));
    $scope.personalInfo.datedob.date = angular.copy(moment($scope.activeUserInfo.info.personalInformation.dob).format("DD"));
    $scope.personalInfo.datedob.year = angular.copy(moment($scope.activeUserInfo.info.personalInformation.dob).format("YYYY"));
    $scope.infoRegistration.personalInformation.gender = angular.copy($scope.activeUserInfo.info.personalInformation.gender);
    $scope.infoRegistration.personalInformation.race = angular.copy($scope.activeUserInfo.info.personalInformation.race);
    $scope.infoRegistration.personalInformation.insurance = angular.copy($scope.activeUserInfo.info.personalInformation.insurance);
    $scope.infoRegistration.personalInformation.employmentStatus = angular.copy($scope.activeUserInfo.info.personalInformation.employmentStatus);
    $scope.infoRegistration.personalInformation.education = angular.copy($scope.activeUserInfo.info.personalInformation.education);
    $scope.infoRegistration.personalInformation.preferHand = angular.copy($scope.activeUserInfo.info.personalInformation.preferHand);
    $scope.error = {};
    $scope.popupValidate = false;
  }

  $scope.personalInfoInit();
  $scope.changeDob = function() {
    $scope.error.date = false;
  }

  $scope.resetSecondPersonalInfo = function() {
    if (typeof window.ga !== "undefined") {
      window.ga.trackEvent('Profile Settings - Personal and Account Info', 'Profile Settings Screen (Personal and Account Info)', 'Cancel', 1);
    }
    form2.$submitted = false;
    //$scope.personalInfoInit();
    $ionicScrollDelegate.scrollTop();
    $scope.initialState();
    $state.go('app.profileSettings');
  }

  var form2 = "";
  $scope.submitSecondPersonalInfo = function(formData) {
    form2 = formData;
    if (typeof window.ga !== "undefined") {
      window.ga.trackEvent('Profile Settings - Personal and Account Info', 'Update and Profile Settings Screen (Personal and Account Info)', 'Update', 1);
    }
    var year = Number($scope.personalInfo.datedob.year);
    var month = Number($scope.personalInfo.datedob.month);
    var date = Number($scope.personalInfo.datedob.date);
    // HAI: Doing minus 1 in the second condition in if because it is inside the array. It takes month strats from 0 and ends at 11.
    if (moment(year + ' ' + (month <= 9 ? '0' + month : month) + ' ' + (date <= 9 ? '0' + date : date), "YYYY MM DD", true).isValid() &&
      moment().diff(moment([year, month - 1, date]), 'years') >= 18 && moment().diff(moment([year, month - 1, date]), 'years') < 110) {
      $scope.personalInfo.completedate = moment(year + ' ' + month + ' ' + date, "YYYY MM DD");
    } else {
      $scope.error.date = true;
      return;
    }

    var info = $scope.infoRegistration.personalInformation;
    if (!info.race || info.race.length == 0) {
      console.log("novalidated");
      return;
    } else {
      $scope.popupValidate = true;
    }

    if (formData.$valid && $scope.popupValidate) {
      $scope.showLoader();
      var usersDS = new $kinvey.UserStore();
      var insuranceHistoryObj = { "to": {}, "from": {} };
      var employmentStatusHistoryObj = { "to": {}, "from": {} };
      var user = $kinvey.User.getActiveUser();
      if (user) {
        $scope.infoRegistration.personalInformation = JSON.parse(angular.toJson($scope.infoRegistration.personalInformation));
        user.data.first_name = $scope.personalInfo.firstName;
        user.data.lastname = $scope.personalInfo.lastName;
        user.data.info.personalInformation.dob = $scope.personalInfo.completedate;
        user.data.info.personalInformation.gender = $scope.infoRegistration.personalInformation.gender;
        user.data.info.personalInformation.preferHand = $scope.infoRegistration.personalInformation.preferHand
        user.data.info.personalInformation.race = JSON.parse(angular.toJson($scope.infoRegistration.personalInformation.race));
        // Insurance check
        if (user.data.info.personalInformation.insurance.text != $scope.infoRegistration.personalInformation.insurance.text) {
          insuranceHistoryObj = { "to": {}, "from": {} }
          insuranceHistoryObj.to.insurance = $scope.infoRegistration.personalInformation.insurance;
          insuranceHistoryObj.to.date = new Date();
          insuranceHistoryObj.from.insurance = user.data.info.personalInformation.insurance;
          insuranceHistoryObj.from.date = user.data.personalInfoCompTime;
        } else {
          insuranceHistoryObj = "";
        }
        user.data.info.personalInformation.insurance = $scope.infoRegistration.personalInformation.insurance;
        // Employment Status check
        if (user.data.info.personalInformation.employmentStatus.text != $scope.infoRegistration.personalInformation.employmentStatus.text) {
          employmentStatusHistoryObj = { "to": {}, "from": {} }
          employmentStatusHistoryObj.to.employmentStatus = $scope.infoRegistration.personalInformation.employmentStatus;
          employmentStatusHistoryObj.to.date = new Date();
          employmentStatusHistoryObj.from.employmentStatus = user.data.info.personalInformation.employmentStatus;
          employmentStatusHistoryObj.from.date = user.data.personalInfoCompTime;
        } else {
          employmentStatusHistoryObj = "";
        }
        user.data.info.personalInformation.employmentStatus = $scope.infoRegistration.personalInformation.employmentStatus;
        user.data.info.personalInformation.education = $scope.infoRegistration.personalInformation.education;
        usersDS.update(user.data)
          .then(function(user) {
            if (employmentStatusHistoryObj == "" && insuranceHistoryObj == "") {
              $scope.hideLoader();
              //$scope.activeUserInfo = user;
              $scope.$emit('reloadAllProfileSettings', user);
              $ionicScrollDelegate.scrollTop();
              $state.go('app.profileSettings');
            } else {
              $scope.updatePersonalHistory(user._id, employmentStatusHistoryObj, insuranceHistoryObj);
            }
            console.log(JSON.stringify(user));
          }).catch(function(error) {
            if(error && error.message == "errorOccurred"){
              $scope.infoEmptyToast();
            }else{
              console.log(error);
              $scope.internalGlitch();
            }  
          });
      }
    }
  }

  $scope.updatePersonalHistory = function(userId, employmentStatusHistoryObj, insuranceHistoryObj) {
    console.log("Personal History Updating");
    var query = new $kinvey.Query();
    query.contains("_acl", { "creator": userId });
    personalInfoHistory.find(query).subscribe(function(data) {
      $scope.userHistoryInfo = data;
    }, function(error) {
      $scope.internalGlitch();
    }, function(data) {
      if ($scope.userHistoryInfo && $scope.userHistoryInfo.length > 0) {
        if($scope.userHistoryInfo[0].employmentStatusHistory && $scope.userHistoryInfo[0].employmentStatusHistory.length > 0 && employmentStatusHistoryObj != "") {
          employmentStatusHistoryObj.from.date = $scope.userHistoryInfo[0].employmentStatusHistory[$scope.userHistoryInfo[0].employmentStatusHistory.length - 1].to.date;
          $scope.userHistoryInfo[0].employmentStatusHistory.push(employmentStatusHistoryObj);
        }
        else if(employmentStatusHistoryObj != ""){
        	$scope.userHistoryInfo[0].employmentStatusHistory =[];
        	$scope.userHistoryInfo[0].employmentStatusHistory.push(employmentStatusHistoryObj);
        }
        if ($scope.userHistoryInfo[0].insuranceHistory && $scope.userHistoryInfo[0].insuranceHistory.length > 0 && insuranceHistoryObj != "") {
          insuranceHistoryObj.from.date = $scope.userHistoryInfo[0].insuranceHistory[$scope.userHistoryInfo[0].insuranceHistory.length - 1].to.date;
          $scope.userHistoryInfo[0].insuranceHistory.push(insuranceHistoryObj);
        }
        else if (insuranceHistoryObj != ""){
        	$scope.userHistoryInfo[0].insuranceHistory =[];
        	$scope.userHistoryInfo[0].insuranceHistory.push(insuranceHistoryObj);
        }
        personalInfoHistory.update($scope.userHistoryInfo).then(function(data) {
          $scope.hideLoader();
          $scope.$emit('reloadAllProfileSettings');
          $ionicScrollDelegate.scrollTop();
          console.log("Personal History Updated");
          $state.go('app.profileSettings');
        }).catch(function(error) {
          $scope.internalGlitch();
          console.log(error);
        });
      } else {
        if (insuranceHistoryObj != "") {
          var insuranceArray = [];
          insuranceArray.push(insuranceHistoryObj);
        } else {
          var insuranceArray = undefined;
        }
        if (employmentStatusHistoryObj != "") {
          var employmentStatusArray = [];
          employmentStatusArray.push(employmentStatusHistoryObj);
        } else {
          var employmentStatusArray = undefined;
        }
        personalInfoHistory.save({
          "insuranceHistory": insuranceArray,
          "employmentStatusHistory": employmentStatusArray
        }).then(function(entity) {
          $scope.hideLoader();
          $scope.$emit('reloadAllProfileSettings');
          $ionicScrollDelegate.scrollTop();
          console.log("Personal History Updated");
          $state.go('app.profileSettings');
        }).catch(function(error) {
          $scope.internalGlitch();
          console.log(error);
        });
      }
    });
  }

  //Address
  $scope.addressInfoInit = function() {
    $scope.infoRegistration.personalInformation.zipCode = angular.copy($scope.activeUserInfo.info.personalInformation.zipCode);
    $scope.activeUserInfo.info.personalInformation.state != undefined ? $scope.infoRegistration.personalInformation.state = angular.copy($scope.activeUserInfo.info.personalInformation.state) : $scope.infoRegistration.personalInformation.state = "";
    $scope.activeUserInfo.info.personalInformation.city != undefined ? $scope.infoRegistration.personalInformation.city = angular.copy($scope.activeUserInfo.info.personalInformation.city) : $scope.infoRegistration.personalInformation.city = "";
    $scope.activeUserInfo.info.personalInformation.address1 != undefined ? $scope.infoRegistration.personalInformation.address1 = angular.copy($scope.activeUserInfo.info.personalInformation.address1) : $scope.infoRegistration.personalInformation.address1 = "";
    $scope.activeUserInfo.info.personalInformation.address2 != undefined ? $scope.infoRegistration.personalInformation.address2 = angular.copy($scope.activeUserInfo.info.personalInformation.address2) : $scope.infoRegistration.personalInformation.address2 = "";
  }

  $scope.addressInfoInit();

  $scope.resetAddressInfo = function() {
    if (typeof window.ga !== "undefined") {
      window.ga.trackEvent('Profile Settings - Personal and Account Info', 'Profile Settings Screen (Personal and Account Info)', 'Cancel', 1);
    }
    form3.$submitted = false;
    //$scope.addressInfoInit();
    $ionicScrollDelegate.scrollTop();
    $scope.initialState();
    $state.go('app.profileSettings');
  }

  var form3 = "";
  $scope.submitAddressInfo = function(formData) {
    form3 = formData;
    if (formData.$valid) {
      $scope.showLoader();
      var usersDS = new $kinvey.UserStore();
      var user = $kinvey.User.getActiveUser();
      if (user) {
        if($scope.infoRegistration.personalInformation.zipCode != $scope.activeUserInfo.info.personalInformation.zipCode){
          if(!user.data.changeMap){
            user.data.changeMap = {"zipcode":true};
          }else{
            user.data.changeMap.zipcode = true;
          }
        }
        user.data.info.personalInformation.zipCode = $scope.infoRegistration.personalInformation.zipCode;
        user.data.info.personalInformation.state = JSON.parse(angular.toJson($scope.infoRegistration.personalInformation.state));
        user.data.info.personalInformation.city = $scope.infoRegistration.personalInformation.city;
        user.data.info.personalInformation.address1 = $scope.infoRegistration.personalInformation.address1;
        user.data.info.personalInformation.address2 = $scope.infoRegistration.personalInformation.address2;
        usersDS.update(user.data)
          .then(function(user) {
            $scope.hideLoader();
            //$scope.activeUserInfo = user;
            $scope.$emit('reloadAllProfileSettings', user);
            $ionicScrollDelegate.scrollTop();
            if (typeof window.ga !== "undefined") {
              window.ga.trackEvent('Profile Settings - Personal and Account Info', 'Update and Profile Settings Screen (Personal and Account Info)', 'Update', 1);
            }
            $state.go('app.profileSettings');
            console.log(JSON.stringify(user));
          }).catch(function(error) {
            if(error && error.message == "errorOccurred"){
              $scope.infoEmptyToast();
            }else{
              console.log(error);
              $scope.internalGlitch();
            } 
          });
      }
    }
  }

  //Email and password

  $scope.accountInfoInit = function() {
    $scope.personalInfoAccountInfo = {
      "emailAddress": $scope.activeUserInfo.email,
      "password": "Xxx12345",
      "cPassword": "Xxx12345",
      "dPassword": "Xxx12345",
      "password1": "Xxx12345",
    };
    $scope.passwordMatch = false;
  }

  $scope.accountInfoInit();

  $scope.resetAddcountInfo = function() {
    if (typeof window.ga !== "undefined") {
      window.ga.trackEvent('Profile Settings - Personal and Account Info', 'Profile Settings Screen (Personal and Account Info)', 'Cancel', 1);
    }
    form1.$submitted = false;
    //$scope.accountInfoInit();
    $ionicScrollDelegate.scrollTop();
    $scope.initialState();
    $state.go('app.profileSettings');
  }

  $scope.changeEmail = function() {
    $scope.alreadyExist = false;
  }

  $scope.passwordFieldChange = function() {
    if ($scope.personalInfoAccountInfo.cPassword && $scope.personalInfoAccountInfo.password && ($scope.personalInfoAccountInfo.password == $scope.personalInfoAccountInfo.cPassword)) {
      $scope.passwordMatch = false;
    } else {
      $scope.passwordMatch = true;
    }
  }

  var form1 = "";
  $scope.submitAddcountInfo1 = function(formData) {
    form1 = formData;
    if (typeof window.ga !== "undefined") {
      window.ga.trackEvent('Profile Settings - Personal and Account Info', 'Update and Profile Settings Screen (Personal and Account Info)', 'Update', 1);
    }
    if (formData.emailAddress.$valid && ((!$scope.passwordMatch && formData.password.$dirty && formData.conpassword.$dirty) || (!formData.password.$dirty && !formData.conpassword.$dirty))) {
      $scope.showLoader();
      var usersDS = new $kinvey.UserStore();
      var user = $kinvey.User.getActiveUser();
      if (user) {
        $scope.personalInfoAccountInfo = JSON.parse(angular.toJson($scope.personalInfoAccountInfo));
        user.data.email = $scope.personalInfoAccountInfo.emailAddress;
        user.data.username = $scope.personalInfoAccountInfo.emailAddress;
        usersDS.update(user.data)
          .then(function(user) {
            if (!$scope.passwordMatch && formData.password.$dirty && formData.conpassword.$dirty) {
              $scope.updatePassword()
            } else {
              $scope.hideLoader();
              //$scope.activeUserInfo = user;
              $scope.$emit('reloadAllProfileSettings', user);
              $ionicScrollDelegate.scrollTop();
              $state.go('app.profileSettings');
              console.log(JSON.stringify(user));
            }
          }).catch(function(error) {
            console.log(error);
            $scope.internalGlitch();
          });
      }
    }
  }
  $scope.emptyPasswordFields = function() {
    $scope.personalInfoAccountInfo.password = "";
  }

  $scope.emptyConfirmPasswordFields = function() {
    $scope.personalInfoAccountInfo.cPassword = "";
  }

  $scope.submitAddcountInfo = function(formData) {
    form1 = formData;
    if (!$scope.passwordMatch && formData.password.$dirty && formData.conpassword.$dirty) {
      $scope.showLoader();
      var promise = $q(function(resolve) {
        resolve($kinvey.User.getActiveUser());
      });
      promise.then(function(user) {
        if (user) {
          return user.update({
            password: $scope.personalInfoAccountInfo.password
          });
        }
      }).then(function(user) {
        $scope.tempUserData = user.data;
        console.log($scope.tempUserData);

        $scope.logoutAndLoginAgain(user)
          .then(function(data) {
            if (formData.emailAddress.$valid && user.data.email != $scope.personalInfoAccountInfo.emailAddress) {
              $scope.updateEmailId();
            }
            $scope.sendEmailToUser(user.data.email, user);
          }, function(error) {
            $scope.hideLoader();
            user.logout();
            $scope.emailAddressChangedLogout();
          });

      }).catch(function(error) {
        continuePasswordReset()
      });
    } else if (!$scope.passwordMatch && formData.emailAddress.$valid && $scope.activeUserInfo.email != $scope.personalInfoAccountInfo.emailAddress) {
      $scope.showLoader();
      $scope.updateEmailId(formData);
    }
  }

  $scope.logoutAndLoginAgain = function(user) {
    var deferred = $q.defer();
    user.logout();
    $timeout(function() {
      var promise = $kinvey.User.login({
        username: $scope.tempUserData.email,
        password: $scope.tempUserData.password
      });
      promise.then(function(user) {
        deferred.resolve(user);
      }).catch(function(error) {
        console.log(error);
        deferred.reject(error);
      });
    }, 1000);

    return deferred.promise;
  }

  $scope.updateEmailId = function(formData) {
    $scope.alreadyExist = false;
    var promise = $q(function(resolve) {
      resolve($kinvey.User.getActiveUser());
    });
    var usersDS = new $kinvey.UserStore();
    // var user = $kinvey.User.getActiveUser();
    promise.then(function(user) {
      if (user) {
        return user.me();
      }
      return user;
    }).then(function(user) {
      $scope.personalInfoAccountInfo = JSON.parse(angular.toJson($scope.personalInfoAccountInfo));
      user.data.email = $scope.personalInfoAccountInfo.emailAddress;
      user.data.username = $scope.personalInfoAccountInfo.emailAddress;
      // $state.go('app.home');
      usersDS.update(user.data)
        .then(function(users) {
          user.logout();
          $scope.emailAddressChangedLogout();
        }).catch(function(error) {
          console.log(error);
          $scope.hideLoader();
          if (error && error.message && error.message.indexOf('This username is already taken') > -1) {
            $scope.alreadyExist = true;
            $scope.$apply();
          }
        });
    }).catch(function(error) {
      $scope.internalGlitch();
    });
  }

  $scope.sendEmailToUser = function(emailId, user) {
    var promise = $kinvey.CustomEndpoint.execute('emailTemp-resetPasswordConfirmation', {
      username: emailId
    });
    promise.then(function(response) {
      continuePasswordReset(user);
    }).catch(function(err) {
      continuePasswordReset(user);
    });
  }

  $scope.emailAddressChangedLogout = function() {
    $ionicHistory.clearCache();
    $ionicHistory.clearHistory();
    localStorage.removeItem('verification');
    $scope.hideLoader();
    $state.go('login');
  }

  function continuePasswordReset(user) {
    $scope.hideLoader();
    //$scope.activeUserInfo = user;
    $scope.$emit('reloadAllProfileSettings', user);
    $ionicScrollDelegate.scrollTop();
    $state.go('app.profileSettings');
  }

  $scope.navigateTo = function() {
    if (typeof window.ga !== "undefined") {
      window.ga.trackEvent('Profile Settings - Personal and Account Info', 'Profile Settings Screen (Personal and Account Info)', 'Back', 1);
    }
    $scope.initialState();
    $state.go('app.profileSettings');
  }
}]);
