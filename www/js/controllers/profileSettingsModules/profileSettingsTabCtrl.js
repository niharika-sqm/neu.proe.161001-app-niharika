neuroScienceApp.controller('profileSettingsTabCtrl', ['$scope', '$state', '$kinvey','$timeout', function($scope, $state, $kinvey,$timeout) {

  $timeout(function(){
    window.ga.trackView('Profile Settings - Setting Tabs (Heat Alert and Tips)');
  }, 1000);

  //$scope.accordian.current = "";
  $scope.accordian.current = "notification1";
  $scope.profileSettingsInit = function() {
    $scope.infoRegistration.notifications = { "tips": true, "heatAlerts": true };
    if ($scope.activeUserInfo.notificationSettings != undefined) {
      $scope.infoRegistration.notifications = angular.copy($scope.activeUserInfo.notificationSettings);
    }
  }
  $scope.profileSettingsInit();

  $scope.updateNotificationSetting = function(formData) {
    $scope.showLoader();
    var usersDS = new $kinvey.UserStore();
    var user = $kinvey.User.getActiveUser();
    if (user) {
      user.data.notificationSettings = JSON.parse(angular.toJson($scope.infoRegistration.notifications));
      usersDS.update(user.data)
        .then(function(user) {
          if(formData == "tips"){
            if (typeof window.ga !== "undefined") {
              window.ga.trackEvent('Profile Settings - Settings Tab', 'On / Off Tips Notification', 'Tips & Facts', 1);
            }
            $scope.setTipNotification();
          }
          else{
            if (typeof window.ga !== "undefined") {
              window.ga.trackEvent('Profile Settings - Settings Tab', 'On / Off HeatAlert Notification', 'Heat Alert', 1);
            }
            if($scope.activeUserInfo.heatAlerts != undefined && $scope.activeUserInfo.heatAlerts.maxTemperature != undefined && $scope.activeUserInfo.heatAlerts.maxTemperature.id != 1 && $scope.activeUserInfo.heatAlerts.cityLat !=undefined && $scope.activeUserInfo.heatAlerts.cityLat !=undefined){
              $scope.setHeatAlertNotification();
            }  
            else{
              $scope.hideLoader();
            }  
          }
          console.log(JSON.stringify(user));
        }).catch(function(error) {
          console.log(error);
          $scope.internalGlitch();
        });
    }
  }

  $scope.setTipNotification = function(){
    var promise = $kinvey.CustomEndpoint.execute('userNotifApi',{"userId":$scope.activeUserInfo._id,"tipsNotification":$scope.infoRegistration.notifications.tips});
    promise.then(function(response) {  
      console.log(response);   
      $scope.hideLoader();      
    }).catch(function(error) {
      $scope.internalGlitch();
      console.log(error);
    });
  }

  $scope.setHeatAlertNotification = function(){
    var userObj = {"id":$scope.activeUserInfo._id,"maxTempSet":$scope.activeUserInfo.heatAlerts.maxTemperature.text};
    if($scope.infoRegistration.notifications.heatAlerts){
      var promise = $kinvey.CustomEndpoint.execute('latLongApi', {
        latitude: $scope.activeUserInfo.heatAlerts.cityLat,
        longitude:$scope.activeUserInfo.heatAlerts.cityLng,
        add:true,
        user:userObj
      });
      promise.then(function(response) {  
        $scope.hideLoader();
      }).catch(function(error) {
        $scope.internalGlitch();
        console.log(error);
      }); 
    }
    else{
      var promise = $kinvey.CustomEndpoint.execute('latLongApi', {
        latitude: $scope.activeUserInfo.heatAlerts.cityLat,
        longitude:$scope.activeUserInfo.heatAlerts.cityLng,
        remove:true,
        user:userObj
      });
      promise.then(function(response) {  
        $scope.hideLoader();
      }).catch(function(error) { 
        $scope.internalGlitch();
        console.log(error);
      }); 
     } 
  }

  $scope.navigateTo = function(){
    if (typeof window.ga !== "undefined") {
      window.ga.trackEvent('Profile Settings - Settings Tab', 'Profile Settings Screen (Settings Tab)', 'Back', 1);
    }
    $scope.$emit('reloadAllProfileSettings');
    $state.go('app.profileSettings');
  }
  
}]);
