neuroScienceApp.controller('profileSettingsCtrl', ["$scope", '$ionicPopup', '$kinvey', '$q', '$state', '$ionicScrollDelegate', '$timeout', 'closePopupService','profileData', function($scope, $ionicPopup, $kinvey, $q, $state, $ionicScrollDelegate, $timeout, closePopupService,profileData) {
  //$scope.lockOrientation("lock");  
  // $scope.unlockOrientation();

  $timeout(function(){
    window.ga.trackView('Profile Settings Landing Screen');
  }, 1000);
  
  $scope.activeUserInfo = profileData;
  $scope.hideLoader();
  $scope.me = function() {
    $scope.showLoader();
    var promise = null;
    promise = new $q(function(resolve) {
      resolve($kinvey.User.getActiveUser().me());
    });
    promise.then(function(user) {
      $scope.activeUserInfo = user.data;
      $scope.hideLoader();
    }).then(function(user) {}).catch(function(error) {
      $scope.internalGlitch();
    });
  }
  // $scope.me();

  $scope.$on('reloadAllProfileSettings', function(event, data) {
    //$scope.activeUserInfo = data;
    $scope.me();
    $scope.initialState();
  });

  $scope.accordian = { current: "personal1" };
  $scope.profileAccordian = function(text) {
    if (text == $scope.accordian.current){ $scope.accordian.current = "";}
    else {$scope.accordian.current = text;}
    $ionicScrollDelegate.resize();
    $ionicScrollDelegate.scrollTop();
  }

  /*JVI Need to filter the below code*/
  $scope.initialState = function(){
    $scope.registrationData = {};
    $scope.infoRegistration = {};
    $scope.infoRegistration.personalInformation = { "dob": "", "gender": "", "race": "", "zipCode": "", "insurance": "", "employmentStatus": "", "education": "" };
    $scope.datedob = { 'month': "", "date": "", "year": "" };
    $scope.dateOfLastRelapse = { 'month': "", "year": "" };
    $scope.infoRegistration.msHistoryA = {};
    $scope.infoRegistration.msHistoryB = {};
    $scope.infoRegistration.treatmentHistory = {};
    $scope.infoRegistration.selectHCP = {};
    $scope.selectedDoctorArray = [];
    // Page 2 
    $scope.racesOptions = [{ id: 1, text: "White", checked: false }, { id: 2, text: "Black or African American", checked: false }, { id: 3, text: "American Indian or Alaska Native", checked: false }, { id: 4, text: "Asian", checked: false }, { id: 5, text: "Native Hawaiian or Other Pacific Islander", checked: false }];
    $scope.insuranceOptions = [{id:1, text:"Private, employer-paid insurance", checked:false},{id:2, text:"Private, self-paid insurance (including insurance through the Affordable Care Act)", checked:false},{id:3, text:"Medicare or Medicaid", checked:false},{id:4, text:"Other", checked:false},{id:5, text:"None", checked:false}];
    $scope.employmentStatusOptions = [{id:1, text:'Working full-time without any MS-related limitations', checked:false},{id:2, text:'Working full-time with limitations due to MS', checked:false},{id:3, text:'Working part-time due to MS-related limitations', checked:false},{id:4, text:'Working part-time', checked:false},{id:5, text:'Unemployed- Looking for work', checked:false},{id:6, text:'Retired', checked:false},{id:7, text:'Disabled- permanently or temporarily', checked:false},{id:8, text:'Stay at home', checked:false},{id:9, text:'Student', checked:false},{id:10, text:'Sick leave', checked:false},{id:11, text:'Maternity leave', checked:false}];

    $scope.educationOptions = [{ id: 1, text: "Elementary school", checked: false }, { id: 2, text: "Middle school", checked: false }, { id: 3, text: "High school diploma/GED", checked: false }, { id: 4, text: "Associate's degree", checked: false }, { id: 5, text: "Bachelor's degree", checked: false }, { id: 6, text: "Master's degree", checked: false }, { id: 7, text: "Doctoral degree", checked: false }, { id: 8, text: "No formal education", checked: false }];

    $scope.statesOptions = [{ id: 1, text: "Alabama", "abbreviations": "AL", checked: false }, { id: 2, text: "Alaska", "abbreviations": "AK", checked: false }, { id: 3, text: "Arizona", "abbreviations": "AZ", checked: false }, { id: 4, text: "Arkansas", "abbreviations": "AR", checked: false }, { id: 5, text: "California", "abbreviations": "CA", checked: false }, { id: 6, text: "Colorado", "abbreviations": "CO", checked: false }, { id: 7, text: "Connecticut", "abbreviations": "CT", checked: false }, { id: 8, text: "Delaware", "abbreviations": "DE", checked: false }, { id: 9, text: "Florida", "abbreviations": "FL", checked: false }, { id: 10, text: "Georgia", "abbreviations": "GA", checked: false }, { id: 11, text: "Hawaii", "abbreviations": "HI", checked: false }, { id: 12, text: "Idaho", "abbreviations": "ID", checked: false }, { id: 13, text: "Illinois", "abbreviations": "IL", checked: false }, { id: 14, text: "Indiana", "abbreviations": "IN", checked: false }, { id: 15, text: "Iowa", "abbreviations": "IA", checked: false }, { id: 16, text: "Kansas", "abbreviations": "KS", checked: false }, { id: 17, text: "Kentucky", "abbreviations": "KY", checked: false }, { id: 18, text: "Louisiana", "abbreviations": "LA", checked: false }, { id: 19, text: "Maine", "abbreviations": "ME", checked: false }, { id: 20, text: "Maryland", "abbreviations": "MD", checked: false }, { id: 21, text: "Massachusetts", "abbreviations": "MA", checked: false }, { id: 22, text: "Michigan", "abbreviations": "MI", checked: false }, { id: 23, text: "Minnesota", "abbreviations": "MN", checked: false }, { id: 24, text: "Mississippi", "abbreviations": "MS", checked: false }, { id: 25, text: "Missouri", "abbreviations": "MO", checked: false }, { id: 26, text: "Montana", "abbreviations": "MT", checked: false }, { id: 27, text: "Nebraska", "abbreviations": "NE", checked: false }, { id: 28, text: "Nevada", "abbreviations": "NV", checked: false }, { id: 29, text: "New Hampshire", "abbreviations": "NH", checked: false }, { id: 30, text: "New Jersey", "abbreviations": "NJ", checked: false }, { id: 31, text: "New Mexico", "abbreviations": "NM", checked: false }, { id: 32, text: "New York", "abbreviations": "NY", checked: false }, { id: 33, text: "North Carolina", "abbreviations": "NC", checked: false }, { id: 34, text: "North Dakota", "abbreviations": "ND", checked: false }, { id: 35, text: "Ohio", "abbreviations": "OH", checked: false }, { id: 36, text: "Oklahoma", "abbreviations": "OK", checked: false }, { id: 37, text: "Oregon", "abbreviations": "OR", checked: false }, { id: 38, text: "Pennsylvania", "abbreviations": "PA", checked: false }, { id: 39, text: "Rhode Island", "abbreviations": "RI", checked: false }, { id: 40, text: "South Carolina", "abbreviations": "SC", checked: false }, { id: 41, text: "South Dakota", "abbreviations": "SD", checked: false }, { id: 42, text: "Tennessee", "abbreviations": "TN", checked: false }, { id: 43, text: "Texas", "abbreviations": "TX", checked: false }, { id: 44, text: "Utah", "abbreviations": "UT", checked: false }, { id: 45, text: "Vermont", "abbreviations": "VT", checked: false }, { id: 46, text: "Virginia", "abbreviations": "VA", checked: false }, { id: 47, text: "Washington", "abbreviations": "WA", checked: false }, { id: 48, text: "West Virginia", "abbreviations": "WV", checked: false }, { id: 49, text: "Wisconsin", "abbreviations": "WI", checked: false }, { id: 50, text: "Wyoming", "abbreviations": "WY", checked: false }];
    // Page 3
    $scope.ageOfFirstSymptomsOptions = [];
    $scope.ageAtDiagnosisOptions = [];
    for (var i = 0; i <= 105; i++) {
      $scope.ageOfFirstSymptomsOptions.push({ id: i + 1, text: i + 5, checked: false, scroll: false });
      $scope.ageAtDiagnosisOptions.push({ id: i + 1, text: i + 5, checked: false, scroll: false });
    }
    $scope.initialDiagnosisOptions = [{ id: 1, text: "Relapsing-remitting MS (RRMS)", checked: false }, { id: 2, text: "Secondary progressive MS (SPMS)", checked: false }, { id: 3, text: "Primary progressive MS (PPMS)", checked: false }, { id: 4, text: "Progressive relapsing MS (PRMS)", checked: false }, { id: 5, text: "Clinically isolated syndrome", checked: false }, { id: 6, text: "Not sure", checked: false },{id:7, text:"I don’t have MS", checked:false}];
    $scope.currentDiagnosisOptions = [{ id: 1, text: "Relapsing-remitting MS (RRMS)", checked: false }, { id: 2, text: "Secondary progressive MS (SPMS)", checked: false }, { id: 3, text: "Primary progressive MS (PPMS)", checked: false }, { id: 4, text: "Progressive relapsing MS (PRMS)", checked: false }, { id: 5, text: "Clinically isolated syndrome", checked: false }, { id: 6, text: "Not sure", checked: false },{id:7, text:"I don’t have MS", checked:false}];
     $scope.familyMemberRelationshipOptions = [{id:1, text:'I prefer not to answer', checked:false},{id:2, text:'Mother (biological)', checked:false},{id:3, text:'Father (biological)', checked:false},{id:4, text:'Sibling', checked:false},{id:5, text:'Daughter', checked:false},{id:6, text:'Son', checked:false},{id:7, text:'Maternal grandmother ', checked:false},{id:8, text:"Maternal grandfather", checked:false},{id:9, text:'Paternal grandmother', checked:false},{id:10, text:'Paternal grandfather', checked:false},{id:11, text:'Aunt, uncle, or cousin', checked:false}];

    // Page 4
    $scope.numMSRelapsesLifeTimeOptions = [{ id: 1, text: "1-5", checked: false, range: 5 }, { id: 2, text: "6-10", checked: false, range: 10 }, { id: 3, text: "10+", checked: false, range: 12 }];
    $scope.overAllAbilityOptions = [{ value: "Normal", cssClass: "normal", detail: "I may have some mild symptoms, mostly sensory due to MS, but they do not limit my activity. If I do have an attack, I return to normal when the attack has passed." }, { value: "Mild Disability", cssClass: "mildDisability", detail: "I have some noticeable symptoms from my MS but they are minor and have only a small effect on my lifestyle." }, { value: "Moderate Disability", cssClass: "moderateDisability", detail: "I don't have any limitations in my walking ability. However, I do have significant problems due to MS that limit daily activities in other ways." }, { value: "Gait Disability", cssClass: "gaitDisability", detail: "MS does interfere with my activities, especially my walking. I can work a full day, but athletic or physically demanding activities are more difficult than they used to be. I usually don't need a cane or other assistance to walk, but I might need some assistance during an attack." }, { value: "Early Cane", cssClass: "earlyCane", detail: "I use a cane or a single crutch or some other form of support (such as touching the wall, or leaning on someone's arm) for walking all the time or part of the time, especially when walking outside. I think I can walk 25 feet in 20 seconds without a cane or crutch. I may use a scooter or wheelchair if I want to go greater distances." }, { value: "Late Cane", cssClass: "lateCane", detail: "To be able to walk 25 feet, I have to have a cane, crutch or someone to hold onto. I can get around the house or other buildings by holding onto furniture or touching the walls for support. I may use a scooter or wheelchair if I want to go greater distances." }, { value: "Bilateral Support", cssClass: "bilateralSupport", detail: "To be able to walk as far as 25 feet I must have 2 canes or crutches or a walker. I may use a scooter or wheelchair for longer distances." }, { value: "Wheelchair / Scooter", cssClass: "wheelchairScooter", detail: "My main form of mobility is a wheelchair. I may be able to stand and/or take one or two steps, but I can't walk 25 feet, even with crutches or a walker." }, { value: "Bedridden", cssClass: "bedridden", detail: "Unable to sit in a wheelchair for more than one hour." }]
      // $scope.numMSRelapsesLastYearOptions = [{id:1, text:"0", checked:false, range:0},{id:2, text:"1-5", checked:false, range:5},{id:3, text:"6-10", checked:false ,range:10},{id:4, text:"10+", checked:false, range:12}];
      // Page 5
    $scope.currentTreatmentOptions = [{id:1, value:'Aubagio', checked:false, text:'Aubagio<sup>®</sup> (teriflunomide)' },{id:2, value:'Avonex', checked:false, text:'Avonex<sup>®</sup> (interferon beta-1a)'},{id:3, value:'Betaseron', checked:false, text:'Betaseron<sup>®</sup> (interferon beta-1b)'},{id:4, value:'Copaxone', checked:false, text:'Copaxone<sup>®</sup> (glatiramer acetate injection)'},{id:5, value:'Extavia', checked:false, text:'Extavia<sup>®</sup> (interferon beta-1b)'},{id:6, value:'Gilenya', checked:false, text:'Gilenya<sup>®</sup> (fingolimod)'},{id:7, value:'Lemtrada', checked:false, text:'Lemtrada<sup>®</sup> (alemtuzumab)'},{id:15, value:'Ocrevus', checked:false, text:'Ocrevus<sup>®</sup> (ocrelizumab)'},{id:8, value:"Plegridy", checked:false, text:'Plegridy<sup>®</sup> (peginterferon beta-1a)'},{id:9, value:'Rebif', checked:false, text:'Rebif<sup>®</sup> (interferon beta-1a)'},{id:10, value:'Rituxan', checked:false, text:'Rituxan<sup>®</sup> (rituximab)'},{id:11, value:'Tecfidera', checked:false, text:'Tecfidera<sup>®</sup> (dimethyl fumarate)'},{id:12, value:'Tysabri', checked:false, text:'Tysabri<sup>®</sup> (natalizumab)'},{id:13, value:'Zinbryta', checked:false, text:'Zinbryta™ (daclizumab)'},{id:14, value:'None', checked:false, text:'None'}];
  
    /*$scope.currentTreatmentPeriodOptions = [{ id: 1, text: '1 Month', checked: false }, { id: 2, text: '3 Months', checked: false }, { id: 3, text: '6 Months', checked: false }, { id: 4, text: '1 Year', checked: false }, { id: 5, text: '1.5 Years', checked: false }, { id: 6, text: '2 Years', checked: false }, { id: 7, text: '3 Years', checked: false }, { id: 8, text: "4 Years", checked: false }, { id: 9, text: '5 Years', checked: false }, { id: 10, text: '6 Years', checked: false }, { id: 11, text: '7 Years', checked: false }, { id: 12, text: '8 Years', checked: false }, { id: 13, text: '9 Years', checked: false }, { id: 14, text: '10 Years', checked: false }, { id: 15, text: '>10 Years', checked: false }];*/

    $scope.currentTreatmentPeriodOptions = [];
    for(var i=new Date().getFullYear();i<=new Date().getFullYear();i++){
      $scope.currentTreatmentPeriodOptions.push({id:i,text:i,checked:false})
    }
    
   // $scope.currentTreatmentEffectivenessOptions = [{ id: 1, text: "Very effective", checked: false }, { id: 2, text: "Partially effective", checked: false }, { id: 3, text: "Not sure", checked: false }, { id: 4, text: "Not effective", checked: false }];

   //Text Changed
   $scope.currentTreatmentEffectivenessOptions = [{ id: 1, text: "Completely effective", checked: false }, { id: 2, text: "Mostly effective", checked: false }, { id: 3, text: "Slightly effective", checked: false }, { id: 4, text: "Not effective", checked: false }];

    $scope.lastTreatmentOptions = [{id:1, value:'Aubagio', checked:false, text:'Aubagio<sup>®</sup> (teriflunomide)' },{id:2, value:'Avonex', checked:false, text:'Avonex<sup>®</sup> (interferon beta-1a)'},{id:3, value:'Betaseron', checked:false, text:'Betaseron<sup>®</sup> (interferon beta-1b)'},{id:4, value:'Copaxone', checked:false, text:'Copaxone<sup>®</sup> (glatiramer acetate injection)'},{id:5, value:'Extavia', checked:false, text:'Extavia<sup>®</sup> (interferon beta-1b)'},{id:6, value:'Gilenya', checked:false, text:'Gilenya<sup>®</sup> (fingolimod)'},{id:7, value:'Lemtrada', checked:false, text:'Lemtrada<sup>®</sup> (alemtuzumab)'},{id:15, value:'Ocrevus', checked:false, text:'Ocrevus<sup>®</sup> (ocrelizumab)'},{id:8, value:"Plegridy", checked:false, text:'Plegridy<sup>®</sup> (peginterferon beta-1a)'},{id:9, value:'Rebif', checked:false, text:'Rebif<sup>®</sup> (interferon beta-1a)'},{id:10, value:'Rituxan', checked:false, text:'Rituxan<sup>®</sup> (rituximab)'},{id:11, value:'Tecfidera', checked:false, text:'Tecfidera<sup>®</sup> (dimethyl fumarate)'},{id:12, value:'Tysabri', checked:false, text:'Tysabri<sup>®</sup> (natalizumab)'},{id:13, value:'Zinbryta', checked:false, text:'Zinbryta™ (daclizumab)'}];

    /*$scope.yearsTakenOptions = [{ id: 1, text: "Less than 1", checked: false }];
    for (var i = 1; i <= 50; i++) {
      $scope.yearsTakenOptions.push({ id: i + 1, text: i, checked: false })
    }*/

    $scope.yearsTakenOptions = [];
    for(var i=1980;i<=new Date().getFullYear();i++){
      $scope.yearsTakenOptions.push({id:i,text:i,checked:false})
    }

    $scope.yearsStoppedOptions = [];
    for(var i=1980;i<=new Date().getFullYear();i++){
      $scope.yearsStoppedOptions.push({id:i,text:i,checked:false})
    }

    $scope.takingAdditionalMedicationsOptions =[{text:'-Fatigue-', title:true}, {id:1, text:'Symmetrel<sup>®</sup> (amantadine)', checked:false,category:"Fatigue"}, {id:2, text:'Provigil<sup>®</sup> (modafinil)', checked:false,category:"Fatigue"}, {id:3, text:'Nuvigil<sup>®</sup> (armodafinil)', checked:false,category:"Fatigue"}, {id:4, text:'Adderall<sup>®</sup> (amphetamine)', checked:false,category:"Fatigue"}, {id:5, text:'Ritalin<sup>®</sup> (methylphenidate)', checked:false,category:"Fatigue"}, {id:6, text:'Concerta<sup>®</sup> (methylphenidate)', checked:false,category:"Fatigue"},{text:'-Cognition-', title:true}, {id:7, text:'Aricept<sup>®</sup> (donepezil)', checked:false,category:"Cognition"}, {id:8, text:'Exelon<sup>®</sup> (rivastigmine)', checked:false,category:"Cognition"}, {id:9, text:"Namenda<sup>®</sup> (memantine)", checked:false,category:"Cognition"},{text:'-Muscle Spasm-', title:true}, {id:10, text:'Zanaflex<sup>®</sup> (tizanidine)', checked:false,category:"Muscle Spasm"}, {id:11, text:'Lioresal<sup>®</sup> (baclofen)', checked:false,category:"Muscle Spasm"}, {id:12, text:'Valium<sup>®</sup> (diazepam)', checked:false,category:"Muscle Spasm"}, {id:13, text:'Klonopin<sup>®</sup> (clonazepam)', checked:false,category:"Muscle Spasm"},{text:'-Bladder Dysfunction-', title:true}, {id:14, text:'Detrol<sup>®</sup> (tolterodine)', checked:false,category:"Bladder"}, {id:15, text:'Ditropan<sup>®</sup> (oxybutynin)', checked:false,category:"Bladder"}, {id:16, text:'Oxytrol<sup>®</sup> (oxybutynin)', checked:false,category:"Bladder"}, {id:17, text:'Tofranil<sup>®</sup> (imipramine)', checked:false,category:"Bladder"},{text:'-Mobility-', title:true}, {id:18, text:'Ampyra<sup>®</sup> (dalfampridine)', checked:false,category:"Mobility"}, {text:'-Depression-', title:true}, {id:19, text:"Zoloft<sup>®</sup> (sertraline)", checked:false,category:"Depression"}, {id:20, text:'Wellbutrin<sup>®</sup> (bupropion)', checked:false,category:"Depression"}, {id:21, text:'Prozac<sup>®</sup> (fluoxetine)', checked:false,category:"Depression"}, {id:22, text:'Paxil<sup>®</sup> (paroxetine)', checked:false,category:"Depression"}, {id:23, text:'Effexor<sup>®</sup>, Effexor XR<sup>®</sup> (venlafaxine)', checked:false,category:"Depression"}, {id:24, text:'Cymbalta<sup>®</sup> (duloxetine)', checked:false,category:"Depression"}, {text:'-Sleep-', title:true}, {id:25, text:'Ambien<sup>®</sup> (zolpidem)', checked:false,category:"Sleep"}, {id:26, text:'Lunesta<sup>®</sup> (eszopiclone)', checked:false,category:"Sleep"}, {id:27, text:'Melatonin', checked:false,category:"Sleep"}, {id:28, text:'Rozerem<sup>®</sup> (ramelteon)', checked:false,category:"Sleep"}, {text:'-Pain-', title:true}, {id:29, text:"Neurontin<sup>®</sup> (gabapentin)", checked:false,category:"Pain"}, {id:30, text:'Tofranil<sup>®</sup> (imipramine)', checked:false,category:"Pain"}, {id:31, text:'Lyrica<sup>®</sup> (pregabalin)', checked:false,category:"Pain"}, {id:32, text:'Pamelor<sup>®</sup> (nortriptyline) ', checked:false,category:"Pain"}, {id:33, text:'Elavil<sup>®</sup> (amitriptyline)', checked:false,category:"Pain"}, {id:34, text:'Dilantin<sup>®</sup> (phenytoin)', checked:false,category:"Pain"}, {id:35, text:'Cymbalta<sup>®</sup> (duloxetine)', checked:false,category:"Pain"}];
  }
  $scope.initialState();
  
  $scope.optionSingleSelect = null;
  $scope.typeSingleSelect = null;
  $scope.typeMultiSelect = null;
  $scope.optionMultiSelect = null;

  $scope.openSingleSelect = function(options, popupType, screenType, title, index, key) {
    $scope.$emit('disbaleBG');
    if(popupType == "priorTreatments.yearsTaken"){
      if(index == 0){
        $scope.yearsTakenOptions = [];
        $scope.infoRegistration.treatmentHistory.currentTreatmentPeriod != undefined ? $scope.priorTreatmentStartDateLimit = $scope.infoRegistration.treatmentHistory.currentTreatmentPeriod.text : $scope.priorTreatmentStartDateLimit = new Date().getFullYear();
        for(var i=1980;i<=$scope.priorTreatmentStartDateLimit;i++){
          $scope.yearsTakenOptions.push({id:i,text:i,checked:false})
        }
        options = $scope.yearsTakenOptions;
      }
      else{
        $scope.yearsTakenOptions = [];
        $scope.priorTreatmentStartDateLimit = $scope.infoRegistration.treatmentHistory.priorTreatments[index-1].yearsTaken.text;
        $scope.priorTreatmentStartDateLimit == undefined ? $scope.priorTreatmentStartDateLimit = new Date().getFullYear():"";
        for(var i=1980;i<=$scope.priorTreatmentStartDateLimit;i++){
          $scope.yearsTakenOptions.push({id:i,text:i,checked:false})
        }
        options = $scope.yearsTakenOptions;
      }
    }
    if(popupType == "priorTreatments.yearsStopped"){
      if(index == 0){
        $scope.yearsStoppedOptions = [];
        if($scope.infoRegistration.treatmentHistory.priorTreatments && $scope.infoRegistration.treatmentHistory.priorTreatments[index].yearsTaken){
          $scope.priorTreatmentEndYearStartLimit = $scope.infoRegistration.treatmentHistory.priorTreatments[index].yearsTaken.text;
          $scope.priorTreatmentEndYearStartLimit == undefined ? $scope.priorTreatmentEndYearStartLimit = 1980:"";
        }else{
          $scope.priorTreatmentEndYearStartLimit = 1980;
        }      
        $scope.infoRegistration.treatmentHistory.currentTreatmentPeriod != undefined ? $scope.priorTreatmentEndYearEndLimit = $scope.infoRegistration.treatmentHistory.currentTreatmentPeriod.text : $scope.priorTreatmentEndYearEndLimit = new Date().getFullYear();
        for(var i=$scope.priorTreatmentEndYearStartLimit;i<=$scope.priorTreatmentEndYearEndLimit;i++){
          $scope.yearsStoppedOptions.push({id:i,text:i,checked:false})
        }
        options = $scope.yearsStoppedOptions;
      }
      else{
        $scope.yearsStoppedOptions = [];       
        if($scope.infoRegistration.treatmentHistory.priorTreatments[index].yearsTaken.text == undefined){
          $scope.priorTreatmentEndYearStartLimit = $scope.infoRegistration.treatmentHistory.priorTreatments[index-1].yearsTaken.text;
        }
        else{
          $scope.priorTreatmentEndYearStartLimit = $scope.infoRegistration.treatmentHistory.priorTreatments[index].yearsTaken.text;
        }
        $scope.priorTreatmentEndYearStartLimit == undefined ? $scope.priorTreatmentEndYearStartLimit = 1980:"";
        $scope.priorTreatmentEndYearEndLimit = $scope.infoRegistration.treatmentHistory.priorTreatments[index-1].yearsTaken.text;
        $scope.priorTreatmentEndYearEndLimit == undefined ? $scope.priorTreatmentEndYearEndLimit = new Date().getFullYear():"";
        for(var i=$scope.priorTreatmentEndYearStartLimit;i<=$scope.priorTreatmentEndYearEndLimit;i++){
          $scope.yearsStoppedOptions.push({id:i,text:i,checked:false})
        }
        options = $scope.yearsStoppedOptions;
      }
    }
    $scope.typeSingleSelect = popupType;
    $scope.optionSingleSelect = options;
    $scope.screenTypeSingleSelect = screenType;
    $scope.index = index;
    $scope.key = key;
    if ($scope.typeSingleSelect.split('.').length > 1) {
      var child1 = $scope.typeSingleSelect.split('.')[0];
      var child2 = $scope.typeSingleSelect.split('.')[1];
      if ($scope.infoRegistration[$scope.screenTypeSingleSelect][child1] && $scope.infoRegistration[$scope.screenTypeSingleSelect][child1].length > 0) {
        if ($scope.infoRegistration[$scope.screenTypeSingleSelect][child1][$scope.index]) {
          if ((popupType == 'priorTreatments.lastTreatment' || popupType == 'priorTreatments.yearsTaken') && ($scope.infoRegistration[$scope.screenTypeSingleSelect][child1][$scope.index][child2] == '')) {
            for (var i = 0; i < $scope.optionSingleSelect.length; i++) {
              if ($scope.optionSingleSelect[i].checked == true) {
                $scope.optionSingleSelect[i].checked = false;
              }
            }
          } else {
            $scope.$emit('updateSingleSelectedArray', $scope.infoRegistration[$scope.screenTypeSingleSelect][child1][$scope.index][child2]);
          }
        } else {
          $scope.infoRegistration[$scope.screenTypeSingleSelect][child1][$scope.index] = {};
          $scope.$emit('updateSingleSelectedArray', $scope.infoRegistration[$scope.screenTypeSingleSelect][child1][$scope.index][child2]);
        }
      } else {
        $scope.infoRegistration[$scope.screenTypeSingleSelect][child1] = [];
        $scope.$emit('updateSingleSelectedArray', $scope.infoRegistration[$scope.screenTypeSingleSelect][child1][$scope.index]);
      }
    } else {
      if ($scope.infoRegistration[$scope.screenTypeSingleSelect][$scope.typeSingleSelect]) {
        $scope.$emit('updateSingleSelectedArray', $scope.infoRegistration[$scope.screenTypeSingleSelect][$scope.typeSingleSelect]);
      }
    }
    var singleSelectPopup = $ionicPopup.show({
      title: title,
      templateUrl: 'templates/genericTemplate/singleSelectPopup.html',
      cssClass: "selectBoxPopup",
      scope: $scope,
      buttons: [{
        text: 'Cancel',
        type: 'button-block button-outline button-darkPurple',
        onTap: function() {
          return false;
        }
      }, {
        text: 'Okay',
        type: 'button-block button-outline button-darkPurple bold',
        onTap: function() {
          return true;
        }
      }]
    });
    closePopupService.register(singleSelectPopup);

    /*if ((popupType == "ageAtDiagnosis" || popupType == "ageOfFirstSymptoms") && options[25].checked) {
        $timeout(function() {
            $ionicScrollDelegate.$getByHandle('scrollPopup').scrollTo(0, 700, true);
        }, 500);
    }*/

    singleSelectPopup.then(function(res) {
      $timeout(function(){
        $scope.$emit('enableBG');
      }, 1000);
      console.log($scope.singleSelection, res);
      if (res) {
        if (popupType == 'numMSRelapsesLifeTime') {
          $scope.infoRegistration.msHistoryB.numMSRelapsesLastYear = false;
          $scope.numMSRelapsesLifeTimeData = angular.copy($scope.singleSelection.range);
          $scope.$broadcast('updateRelapsesLastYearOptions', $scope.numMSRelapsesLifeTimeData);
          /*$scope.numMSRelapsesLastYearOptions = [];
          for (var i = 0; i <= $scope.numMSRelapsesLifeTimeData; i++) {
              $scope.numMSRelapsesLastYearOptions.push({
                  id: i + 1,
                  text: i,
                  checked: false
              });
          }*/
        }
        if ($scope.typeSingleSelect.split('.').length > 1) {
          var child1 = $scope.typeSingleSelect.split('.')[0];
          var child2 = $scope.typeSingleSelect.split('.')[1];
          if ($scope.infoRegistration[$scope.screenTypeSingleSelect][child1].length == 0) {
            $scope.infoRegistration[$scope.screenTypeSingleSelect][child1] = [];
          }
          if ($scope.infoRegistration[$scope.screenTypeSingleSelect][child1][$scope.index] && Object.keys($scope.infoRegistration[$scope.screenTypeSingleSelect][child1][$scope.index]).length > 0) {
            $scope.infoRegistration[$scope.screenTypeSingleSelect][child1][$scope.index][child2] = angular.copy($scope.singleSelection);
            $scope.infoRegistration[$scope.screenTypeSingleSelect][child1][$scope.index].id = $scope.key;
          } else {
            $scope.infoRegistration[$scope.screenTypeSingleSelect][child1][$scope.index] = {};
            $scope.infoRegistration[$scope.screenTypeSingleSelect][child1][$scope.index][child2] = angular.copy($scope.singleSelection);
            $scope.infoRegistration[$scope.screenTypeSingleSelect][child1][$scope.index].id = $scope.key;
          }
          if(child1 == "priorTreatments" && child2 == "yearsTaken"){
            delete ($scope.infoRegistration[$scope.screenTypeSingleSelect][child1][$scope.index]['yearsStopped']);
          }
          $scope.$emit('emptySingleSelectedArray');
        } else {
          if ($scope.singleSelection == '') {
            for (var i = 0; i < $scope.optionSingleSelect.length; i++) {
              if ($scope.optionSingleSelect[i].checked == true) {
                $scope.infoRegistration[$scope.screenTypeSingleSelect][$scope.typeSingleSelect] = $scope.optionSingleSelect[i];
                break;
              }
            }
          } else {
            $scope.infoRegistration[$scope.screenTypeSingleSelect][$scope.typeSingleSelect] = angular.copy($scope.singleSelection);
          }
          $scope.$emit('emptySingleSelectedArray');
        }
        console.log($scope.infoRegistration);
      } else {
        $scope.$emit('emptySingleSelectedArray');
        for (var i = 0; i < $scope.optionSingleSelect.length; i++) {
          if ($scope.optionSingleSelect[i].checked == true) {
            $scope.optionSingleSelect[i].checked = false;
            break;
          }
        }
      }
    });
  }
  $scope.openMultiSelect = function(options, popupType, screenType, title) {
    $scope.$emit('disbaleBG');
    $scope.typeMultiSelect = popupType;
    $scope.optionMultiSelect = options;
    $scope.screenTypeMultiSelect = screenType;
    if ($scope.infoRegistration[$scope.screenTypeMultiSelect][$scope.typeMultiSelect]) {
      var selectedData = $scope.infoRegistration[$scope.screenTypeMultiSelect][$scope.typeMultiSelect];
      for (var i = 0; i < selectedData.length; i++) {
        for(var j=0; j<$scope.optionMultiSelect.length;j++){
          if(selectedData[i].id == $scope.optionMultiSelect[j].id){
            $scope.optionMultiSelect[j].checked = true;
            break;
          }
        }
      }
      $scope.$emit('updateMultiSelectedArray', $scope.infoRegistration[$scope.screenTypeMultiSelect][$scope.typeMultiSelect])
    }
    var multiSelectPopup = $ionicPopup.show({
      title: title,
      subTitle: "Select all that apply",
      templateUrl: 'templates/genericTemplate/multiSelectPopup.html',
      cssClass: "selectBoxPopup",
      scope: $scope,
      buttons: [{
        text: 'Cancel',
        type: 'button-block button-outline button-darkPurple',
        onTap: function() {
          return false;
        }
      }, {
        text: 'Okay',
        type: 'button-block button-outline button-darkPurple bold',
        onTap: function() {
          return true;
        }
      }]
    });
    closePopupService.register(multiSelectPopup);
    multiSelectPopup.then(function(res) {
      $timeout(function(){
        $scope.$emit('enableBG');
      }, 1000);
      console.log($scope.multiSelection, res);
      if (res) {
        $scope.infoRegistration[$scope.screenTypeMultiSelect][$scope.typeMultiSelect] = angular.copy($scope.multiSelection);
        $scope.$emit('emptyMultiSelectedArray');
        console.log($scope.infoRegistration);
      } else {
        $scope.$emit('emptyMultiSelectedArray');
        for (var i = 0; i<$scope.optionMultiSelect.length; i++) {
          if ($scope.optionMultiSelect[i].checked == true) {
            $scope.optionMultiSelect[i].checked = false;
          }
        }
      }
    });
  }

  $scope.logoutFromRegistration = function() {
    $scope.showLoader();
    var promise = new $q(function(resolve) {
      resolve($kinvey.User.getActiveUser());
    });
    promise.then(function(user) {
      if (user) {
        return user.logout();
      }
      return user;
    }).then(function() {
      console.log("user logout success");
      localStorage.removeItem('verification');
      $scope.hideLoader();
      $state.go('login');
    }).catch(function(error) {
      console.log(error);
      $scope.hideLoader();
      localStorage.removeItem('verification');
      $state.go('login');
    });
  }
  $scope.$on('dontGoBackShowPopup', function() {
    $scope.cancelRegistration();
  });
}]);
