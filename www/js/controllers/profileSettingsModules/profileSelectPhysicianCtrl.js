neuroScienceApp.controller('profileSelectPhysicianCtrl', ['$scope', '$state', '$kinvey', '$q', '$ionicPopup', '$ionicScrollDelegate','closePopupService','$timeout', function($scope, $state, $kinvey, $q, $ionicPopup, $ionicScrollDelegate,closePopupService,$timeout) {

  $timeout(function(){
    window.ga.trackView('Profile Settings - Select Neurologist');
  }, 1000);

  //$scope.accordian.current = "";
  $scope.accordian.current = "physical1";
  $scope.profileSelectPhysicianInit = function() {
    $scope.infoRegistration.selectHCP = angular.copy($scope.activeUserInfo.info.selectHCP);
    $scope.selectedDoctorArray = [];
    $scope.selectedDoctorNew = [];
    if ($scope.infoRegistration.selectHCP.neurologist != undefined) {
      for (var i = 0; i < $scope.infoRegistration.selectHCP.neurologist.length; i++) {
        $scope.selectedDoctorArray.push($scope.infoRegistration.selectHCP.neurologist[i]);
      }
    }
    if ($scope.selectedDoctorArray.length > 0) {
      $scope.showDoctorsList = true;
    } else {
      $scope.showDoctorsList = false;
    }
    $scope.doctorSearch = { "key": '', 'limit': 100 };
    $scope.showSpinner = false;
    $scope.showContent = false;
    $scope.showDocNotFound = false;
    $scope.doctorNotFoundMessage = false;
    $scope.isDoctorNotFoundClicked = false;
    $scope.doctors = [];
  }
  $scope.profileSelectPhysicianInit();

  $scope.searchDoctor = function() {
    if ($scope.doctorSearch.key.length > 2) {
      $scope.showSpinner = true;
      var promise = $kinvey.CustomEndpoint.execute('getDoctorList', $scope.doctorSearch);
      promise.then(function(response) {
        console.log(response);
        $scope.showSpinner = false;
        $scope.showContent = true;
        $scope.doctors = [];
        $scope.doctors = response;
        // $ionicScrollDelegate.$getByHandle('doctorListScroll').scrollTop();
        if ($scope.doctors.length == 0) {
          $scope.showContent = false;
          $scope.showDocNotFound = true;
        }
        $scope.$apply();
      }).catch(function(err) {
        console.log(err);
      });
    } else {
      $scope.showSpinner = false;
      $scope.showContent = false;
      $scope.showDocNotFound = false;
      $scope.doctors = [];
      // $ionicScrollDelegate.$getByHandle('doctorListScroll').scrollTop();
    }
  };

  $scope.onDoctorSelect = function($item) {
    if (typeof window.ga !== "undefined") {
      window.ga.trackEvent('Profile Settings - Select Physician', 'Select Physican found', 'Search Physician', 1);
    }
    $scope.showContent = false;
    $scope.showDocNotFound = false;
    $scope.doctorSearch.key = '';
    if ($scope.selectedDoctorArray.length != 0) {
      for (var c = 0; c < $scope.selectedDoctorArray.length; c++) {
        if ($item._id != undefined && $scope.selectedDoctorArray[c]._id != undefined && $item._id == $scope.selectedDoctorArray[c]._id) {
          return;
        }
        if ($item._id != undefined && $scope.selectedDoctorArray[c].id != undefined && $item._id == $scope.selectedDoctorArray[c].id) {
          return;
        }
      }
      $scope.infoRegistration.selectHCP.agreedToSharedInformation = false;
      $scope.selectedDoctorArray.push($item);
      $scope.selectedDoctorNew.push($item._id);
    } else {
      $scope.infoRegistration.selectHCP.agreedToSharedInformation = false;
      $scope.selectedDoctorArray.push($item);
      $scope.selectedDoctorNew.push($item._id);
    }

    if ($scope.showDoctorsList == false) {
      $scope.showDoctorsList = true;
      // $scope.doctorValidation = true;
      // $scope.errorVisibilityCheckOnDoctorInfo = false;
    }
    console.log($scope.selectedDoctorArray);
  };

  $scope.removeSelectedDoctor = function($item) {
    for (var i = 0; i < $scope.selectedDoctorArray.length; i++) {
      if ($item._id != undefined && $scope.selectedDoctorArray[i]._id != undefined && $item._id == $scope.selectedDoctorArray[i]._id) {
        $scope.selectedDoctorArray.splice(i, 1);
      }
      if ($item.id != undefined && $scope.selectedDoctorArray[i].id != undefined && $item.id == $scope.selectedDoctorArray[i].id) {
        $scope.selectedDoctorArray.splice(i, 1);
      }
    }
    for (var i = 0; i < $scope.selectedDoctorNew.length; i++) {
      if ($item._id == $scope.selectedDoctorNew[i]._id) {
        $scope.selectedDoctorNew.splice(i, 1);
      }
    }
  };

  $scope.doctorNotFoundClicked = function() {
    if (typeof window.ga !== "undefined") {
      window.ga.trackEvent('Profile Settings - Select Physician', "Select I Can't Find My Neurologist", "I Can't Find My Neurologist", 1);
    }
    $scope.doctorNotFoundMessage = true;
    $scope.isDoctorNotFoundClicked = true;
    var promise = $q(function(resolve) {
      resolve($kinvey.User.getActiveUser());
    });
    promise.then(function(user) {
      $scope.user = user.data;
      var sendDocNotFoundEmail = $kinvey.CustomEndpoint.execute('sendDocNotFoundEmail', { username: $scope.user.username });
      sendDocNotFoundEmail.then(function(response) {
        console.log(response);
      }).catch(function(err) {
        console.log(err);
      });
    });
  };

  var form1 = "";
  $scope.submitHCPForm = function(formData) {
    form1 = formData;
    if (formData.$valid) {
      $scope.infoRegistration.selectHCP.HCPNotFound = angular.copy($scope.isDoctorNotFoundClicked);
      var user = $kinvey.User.getActiveUser();
      if (user) {
        $scope.infoRegistration.selectHCP.neurologist = [];
        for (var i = 0; i < $scope.selectedDoctorArray.length; i++) {
          $scope.infoRegistration.selectHCP.neurologist.push({ id: $scope.selectedDoctorArray[i]._id || $scope.selectedDoctorArray[i].id, physician_full_name: $scope.selectedDoctorArray[i].physician_full_name, practice_street: $scope.selectedDoctorArray[i].practice_street, practice_state: $scope.selectedDoctorArray[i].practice_state, practice_city: $scope.selectedDoctorArray[i].practice_city, zip: $scope.selectedDoctorArray[i].zip })
        }
        if ($scope.selectedDoctorArray.length == 0 && $scope.isDoctorNotFoundClicked == false) {
          $scope.noDoctorSelectPopup = $ionicPopup.show({
            title: "Are you sure you don’t want to share your information? We value your privacy and only your healthcare team will have access to your personal information.",
            template: '<a class="popClose" ng-click="noDoctorSelectPopup.close()"><span></span><span></span></a>',
            scope: $scope,
            buttons: [{
              text: 'Skip',
              type: 'button-block button-outline button-darkPurple skip',
              onTap: function() {
                return true;
              }
            }, {
              text: 'Select Your<br>HCP',
              type: 'button-block button-block button-darkPurple bold',
              onTap: function() {
                return false;
              }
            }]
          });
          closePopupService.register($scope.noDoctorSelectPopup);
          $scope.noDoctorSelectPopup.then(function(res) {
            if (res) {
              $scope.continuePhysicianUpdate(user);
            }
          });
        } else if($scope.selectedDoctorArray.length > 0) {
          if ($scope.infoRegistration.selectHCP.agreedToSharedInformation) {
            $scope.continuePhysicianUpdate(user);
          } else {
            $scope.notShareWithDoctorPopup = $ionicPopup.show({
              title: "You’ve selected your HCP. In order to get the full benefit of MS Care Connect and its features, you need to share your information with your healthcare team. Your health information and identity are kept secure.",
              template: '<a class="popClose" ng-click="notShareWithDoctorPopup.close()"><span></span><span></span></a>',
              scope: $scope,
              buttons: [{
                text: 'Skip',
                type: 'button-block button-outline button-darkPurple skip',
                onTap: function() {
                  return false;
                }
              }, {
                text: 'Share Info',
                type: 'button-block  button-darkPurple',
                onTap: function() {
                  return true;
                }
              }]
            });
            closePopupService.register($scope.notShareWithDoctorPopup);
            $scope.notShareWithDoctorPopup.then(function(res) {
              if (res) {
                console.log("Send Email to user. For sharing");
                $scope.infoRegistration.selectHCP.agreedToSharedInformation = true;
                $scope.continuePhysicianUpdate(user);
              } else if (!res) {
                $scope.continuePhysicianUpdate(user);
              }

            });
          }
        }else if($scope.selectedDoctorArray.length == 0 && $scope.isDoctorNotFoundClicked == true){
          $scope.continuePhysicianUpdate(user);
        }else{
          $scope.afterPhysicanUpdate(user);
        }
      }
    }
  };

  $scope.continuePhysicianUpdate = function(user) {

    $scope.showLoader();
    var usersDS = new $kinvey.UserStore();
    user.data.info.selectHCP = JSON.parse(angular.toJson($scope.infoRegistration.selectHCP));
    usersDS.update(user.data)
      .then(function(user) {
        if ($scope.infoRegistration.selectHCP.agreedToSharedInformation && $scope.selectedDoctorNew.length > 0) { // agree to share 
          var promise = $kinvey.CustomEndpoint.execute('emailTemp-shareInfoWithHCP', {
            doctorId: $scope.selectedDoctorNew,
            user:$kinvey.User.getActiveUser().data
          });
          promise.then(function(response) {
            $scope.afterPhysicanUpdate(user);
          }).catch(function(err) {
            $scope.afterPhysicanUpdate(user);
          });
        } else {
          $scope.afterPhysicanUpdate(user);
        }
      }).catch(function(error) {
        afterPhysicanUpdate(user);
      });
  }

  $scope.afterPhysicanUpdate = function(user) {
    if (typeof window.ga !== "undefined") {
      window.ga.trackEvent('Profile Settings - Select Physician', 'Update and Profile Settings Screen (Select Physician)', 'Update', 1);
    }
    $scope.hideLoader();
    $scope.$emit('reloadAllProfileSettings', user);
    console.log(JSON.stringify(user));
    $ionicScrollDelegate.scrollTop();
    $state.go('app.profileSettings');
  }

  $scope.resetPhysicianForm = function() {
    if (typeof window.ga !== "undefined") {
      window.ga.trackEvent('Profile Settings - Select Physician', 'Profile Settings Screen (Select Physician)', 'Cancel', 1);
    }
    form1.$submitted = false;
    $ionicScrollDelegate.scrollTop();
    $scope.initialState();
    $state.go('app.profileSettings');
  }
  $scope.navigateTo = function(){
    if (typeof window.ga !== "undefined") {
      window.ga.trackEvent('Profile Settings - Select Physician', 'Profile Settings Screen (Select Physician)', 'Back', 1);
    }
    $scope.initialState();
    $state.go('app.profileSettings');
  }

}]);
