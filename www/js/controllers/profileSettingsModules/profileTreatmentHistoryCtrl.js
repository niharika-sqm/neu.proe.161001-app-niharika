neuroScienceApp.controller('profileTreatmentHistoryCtrl', ['$scope', '$state', '$kinvey', '$ionicScrollDelegate','$timeout','$ionicPopup','closePopupService', function($scope, $state, $kinvey, $ionicScrollDelegate,$timeout,$ionicPopup,closePopupService) {

  $timeout(function(){
    window.ga.trackView('Profile Settings - Treatment History');
  }, 1000);

  $scope.reasonToStopMedication = [{ id: 1, text: "Failed to relieve my symptoms", checked: false }, { id: 2, text: "Safety concerns", checked: false }, { id: 3, text: "Too many side effects", checked: false }, { id: 4, text: "Insurance/Cost", checked: false }, { id: 5, text: "Inconvenience", checked: false }];

  $scope.treatmentUpdatePopupVariable = { "popup1Title": "New MS Medication", "popup1ScreenType": "newMSMedication", "popup2Title": "Why did you stop?", "popup2ScreenType": "reasonToStopMedication" };
  $scope.treatment = { "newMSMedication": "", "newMSMedicationError": false, "reasonToStopMedication": "", "reasonToStopMedicationError": false }
  $scope.reasonForChange = undefined;

  //$scope.accordian.current = "";
  $scope.accordian.current = "treatmenthistory1";
  $scope.profileTreatmentHistoryInit = function() {
    $scope.infoRegistration.treatmentHistory = angular.copy($scope.activeUserInfo.info.treatmentHistory);
    $scope.treatmentHistoryerror = {};
    $scope.treatmentHistoryerror.index = -1;
    $scope.treatmentHistoryerror.iserror = true;
    $scope.priorTreatments = [];
    if ($scope.infoRegistration.treatmentHistory.priorTreatments) {
      for (var i = 0; i < $scope.infoRegistration.treatmentHistory.priorTreatments.length; i++) {
        $scope.priorTreatments.push(i+1);
      }
    }else{
      $scope.priorTreatments.push($scope.priorTreatments.length+1);
    }
  }

  $scope.profileTreatmentHistoryInit();

  //Part1 Submit 
  var form1 = ""
  $scope.submitTreatmentHistoryPart1 = function(formData) {
    if (typeof window.ga !== "undefined") {
      window.ga.trackEvent('Profile Settings - Treatment History', 'Update and Profile Settings Screen (Treatment History)', 'Update', 1);
    }
    form1 = formData;
    if (formData.$valid) {
      var usersDS = new $kinvey.UserStore();
      var user = $kinvey.User.getActiveUser();
      if (user) {
        $scope.infoRegistration.treatmentHistory = JSON.parse(angular.toJson($scope.infoRegistration.treatmentHistory));       
        if(($scope.infoRegistration.treatmentHistory.currentTreatment.value != $scope.activeUserInfo.info.treatmentHistory.currentTreatment.value) && ($scope.activeUserInfo.info.treatmentHistory.currentTreatment.text != "None")){
          // reason for change pop up
          $scope.reasonForChangePopup($scope.activeUserInfo);
        }else{
          // no reason for change is need if (None --> medication and time is change only)
          $scope.submitTreatmentHistoryAfterPopup();
        }
      }
    }
  }


  // Submit handle for form 1 after Reason form change is selected.
  $scope.submitTreatmentHistoryAfterPopup = function(){
    $scope.showLoader();
    var usersDS = new $kinvey.UserStore();
    var user = $kinvey.User.getActiveUser();
    if (user) {
      $scope.infoRegistration.treatmentHistory = JSON.parse(angular.toJson($scope.infoRegistration.treatmentHistory));       
      if($scope.infoRegistration.treatmentHistory.currentTreatment.value != $scope.activeUserInfo.info.treatmentHistory.currentTreatment.value){
        var treatmentHistoryObj = {"to":{},"from":{},"treatmentChangeDate":new Date(),"reason":$scope.reasonForChange};
        /*reatmentHistoryObj.to = {"treatment":$scope.infoRegistration.treatmentHistory.currentTreatment,"yearStarted":$scope.infoRegistration.treatmentHistory.currentTreatmentPeriod}
        treatmentHistoryObj.from = {"treatment":$scope.activeUserInfo.info.treatmentHistory.currentTreatment,"yearStarted":$scope.activeUserInfo.info.treatmentHistory.currentTreatmentPeriod == undefined ? {}:$scope.activeUserInfo.info.treatmentHistory.currentTreatmentPeriod}
        if(user.data.treatmentHistory){
          user.data.treatmentHistory.push(treatmentHistoryObj);          
        }
        else{
          user.data.treatmentHistory = [];
          user.data.treatmentHistory.push(treatmentHistoryObj);
        }*/
        if(!$scope.infoRegistration.treatmentHistory.priorTreatments){
          $scope.infoRegistration.treatmentHistory.priorTreatments = [];
        }
        if($scope.activeUserInfo.info.treatmentHistory.currentTreatmentPeriod){
          var yearsStopped = {"id":new Date().getFullYear(),"text":new Date().getFullYear(),"checked":true};
          var priorTeatmentsUpdated = {"id":$scope.infoRegistration.treatmentHistory.priorTreatments.length+1,"lastTreatment":$scope.activeUserInfo.info.treatmentHistory.currentTreatment,"yearsTaken":$scope.activeUserInfo.info.treatmentHistory.currentTreatmentPeriod,"yearsStopped":yearsStopped,"treatmentChangeDate":new Date(),"reason":$scope.reasonForChange};
          if($scope.infoRegistration.treatmentHistory.priorTreatments.length >0){
            var pt= [];
            pt.push(priorTeatmentsUpdated);
            for(var i=0;i<$scope.infoRegistration.treatmentHistory.priorTreatments.length;i++){
              pt.push($scope.infoRegistration.treatmentHistory.priorTreatments[i]);
            }
            $scope.infoRegistration.treatmentHistory.priorTreatments = pt;
            for(var i=0;i<$scope.infoRegistration.treatmentHistory.priorTreatments.length;i++){
              $scope.infoRegistration.treatmentHistory.priorTreatments[i].id = i+1;
            }
          }
          else{
            $scope.infoRegistration.treatmentHistory.priorTreatments.push(priorTeatmentsUpdated);
          }
          $scope.infoRegistration.treatmentHistory = JSON.parse(angular.toJson($scope.infoRegistration.treatmentHistory));
          user.data.info.treatmentHistory.priorTreatments = JSON.parse(angular.toJson($scope.infoRegistration.treatmentHistory.priorTreatments));
          user.data.info.treatmentHistory.previousTreatmentTaken = "yes";
        }      
      }
      if($scope.infoRegistration.treatmentHistory.currentTreatment && $scope.infoRegistration.treatmentHistory.currentTreatment.value == "None"){
        delete(user.data.info.treatmentHistory.currentTreatmentEffectiveness);
        delete(user.data.info.treatmentHistory.currentTreatmentPeriod);
        delete(user.data.info.treatmentHistory.currentWeight);
      }else{
        user.data.info.treatmentHistory.currentTreatmentEffectiveness = $scope.infoRegistration.treatmentHistory.currentTreatmentEffectiveness;
        user.data.info.treatmentHistory.currentTreatmentPeriod = $scope.infoRegistration.treatmentHistory.currentTreatmentPeriod;
        user.data.info.treatmentHistory.currentWeight = $scope.infoRegistration.treatmentHistory.currentWeight;
      }     
      user.data.info.treatmentHistory.currentTreatment = $scope.infoRegistration.treatmentHistory.currentTreatment;
      if ($scope.infoRegistration.treatmentHistory.takingAdditionalMedications != undefined) {
        user.data.info.treatmentHistory.takingAdditionalMedications = JSON.parse(angular.toJson($scope.infoRegistration.treatmentHistory.takingAdditionalMedications));
      }
      usersDS.update(user.data)
      .then(function(user) {
        $scope.hideLoader();
        $scope.updateCurrentTreatment(user);
        $scope.$emit('reloadAllProfileSettings', user);
        $ionicScrollDelegate.scrollTop();
        $state.go('app.profileSettings');
        console.log(JSON.stringify(user));
      }).catch(function(error) {
        if(error && error.message == "errorOccurred"){
          $scope.infoEmptyToast();
        }else{
          console.log(error);
          $scope.internalGlitch();
        } 
      });
    }
  }

  // Change medication pop up (will not appear if none -> medication)
  $scope.reasonForChangePopup = function(user){

    var select2 = 
      '<div>' +
      '<div class="item item-input item-borderless no-padding">' +
      '<div class="selectBoxWrapper  item-floating-label">' +
      '<div class="selectBox" ng-class="treatment.reasonToStopMedicationError ? errorClass:noerrorClass" ng-click="openSingleSelectTreatmentProfile(reasonToStopMedication,treatmentUpdatePopupVariable.popup2ScreenType,treatmentUpdatePopupVariable.popup2ScreenType,treatmentUpdatePopupVariable.popup2Title)">' +

      '<div class="multiLine" ng-if="!treatment.reasonToStopMedication || (treatment.reasonToStopMedication && treatment.reasonToStopMedication.length==0)" style="line-height:2">' +
      // '<div class="upperCase">Why did you stop?</div>' +
      '<div class="upperCase" >Select all that apply</div>' +
      '</div>' +
      '<div class="floatingSelect" ng-if="treatment.reasonToStopMedication && treatment.reasonToStopMedication.length>0">'+
      // '<span class="input-label upperCase font40">Choose one</span><div class="singleLine darkGray upperCase" >{{treatment.reasonToStopMedication.text}}</div>' +
      '<span class="input-label upperCase font40">Choose one</span><div class="singleLine darkGray" ><span ng-repeat="value in treatment.reasonToStopMedication">{{value.text}}<span ng-hide="$last">, </span></span></div>' +
      '</div></div></div></div><div ng-if="treatment.reasonToStopMedicationError"><div class="height10"></div><p class="errorMsg text-center">Please select one.</p></div><div class="height20"></div></div>';  
    var template = '<div class="height10"></div><a class="popClose" ng-click="treatmentIonicPopUp.close()"></a>' + select2;
    var treatmentText = "Treatment"
    // var title = "Are you still not taking any medication for your MS?"
    var title = "Please select the reason(s) you stopped or switched treatment"
    // if(user.info.treatmentHistory.currentTreatment.text != "None"){
    //   title = "Are you still taking "+user.info.treatmentHistory.currentTreatment.text;
    // }
    $scope.treatmentIonicPopUp = $ionicPopup.show({
      title: title,
      template: template,
      scope: $scope,
      buttons: [{
        text: 'Cancel',
        type: 'button-block button-outline button-darkPurple skip font40',
        onTap: function() {
          return false;
        }
      }, {
        text: 'Save and Continue',
        type: 'button-block button-darkPurple font40',
        onTap: function(e) {
          if(treatmentText == "None"){
            // TODO 
          }
          else{
            if ($scope.treatment.reasonToStopMedication == undefined || $scope.treatment.reasonToStopMedication == "") {
              $scope.treatment.reasonToStopMedicationError = true; 
              e.preventDefault();
            }else {
              $scope.treatment.reasonToStopMedicationError = false;
              return true;
            }
          }
        }
      }]
    });
    closePopupService.register($scope.treatmentIonicPopUp);
    $scope.treatmentIonicPopUp.then(function(res) {
      if(res){
        var treatmentObj= JSON.parse(angular.toJson($scope.treatment));
        $scope.reasonForChange = treatmentObj.reasonToStopMedication;
        $scope.submitTreatmentHistoryAfterPopup();
        $scope.$emit('emptyMultiSelectedArray');
      }
      else{
        $scope.reasonForChange = undefined;
        $scope.treatment.reasonToStopMedication = "";
        $scope.$emit('emptyMultiSelectedArray');
        for (var i = 0; i < $scope.reasonToStopMedication.length; i++) {
          if ($scope.reasonToStopMedication[i].checked == true) {
            $scope.reasonToStopMedication[i].checked = false;
            break;
          }
        }
      }
    });
  }

  // Open pop up for reason to change
  $scope.openSingleSelectTreatmentProfile = function(options, popupType, screenType, title) {
    /*$scope.typeSingleSelect = popupType;
    $scope.optionSingleSelect = options;
    $scope.screenTypeSingleSelect = screenType;*/
    $scope.typeMultiSelect = popupType;
    $scope.optionMultiSelect = options;
    $scope.screenTypeMultiSelect = screenType;

    if($scope.treatment[$scope.screenTypeMultiSelect]){
      var selectedData = $scope.treatment[$scope.screenTypeMultiSelect];
      for(var j=0;j<selectedData.length;j++){
        for (var i = 0; i<$scope.optionMultiSelect.length; i++) {
          if (selectedData[j].id == $scope.optionMultiSelect[i].id) {
            $scope.optionMultiSelect[i].checked = true;
          }
        } 
      }
      $scope.$emit('updateMultiSelectedArray',$scope.treatment[$scope.screenTypeMultiSelect]);
    }

    var multiSelectPopup = "";
    multiSelectPopup = $ionicPopup.show({
      title: title,
      templateUrl: 'templates/genericTemplate/multiSelectPopup.html',
      cssClass: "selectBoxPopup",
      scope: $scope,
      buttons: [{
        text: 'Cancel',
        type: 'button-block button-outline button-darkPurple font56',
        onTap: function() {
          return false;
        }
      }, {
        text: 'Okay',
        type: 'button-block button-outline button-darkPurple bold font56',
        onTap: function() {
          return true;
        }
      }]
    });
    closePopupService.register(multiSelectPopup);
    multiSelectPopup.then(function(res) {
      console.log($scope.multiSelection, res);
      if (res) {
        if ($scope.typeMultiSelect == "newMSMedication") $scope.treatment.newMSMedicationError = false;
        else $scope.treatment.reasonToStopMedicationError = false;
        $scope.treatment[$scope.screenTypeMultiSelect] = $scope.multiSelection;

        // $scope.$emit('emptyMultiSelectedArray');
      } else {
        $scope.$emit('emptyMultiSelectedArray');
        for (var i = 0; i < $scope.optionMultiSelect.length; i++) {
          if ($scope.optionMultiSelect[i].checked == true) {
            $scope.optionMultiSelect[i].checked = false;
          }
        }
      }
    });
  }

  $scope.updateCurrentTreatment = function(user){
    console.log(user);
    var promise = $kinvey.CustomEndpoint.execute('userNotifApi',{"userId":user._id,"userRegistering":false,"currentTreatment":$scope.infoRegistration.treatmentHistory.currentTreatment});
    promise.then(function(response) {
      console.log(response);
    }).catch(function(err) {
      $scope.internalGlitch();
    });
  }

  $scope.resetTreatmentHistoryPart1 = function() {
    form1.$submitted = false;
    $scope.infoRegistration.treatmentHistory.currentTreatment = angular.copy($scope.activeUserInfo.info.treatmentHistory.currentTreatment);
    $scope.infoRegistration.treatmentHistory.currentWeight = angular.copy($scope.activeUserInfo.info.treatmentHistory.currentWeight);
    $scope.infoRegistration.treatmentHistory.currentTreatmentEffectiveness = angular.copy($scope.activeUserInfo.info.treatmentHistory.currentTreatmentEffectiveness);
    $scope.infoRegistration.treatmentHistory.currentTreatmentPeriod = angular.copy($scope.activeUserInfo.info.treatmentHistory.currentTreatmentPeriod);
    $scope.infoRegistration.treatmentHistory.takingAdditionalMedications = angular.copy($scope.activeUserInfo.info.treatmentHistory.takingAdditionalMedications);
    $ionicScrollDelegate.scrollTop();
    $scope.initialState();
    if (typeof window.ga !== "undefined") {
      window.ga.trackEvent('Profile Settings - Treatment History', 'Profile Settings Screen (Treatment History)', 'Cancel', 1);
    }
    $state.go('app.profileSettings');
  }

  //Part2

  $scope.resetTreatmentHistoryPart2 = function() {
    form2.$submitted = false;
    $scope.priorTreatments = [];
    $scope.priorTreatments.push($scope.priorTreatments.length + 1);
    $scope.infoRegistration.treatmentHistory.previousTreatmentTaken = angular.copy($scope.activeUserInfo.info.treatmentHistory.previousTreatmentTaken);
    if ($scope.activeUserInfo.info.treatmentHistory.priorTreatments != undefined) {
      $scope.infoRegistration.treatmentHistory.priorTreatments = JSON.parse(angular.toJson($scope.activeUserInfo.info.treatmentHistory.priorTreatments));
    }
    $ionicScrollDelegate.scrollTop();
    $scope.initialState();
    if (typeof window.ga !== "undefined") {
      window.ga.trackEvent('Profile Settings - Treatment History', 'Profile Settings Screen (Treatment History)', 'Cancel', 1);
    }
    $state.go('app.profileSettings');
  }

  var form2 = "";
  $scope.submitTreatmentHistoryPart2 = function(formData) {
    if (typeof window.ga !== "undefined") {
      window.ga.trackEvent('Profile Settings - Treatment History', 'Update and Profile Settings Screen (Treatment History)', 'Update', 1);
    }
    $scope.treatmentHistoryerror.iserror = true;
    console.log($scope.infoRegistration, $scope.infoRegistration.treatmentHistory.priorTreatments);
    var info = $scope.infoRegistration.treatmentHistory;
    form2 = formData;
    $scope.$watch(
      function(scope) {
        return $scope.infoRegistration.treatmentHistory.currentTreatment },
      function(newValue, oldValue) {
        if (newValue != oldValue) {
          if (newValue && oldValue == undefined && $scope.infoRegistration.treatmentHistory.currentTreatment && typeof $scope.infoRegistration.treatmentHistory.currentTreatment == 'object') {
            form2.$submitted = false;
          }
        }
      }
    );
    if (info.previousTreatmentTaken == 'yes') {
      if (info.priorTreatments) {
        for (var i = 0; i < $scope.priorTreatments.length; i++) {
          if (!info.priorTreatments[i].lastTreatment || !info.priorTreatments[i].yearsTaken || !info.priorTreatments[i].yearsStopped) {
            if(!info.priorTreatments[i].lastTreatment || !info.priorTreatments[i].yearsTaken){
              $scope.treatmentHistoryerror.index = i;
              $scope.treatmentHistoryerror.iserror = false;
            }
            if(!info.priorTreatments[i].lastTreatment || !info.priorTreatments[i].yearsStopped){
              $scope.treatmentHistoryerror.index = i;
              $scope.treatmentHistoryerror.iserror = false;
            }
            return;
          }
        }
      } else {
        $scope.treatmentHistoryerror.index = 0;
        $scope.treatmentHistoryerror.iserror = false;
        return;
      }
    }

    if (formData.$valid) {
      $scope.showLoader();
      var usersDS = new $kinvey.UserStore();
      var user = $kinvey.User.getActiveUser();
      if (user) {
        $scope.infoRegistration.treatmentHistory = JSON.parse(angular.toJson($scope.infoRegistration.treatmentHistory));
        user.data.info.treatmentHistory.previousTreatmentTaken = $scope.infoRegistration.treatmentHistory.previousTreatmentTaken;
        if ($scope.infoRegistration.treatmentHistory.priorTreatments != undefined) {
          user.data.info.treatmentHistory.priorTreatments = JSON.parse(angular.toJson($scope.infoRegistration.treatmentHistory.priorTreatments));
          for(var i=0;i<user.data.info.treatmentHistory.priorTreatments.length;i++){
            for(var j=i;j<user.data.info.treatmentHistory.priorTreatments.length;j++){
              if(user.data.info.treatmentHistory.priorTreatments[i].yearsTaken.text<user.data.info.treatmentHistory.priorTreatments[j].yearsTaken.text){
                var swap = user.data.info.treatmentHistory.priorTreatments[i];
                user.data.info.treatmentHistory.priorTreatments[i] = user.data.info.treatmentHistory.priorTreatments[j];
                user.data.info.treatmentHistory.priorTreatments[j] = swap;
              }
            }
          }
          for(var i=0;i<user.data.info.treatmentHistory.priorTreatments.length;i++){
            user.data.info.treatmentHistory.priorTreatments[i].id = i+1;
          }
        }else if(user.data.info.treatmentHistory.priorTreatments && $scope.infoRegistration.treatmentHistory.previousTreatmentTaken == 'no'){
          delete user.data.info.treatmentHistory.priorTreatments;
        }
        usersDS.update(user.data)
          .then(function(user) {
            $scope.hideLoader();
            console.log(JSON.stringify(user));
            $scope.$emit('reloadAllProfileSettings', user);
            $ionicScrollDelegate.scrollTop();
            $state.go('app.profileSettings');
          }).catch(function(error) {
            console.log(error);
            if(error && error.message == "errorOccurred"){
              $scope.infoEmptyToast();
            }else{
              console.log(error);
              $scope.internalGlitch();
            } 
          });
      }
    }
  }

  $scope.addAnotherTreatment = function() {
    var key = $scope.priorTreatments[$scope.priorTreatments.length - 1] + 1;
    $scope.priorTreatments.push(key);
    if (!$scope.infoRegistration.treatmentHistory.priorTreatments) {
      $scope.infoRegistration.treatmentHistory.priorTreatments = [];
      $scope.infoRegistration.treatmentHistory.priorTreatments.push({ id: key, lastTreatment: '', yearsTaken: '',yearsStopped:'' });
    } else {
      $scope.infoRegistration.treatmentHistory.priorTreatments.push({ id: key, lastTreatment: '', yearsTaken: '',yearsStopped:'' });
    }
    $scope.treatmentHistoryerror.index = $scope.priorTreatments.length;
    console.log($scope.priorTreatments);
  }

  $scope.removeAnotherTreatment = function(index, id) {
    if ($scope.priorTreatments.length == 1) {
      return;
    }
    var priorTreatmentsArray = $scope.infoRegistration.treatmentHistory.priorTreatments;
    if (priorTreatmentsArray) {
      for (var i = 0; i < priorTreatmentsArray.length; i++) {
        if (priorTreatmentsArray[i].id == id) {
          priorTreatmentsArray.splice(i, 1);
        }
        console.log("remove");
      }
    }
    $scope.priorTreatments.splice(index, 1);
    console.log($scope.priorTreatments, $scope.infoRegistration);
  }

  $scope.previousTreatmentTakenChange = function() {
    if($scope.infoRegistration.treatmentHistory.previousTreatmentTaken == 'no') {
      $scope.priorTreatments = [];
      $scope.priorTreatments.push(1);
      if ($scope.infoRegistration.treatmentHistory.priorTreatments && $scope.infoRegistration.treatmentHistory.priorTreatments.length >= 0) {
        delete $scope.infoRegistration.treatmentHistory['priorTreatments'];
      }
      $scope.treatmentHistoryerror.iserror = true;
    } else {
      $scope.treatmentHistoryerror.iserror = true;
    }
  }

  $scope.navigateTo = function(){
    if (typeof window.ga !== "undefined") {
      window.ga.trackEvent('Profile Settings - Treatment History', 'Profile Settings Screen (Treatment History)', 'Back', 1);
    }
    $scope.initialState();
    $state.go('app.profileSettings');
  }

}]);
