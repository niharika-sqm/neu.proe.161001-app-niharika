var gulp = require('gulp');
var concat = require('gulp-concat');
var sass = require('gulp-sass');
var minifyCss = require('gulp-minify-css');
var rename = require('gulp-rename');
var uglify = require('gulp-uglify');
var htmlreplace = require('gulp-html-replace');
var inject = require('gulp-inject');
var templateCache = require('gulp-angular-templatecache');
var jScrambler = require('gulp-jscrambler');
var javascriptObfuscator = require('gulp-javascript-obfuscator');

var paths = {
  sass: ['./scss/**/*.scss']
};

var files = { 
  jsSandbox: [
    './www/js/app.js',
    './www/js/config/config-sandbox.js',
    './www/js/controllers/**/*.js',
    './www/js/services/**/*.js',
    './www/js/filters/**/*.js',
    './www/js/appCtrl.js',
    './www/js/bodyCtrl.js',
    './www/js/constant.js'
  ],
  jsDev: [
    './www/js/app.js',
    './www/js/config/config-dev.js',
    './www/js/controllers/**/*.js',
    './www/js/services/**/*.js',
    './www/js/filters/**/*.js',
    './www/js/appCtrl.js',
    './www/js/bodyCtrl.js',
    './www/js/constant.js'
  ],
  jsQa: [
    './www/js/app.js',
    './www/js/config/config-qa.js',
    './www/js/controllers/**/*.js',
    './www/js/services/**/*.js',
    './www/js/filters/**/*.js',
    './www/js/appCtrl.js',
    './www/js/bodyCtrl.js',
    './www/js/constant.js'
  ],
  jsQaMaster: [
    './www/js/app.js',
    './www/js/config/config-qamaster.js',
    './www/js/controllers/**/*.js',
    './www/js/services/**/*.js',
    './www/js/filters/**/*.js',
    './www/js/appCtrl.js',
    './www/js/bodyCtrl.js',
    './www/js/constant.js'
  ],
  jsMaster: [
    './www/js/app.js',
    './www/js/config/config-master.js',
    './www/js/controllers/**/*.js',
    './www/js/services/**/*.js',
    './www/js/filters/**/*.js',
    './www/js/appCtrl.js',
    './www/js/bodyCtrl.js',
    './www/js/constant.js'
  ],
  jsClient: [
    './www/js/app.js',
    './www/js/config/config-client.js',
    './www/js/controllers/**/*.js',
    './www/js/services/**/*.js',
    './www/js/filters/**/*.js',
    './www/js/appCtrl.js',
    './www/js/bodyCtrl.js',
    './www/js/constant.js'
  ],
  templates:['./www/templates/**/*.html'],
  googleJsonSandbox:"./hooks/googleJson/sandbox/google-services.json",
  googleJsonDev:"./hooks/googleJson/dev/google-services.json",
  googleJsonQa:"./hooks/googleJson/qa/google-services.json",
  googleJsonQaMaster:"./hooks/googleJson/qaMaster/google-services.json",
  googleJsonMaster:"./hooks/googleJson/master/google-services.json",
  googleJsonClient:"./hooks/googleJson/client/google-services.json"
}

var a= new Date();
var timeStamp = a.getFullYear()+''+((a.getMonth()+1)<10?('0'+(a.getMonth()+1)):(a.getMonth()+1))+''+(a.getDate()<=9?('0'+(a.getDate())):(a.getDate()))+''+(a.getHours()<=9?('0'+(a.getHours())):(a.getHours()))+''+(a.getMinutes()<=9?('0'+(a.getMinutes())):(a.getMinutes()));

gulp.task('default', ['sass']);
gulp.task('serve:before', ['watch']);

gulp.task('sass', function(done) {
  gulp.src('./scss/ionic.app.scss')
    .pipe(sass())
    .on('error', sass.logError)
    .pipe(gulp.dest('./www/css/'))
    .pipe(minifyCss({
      keepSpecialComments: 0
    }))
    .pipe(rename({ extname: '.min.css' }))
    .pipe(gulp.dest('./www/css/'))
    .on('end', done);
});

gulp.task('watch', function() {
  gulp.watch(paths.sass, ['sass']);
});

gulp.task('version', function() {
  gulp.src(['./www/templates/authenticationModules/login.html']) //,'./www/templates/menu.html'
    .pipe(htmlreplace({
     'version': "<!-- build:version -->v."+timeStamp+"<!-- endbuild -->"
    }))
    .pipe(gulp.dest('./www/templates/authenticationModules'));
}); 

gulp.task('version-prod', function() {
  gulp.src(['./www/templates/authenticationModules/login.html']) //,'./www/templates/menu.html'
    .pipe(htmlreplace({
     'version': "<!-- for production only v."+timeStamp+"-->"
    }))
    .pipe(gulp.dest('./www/templates/authenticationModules'));
}); 

var templateOptions = {
  templateHeader : 'neuroScienceApp.run([\'$templateCache\', function($templateCache) {',
  templateBody: '$templateCache.put(\'templates/<%= url %>\',\'<%= contents %>\');'
}

gulp.task('templateJs', function () {
  gulp.src(files.templates)
    .pipe(templateCache('templates.js',templateOptions))
    .pipe(gulp.dest('./www/builds'));
});
gulp.task('scripts-sandbox', function() {
  gulp.src(files.jsSandbox)
  .pipe(concat('app.min.js'))
  .pipe(uglify())
  .pipe(gulp.dest('./www/builds'))
  gulp.src(files.googleJsonSandbox)
  .pipe(gulp.dest('platforms/android/'));
});

gulp.task('scripts-dev', function() {
  gulp.src(files.jsDev)
  .pipe(concat('app.min.js'))
  .pipe(uglify())
  .pipe(gulp.dest('./www/builds'))
  gulp.src(files.googleJsonDev)
  .pipe(gulp.dest('platforms/android/'));
});

gulp.task('scripts-qa', function() {
  gulp.src(files.jsQa)
  .pipe(concat('app.min.js'))
  .pipe(uglify())
  .pipe(gulp.dest('./www/builds'))
  gulp.src(files.googleJsonQa)
  .pipe(gulp.dest('platforms/android/'));
});

gulp.task('scripts-qamaster', function() {
  gulp.src(files.jsQaMaster)
  .pipe(concat('app.min.js'))
  .pipe(uglify())
  .pipe(gulp.dest('./www/builds'))
  gulp.src(files.googleJsonQaMaster)
  .pipe(gulp.dest('platforms/android/'));
});

gulp.task('scripts-master', function() {
  gulp.src(files.jsMaster)
  .pipe(concat('app.min.js'))
  .pipe(uglify())
  .pipe(gulp.dest('./www/builds'))
  gulp.src(files.googleJsonMaster)
  .pipe(gulp.dest('platforms/android/'));
});

gulp.task('scripts-client', function() {
  gulp.src(files.jsClient)
  .pipe(concat('app.min.js'))
  .pipe(uglify())
  .pipe(gulp.dest('./www/builds'))
  gulp.src(files.googleJsonClient)
  .pipe(gulp.dest('platforms/android/'));
});

/*gulp.task('obfuscation', function() {
  gulp.src('./www/builds/app.min.js')
  .pipe(jScrambler({
    "keys": {
      "accessKey": '507BE9E97D114B750B9837A1161ABC6B0DEB8885',
      "secretKey": 'C44BF459A90BB1DF78E794C99EEBDFCAE5DF897C'
    },
    "applicationId":"57f39ca44e9175a700483b47",
    "params":[
      {
        "name": "whitespaceRemoval"
      },
      {
        "name": "identifiersRenaming",
        "options": {
          "mode": "SAFEST"
        }
      },
      {
        "name": "stringConcealing",
        "options": {}
      },
      {
        "name": "deadCodeInjection"
      },
      {
        "name": "regexObfuscation"
      },
      {
        "name": "functionReordering"
      },
      {
        "name": "dotToBracketNotation"
      },
      {
        "name": "functionOutlining"
      },
      {
        "name": "booleanToAnything"
      },
      {
        "name": "propertyKeysObfuscation"
      }
    ],
    "areSubscribersOrdered": false,
    "applicationTypes": {
      "webBrowserApp": false,
      "desktopApp": false,
      "serverApp": false,
      "hybridMobileApp": true,
      "javascriptNativeApp": false,
      "html5GameApp": false
    },
    "languageSpecifications": {
      "es5": true,
      "es6": false,
      "es7": false
    },
    "useRecommendedOrder": true
  }))
  .pipe(gulp.dest('./'));
});*/

gulp.task('obfuscation', function() {
  gulp.src('./www/builds/app.min.js')
  .pipe(javascriptObfuscator({
    compact:true,
    sourceMap: false,
    identifierNamesGenerator: 'mangled',
    stringArray: true,
    rotateStringArray: true,
    stringArrayEncoding: false,
    stringArrayThreshold: 0.8,
    target: 'browser',
    transformObjectKeys: false,
    seed: 0
  }))
  .pipe(gulp.dest('./www/builds/'));
});

gulp.task('inject_app',function(){
  gulp.src('./www/index.html')
  .pipe(inject(gulp.src('./www/builds/*.js', {read: false}), {name: 'appJs',relative:true}))
  .pipe(gulp.dest('./www'));
});